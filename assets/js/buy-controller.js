function stripeResponseHandler(status, response)
{
    if (response.error) 
    {
        // Stripe.js failed to generate a token. The error message will explain why.
        // Usually, it's because the customer mistyped their card info.
        // You should customize this to present the message in a pretty manner:
//         alert(response.error.message);
        swal({
						title: "Error",
						text: response.error.message,
//                 		type: "success"
					});
					$('#buy-submit-button').removeAttr("disabled");
					var elem = document.getElementById("buy-submit-button");
        elem.value = "Start My Subscription";
    } 
    else
    {   
        // Stripe.js generated a token successfully. We're ready to charge the card!
        var token = response.id;
        var firstName = $("#first-name").val();
        var lastName = $("#last-name").val();
        var email = $("#email").val();
        var plan = $("#membership_id").val();
        var coupon = $("#coupon").val();
//         console.log(coupon);
 
        // We need to know what amount to charge. Assume $20.00 for the tutorial. 
        // You would obviously calculate this on your own:
//         var price = 20;
 
        // Make the call to the server-script to process the order.
        // Pass the token and non-sensitive form information.
        var request = $.ajax ({
            type: "POST",
            url: base_url+'register/checkout_validation/',
            dataType: "json",
            data: {
                "stripeToken" : token,
                "firstName" : firstName,
                "lastName" : lastName,
                "plan" : plan,
                "email" : email,
                "coupon" : coupon
                
//                 "price" : price
                }
        });
 
        request.done(function(msg)
        {
            if (msg.success === 1)
            {
                // Customize this section to present a success message and display whatever
                // should be displayed to the user.
//                 alert("The credit card was charged successfully!");
                window.location = base_url+"index.php/register/success";
            }
            else
            {
                // The card was NOT charged successfully, but we interfaced with Stripe
                // just fine. There's likely an issue with the user's credit card.
                // Customize this section to present an error explanation
//                 alert("The user's credit card failed.");
                swal({
						title: "Error",
						text: msg.success,
//                 		type: "success"
					});
					$('#buy-submit-button').removeAttr("disabled");
					var elem = document.getElementById("buy-submit-button");
        elem.value = "Start My Subscription";
            }
        });
 
        request.fail(function(jqXHR, textStatus)
        {
            // We failed to make the AJAX call to pay.php. Something's wrong on our end.
            // This should not normally happen, but we need to handle it if it does.
            swal({
						title: "Error: Failed to call buy.php",
						// text: "You clicked the button!",
//                 		type: "success"
					});
        });
    }
}

function showErrorDialogWithMessage(message)
{
    // For the tutorial, we'll just do an alert. You should customize this function to 
    // present "pretty" error messages on your page.
    alert(message);
    swal({
						title: "Error",
						text: message,
//                 		type: "success"
					});
 
    // Re-enable the order button so the user can try again
    $('#buy-submit-button').removeAttr("disabled");
    var elem = document.getElementById("buy-submit-button");
        elem.value = "Start My Subscription";
}
 
$(document).ready(function() 
{
    $('#buy-form').submit(function(event)
    {
        // immediately disable the submit button to prevent double submits
        $('#buy-submit-button').attr("disabled", "disabled");
//         $('#buy-submit-button').value = "Processing...";
        var elem = document.getElementById("buy-submit-button");
        elem.value = "Processing...";
         
        var fName = $('#first-name').val();
        var lName = $('#last-name').val();
        var email = $('#email').val();
        var cardNumber = $('#card-number').val();
        var cardCVC = $('#card-security-code').val();
        var fullName = fName.concat(" ",lName);
         
        // First and last name fields: make sure they're not blank
        if (fName === "") {
	        swal({
						title: "Error",
						text: "Please enter your first name.",
//                 		type: "success"
					});
					$('#buy-submit-button').removeAttr("disabled");
					var elem = document.getElementById("buy-submit-button");
        elem.value = "Start My Subscription";
//             showErrorDialogWithMessage("Please enter your first name.");
            return;
        }
        if (lName === "") {
            swal({
						title: "Error",
						text: "Please enter your last name.",
//                 		type: "success"
					});
					$('#buy-submit-button').removeAttr("disabled");
					var elem = document.getElementById("buy-submit-button");
        elem.value = "Start My Subscription";
            return;
        }
         
        // Validate the email address:
/*
        var emailFilter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (email === "") {
            showErrorDialogWithMessage("Please enter your email address.");
            return;
        } else if (!emailFilter.test(email)) {
            showErrorDialogWithMessage("Your email address is not valid.");
            return;
        }
*/
          
        // Stripe will validate the card number and CVC for us, so just make sure they're not blank
        if (cardNumber === "") {
            swal({
						title: "Error",
						text: "Please enter your card number.",
//                 		type: "success"
					});
					$('#buy-submit-button').removeAttr("disabled");
					var elem = document.getElementById("buy-submit-button");
        elem.value = "Start My Subscription";
            return;
        }
        if (cardCVC === "") {
            swal({
						title: "Error",
						text: "Please enter your card's security code.",
//                 		type: "success"
					});
					$('#buy-submit-button').removeAttr("disabled");
					var elem = document.getElementById("buy-submit-button");
        elem.value = "Start My Subscription";
            return;
        }
         
        // Boom! We passed the basic validation, so we're ready to send the info to 
        // Stripe to create a token! (We'll add this code soon.)
        Stripe.createToken({
		    number: cardNumber,
		    cvc: cardCVC,
		    exp_month: $('#ccmonth').val(),
		    exp_year: $('#ccyear').val(),
		    name: fullName
		}, stripeResponseHandler);
		 
		// Prevent the default submit action on the form
		return false;
         
    });
});