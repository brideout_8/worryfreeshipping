base_url = window.location.origin+"/";
$(document).ready(function() {

// Create a Stripe client.
    var stripe = Stripe('pk_test_h422SasxYd0FTsRYC97rDNZE');

// Create an instance of Elements.
    var elements = stripe.elements();

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
    var style = {
        base: {
            color: '#32325d',
            lineHeight: '18px',
            fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
            fontSmoothing: 'antialiased',
            fontSize: '16px',
            '::placeholder': {
                color: '#aab7c4'
            }
        },
        invalid: {
            color: '#fa755a',
            iconColor: '#fa755a'
        }
    };

// Create an instance of the card Element.
    var card = elements.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>.
    card.mount('#card-element');

// Handle real-time validation errors from the card Element.
    card.addEventListener('change', function (event) {
        var displayError = document.getElementById('card-errors');
        if (event.error) {
            displayError.textContent = event.error.message;
        } else {
            displayError.textContent = '';
        }
    });

// Handle form submission.
    var form = document.getElementById('payment-form');
    form.addEventListener('submit', function (event) {
        event.preventDefault();

        stripe.createToken(card).then(function (result) {
            if (result.error) {
                // Inform the user if there was an error.
                var errorElement = document.getElementById('card-errors');
                errorElement.textContent = result.error.message;
            } else {
                // Send the token to your server.
                var fName = $('#fullName').val();
                var phone = $('#phoneNumber').val();
                var company = $('#companyName').val();
                var email = $('#email').val();
                var password = $('#password').val();
                if (fName === "") {
                    swal({
                        title: "Error",
                        text: "Please enter your name."
                    });
                    return;
                }
                if (phone === "") {
                    swal({
                        title: "Error",
                        text: "Please enter your phone number."
                    });
                    return;
                }
                if (company === "") {
                    swal({
                        title: "Error",
                        text: "Please enter your phone number."
                    });
                    return;
                }
                if (email === "") {
                    swal({
                        title: "Error",
                        text: "Please enter your email."
                    });
                    return;
                }
                if (password === "") {
                    swal({
                        title: "Error",
                        text: "Please enter a password."
                    });
                    return;
                }
                stripeTokenHandler(result.token, fName, phone, company, email, password);
            }
        });
    });
});

function stripeTokenHandler(token, name, phone, company, email, password) {
    // Insert the token ID into the form so it gets submitted to the server
    var form = document.getElementById('payment-form');
    var hiddenInput = document.createElement('input');
    hiddenInput.setAttribute('type', 'hidden');
    hiddenInput.setAttribute('name', 'stripeToken');
    hiddenInput.setAttribute('value', token.id);
    form.appendChild(hiddenInput);

    var formArray = {token: token, name: name, phone: phone, company: company, email: email, password: password};
    jQuery.post(base_url+'register/checkout', formArray, function(data) {
        if (data.success) {
            swal({
                title: "Success",
                text: data.token.id
            })
        } else {
            swal({
                title: "Error",
                text: data.message
            });

        }
    }, 'json');
    // Submit the form
    // form.submit();
}