// base_url = 'http://localhost:8888/';
base_url = window.location.origin+"/";
loaderImage = base_url + "assets/img/loader.gif";

function emailUs() {

    var formArray = $('#message_form').serialize();
    var c = $('.ladda-button').ladda();
    c.ladda('start');
    jQuery.post(base_url+'landing/sendEmail' , formArray, function(data) {
        c.ladda("stop");

        if (data.success)
        {
            swal({
                title: "Success",
                text: "Your message has been sent",
                type: "success"
            })
        }
        else
        {
            if(data.error == 1){
                swal({
                    title: "Error",
                    text: "Please fill in all forms",
                });
            }
        }
    }, 'json');

    return false;
}

function sendForm() {

    var formArray = $('#customerForm').serialize();
    var c = $('.ladda-button').ladda();
    c.ladda('start');
    jQuery.post(base_url+'landing/sendForm' , formArray, function(data) {
        c.ladda("stop");
        if (data.success)
        {
            swal({
                title: "Success",
                text: "Your profile has been sent",
                type: "success"
            });
        }
        else
        {
            if(data.error == 1){
                swal({
                    title: "Error",
                    text: "Please fill in all fields",
                });
            } else {
                swal({
                    title: "Error",
                    text: data.message,
                })
            }
        }
    }, 'json');

    return false;
}

function changepassword() {
    var formArray = $('#changepassword_form').serialize();
    document.changepassword_form.changeBtn.value = "Changing...";
    document.changepassword_form.changeBtn.disabled = true;

    jQuery.post(base_url+'password_reset/changepassword_validation/' , formArray, function(data) {
        if (data.success) {
            document.changepassword_form.changeBtn.value = "Password Changed";
            swal({
                title: "Success",
                text: data.message,
                type: "success"
            },
                function (isConfirm) {
                    if(isConfirm) {
                        window.location = base_url+"login";
                    }
                })
        } else {
            document.changepassword_form.changeBtn.value = "Change Password";
            document.changepassword_form.changeBtn.disabled = false;
            swal({
                title: "Error",
                text: data.message,
                type: "warning"
            })
        }
    }, 'json');

    return false;
}

function setPassword() {
    var formArray = $('#setPasswordForm').serialize();
    document.setPasswordForm.changeBtn.value = "Setting...";
    document.setPasswordForm.changeBtn.disabled = true;

    jQuery.post(base_url+'register/createPassword' , formArray, function(data) {
        if (data.success) {
            document.setPasswordForm.changeBtn.value = "Password Set";
            swal({
                title: "Success",
                text: data.message,
                type: "success"
            },
                function (isConfirm) {
                    if(isConfirm) {
                        window.location = base_url+"login";
                    }
                })
        } else {
            document.setPasswordForm.changeBtn.value = "Set Password";
            document.setPasswordForm.changeBtn.disabled = false;
            swal({
                title: "Error",
                text: data.message,
                type: "warning"
            })
        }
    }, 'json');

    return false;
}