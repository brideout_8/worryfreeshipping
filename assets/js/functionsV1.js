// base_url = 'http://localhost:8888/';
base_url = window.location.origin+"/";
loaderImage = base_url + "assets/img/loader.gif";

function getNewOrders() {
    //Pull new orders into database
    var tab = localStorage.getItem('activeTab');
    if(tab == "#tab-1") {
        var div = "newOrders";
    } else if(tab == "#tab-2") {
        var div = "pendingOrders";
    } else if(tab == "#tab-3") {
        var div = "ordersToPull";
    } else if(tab == "#tab-4") {
        var div = "ordersToShip";
    } else if(tab == "#tab-5") {
        var div = "ordersOnHold";
    } else {
        var div = "allOrders";
    }
    var c = $('.ladda-button').ladda();
    c.ladda('start');
    jQuery.post(base_url+'fulfillment/getOrders' , function(data) {
        c.ladda('stop');
        if (data.success) {
            swal({
                    title: "Orders Added " + data.orders.toString(),
                    // text: "Gift Card Only "+data.giftCardOnly.toString()+"\nGift Card Plus Others "+data.giftCardPlus.toString()+"\nGift Card Payments "+data.giftCardPayment.toString()+"\n Gift Card Order #s"+data.giftCardOrderNumbers,
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#0d8ddb",
                    confirmButtonText: "Ok",
                    cancelButtonText: "Close",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        if(data.orders.toString() != "0") {
                            tableInfo(div);
                        }
                        // location.reload();
                    } else {
                        // location.reload();
                    }
                });
        } else {
            if (data.error == 1){
                swal({
                    title: "Error",
                    text: data.message
                });
            } else if (data.error = 2) {
                swal({
                        title: "Trial Expired",
                        text: "You have reached your 20 order limit on the trial",
                        // type: "success",
                        showCancelButton: true,
                        confirmButtonColor: "#0d8ddb",
                        confirmButtonText: "Select Plan",
                        cancelButtonText: "Close",
                        closeOnConfirm: false,
                        closeOnCancel: true
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            window.location.href = base_url+"register/signUp";
                            // location.reload();
                        } else {
                            // location.reload();
                        }
                    });
            }
        }
    }, 'json');
}

function addStartingOrder() {
    //Pull first order into database
    var formArray = $('#addStoreNumberForm').serialize();
    var c = $('.ladda-button').ladda();
    c.ladda('start');
    jQuery.post(base_url+'fulfillment/getStartingOrder', formArray, function(data) {
        c.ladda('stop');
        if (data.success) {
            swal({
                    title: "Order Added",
                    text: "You can now find this order on the fulfillment page.",
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#0d8ddb",
                    confirmButtonText: "Ok",
                    cancelButtonText: "Close",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        if(data.count.toString() != "0") {
                            if(data.newUser === "no") {
                                top.location = base_url + "fulfillment";
                            } else {
                                top.location = base_url + "register/connectCarrier"
                            }
                        }
                        // location.reload();
                    } else {
                        // location.reload();
                    }
                });
        } else {
            if (data.error == 1){
                swal({
                    title: "Error",
                    text: data.message
                });
            } else if (data.error = 2) {
                swal({
                        title: "Trial Expired",
                        text: "You have reached your 20 order limit on the trial",
                        // type: "success",
                        showCancelButton: true,
                        confirmButtonColor: "#0d8ddb",
                        confirmButtonText: "Select Plan",
                        cancelButtonText: "Close",
                        closeOnConfirm: false,
                        closeOnCancel: true
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            window.location.href = base_url+"register/signUp";
                            // location.reload();
                        } else {
                            // location.reload();
                        }
                    });
            }
        }
    }, 'json');
}

//Used for FulfillmentOverview
function tableInfo(div) {
    $('#'+div).html('<img height="50" width="50" style=" position: absolute; top: 50%; left: 50%;" src="' + loaderImage + '">');
    $.ajax({
        url: base_url + 'fulfillment/fulfillmentMetrics/'+div,
        type: 'POST',
        success: function (data) {
            $('#'+div).html(data);
            $('#'+div).slideDown('slow');
            var oTable = $('.dataTables-example').DataTable({
                retrieve: true,
                "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                "iDisplayLength": 25,
                dom: '<"html5buttons"B>lTfgitp',
                // "order": [[ 7, "asc" ]],
                "order": [[7,'desc']],
                "columnDefs": [
                    {
                        "targets": [ 7 ],
                        "visible": false,
                        "searchable": false
                    }
                    // {
                    //     "targets": [ 8 ],
                    //     "visible": false,
                    //     "searchable": false
                    // }
                ],
                buttons: [
                    {extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'Orders'},
                    {extend: 'pdf', title: 'Orders'},

                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]

            });
        }
    });
}

//Used for ReturnsManager
function tableInfoReturns(div) {
    $('#'+div).html('<img height="50" width="50" style=" position: absolute; top: 50%; left: 50%;" src="' + loaderImage + '">');
    $.ajax({
        url: base_url + 'returns/returnsMetrics/'+div,
        type: 'POST',
        success: function (data) {
            $('#'+div).html(data);
            $('#'+div).slideDown('slow');
            var oTable = $('.dataTables-example').DataTable({
                retrieve: true,
                "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                "iDisplayLength": 25,
                dom: '<"html5buttons"B>lTfgitp',
                // "order": [[ 7, "asc" ]],
                "order": [[7,'desc']],
                "columnDefs": [
                    {
                        "targets": [ 7 ],
                        "visible": false,
                        "searchable": false
                    }
                    // {
                    //     "targets": [ 8 ],
                    //     "visible": false,
                    //     "searchable": false
                    // }
                ],
                buttons: [
                    {extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'Orders'},
                    {extend: 'pdf', title: 'Orders'},

                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]

            });
        }
    });
}

function updateReturn(status, orderId) {
    var c = $('.ladda-button').ladda();
    c.ladda('start');
    var formArray = {status: status, orderId: orderId};
    jQuery.post(base_url+'returns/updateStatus' , formArray, function(data) {
        c.ladda('stop');
        if (data.success) {
            location.reload();
        } else {
            if (data.error == 1){
                swal({
                    title: "Error",
                    text: data.message
                });
            } else {
                swal({
                    title: "Error",
                    text: data.message
                });
            }
        }
    }, 'json');

    return false;
}

function findReturnOrder() {
    var c = $('.ladda-button').ladda();
    c.ladda('start');
    var formArray = $('#newOrderReturnForm').serialize();
    jQuery.post(base_url+'returns/findReturnOrder' , formArray, function(data) {
        c.ladda('stop');
        if (data.success) {
            top.location = base_url+"returns/newReturn?orderId="+data.orderId;
        } else {
            if (data.error == 1){
                swal({
                    title: "Error",
                    text: data.message
                });
            } else {
                swal({
                    title: "Error",
                    text: data.message
                });
            }
        }
    }, 'json');

    return false;
}

function quickOrderFind() {
    var c = $('.ladda-button').ladda();
    c.ladda('start');
    var formArray = $('#findOrderForm').serialize();
    jQuery.post(base_url+'fulfillment/quickOrderFind' , formArray, function(data) {
        c.ladda('stop');
        if (data.success) {
            top.location = base_url+"fulfillment/orderOverview/"+data.storeName+"/"+data.orderNumber;
        } else {
            if (data.error == 1){
                swal({
                    title: "Error",
                    text: data.message
                });
            } else {
                swal({
                    title: "Error",
                    text: data.message
                });
            }
        }
    }, 'json');

    return false;
}

function tableData(order, type) {
    return  $('.dataTables-example').DataTable({
        "aLengthMenu": [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"] ],
        "iDisplayLength" : 25,
        "order": [[ order, type ]],
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            { extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'Completed Orders'},
            {extend: 'pdf', title: 'Completed Orders'},
            {extend: 'print',
                customize: function (win){
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
            }
        ]
    });
}

function GetClientPage() {
    $('#listings').html('<img height="50" width="50" style=" position: absolute; top: 50%; left: 50%;" src="'+loaderImage+'">');
    $.ajax({
        url  	: base_url+'client/getMetrics',
        type 	: 'POST',
        success : function(data){
            $('#listings').html(data);
            $('#listings').slideDown('slow');
            tableData(1, "dsc");
        }
    });
}

function GetClientsOverviewPage() {
    var formArray = $('#clientM').serialize();
    var c = $('.ladda-button').ladda();
    c.ladda('start');
    $.ajax({
        url  	: base_url+'fulfillment/clientTable',
        type 	: 'POST',
        data    : formArray,
        success : function(data){
            c.ladda('stop');
            $('#listings').html(data);
            $('#listings').slideDown('slow');
            tableData(1, "dsc");
        }
    });
}

function GetEmployeePage(store) {

    $('.formError').remove();

    $('#listings').html('<img height="50" width="50" style=" position: absolute; top: 50%; left: 50%;" src="'+loaderImage+'">');

    $.ajax({
        url  	: base_url+'fulfillment/getOrderTable/'+store,
        type 	: 'POST',
        success : function(data){
            $('#listings').html(data);
            $('#listings').slideDown('slow');
            tableData(0,"dsc");
        }
    });
}

function saveHold() {
    var c = $('.ladda-button').ladda();
    c.ladda('start');
    var formArray = $('#holdForm').serialize();
    jQuery.post(base_url+'fulfillment/onHold' , formArray, function(data) {
        c.ladda('stop');
        if (data.success) {
            location.reload();
        } else {
            if (data.error == 1){
                swal({
                    title: "Error",
                    text: data.message
                });
            } else {
                swal({
                    title: "Error",
                    text: data.message
                });
            }
        }
    }, 'json');

    return false;
}

function saveInternalNote() {
    var c = $('.ladda-button').ladda();
    c.ladda('start');
    var formArray = $('#notesForm').serialize();
    jQuery.post(base_url+'fulfillment/addInternalNote' , formArray, function(data) {
        c.ladda('stop');
        if (data.success) {
            location.reload();
        } else {
            if (data.error == 1){
                swal({
                    title: "Error",
                    text: data.message
                });
            } else {
                swal({
                    title: "Error",
                    text: data.message
                });
            }
        }
    }, 'json');

    return false;
}

function GetRRInventoryPage() {
    $('#rr').html('<img height="50" width="50" style=" position: absolute; top: 50%; left: 50%;" src="'+loaderImage+'">');

    $.ajax({
        url  	: base_url+'fulfillment/getRRInventoryTable/',
        type 	: 'POST',
        success : function(data){
            $('#rr').html(data);
            $('#rr').slideDown('slow');
            $('.dataTables-example tfoot th').each( function () {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            } );
            var table = $('.dataTables-example').DataTable({
                "aLengthMenu": [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"] ],
                "iDisplayLength" : 100,
                dom: '<"html5buttons"B>lTfgitp',
                "order": [[ 0, "desc" ]],
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'Completed Orders'},
                    {extend: 'pdf', title: 'Completed Orders'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]

            });
            // Apply the search
            table.columns().every( function () {
                var that = this;

                $( 'input', this.footer() ).on( 'keyup change', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                });
            });
        }
    });
}

function GetDDAInventoryPage() {
    $('#dda').html('<img height="50" width="50" style=" position: absolute; top: 50%; left: 50%;" src="'+loaderImage+'">');

    $.ajax({
        url  	: base_url+'fulfillment/getDDAInventoryTable/',
        type 	: 'POST',
        success : function(data){
            $('#dda').html(data);
            $('#dda').slideDown('slow');
            $('.dataTables-dda tfoot th').each( function () {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            });
            var oTable = $('.dataTables-dda').DataTable({
                // retrieve: true,
                "aLengthMenu": [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"] ],
                "iDisplayLength" : 100,
                dom: '<"html5buttons"B>lTfgitp',
                "order": [[ 0, "desc" ]],
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'Completed Orders'},
                    {extend: 'pdf', title: 'Completed Orders'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]

            });
            // Apply the search
            oTable.columns().every( function () {
                var that = this;

                $( 'input', this.footer() ).on( 'keyup change', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                });
            });
        }
    });
}

function getFulfillmentOverviewPack() {

    $('#pack').html('<img height="50" width="50" style=" position: absolute; top: 50%; left: 50%;" src="'+loaderImage+'">');

    $.ajax({
        url  	: base_url+'fulfillment/fulfillmentOverviewPack',
        type 	: 'POST',
        success : function(data) {
            $('#pack').html(data);
            $('#pack').slideDown('slow');
            tableData(0, "asc");
        }
    });
}

function getFulfillmentOverviewShip() {

    $('#ship').html('<img height="50" width="50" style=" position: absolute; top: 50%; left: 50%;" src="'+loaderImage+'">');

    $.ajax({
        url  	: base_url+'fulfillment/fulfillmentOverviewShip',
        type 	: 'POST',
        success : function(data){
            $('#ship').html(data);
            $('#ship').slideDown('slow');
            tableData(0,"asc");
        }
    });
}

function getOrders(store) {
    // var formArray = $('#bodyCompForm').serialize();
    // $('.formError').remove();
    var c = $('.ladda-button').ladda();
    c.ladda('start');
    jQuery.post(base_url+'fulfillment/getOrders/'+store , function(data) {
        c.ladda('stop');
        if (data.success) {
            swal({
                    title: "Orders Added " + data.count.toString(),
                    text: "Gift Card Only "+data.giftCardOnly.toString()+"\nGift Card Plus Others "+data.giftCardPlus.toString()+"\nGift Card Payments "+data.giftCardPayment.toString()+"\n Gift Card Order #s"+data.giftCardOrderNumbers,
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#0d8ddb",
                    confirmButtonText: "Ok",
                    cancelButtonText: "Close",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        //View order
                        location.reload();
                    } else {
                        location.reload();
                    }
                });
        } else {
            if (data.error == 1){
                swal({
                    title: "Error",
                    text: data.message
                });
            } else {
                swal({
                    title: "Error",
                    text: data.message
                });
            }
        }
    }, 'json');

    return false;
}

//scanProducts
function itemScanned(value, orderNumber, orderStoreNumber, storeName) {
    var y = 1;
    var z = 0;
    var totalQty = 0;
    var scannedTotal = 0;
    var correctItem = false;
    var mysterySkus = $("#mysterySku").val();
    $('#inventory .sku').each(function() {
        z++;
        var x2 = document.getElementById("inventory").rows[y].cells;
        var qty2 = x2[3].innerHTML;
        var scanned = x2[4].innerHTML;
        console.log(x2.length);
        if(x2.length > 6) {
            var packed = x2[6].innerHTML;
        }
        if(isNaN(parseInt(packed))) {
            var packedInt = 0;
        } else {
            packedInt = parseInt(packed);
        }
        totalQty = parseInt(qty2) + totalQty;
        scannedTotal = parseInt(scanned) + packedInt + scannedTotal;
        if(value === $(this).html()) {
            var x = document.getElementById("inventory").rows[y].cells;
            var qty = x[3].innerHTML;
            correctItem = true;
            if(qty === scanned || qty === (parseInt(scanned) + packedInt).toString()) {
                swal({
                    title: "Item(s) Already Packed",
                    type: "error"
                })
            } else if(value === "MYSTERYTEE" && mysterySkus === "") {
                swal({
                    title: "Please enter a sku for the mystery item.",
                    type: "error"
                })
            } else {
                scannedTotal = 1 + scannedTotal;
                var qtyNum = parseInt(x[4].innerHTML);
                if(x2.length > 6) {
                    var packedNum = parseInt(x[6].innerHTML);
                }
                if(isNaN(packedNum)) {
                    packedNum = 0;
                }
                x[4].innerHTML = (qtyNum + 1).toString();
                if(qty === x[4].innerHTML || qty === (packedNum + parseInt(x[4].innerHTML)).toString()) {
                    $("#inventory").find("tbody tr").eq(y-1).css('backgroundColor', '#99FF87');
                } else {
                    $("#inventory").find("tbody tr").eq(y-1).css('backgroundColor', '#FFFC41');
                }
            }
        }
        y++;
    });
    if(correctItem === false) {
        swal({
            title: "Wrong Item",
            type: "error"
        })
    }
    if(totalQty === (scannedTotal)) {
        swal({
                title: "All items packed",
                type: "success",
                confirmButtonColor: "#0d8ddb",
                confirmButtonText: "Ok"
            },
            function (isConfirm) {
                if (isConfirm) {
                    // var $encodedMysterySkus = encodeURIComponent(mysterySkus);
                    $('#orderNumberD').html('<img height="50" width="50" style=" position: absolute; top: 50%; left: 50%;" src="'+base_url+'assets/img/loader.gif">');
                    window.location.href = base_url+"fulfillment/markedPacked?orderId="+orderNumber+"&orderStoreNumber="+orderStoreNumber+"&storeName="+storeName;
                } else {

                }
            });
    }
    $("#orderNumber").val('');
}

function checkPartiallyPacked() {
    var y = 1;
    var z = 0;
    var totalQty = 0;
    var scannedTotal = 0;
    $('#inventory .sku').each(function() {
        z++;
        var x2 = document.getElementById("inventory").rows[y].cells;
        var qty2 = x2[3].innerHTML;
        var scanned = x2[4].innerHTML;
        if(x2.length > 6) {
            var packed = x2[6].innerHTML;
        }
        if(isNaN(parseInt(packed))) {
            var packedInt = 0;
        } else {
            packedInt = parseInt(packed);
        }
        totalQty = parseInt(qty2) + totalQty;
        scannedTotal = parseInt(scanned) + scannedTotal + packedInt;
        var x = document.getElementById("inventory").rows[y].cells;
        var qty = x[3].innerHTML;
        var qtyNum = parseInt(x[4].innerHTML);
        if(x2.length > 6) {
            var packedNum = parseInt(x[6].innerHTML);
        }
        if(isNaN(packedNum)) {
            packedNum = 0;
        }
        x[4].innerHTML = (qtyNum).toString();
        if(qty === x[4].innerHTML || qty === (packedNum + parseInt(x[4].innerHTML)).toString()) {
            $("#inventory").find("tbody tr").eq(y-1).css('backgroundColor', '#99FF87');
        } else if (packedNum == 0) {
            if(x % 2 === 0) {
                $("#inventory").find("tbody tr").eq(y-1).css('backgroundColor', '#ffffff');
            } else {
                $("#inventory").find("tbody tr").eq(y - 1).css('backgroundColor', '#ffffff');
            }
        } else {
            $("#inventory").find("tbody tr").eq(y-1).css('backgroundColor', '#FFFC41');
        }
        y++;
    });
}

function clearScanned() {
    var y = 1;
    $('#inventory .sku').each(function() {
        var x = document.getElementById("inventory").rows[y].cells;
        x[4].innerHTML = "0";
        if(y % 2 === 0) {
            $("#inventory").find("tbody tr").eq(y - 1).css('backgroundColor', '#ffffff');
        } else {
            $("#inventory").find("tbody tr").eq(y - 1).css('backgroundColor', '#ffffff');
        }
        y++;
    });
    $("#orderNumber").focus();
}

function markedPartial(orderObjectId, orderNumber, orderStoreNumber, storeName) {
    swal({
            title: "Mark Partially Packed?",
            text: "Are you sure you want to mark this order partial?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#0d8ddb",
            confirmButtonText: "Yes",
            cancelButtonText: "Cancel",
            closeOnConfirm: true,
            closeOnCancel: true
        },
        function (isConfirm) {
            if(isConfirm) {
            var formArray = {};
            var z = 0;
            var y = 1;
            $('#inventory .sku').each(function() {
                // console.log(document.getElementById("productId[".concat(z.toString().concat("]"))));
                var x2 = document.getElementById("inventory").rows[y].cells;
                var scanned = parseInt(x2[4].innerHTML);
                if(x2.length > 5) {
                    var packedAlready = parseInt(x2[5].innerHTML);
                } else {
                    var packedAlready = 0;
                }
                var itemsPacked = [];
                for(var a=0;a<scanned;a++) {
                    itemsPacked.push(document.getElementById("productId[".concat(z.toString().concat("]"))).value);
                    console.log(document.getElementById("productId[".concat(z.toString().concat("]"))).value);
                    // formArray.push(itemsPacked);
                }
                for(var a=0;a<packedAlready;a++) {
                    itemsPacked.push(document.getElementById("productId[".concat(z.toString().concat("]"))).value);
                    console.log(document.getElementById("productId[".concat(z.toString().concat("]"))).value);
                    // formArray.push(itemsPacked);
                }
                formArray[document.getElementById("productSku[".concat(z.toString().concat("]"))).value] = itemsPacked;
                // formArray[$(this).html()] = itemsPacked;
                z++;
                y++;
            });
            console.log(formArray);

            jQuery.post(base_url+'fulfillment/markedPartial/'+orderObjectId, formArray, function(data) {

                if (data.success) {
                    swal({
                            title: "Order marked partial",
                            type: "success"

                        },
                        function (isConfirm) {
                            if (isConfirm) {
                                $('#orderNumberD').html('<img height="50" width="50" style=" position: absolute; top: 50%; left: 50%;" src="'+base_url+'assets/img/loader.gif">');
                                window.location.href = base_url+"fulfillment/orderOverviewFromPartial?orderStoreNumber="+orderStoreNumber+"&storeName="+storeName;
                            } else {
                                location.reload();
                            }
                        });
                } else {
                    if (data.error == 1){
                        swal({
                            title: "Error",
                            text: data.message
                        });
                    } else {
                        swal({
                            title: "Error",
                            text: data.message
                        });
                    }
                }
            }, 'json');

            return false;
        }
    });
}

function shippingAddressError() {
    swal({
        title: "No shipping address",
        text: "Please enter a shipping address before shipping the order."
    })
}
//shipOrder

function hideShippingMethod() {
    $('#shippingPackage').hide();
    $('#shippingMethodsButton').show();
}

function updateShippingCost(num) {
    // var str = str.split(":").pop();
    // console.log(num);
    var cost = $('input[name^="totalCost"]');
    var serviceCodeB = $('input[name^="serviceCodeB"]');
    var carrierIdB = $('input[name^="carrierIdB"]');
    var carrierNameB = $('input[name^="carrierNameB"]');
    // console.log(cost[num].value);
    // console.log(serviceCodeB[num].value);
    $("#rate").text('$'+cost[num].value);
    $("#carrierId").val(carrierIdB[num].value);
    $("#serviceCode").val(serviceCodeB[num].value);
    $("#carrierName").val(carrierNameB[num].value);
    // $("#shippingCost").val(shipping.split("$").pop());
    $("#downloadBtn").prop("disabled", false);
    $("#downloadBtn").html('Purchase Shipping Label');
}

function packageChange(international, nonOrder) {
    var Package = $('#packageType option:selected').val();
    var shipmentWeight = $('#weightHidden').val();
    var shipmentWeightMeasurement = $('#weightMeasurement option:selected').val();
    // console.log(Package);
    var length = $("#length");
    var width = $("#width");
    var height = $("#height");
    var weight = $('#weight');
    var packageType = $('#packaging');
    var packageCarrier = $('#packageCarrier');
    var formArray = {"packageId":Package, "shipmentWeight": shipmentWeight, "shipmentWeightMeasurement": shipmentWeightMeasurement, "nonOrder": nonOrder};
    //Get box from company
    jQuery.post(base_url+'fulfillment/getPackage/', formArray, function(data) {
        // console.log(boxArray);
        if(data.success) {
            if(data.packageCarrier === "none") {
                length.val(data.length);
                width.val(data.width);
                height.val(data.height);
                weight.val(data.weight);
                length.prop("disabled", false);
                width.prop("disabled", false);
                height.prop("disabled", false);
            } else {
                //Carrier packages do not need dimensions
                length.val("");
                width.val("");
                height.val("");
                weight.val(data.weight);
                length.prop("disabled", true);
                width.prop("disabled", true);
                height.prop("disabled", true);
            }
                packageType.val(data.packageType);
                packageCarrier.val(data.packageCarrier);
                if (international === false) {
                    getRate();
                } else {
                    hideShippingMethod();
                }
        } else {
            length.val("");
            width.val("");
            height.val("");
            packageType.val("package");
            packageCarrier.val("none");
            hideShippingMethod();
        }
    }, 'json');
}

function updateOrders() {
    // $("#downloadBtn").html('Creating Label...');
    // document.shipForm.downloadBtn.disabled=true;
    // var formArray = $('#shipForm').serialize();
    jQuery.post(base_url+'fulfillment/updateServerOrders/', function(data) {
        if (data.success) {
            $("#downloadBtn").html('Label Downloaded');
            download(data.label+"?download=0");
        } else {
            $("#downloadBtn").html('Download Label');
            document.shipForm.downloadBtn.disabled=false;
            if (data.error === 1){
                swal({
                    title: "Error",
                    text: data.message
                });
            } else {
                swal({
                    title: "Error",
                    text: data.message
                });
            }
        }
    }, 'json');

    return false;
}

function getLabel() {
    var l = $('.ladda-button').ladda();
    l.ladda('start');
    var formArray = $('#shipForm').serialize();
    jQuery.post(base_url+'fulfillment/getLabel/', formArray, function(data) {
        l.ladda('stop');
        if (data.success) {
            $("#downloadBtn").html('Label Created');
            $("#downloadBtn").prop('disabled', true);

            $('#labelLink').val(data.label+"?download=0");
            $('#labelCost').val(data.cost);
            $('#trackingNumber').val(data.trackingNumber);
            var form = document.getElementById('shippingLabelForm');
            form.submit();
            // download(data.label+"?download=0");
        } else if(data.error === 2) {
            $("#downloadBtn").html('Download Label');
            swal({
                    title: "Error",
                    text: data.message,
                    // type: "",
                    showCancelButton: true,
                    confirmButtonColor: "#0d8ddb",
                    confirmButtonText: "Purchase Labels",
                    cancelButtonText: "Cancel",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        top.location = base_url+"settings/buyLabels";
                    }
                });
        } else {
            $("#downloadBtn").html('Download Label');

                swal({
                    title: "Error",
                    text: data.message
                });

        }
    }, 'json');

    return false;
}

function getNonLabel() {
    var l = $('.ladda-button').ladda();
    l.ladda('start');
    var formArray = $('#shipForm').serialize();
    jQuery.post(base_url+'fulfillment/getNonOrderLabel/', formArray, function(data) {
        l.ladda('stop');
        if (data.success) {
            // $("#downloadBtn").html('Label Downloaded');
            // download(data.label+"?download=0");
            $("#downloadBtn").html('Label Created');
            $("#downloadBtn").prop('disabled', true);

            $('#labelLink').val(data.label+"?download=0");
            $('#labelCost').val(data.cost);
            $('#trackingNumber').val(data.trackingNumber);
            var form = document.getElementById('shippingNonLabelForm');
            form.submit();
        } else {
            $("#downloadBtn").html('Download Label');
            if (data.error === 1){
                swal({
                    title: "Error",
                    text: data.message
                });
            } else if(data.error === 2) {
                swal({
                        title: "Error",
                        text: data.message,
                        // type: "",
                        showCancelButton: true,
                        confirmButtonColor: "#0d8ddb",
                        confirmButtonText: "Purchase Labels",
                        cancelButtonText: "Cancel",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            top.location = base_url+"settings/buyLabels";
                        }
                    });
            } else {
                swal({
                    title: "Error",
                    text: data.message
                });
            }
        }
    }, 'json');

    return false;
}

function returnLabel(infoForm, labelForm) {
    var l = $('.ladda-button').ladda();
    l.ladda('start');
    var formArray = $(infoForm).serialize();
    jQuery.post(base_url+'returns/getReturnLabel/', formArray, function(data) {
        l.ladda('stop');
        if (data.success) {
            $("#downloadBtn").html('Label Created');
            // download(data.label+"?download=0");

            // $('#labelLink').val(data.label);
            // $('#labelCost').val(data.cost);
            // $('#trackingNumber').val(data.trackingNumber);
            var form = document.getElementById(labelForm);
            document.getElementById("labelLink").value = data.label;
            document.getElementById("labelCost").value = data.cost;
            document.getElementById("trackingNumber").value = data.trackingNumber;
            form.submit();
        } else {
            $("#downloadBtn").html('Download Label');
            if (data.error === 1){
                swal({
                    title: "Error",
                    text: data.message
                });
            } else if(data.error === 2) {
                swal({
                        title: "Error",
                        text: data.message,
                        // type: "",
                        showCancelButton: true,
                        confirmButtonColor: "#0d8ddb",
                        confirmButtonText: "Purchase Labels",
                        cancelButtonText: "Cancel",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            top.location = base_url + "settings/buyLabels";
                        }
                    });
            } else {
                swal({
                    title: "Error",
                    text: data.message
                });
            }
        }
    }, 'json');

    return false;
}

function validateAddress(form) {
    var formArray = $(form).serialize();
    jQuery.post(base_url+'fulfillment/validateAddress/', formArray, function(data) {
        if (data.success) {
            if(data.status === "verified") {
                $("#addressVerify").css('color', 'green');
                $("#addressVerify").text(data.status);
            } else {
                $("#addressVerify").css('color', 'red');
                $("#addressVerify").text(data.status);
            }
        } else {
            if (data.error === 1){
                swal({
                    title: "Error",
                    text: data.message
                });
            } else {
                swal({
                    title: "Error",
                    text: data.message
                });
            }
        }
    }, 'json');

    return false;
}

function increaseLabelCost() {
    var labelCount = $('#numberOfLabels').val();
    var costPerLabel = $("#costPer").html().replace('$', '');
    var totalCost = (labelCount * parseFloat(costPerLabel)).toFixed(2);
    var currentLabelsRemaining = $("#currentLabelsRemaining").html();
    var newLabelsRemaining = Number(currentLabelsRemaining) + Number(labelCount);
    $("#labelsRemaining").html(newLabelsRemaining);
    $("#totalCost").html("$"+totalCost);
}

function purchaseLabels() {
    var formArray = $('#buyLabelsForm').serialize();
    var l = $('.ladda-button').ladda();
    l.ladda('start');
    jQuery.post(base_url+'settings/purchaseLabels' , formArray, function(data) {
        l.ladda('stop');
        if (data.success) {
            swal({
                    title: "Success",
                    text: "Labels purchased",
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#0d8ddb",
                    confirmButtonText: "Ok",
                    cancelButtonText: "Cancel",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        top.location = base_url+"settings/account";
                    }
                });
        } else {
            swal({
                title: "Error",
                text: data.message,
                type: "warning"
            });
        }
    }, 'json');
}

function resetTour() {
    var l = $('.ladda-button').ladda();
    l.ladda('start');
    jQuery.post(base_url + 'settings/resetTour/', function (data) {
        l.ladda('stop');
           swal({
               title: "Success",
               type: "success",
               text: "Tour reset"
           })
    }, 'json');

    return false;
}

function createTicket() {
    var formArray = $('#addTicketForm').serialize();
    var l = $('.ladda-button').ladda();
    l.ladda('start');
    jQuery.post(base_url+'support/createTicket' , formArray, function(data) {
        l.ladda('stop');
        if (data.success) {
            swal({
                    title: "Success",
                    text: "The support ticket has been received. You will receive an email with the fix to your issue.",
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#0d8ddb",
                    confirmButtonText: "Ok",
                    cancelButtonText: "Cancel",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        top.location = base_url+"support";
                    }
                });
        } else {
            swal({
                title: "Error",
                text: data.message,
                type: "warning"
            });
        }
    }, 'json');
}

function createScanForm() {
    swal({
            title: "Create Scan Form?",
            text: "You will not be able to send anymore packages today.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#0d8ddb",
            confirmButtonText: "Yes",
            cancelButtonText: "Cancel",
            closeOnConfirm: true,
            closeOnCancel: true
        },
        function (isConfirm) {
            if (isConfirm) {
                var l = $('.ladda-button').ladda();
                l.ladda('start');
                jQuery.post(base_url + 'fulfillment/createScanForm/', function (data) {
                    l.ladda('stop');
                    if (data.success) {
                        download(data.scanForm);
                    } else {
                        if (data.error === 1) {
                            swal({
                                title: "Error",
                                text: data.message
                            });
                        } else {
                            swal({
                                title: "Error",
                                text: data.message
                            });
                        }
                    }
                }, 'json');

                return false;
            }
        });
}

function createScanForm2() {
    swal({
            title: "Create Scan Form?",
            text: "You will not be able to send anymore packages today.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#0d8ddb",
            confirmButtonText: "Yes",
            cancelButtonText: "Cancel",
            closeOnConfirm: true,
            closeOnCancel: true
        },
        function (isConfirm) {
            if (isConfirm) {
                var l = $('.ladda-button').ladda();
                l.ladda('start');
                jQuery.post(base_url + 'fulfillment/createScanForm2/', function (data) {
                    l.ladda('stop');
                    if (data.success) {

                        // download(data.scanForm);
                    } else {
                        if (data.error === 1) {
                            swal({
                                title: "Error",
                                text: data.message
                            });
                        } else {
                            swal({
                                title: "Error",
                                text: data.message
                            });
                        }
                    }
                }, 'json');

                return false;
            }
        });
}

function createScanForm3() {
    swal({
            title: "Create Scan Form?",
            text: "You will not be able to send anymore packages today.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#0d8ddb",
            confirmButtonText: "Yes",
            cancelButtonText: "Cancel",
            closeOnConfirm: true,
            closeOnCancel: true
        },
        function (isConfirm) {
            if (isConfirm) {
                var l = $('.ladda-button').ladda();
                l.ladda('start');
                jQuery.post(base_url + 'fulfillment/listWebhook/', function (data) {
                    l.ladda('stop');
                    swal({
                        title: "Info",
                        text: data.message
                    });
                }, 'json');

                return false;
            }
        });
}

function getRate() {
    var l = $('.ladda-button').ladda();
    l.ladda('start');
    $("#rate").text('Select Shipping Type');
    var formArray = $('#shipForm').serialize();
    jQuery.post(base_url+'fulfillment/getRates', formArray, function(data) {
        l.ladda('stop');
        if (data.success) {
            $('#shippingMethodsButton').hide();
            $('#shippingPackage').show();
            $("#shippingPackage").html(data.shippingRates);
            $('#downloadBtn').prop("disabled", true);
        } else {
            if (data.error === 1){
                swal({
                    title: "Error",
                    text: data.message
                });
            } else if(data.error === 2) {
                $("#rate").text('Enter Customs Data For Rate');
            } else if(data.error === 3) {
                //No shipping carrier set up
                swal({
                        title: "Notice",
                        text: data.message,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#0d8ddb",
                        confirmButtonText: "Settings",
                        cancelButtonText: "Close",
                        closeOnConfirm: false,
                        closeOnCancel: true
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            top.location = base_url+"settings/shipping";
                        }
                    });
            } else  {
                swal({
                    title: "Error",
                    text: data.message
                });
            }
        }
    }, 'json');

    return false;
}

function getReturnRate(form) {
    var l = $('.ladda-button').ladda();
    l.ladda('start');
    $("#rate").text('Select Shipping Type');
    var formArray = $(form).serialize();
    jQuery.post(base_url+'returns/getReturnRates', formArray, function(data) {
        l.ladda('stop');
        if (data.success) {
            $('#shippingMethodsButton').hide();
            $('#shippingPackage').show();
            $("#shippingPackage").html(data.shippingRates);
            $('#downloadBtn').prop("disabled", true);
        } else {
            if (data.error === 1){
                swal({
                    title: "Error",
                    text: data.message
                });
            } else if(data.error === 2) {
                $("#rate").text('Enter Customs Data For Rate');
            } else if(data.error === 3) {
                //No shipping carrier set up
                swal({
                        title: "Notice",
                        text: data.message,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#0d8ddb",
                        confirmButtonText: "Settings",
                        cancelButtonText: "Close",
                        closeOnConfirm: false,
                        closeOnCancel: true
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            top.location = base_url+"settings/shipping";
                        }
                    });
            } else  {
                swal({
                    title: "Error",
                    text: data.message
                });
            }
        }
    }, 'json');

    return false;
}

function archiveOrder(storeName, orderObjectId, orderId) {
    var formArray = {storeName: storeName, orderObjectId: orderObjectId, orderId: orderId}
    jQuery.post(base_url+'fulfillment/archiveOrder/', formArray, function(data) {
        if (data.success) {
            location.reload();
        } else {
            swal({
                title: "Error",
                text: data.message
            });
        }
    }, 'json');

    return false;
}

function saveWarehouse() {
    // document.ordersForm.getOrdersBtn.value="Retrieving...";
    var formArray = $('#shipForm').serialize();
    // $('.formError').remove();

    jQuery.post(base_url+'fulfillment/saveWarehouse/', formArray, function(data) {
        if (data.success) {
            swal({
                title: "Success",
                type: "success"
            })
            // document.ordersForm.getOrdersBtn.value="Get Orders";
            // document.ordersForm.getOrdersBtn.disabled=true;
            // console.log("hi");
            // download(data.label);
        } else {
            // document.ordersForm.getOrdersBtn.value="Get Orders";
            // document.ordersForm.getOrdersBtn.disabled=false;
            if (data.error === 1){
                swal({
                    title: "Error",
                    text: data.message
                });
            } else {
                swal({
                    title: "Error",
                    text: data.message
                });
            }
        }
    }, 'json');

    return false;
}

function refreshLocations() {
    var c = $('.ladda-buttonLocation').ladda();
    c.ladda('start');
    jQuery.post(base_url+'settings/refreshLocations' , function(data) {
        c.ladda('stop');
        if (data.success) {
            swal({
                title: "Success",
                text: "Locations Updated",
                type: "success",
                showCancelButton: false,
                confirmButtonColor: "#0d8ddb",
                confirmButtonText: "Ok",
                cancelButtonText: "Close",
                closeOnConfirm: false,
                closeOnCancel: true
            },
                function (isConfirm) {
                    if(isConfirm) {
                        location.reload();
                    }
                })
        } else {
            if (data.error == 1){
                swal({
                    title: "Error",
                    text: data.message
                });
            } else {
                swal({
                    title: "Error",
                    text: data.message
                });
            }
        }
    }, 'json');

    return false;
}

function download(url){
    window.open(url);

}

function trackPackage(trackingNumber) {
    window.open("https://tools.usps.com/go/TrackConfirmAction?tLabels="+trackingNumber);
}

function voidLabel(labelId, orderObjectId) {
    swal({
            title: "Void Label?",
            text: "This cannot be undone",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#0d8ddb",
            confirmButtonText: "Void",
            cancelButtonText: "Close",
            closeOnConfirm: false,
            closeOnCancel: true
        },
        function (isConfirm) {
            if (isConfirm) {
                var c = $('.ladda-button').ladda();
                c.ladda('start');
                var formArray = {"labelId": labelId, "orderObjectId": orderObjectId};
                jQuery.post(base_url+'fulfillment/voidLabel' , formArray, function(data) {
                    c.ladda('stop');
                    if (data.success) {
                        swal ({
                            title: "Success",
                            text: "Label Voided",
                            type: "success"
                        });
                    } else {
                        if (data.error == 1){
                            swal({
                                title: "Error",
                                text: data.message
                            });
                        } else {
                            swal({
                                title: "Denied",
                                text: data.message
                            });
                        }
                    }
                }, 'json');

                return false;
            }
        });
}

function GetClientProfile() {

    $('#listings').html('<img height="50" width="50" style=" position: absolute; top: 50%; left: 50%;" src="'+loaderImage+'">');

    $.ajax({
        url  	: base_url+'client/getProfile',
        type 	: 'POST',
        success : function(data){
            $('#listings').html(data);
            $('#listings').slideDown('slow');
            tableData(4,"asc");
        }
    });
}

function setCountryCode(provinceCode) {
    // var countryCode = $('#country_code').find(":selected").val();
    var country = $('#country_code').find(":selected").text();
    // console.log(countryCode);
    // $('#country').val(countryCode);//Set for label
    $('#countryAddress').val(country);//Set for address change
    var formArray = {"country":country};
    //Get box from company
    jQuery.post(base_url+'fulfillment/getProvince/', formArray, function(data) {
        // console.log(data.provinceCodes);
        $('#province_code').children().remove();
        $.each( data.provinceCodes, function( key, value ) {
            $('#province_code').append($('<option>', {
                value: key,
                text: value
            }));
        });
        $('#province_code').val(provinceCode);
        console.log(provinceCode);
        // console.log($('#province_code').val())

    }, 'json');

}

function setProvinceCode() {
    // var provinceCode = $('#province_code').find(":selected").val();
    var province = $('#province_code').find(":selected").text();
    // $('#state').val(provinceCode);//Set for label
    $('#provinceAddress').val(province);//Set for address change
}

function saveCustomerAddress(locationOfForm) {
    var c = $('.ladda-button').ladda();
    c.ladda('start');
    if(locationOfForm == "overview") {
        var formArray = $('#orderAddressForm').serialize();
    } else {
        var formArray = $('#addressForm').serialize();
    }
    jQuery.post(base_url+'fulfillment/saveCustomerAddress' , formArray, function(data) {
        c.ladda('stop');
        if (data.success) {
            location.reload();
        } else {
            if (data.error == 1){
                swal({
                    title: "Error",
                    text: data.message
                });
            } else {
                swal({
                    title: "Error",
                    text: data.message
                });
            }
        }
    }, 'json');

    return false;
}

function addStore() {
    var formArray = $('#addStoreForm').serialize();
    var l = $('.ladda-button').ladda();
    l.ladda('start');
    jQuery.post(base_url+'settings/newStoreInstall' , formArray, function(data) {
        if (data.success) {
            l.ladda('stop');
            window.location = data.url;
        } else {
            l.ladda('stop');
            if(data.error == 1){
                swal({
                    title: "Error",
                    text: data.message,
                    type: "warning"
                });
            } else {
                swal({
                    title: "Error",
                    text: data.message
                })
            }
        }
    }, 'json');
}

function updateStore() {
    var formArray = $('#editStoreForm').serialize();
    var l = $('.ladda-button').ladda();
    l.ladda('start');
    jQuery.post(base_url+'settings/updateStore' , formArray, function(data) {
        l.ladda('stop');
        if (data.success) {
            swal({
                title: "Success",
                text: "Store updated",
                type: "success"
            })
        } else {
            swal({
                title: "Error",
                text: data.message,
                type: "warning"
            });
        }
    }, 'json');
}

function enableSave() {
    $('#saveBtn').prop("disabled", false);
}

function changeWeight(value) {
    $('#weightMeasurement').children().remove();
    if(value === "metric") {
        $('#weightMeasurement').append($('<option>', {
            value: "kg",
            text: "Kilogram (kg)"
        }));
        $('#weightMeasurement').append($('<option>', {
            value: "g",
            text: "Gram (g)"
        }));
    } else {
        $('#weightMeasurement').append($('<option>', {
            value: "oz",
            text: "Ounce (oz)"
        }));
        $('#weightMeasurement').append($('<option>', {
            value: "lb",
            text: "Pound (lb)"
        }));
    }
    // $.each( data.rate, function( key, value ) {
    //     $('#weightMeasurement').append($('<option>', {
    //         value: key,
    //         text: value[0] + " $" + value[1]
    //     }));
    // });
    $('#saveBtn').prop("disabled", false);
}

function disconnectStore(storeId) {
    swal({
        title: "Disconnect Store?",
        text: "This will delete all of your orders and any data. This includes but not limited to: packed items, shipped items, tracking numbers, notes, shipping costs, etc",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#0d8ddb",
        confirmButtonText: "Disconnect",
        cancelButtonText: "Cancel",
        closeOnConfirm: true,
        closeOnCancel: true
    },
    function (isConfirm) {
        if (isConfirm) {
            var l = $('.ladda-button').ladda();
            l.ladda('start');
            jQuery.post(base_url + 'settings/disconnectStore?id=' + storeId, function (data) {
                l.ladda('stop');
                if (data.success) {
                    top.location = base_url+"settings/stores";
                } else {
                    if (data.error === 1) {
                        swal({
                            title: "Error",
                            text: data.message
                        });
                    } else {
                        swal({
                            title: "Error",
                            text: data.message
                        });
                    }
                }
            }, 'json');

        }
    });
}

function updateAccount() {
    var formArray = $('#accountForm').serialize();
    var l = $('.ladda-button').ladda();
    l.ladda('start');
    jQuery.post(base_url+'settings/updateAccount' , formArray, function(data) {
        l.ladda('stop');
        if (data.success) {
            swal({
                title: "Success",
                text: "Account Updated",
                type: "success"
            })
        } else {
            swal({
                title: "Error",
                text: data.message,
                type: "warning"
            });
        }
    }, 'json');
}

function updateBilling() {
    var formArray = $('#companyAddressForm').serialize();
    var l = $('.ladda-button').ladda();
    l.ladda('start');
    jQuery.post(base_url+'settings/updateBilling' , formArray, function(data) {
        l.ladda('stop');
        if (data.success) {
            swal({
                title: "Success",
                text: "Billing Updated",
                type: "success"
            })
        } else {
            swal({
                title: "Error",
                text: data.message,
                type: "warning"
            });
        }
    }, 'json');
}

function updateShippingFromAddress() {
    var c = $('.ladda-button').ladda();
    c.ladda('start');
    var formArray = $('#shippingFromForm').serialize();
    jQuery.post(base_url+'settings/saveShippingFrom' , formArray, function(data) {
        c.ladda('stop');
        if (data.success) {
            location.reload();
        } else {
            swal({
                title: "Error",
                text: data.message
            });
        }
    }, 'json');

    return false;
}

function addPackage() {
    var c = $('.ladda-button').ladda();
    c.ladda('start');
    var formArray = $('#addPackageForm').serialize();
    jQuery.post(base_url+'settings/addPackage' , formArray, function(data) {
        c.ladda('stop');
        if (data.success) {
            location.reload();
        } else {
            swal({
                title: "Error",
                text: data.message
            });
        }
    }, 'json');

    return false;
}

function updatePackage() {
    var c = $('.ladda-button').ladda();
    c.ladda('start');
    var formArray = $('#editPackageForm').serialize();
    jQuery.post(base_url+'settings/updatePackage' , formArray, function(data) {
        c.ladda('stop');
        if (data.success) {
            location.reload();
        } else {
            swal({
                title: "Error",
                text: data.message
            });
        }
    }, 'json');

    return false;
}

function deletePackage() {
    var c = $('.ladda-button').ladda();
    c.ladda('start');
    var formArray = $('#editPackageForm').serialize();
    jQuery.post(base_url+'settings/deletePackage' , formArray, function(data) {
        c.ladda('stop');
        if (data.success) {
            location.reload();
        } else {
            swal({
                title: "Error",
                text: data.message
            });
        }
    }, 'json');

    return false;
}

function addCarrierPackage() {
    var c = $('.ladda-button').ladda();
    c.ladda('start');
    var formArray = $('#addCarrierPackageForm').serialize();
    jQuery.post(base_url+'settings/addPackage' , formArray, function(data) {
        c.ladda('stop');
        if (data.success) {
            location.reload();
        } else {
            swal({
                title: "Error",
                text: data.message
            });
        }
    }, 'json');

    return false;
}

function updateCarrierPackage() {
    var c = $('.ladda-button').ladda();
    c.ladda('start');
    var formArray = $('#editCarrierPackageForm').serialize();
    jQuery.post(base_url+'settings/updatePackage' , formArray, function(data) {
        c.ladda('stop');
        if (data.success) {
            location.reload();
        } else {
            swal({
                title: "Error",
                text: data.message
            });
        }
    }, 'json');

    return false;
}

function updateReturnsToAddress() {
    var c = $('.ladda-button').ladda();
    c.ladda('start');
    var formArray = $('#returnsToForm').serialize();
    jQuery.post(base_url+'settings/saveReturnsToFrom' , formArray, function(data) {
        c.ladda('stop');
        if (data.success) {
            location.reload();
        } else {
            swal({
                title: "Error",
                text: data.message
            });
        }
    }, 'json');
    return false;
}

function createStampsAccount() {
    var c = $('.ladda-button').ladda();
    c.ladda('start');
    var formArray = $('#stampsForm').serialize();
    jQuery.post(base_url+'settings/createStampsAccount' , formArray, function(data) {
        c.ladda('stop');
        if (data.success) {
            swal({
                    title: "Account Created",
                    // text: "Gift Card Only "+data.giftCardOnly.toString()+"\nGift Card Plus Others "+data.giftCardPlus.toString()+"\nGift Card Payments "+data.giftCardPayment.toString()+"\n Gift Card Order #s"+data.giftCardOrderNumbers,
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#0d8ddb",
                    confirmButtonText: "Ok",
                    cancelButtonText: "Close",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        top.location = base_url+"settings/shipping";
                    } else {
                        // location.reload();
                    }
                });
        } else {
            swal({
                title: "Error",
                text: data.message
            });
        }
    }, 'json');

    return false;
}

function shippingAccountStatus(status, service, carrierId) {
    if(status == "disabled") {
        var message = "If you disable the account you will no longer be able to use it to ship.";
        var button = "Disable";
    } else {
        var message = "You can only have one "+ service + " account active at one time. If there is another one active, it will be changed to disabled.";
        var button = "Enable";
    }
    swal({
            title: "Confirm",
            text: message,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#0d8ddb",
            confirmButtonText: button,
            cancelButtonText: "Close",
            closeOnConfirm: false,
            closeOnCancel: true
        },
        function (isConfirm) {
            if (isConfirm) {
                var c = $('.ladda-button').ladda();
                c.ladda('start');
                jQuery.post(base_url+'settings/changeShippingAccountStatus?id='+carrierId+'&status='+status , function(data) {
                    c.ladda('stop');
                    if (data.success) {
                          location.reload();
                    } else {
                        swal({
                            title: "Error",
                            text: data.message
                        });
                    }
                }, 'json');

                return false;
            }
        });
}

function addFundsToCarrier() {
    var c = $('.ladda-button').ladda();
    c.ladda('start');
    var formArray = $('#addFundsForm').serialize();
    jQuery.post(base_url+'settings/addFundsToCarrier' , formArray, function(data) {
        c.ladda('stop');
        if (data.success) {
            swal({
                    title: "Funds Added",
                    // text: "Gift Card Only "+data.giftCardOnly.toString()+"\nGift Card Plus Others "+data.giftCardPlus.toString()+"\nGift Card Payments "+data.giftCardPayment.toString()+"\n Gift Card Order #s"+data.giftCardOrderNumbers,
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#0d8ddb",
                    confirmButtonText: "Ok",
                    cancelButtonText: "Close",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        location.reload();
                    }
                });
        } else {
            swal({
                title: "Error",
                text: data.message
            });
        }
    }, 'json');

    return false;
}

function connectStampsAccount() {
    var c = $('.ladda-button').ladda();
    c.ladda('start');
    var formArray = $('#stampsUserForm').serialize();
    jQuery.post(base_url+'settings/connectStamps' , formArray, function(data) {
        c.ladda('stop');
        if (data.success) {
            swal({
                    title: "Account Added",
                    text: "To start using this account, you will need to enable it on in the settings tab under shipping. When you click ok, you will be automatically sent to the page is enable the carrier.",
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#0d8ddb",
                    confirmButtonText: "Ok",
                    cancelButtonText: "Close",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        top.location = base_url+"settings/shipping"
                    }
                });
        } else {
            swal({
                title: "Error",
                text: data.message
            });
        }
    }, 'json');

    return false;
}

function connectEndiciaAccount() {
    var c = $('.ladda-button').ladda();
    c.ladda('start');
    var formArray = $('#endiciaUserForm').serialize();
    jQuery.post(base_url+'settings/connectEndicia' , formArray, function(data) {
        c.ladda('stop');
        if (data.success) {
            swal({
                    title: "Account Added",
                    text: "To start using this account, you will need to enable it on in the settings tab under shipping. When you click ok, you will be automatically sent to the page is enable the carrier.",
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#0d8ddb",
                    confirmButtonText: "Ok",
                    cancelButtonText: "Close",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        top.location = base_url+"settings/shipping"
                    }
                });
        } else {
            swal({
                title: "Error",
                text: data.message
            });
        }
    }, 'json');

    return false;
}

function connectUPSAccount() {
    var c = $('.ladda-button').ladda();
    c.ladda('start');
    var formArray = $('#upsUserForm').serialize();
    jQuery.post(base_url+'settings/connectUPS' , formArray, function(data) {
        c.ladda('stop');
        if (data.success) {
            swal({
                    title: "Account Added",
                    text: "To start using this account, you will need to enable it on in the settings tab under shipping. When you click ok, you will be automatically sent to the page is enable the carrier.",
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#0d8ddb",
                    confirmButtonText: "Ok",
                    cancelButtonText: "Close",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        top.location = base_url+"settings/shipping"
                    }
                });
        } else {
            swal({
                title: "Error",
                text: data.message
            });
        }
    }, 'json');

    return false;
}

function connectFedexAccount() {
    var c = $('.ladda-button').ladda();
    c.ladda('start');
    var formArray = $('#fedexUserForm').serialize();
    jQuery.post(base_url+'settings/connectFedex' , formArray, function(data) {
        c.ladda('stop');
        if (data.success) {
            swal({
                    title: "Account Added",
                    text: "To start using this account, you will need to enable it on in the settings tab under shipping. When you click ok, you will be automatically sent to the page is enable the carrier.",
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#0d8ddb",
                    confirmButtonText: "Ok",
                    cancelButtonText: "Close",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        top.location = base_url+"settings/shipping"
                    }
                });
        } else {
            swal({
                title: "Error",
                text: data.message
            });
        }
    }, 'json');

    return false;
}

//clientProfile
function updateClientProfile() {
    var formArray = $('#profileForm').serialize();
    var l = $('.ladda-button').ladda();
    l.ladda('start');
    jQuery.post(base_url+'client/updateProfile' , formArray, function(data) {
        if (data.success) {
            l.ladda('stop');
            swal({
                title: "Success",
                text: "Profile Updated",
                type: "success"
            });
        } else {
            l.ladda('stop');
            if(data.error == 1){
                swal({
                    title: "Error",
                    text: data.message,
                    type: "warning"
                });
            } else {
                swal({
                    title: "Error",
                    text: data.message
                })
            }
        }
    }, 'json');

    return false;
}

function createUser() {
    var formArray = $('#addUserForm').serialize();
    var l = $('.ladda-button').ladda();
    l.ladda('start');
    jQuery.post(base_url+'settings/createClient' , formArray, function(data) {
        l.ladda('stop');
        if (data.success) {
            swal({
                title: "Success",
                text: "Account Added",
                type: "success"
            })
        } else {
            swal({
                title: "Error",
                text: data.message,
                type: "warning"
            });
        }
    }, 'json');

    return false;
}

function updateUser() {
    var formArray = $('#updateUserForm').serialize();
    var l = $('.ladda-button').ladda();
    l.ladda('start');
    jQuery.post(base_url+'settings/updateClient' , formArray, function(data) {
        l.ladda('stop');
        if (data.success) {
            swal({
                title: "Success",
                text: "Account Updated",
                type: "success"
            })
        } else {
            swal({
                title: "Error",
                text: data.message,
                type: "warning"
            });
        }
    }, 'json');

    return false;
}

function deactivateClient(wToStatus) {
    if(changeToStatus == "deactivated") {
        var title = "Deactivate Account";
        var text = "Are you sure you want to deactivate the staff account?";
        var button = "Deactivate";
        var link = 'settings/deactivateClient?status=deactivate';
        var success = "Account Deactivated";
    } else {
        var title = "Activate Account";
        var text = "Are you sure you want to activate the staff account?";
        var button = "Activate";
        var link = 'settings/deactivateClient?status=active';
        var success = "Account Activated";
    }
    swal({
            title: title,
            text: text,
            // type: "success",
            showCancelButton: true,
            confirmButtonColor: "#0d8ddb",
            confirmButtonText: button,
            cancelButtonText: "Cancel",
            closeOnConfirm: true,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                var formArray = $('#updateUserForm').serialize();
                var l = $('.ladda-button').ladda();
                l.ladda('start');
                jQuery.post(base_url+link , formArray, function(data) {
                    l.ladda('stop');
                    if (data.success) {
                        location.reload();
                        // swal({
                        //     title: "Success",
                        //     text: success,
                        //     type: "success",
                        //     showCancelButton: false,
                        //     confirmButtonColor: "#0d8ddb",
                        //     confirmButtonText: "Ok",
                        //     cancelButtonText: "Close",
                        //     closeOnConfirm: true,
                        //     closeOnCancel: true
                        // },
                        // function (isConfirm) {
                        //     if(isConfirm) {
                        //         location.reload();
                        //     }
                        // })
                    } else {
                        swal({
                            title: "Error",
                            text: data.message,
                            type: "warning"
                        });
                    }
                }, 'json');

                return false;
            }
        });
}

function deleteClient() {
    swal({
            title: "Confirm",
            text: "Are you sure you want to delete the user? This cannot be undone.",
            // type: "success",
            showCancelButton: true,
            confirmButtonColor: "#0d8ddb",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: true,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                var formArray = $('#updateUserForm').serialize();
                var l = $('.ladda-button').ladda();
                l.ladda('start');
                jQuery.post(base_url+'settings/deleteClient' , formArray, function(data) {
                    l.ladda('stop');
                    if(data.success) {
                        top.location = base_url + "settings/account"
                    } else {
                        swal({
                            title: "Error",
                            text: data.message,
                            type: "warning"
                        });
                    }
                }, 'json');

                return false;
            }
        });
}

//
function addNote() {

    jQuery.post(base_url+'fulfillment/testNote/' ,function(data) {
        if (data.success) {
            swal({
                title: "Success",
                text: data.message,
                type: "success"
            },
                function (isConfirm) {
                    if(isConfirm) {
                        window.location = base_url+"login";
                    }
                })
        } else {
            swal({
                title: "Error",
                text: data.message,
                type: "warning"
            })
        }
    }, 'json');

    return false;
}

function markTourDone(tourDesc) {
    jQuery.post(base_url+'settings/tourComplete?tourDesc='+tourDesc ,function(data) {
        console.log("hi");

    }, 'json');
}