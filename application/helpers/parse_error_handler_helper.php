<?php
/**
 * Created by PhpStorm.
 * User: rapidware
 * Date: 9/19/18
 * Time: 7:35 PM
 */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Parse\ParseClient;
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseSessionStorage;
use ZfrShopify\ShopifyClient;

function parseErrorHandler ($e) {
    $code = $e->getCode();
    switch ($code) {
    case 209: // INVALID_SESSION_TOKEN
        ParseUser::logOut();
//        redirect($this->config->item('base_url'), 'Location');
        redirect('login/logout', 'location');

    break;
    }
};