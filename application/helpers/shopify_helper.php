<?php
/**
 * Created by PhpStorm.
 * User: Marketing101
 * Date: 9/20/18
 * Time: 11:34 AM
 */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function getSubLevel($planName) {
    $returnObject = [];
    switch ($planName) {
        case "Lite":
            $returnObject["subLevel"] = "1";
            $returnObject["labelsRemaining"] = 200;
            $returnObject["terms"] = "0.18 for every label over 200";
            $returnObject["cappedAmount"] = 400;
            $returnObject["perLabel"] = .18;
            break;
        case "Standard":
            $returnObject["subLevel"] = "2";
            $returnObject["labelsRemaining"] = 1000;
            $returnObject["terms"] = "0.15 for every label over 1,000";
            $returnObject["cappedAmount"] = 600;
            $returnObject["perLabel"] = .15;
            break;
        case "Pro":
            $returnObject["subLevel"] = "3";
            $returnObject["labelsRemaining"] = 2500;
            $returnObject["terms"] = "0.13 for every label over 2,500";
            $returnObject["cappedAmount"] = 800;
            $returnObject["perLabel"] = .13;
            break;
        case "Enterprise":
            $returnObject["subLevel"] = "4";
            $returnObject["labelsRemaining"] = 5000;
            $returnObject["terms"] = "0.11 for every label over 5,000";
            $returnObject["cappedAmount"] = 1200;
            $returnObject["perLabel"] = .11;
            break;
        default:
            $returnObject["subLevel"] = "0";
            $returnObject["terms"] = "0.18 for every label over 200";
            $returnObject["labelsRemaining"] = 20;
            $returnObject["cappedAmount"] = 400;
            $returnObject["perLabel"] = .18;
    }

    return $returnObject;
}

function getTrialStatus() {
    $CI = & get_instance();
    if($CI->session->subscriptionLevel == "0" && $CI->session->labelsRemaining == 0) {
        return "expired";
    } else {
        return "active";
    }
}

function getShippingButtons($rateData, $packaging, $shippingCarrier) {
    $buttons = "";
    $y=0;
    for ($x = 0; $x < count($rateData->rate_response->rates); $x++) {
        $rate = $rateData->rate_response->rates[$x];
        $totalCost = $rate->confirmation_amount->amount + $rate->shipping_amount->amount;
        $deliveryDate = new DateTime($rate->estimated_delivery_date);
        //Carrier Packaging
        if ($packaging != "package") {
            //If usps only get priority rate
            if ($shippingCarrier == "usps") {
                if ($rate->service_code != "usps_priority_mail") {
                    continue;
                }
            }
        } else {
            //Get non flat rate pricing
            if($rate->carrier_code == "stamps_com") {
                //Only get non flat rate pricing from stamps
                if($rate->package_type != "package") {
                    continue;
                }
            }
        }
        if($rate->carrier_code == "stamps_com") {
            $carrierImage = base_url()."assets/img/stamps_com.png";
        } else if($rate->carrier_code == "ups") {
            $carrierImage = base_url()."assets/img/ups.png";
        } else if($rate->carrier_code == "fedex") {
            $carrierImage = base_url()."assets/img/fedex.png";
        } else {
            $carrierImage = base_url()."assets/img/endicia-vector-logo.png";
        }
        if ($y == 0) {
            $buttons = '
                <div class="col-md-6 col-lg-6 col-lg-offset-3 col-md-offset-3">
                    <label class="bulky-radio">   
                        <input type="hidden" disabled="" id="carrierIdB[' . $y . ']" name="carrierIdB[' . $y . ']" value="' . $rate->carrier_id . '">                  
                        <input type="hidden" disabled="" id="totalCost[' . $y . ']" name="totalCost[' . $y . ']" value="' . $totalCost . '">                  
                        <input type="hidden" disabled="" id="serviceCodeB[' . $y . ']" name="serviceCodeB[' . $y . ']" value="' . $rate->service_code . '">                                    
                        <input type="hidden" disabled="" id="carrierNameB[' . $y . ']" name="carrierNameB[' . $y . ']" value="' . $rate->carrier_code . '">                                    
                        <input type="radio" name="shipping_label" id="shipping_label_329876373598_unique_identifier_FirstPackage" value="' . $y . '" class="hide" onchange="updateShippingCost(this.value)">
                        <div class="bulky-radio-button">                                           
                                <img style="display: inline-block; float: left; margin-right: 10px" id="shipperImage" class="carrier-service-logo shipperImage" src="'.$carrierImage.'" width="46" height="46" />
                                <p style="margin-top: 4px;">' . $rate->service_type . '<span class="pull-right" style="font-weight: bold">$' . $totalCost . '</span> <br>Expected Delivery Date: <span class="pull-right">' . $deliveryDate->format('l n/d') . '</span></p>                       
                        </div>
                    </label>
                </div>';
        } else {
            $buttons .= '
                <div class="col-md-6 col-lg-6 col-lg-offset-3 col-md-offset-3">
                    <label class="bulky-radio">   
                        <input type="hidden" disabled="" id="carrierIdB[' . $y . ']" name="carrierIdB[' . $y . ']" value="' . $rate->carrier_id . '">
                        <input type="hidden" disabled="" id="totalCost[' . $y . ']" name="totalCost[' . $y . ']" value="' . $totalCost . '">   
                        <input type="hidden" disabled="" id="serviceCodeB[' . $y . ']" name="serviceCodeB[' . $y . ']" value="' . $rate->service_code . '">
                        <input type="hidden" disabled="" id="carrierNameB[' . $y . ']" name="carrierNameB[' . $y . ']" value="' . $rate->carrier_code . '">                    
                        <input type="radio" name="shipping_label" id="shipping_label_329876373598_unique_identifier_FirstPackage" value="' . $y . '" class="hide" onchange="updateShippingCost(this.value)">
                        <div class="bulky-radio-button">                                           
                                <img style="display: inline-block; float: left; margin-right: 10px" id="shipperImage" class="carrier-service-logo shipperImage" src="'.$carrierImage.'" width="46" height="46" />
                                <p style="margin-top: 4px;">' . $rate->service_type . '<span class="pull-right" style="font-weight: bold">$' . $totalCost . '</span> <br>Expected Delivery Date: <span class="pull-right">' . $deliveryDate->format('l n/d') . '</span></p>                       
                        </div>
                    </label>
                </div>';
        }
        $y++;
    }
    return $buttons;
}

    function getDatetimeNow() {
    $tz_object = new DateTimeZone('America/Chicago');
    $datetime = new DateTime();
    $datetime->setTimezone($tz_object);
    return $datetime->format('m/d/Y');
}

function countryCodes() {
    $countries = array
    (
        'AU' => 'Australia',
        'CA' => 'Canada',
        'GB' => 'United Kingdom',
        'US' => 'United States',
    );

    return $countries;
}

function unitedKingdomProvinceCodes() {
    $unitedKingdom_states = array(
        "SWE" => "South West England",
        "GL" => "Greater London",
        "NEL" => "North East London",
        "EM" => "East Midlands",
        "NWE" => "North West England",
        "EOE" => "East Of England",
        "SEE" => "South East England"
    );

    return $unitedKingdom_states;
}

function australiaProvinceCodes() {
    $australian_states = array(
        "NSW" => "New South Wales",
        "VIC" => "Victoria",
        "QLD" => "Queensland",
        "TAS" => "Tasmania",
        "SA" => "South Australia",
        "WA" => "Western Australia",
        "NT" => "Northern Territory",
        "ACT" => "Australian Capital Territory"
    );

    return $australian_states;
}

function canadaProvinceCodes() {
    $canadian_states = array(
        "BC" => "British Columbia",
        "ON" => "Ontario",
        "NL" => "Newfoundland and Labrador",
        "NS" => "Nova Scotia",
        "PE" => "Prince Edward Island",
        "NB" => "New Brunswick",
        "QC" => "Quebec",
        "MB" => "Manitoba",
        "SK" => "Saskatchewan",
        "AB" => "Alberta",
        "NT" => "Northwest Territories",
        "NU" => "Nunavut",
        "YT" => "Yukon Territory"
    );

    return $canadian_states;
}

function usStateCodes() {
    $states = array(
        'AL'=>'Alabama',
        'AK'=>'Alaska',
        'AZ'=>'Arizona',
        'AR'=>'Arkansas',
        'CA'=>'California',
        'CO'=>'Colorado',
        'CT'=>'Connecticut',
        'DE'=>'Delaware',
        'DC'=>'District of Columbia',
        'FL'=>'Florida',
        'GA'=>'Georgia',
        'HI'=>'Hawaii',
        'ID'=>'Idaho',
        'IL'=>'Illinois',
        'IN'=>'Indiana',
        'IA'=>'Iowa',
        'KS'=>'Kansas',
        'KY'=>'Kentucky',
        'LA'=>'Louisiana',
        'ME'=>'Maine',
        'MD'=>'Maryland',
        'MA'=>'Massachusetts',
        'MI'=>'Michigan',
        'MN'=>'Minnesota',
        'MS'=>'Mississippi',
        'MO'=>'Missouri',
        'MT'=>'Montana',
        'NE'=>'Nebraska',
        'NV'=>'Nevada',
        'NH'=>'New Hampshire',
        'NJ'=>'New Jersey',
        'NM'=>'New Mexico',
        'NY'=>'New York',
        'NC'=>'North Carolina',
        'ND'=>'North Dakota',
        'OH'=>'Ohio',
        'OK'=>'Oklahoma',
        'OR'=>'Oregon',
        'PA'=>'Pennsylvania',
        'RI'=>'Rhode Island',
        'SC'=>'South Carolina',
        'SD'=>'South Dakota',
        'TN'=>'Tennessee',
        'TX'=>'Texas',
        'UT'=>'Utah',
        'VT'=>'Vermont',
        'VA'=>'Virginia',
        'WA'=>'Washington',
        'WV'=>'West Virginia',
        'WI'=>'Wisconsin',
        'WY'=>'Wyoming',
    );
    return $states;
}