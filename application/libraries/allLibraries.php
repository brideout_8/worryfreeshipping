<?php
// define location of Parse PHP SDK, e.g. location in "Parse" folder
// Defaults to ./Parse/ folder. Add trailing slash
define( 'PARSE_SDK_DIR', APPPATH.'libraries/parse-php-sdk-master/Parse/' );
// include Parse SDK autoloader
include(APPPATH.'libraries/parse-php-sdk-master/autoload.php' );

include(APPPATH.'libraries/stripe_new/init.php');
include(APPPATH.'libraries/vendor/autoload.php' );