<?php
/**
 * Created by PhpStorm.
 * User: rapidware
 * Date: 11/22/17
 * Time: 6:16 PM
 */?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#333">
    <title>Worry Free Shipping</title>
    <meta name="description" content="Shopify shipping made easy">
    <link rel="shortcut icon" href="<?php echo base_url() ?>/assets/img/Favicon.png">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>/assets/landing/assets/plugins/revolution/revolution/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css">
    <script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"></script>
    <script src="<?php echo base_url() ?>assets/js/functionsLandingV1.js" type="text/javascript" charset="utf-8"></script>
    <!-- REVOLUTION STYLE SHEETS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>/assets/landing/assets/plugins/revolution/revolution/css/settings.css">
    <!-- REVOLUTION LAYERS STYLES -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>/assets/landing/assets/plugins/revolution/revolution/css/layers.css">
    <!-- REVOLUTION NAVIGATION STYLES -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>/assets/landing/assets/plugins/revolution/revolution/css/navigation.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>/assets/landing/assets/css/preload.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>/assets/landing/assets/css/plugins.min.css">
    <!-- TYPEWRITER ADDON -->
    <script type="text/javascript" src="<?php echo base_url() ?>/assets/landing/assets/plugins/revolution/revolution-addons/typewriter/js/revolution.addon.typewriter.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>/assets/landing/assets/plugins/revolution/revolution-addons/typewriter/css/typewriter.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>/assets/landing/assets/css/style.light-blue-500.min.css">
    <!--[if lt IE 9]>
    <!--<script src="assets/landing/assets/js/html5shiv.min.js"></script>-->
<!--    <script src="--><?php //echo base_url() ?><!--/assets/landing/assets/js/respond.min.js"></script>-->
<!--    <![endif]-->
</head>
<style>
    .social-media {
        text-align: center;
    }
</style>
<body>
<div id="ms-preload" class="ms-preload">
    <div id="status">
        <div class="spinner">
            <div class="dot1"></div>
            <div class="dot2"></div>
        </div>
    </div>
</div>
<div class="ms-site-container">
    <!-- Modal -->
<!--    <div class="modal modal-primary" id="ms-account-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">-->
<!--        <div class="modal-dialog animated zoomIn animated-3x" role="document">-->
<!--            <div class="modal-content">-->
<!--                <div class="modal-header d-block shadow-2dp no-pb">-->
<!--                    <button type="button" class="close d-inline pull-right mt-2" data-dismiss="modal" aria-label="Close">-->
<!--                <span aria-hidden="true">-->
<!--                  <i class="zmdi zmdi-close"></i>-->
<!--                </span>-->
<!--                    </button>-->
<!--                    <div class="modal-title text-center">-->
<!--                        <span class="ms-logo ms-logo-white ms-logo-sm mr-1">M</span>-->
<!--                        <h3 class="no-m ms-site-title">Material-->
<!--                            <span>Style</span>-->
<!--                        </h3>-->
<!--                    </div>-->
<!--                    <div class="modal-header-tabs">-->
<!--                        <ul class="nav nav-tabs nav-tabs-full nav-tabs-3 nav-tabs-primary" role="tablist">-->
<!--                            <li class="nav-item" role="presentation">-->
<!--                                <a href="#ms-login-tab" aria-controls="ms-login-tab" role="tab" data-toggle="tab" class="nav-link active withoutripple">-->
<!--                                    <i class="zmdi zmdi-account"></i> Login</a>-->
<!--                            </li>-->
<!--                            <li class="nav-item" role="presentation">-->
<!--                                <a href="#ms-register-tab" aria-controls="ms-register-tab" role="tab" data-toggle="tab" class="nav-link withoutripple">-->
<!--                                    <i class="zmdi zmdi-account-add"></i> Register</a>-->
<!--                            </li>-->
<!--                            <li class="nav-item" role="presentation">-->
<!--                                <a href="#ms-recovery-tab" aria-controls="ms-recovery-tab" role="tab" data-toggle="tab" class="nav-link withoutripple">-->
<!--                                    <i class="zmdi zmdi-key"></i> Recovery Pass</a>-->
<!--                            </li>-->
<!--                        </ul>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="modal-body">-->
<!--                    <div class="tab-content">-->
<!--                        <div role="tabpanel" class="tab-pane fade active show" id="ms-login-tab">-->
<!--                            <form autocomplete="off">-->
<!--                                <fieldset>-->
<!--                                    <div class="form-group label-floating">-->
<!--                                        <div class="input-group">-->
<!--                          <span class="input-group-addon">-->
<!--                            <i class="zmdi zmdi-account"></i>-->
<!--                          </span>-->
<!--                                            <label class="control-label" for="ms-form-user">Username</label>-->
<!--                                            <input type="text" id="ms-form-user" class="form-control"> </div>-->
<!--                                    </div>-->
<!--                                    <div class="form-group label-floating">-->
<!--                                        <div class="input-group">-->
<!--                          <span class="input-group-addon">-->
<!--                            <i class="zmdi zmdi-lock"></i>-->
<!--                          </span>-->
<!--                                            <label class="control-label" for="ms-form-pass">Password</label>-->
<!--                                            <input type="password" id="ms-form-pass" class="form-control"> </div>-->
<!--                                    </div>-->
<!--                                    <div class="row mt-2">-->
<!--                                        <div class="col-md-6">-->
<!--                                            <div class="form-group no-mt">-->
<!--                                                <div class="checkbox">-->
<!--                                                    <label>-->
<!--                                                        <input type="checkbox"> Remember Me </label>-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                        <div class="col-md-6">-->
<!--                                            <button class="btn btn-raised btn-primary pull-right">Login</button>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </fieldset>-->
<!--                            </form>-->
<!--                            <div class="text-center">-->
<!--                                <h3>Login with</h3>-->
<!--                                <a href="javascript:void(0)" class="wave-effect-light btn btn-raised btn-facebook">-->
<!--                                    <i class="zmdi zmdi-facebook"></i> Facebook</a>-->
<!--                                <a href="javascript:void(0)" class="wave-effect-light btn btn-raised btn-twitter">-->
<!--                                    <i class="zmdi zmdi-twitter"></i> Twitter</a>-->
<!--                                <a href="javascript:void(0)" class="wave-effect-light btn btn-raised btn-google">-->
<!--                                    <i class="zmdi zmdi-google"></i> Google</a>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div role="tabpanel" class="tab-pane fade" id="ms-register-tab">-->
<!--                            <form>-->
<!--                                <fieldset>-->
<!--                                    <div class="form-group label-floating">-->
<!--                                        <div class="input-group">-->
<!--                          <span class="input-group-addon">-->
<!--                            <i class="zmdi zmdi-account"></i>-->
<!--                          </span>-->
<!--                                            <label class="control-label" for="ms-form-user">Username</label>-->
<!--                                            <input type="text" id="ms-form-user" class="form-control"> </div>-->
<!--                                    </div>-->
<!--                                    <div class="form-group label-floating">-->
<!--                                        <div class="input-group">-->
<!--                          <span class="input-group-addon">-->
<!--                            <i class="zmdi zmdi-email"></i>-->
<!--                          </span>-->
<!--                                            <label class="control-label" for="ms-form-email">Email</label>-->
<!--                                            <input type="email" id="ms-form-email" class="form-control"> </div>-->
<!--                                    </div>-->
<!--                                    <div class="form-group label-floating">-->
<!--                                        <div class="input-group">-->
<!--                          <span class="input-group-addon">-->
<!--                            <i class="zmdi zmdi-lock"></i>-->
<!--                          </span>-->
<!--                                            <label class="control-label" for="ms-form-pass">Password</label>-->
<!--                                            <input type="password" id="ms-form-pass" class="form-control"> </div>-->
<!--                                    </div>-->
<!--                                    <div class="form-group label-floating">-->
<!--                                        <div class="input-group">-->
<!--                          <span class="input-group-addon">-->
<!--                            <i class="zmdi zmdi-lock"></i>-->
<!--                          </span>-->
<!--                                            <label class="control-label" for="ms-form-pass">Re-type Password</label>-->
<!--                                            <input type="password" id="ms-form-pass" class="form-control"> </div>-->
<!--                                    </div>-->
<!--                                    <button class="btn btn-raised btn-block btn-primary">Register Now</button>-->
<!--                                </fieldset>-->
<!--                            </form>-->
<!--                        </div>-->
<!--                        <div role="tabpanel" class="tab-pane fade" id="ms-recovery-tab">-->
<!--                            <fieldset>-->
<!--                                <div class="form-group label-floating">-->
<!--                                    <div class="input-group">-->
<!--                        <span class="input-group-addon">-->
<!--                          <i class="zmdi zmdi-account"></i>-->
<!--                        </span>-->
<!--                                        <label class="control-label" for="ms-form-user">Username</label>-->
<!--                                        <input type="text" id="ms-form-user" class="form-control"> </div>-->
<!--                                </div>-->
<!--                                <div class="form-group label-floating">-->
<!--                                    <div class="input-group">-->
<!--                        <span class="input-group-addon">-->
<!--                          <i class="zmdi zmdi-email"></i>-->
<!--                        </span>-->
<!--                                        <label class="control-label" for="ms-form-email">Email</label>-->
<!--                                        <input type="email" id="ms-form-email" class="form-control"> </div>-->
<!--                                </div>-->
<!--                                <button class="btn btn-raised btn-block btn-primary">Send Password</button>-->
<!--                            </fieldset>-->
<!--                            </form>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
    <header class="ms-header ms-header-white">
        <!--ms-header-white-->
        <div class="container container-full home" id="home">
            <div class="ms-title">
                <a href="index.html">
                    <!-- <img src="assets/img/demo/logo-header.png" alt=""> -->
<!--                    <span class="ms-logo animated zoomInDown animation-delay-5">M</span>-->
                    <img src="<?php echo base_url() ?>assets/img/WorryFreeShippingIcon.png" height="218px" width="218px">
<!--                    <h1 class="animated fadeInRight animation-delay-6">Material-->
<!--                        <span>Style</span>-->
<!--                    </h1>-->
                </a>
            </div>
<!--            <div class="header-right">-->
<!--                <div class="share-menu">-->
<!--                    <ul class="share-menu-list">-->
<!--                        <li class="animated fadeInRight animation-delay-3">-->
<!--                            <a href="javascript:void(0)" class="btn-circle btn-google">-->
<!--                                <i class="zmdi zmdi-google"></i>-->
<!--                            </a>-->
<!--                        </li>-->
<!--                        <li class="animated fadeInRight animation-delay-2">-->
<!--                            <a href="javascript:void(0)" class="btn-circle btn-facebook">-->
<!--                                <i class="zmdi zmdi-facebook"></i>-->
<!--                            </a>-->
<!--                        </li>-->
<!--                        <li class="animated fadeInRight animation-delay-1">-->
<!--                            <a href="javascript:void(0)" class="btn-circle btn-twitter">-->
<!--                                <i class="zmdi zmdi-twitter"></i>-->
<!--                            </a>-->
<!--                        </li>-->
<!--                    </ul>-->
<!--                    <a href="javascript:void(0)" class="btn-circle btn-circle-primary animated zoomInDown animation-delay-7">-->
<!--                        <i class="zmdi zmdi-share"></i>-->
<!--                    </a>-->
<!--                </div>-->
<!--                <a href="javascript:void(0)" class="btn-circle btn-circle-primary no-focus animated zoomInDown animation-delay-8" data-toggle="modal" data-target="#ms-account-modal">-->
<!--                    <i class="zmdi zmdi-account"></i>-->
<!--                </a>-->
<!--                <form class="search-form animated zoomInDown animation-delay-9">-->
<!--                    <input id="search-box" type="text" class="search-input" placeholder="Search..." name="q" />-->
<!--                    <label for="search-box">-->
<!--                        <i class="zmdi zmdi-search"></i>-->
<!--                    </label>-->
<!--                </form>-->
<!--                <a href="javascript:void(0)" class="btn-ms-menu btn-circle btn-circle-primary ms-toggle-left animated zoomInDown animation-delay-10">-->
<!--                    <i class="zmdi zmdi-menu"></i>-->
<!--                </a>-->
<!--            </div>-->
        </div>
    </header>
    <nav class="navbar navbar-expand-md  navbar-static ms-navbar ms-navbar-white">
        <div class="container container-full">
            <div class="navbar-header">
                <a class="navbar-brand" href="index.html">
                    <!-- <img src="assets/img/demo/logo-navbar.png" alt=""> -->
<!--                    <span class="ms-logo ms-logo-sm">M</span>-->
                    <img src="<?php echo base_url() ?>assets/img/rrlogo.png" height="35px" width="63px">
                    <span class="ms-title">R&R Enterprises
<!--                <strong>Style</strong>-->
              </span>
                </a>
            </div>
            <div class="collapse navbar-collapse" id="ms-navbar">
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a href="#about-us" data-scroll class="nav-link dropdown-toggle animated fadeIn animation-delay-7" role="button">About Us</a>
                    </li >
                    <li class="nav-item page-scroll">
                        <a href="#services" data-scroll class="nav-link dropdown-toggle animated fadeIn animation-delay-7" role="button"> Our Services</a>
                    </li>
<!--                    <li class="nav-item dropdown">-->
<!--                        <a href="#artwork" data-scroll class="nav-link dropdown-toggle animated fadeIn animation-delay-7" role="button"> Artwork</a>-->
<!--                    </li>-->
                    <li class="nav-item dropdown">
                        <a href="<? base_url()?>landing/contact" data-scroll class="nav-link dropdown-toggle animated fadeIn animation-delay-7" role="button"> Contact</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="<?php base_url() ?>landing/customerProfile" class="nav-link dropdown-toggle animated fadeIn animation-delay-7" role="button">Customer Profile</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="<?php base_url() ?>login" class="nav-link dropdown-toggle animated fadeIn animation-delay-7" role="button">Client Login</a>
                    </li>
                </ul>
            </div>
<!--            <a href="javascript:void(0)" class="ms-toggle-left btn-navbar-menu">-->
<!--                <i class="zmdi zmdi-menu"></i>-->
<!--            </a>-->
        </div>
        <!-- container -->
    </nav>
    <div id="rev_slider_1014_1_wrapper" class="rev_slider_wrapper ms-hero-rev fullscreen-container" data-alias="typewriter-effect" data-source="gallery" style="background-color:transparent;padding:0px;">
        <!-- START REVOLUTION SLIDER 5.3.0.2 fullscreen mode -->
        <div id="rev_slider_1014_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.3.0.2">
            <ul>
                <!-- SLIDE  -->
<!--                <li data-index="rs-2800" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="--><?php //echo base_url() ?><!--/assets/landing/assets/img/demo/back_video_100x50.jpg"-->
<!--                    data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">-->
<!--                    <!-- MAIN IMAGE -->
<!--                    <img src="--><?php //echo base_url() ?><!--/assets/landing/assets/img/demo/back_video.jpg" alt="" data-bgposition="center center" data-bgfit="cover" class="rev-slidebg" data-no-retina>-->
<!--                    <!-- LAYERS -->
<!--                    <!-- BACKGROUND VIDEO LAYER -->
<!--                    <div class="rs-background-video-layer" data-forcerewind="on" data-volume="mute" data-videowidth="100%" data-videoheight="100%" data-videomp4="--><?php //echo base_url() ?><!--/assets/landing/assets/media/back_video.mp4" data-videopreload="auto" data-videoloop="loop" data-forceCover="1"-->
<!--                         data-aspectratio="16:9" data-autoplay="true" data-autoplayonlyfirsttime="false"></div>-->
<!--                    <!-- LAYER NR. 1 -->
<!--                    <div class="tp-caption tp-shape tp-shapewrapper " id="slide-2800-layer-7" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-width="full"-->
<!--                         data-height="full" data-whitespace="nowrap" data-type="shape" data-basealign="slide" data-responsive_offset="off" data-responsive="off" data-frames='[{"from":"opacity:0;","speed":500,"to":"o:1;","delay":0,"ease":"Power4.easeOut"},{"speed":5000,"to":"opacity:0;","ease":"Power4.easeInOut"}]'-->
<!--                         data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5;background-color:rgba(0, 0, 0, 0.50);border-color:rgba(0, 0, 0, 0);border-width:0px;">-->
<!--                    </div>-->
<!--                    <!-- LAYER NR. 2 -->
<!--                    <div class="tp-caption   tp-resizeme" id="slide-2800-layer-1" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['-80','-80','-130','-157']" data-fontsize="['130','130','100','80']"-->
<!--                        data-lineheight="['130','130','100','80']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"from":"y:50px;sX:1;sY:1;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'-->
<!--                        data-textAlign="['center','center','center','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; white-space: nowrap; font-size: 130px; line-height: 130px; font-weight: 700; color: rgba(255, 255, 255, 1.00);font-family:Arial, Helvetica, sans-serif;border-width:0px;letter-spacing:-7px;">R&R Enterprises-->
<!--                    </div>-->
<!--                    <!-- LAYER NR. 3 -->
<!--                    <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme" id="slide-2800-layer-3" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['16','16','-54','-89']"-->
<!--                         data-width="60" data-height="3" data-whitespace="nowrap" data-type="shape" data-responsive_offset="on" data-frames='[{"from":"y:50px;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'-->
<!--                         data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 7;background-color:rgba(3, 169, 244, 1.000);border-color:rgba(0, 0, 0, 0);border-width:0px;">-->
<!--                    </div>-->
<!--                    <!-- LAYER NR. 4 -->
<!--                    <div class="tp-caption   tp-resizeme" id="slide-2800-layer-2" data-x="['center','center','center','center']" data-hoffset="['-10','-10','-10','-10']" data-y="['middle','middle','middle','middle']" data-voffset="['65','65','-8','-32']" data-fontsize="['40','40','30','30']"-->
<!--                         data-width="['640','640','480','360']" data-height="none" data-whitespace="['nowrap','nowrap','normal','normal']" data-type="text" data-typewriter='{"lines":"We%20Are%20DirectDriverApparel.com,We%20Are%20R-RRacewear.com","enabled":"on","speed":"60","delays":"1%7C100","looped":"on","cursorType":"one","blinking":"on","word_delay":"off","sequenced":"on","hide_cursor":"off","start_delay":"1500","newline_delay":"1000","deletion_speed":"20","deletion_delay":"1000","blinking_speed":"500","linebreak_delay":"60","cursor_type":"two","background":"off"}'-->
<!--                         data-responsive_offset="on" data-frames='[{"from":"y:50px;sX:1;sY:1;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]' data-textAlign="['center','center','center','center']"-->
<!--                         data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 8; min-width: 640px; max-width: 640px; white-space: nowrap; font-size: 40px; line-height: 40px; font-weight: 300; color: rgba(255, 255, 255, 1.00);font-family:Roboto, sans-serif;border-width:0px;">We Are R&R Racewear</div>-->
                    <!-- LAYER NR. 5 -->
<!--                    <div class="tp-caption rev-btn  tp-resizeme" id="slide-2800-layer-4" data-x="['right','right','center','center']" data-hoffset="['660','550','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['240','240','100','83']"-->
<!--                         data-width="none" data-height="none" data-whitespace="nowrap" data-type="button" data-actions='[{"event":"click","action":"scrollbelow","offset":"px","delay":""}]' data-responsive_offset="on" data-frames='[{"from":"x:-50px;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"150","ease":"Power2.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bg:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 0);bw:2px 2px 2px 2px;"}]'-->
<!--                         data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[50,50,50,50]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[50,50,50,50]" style="z-index: 9; white-space: nowrap; font-size: 15px; line-height: 46px; font-weight: 700; color: rgba(255, 255, 255, 1.00);font-family:Arial, Helvetica, sans-serif;background-color:rgba(0, 0, 0, 0);border-color:rgba(255, 255, 255, 0.25);border-style:solid;border-width:2px;border-radius:4px 4px 4px 4px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;letter-spacing:5px;cursor:pointer;">ABOUT US </div>-->
<!--                    <!-- LAYER NR. 6 -->
<!--                    <div class="tp-caption   tp-resizeme" id="slide-2800-layer-6" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['237','237','158','141']" data-width="none"-->
<!--                         data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"from":"y:50px;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'-->
<!--                         data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 10; white-space: nowrap; font-size: 25px; line-height: 25px; font-weight: 300; color: rgba(255, 255, 255, 1.00);font-family:Roboto, sans-serif;border-width:0px;">or </div>-->
<!--                    <!-- LAYER NR. 7 -->
<!--                    <div class="tp-caption rev-btn  tp-resizeme" id="slide-2800-layer-5" data-x="['left','left','center','center']" data-hoffset="['660','550','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['240','240','224','207']"-->
<!--                         data-width="none" data-height="none" data-whitespace="nowrap" data-type="button" data-actions='[{"event":"click","action":"scrollbelow","offset":"px","delay":""}]' data-responsive_offset="on" data-frames='[{"from":"x:50px;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"150","ease":"Power2.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bg:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 0);bw:2px 2px 2px 2px;"}]'-->
<!--                         data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[50,50,50,50]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[50,50,50,50]" style="z-index: 11; white-space: nowrap; font-size: 15px; line-height: 46px; font-weight: 700; color: rgba(255, 255, 255, 1.00);font-family:Arial, Helvetica, sans-serif;background-color:rgba(0, 0, 0, 0);border-color:rgba(255, 255, 255, 0.25);border-style:solid;border-width:2px;border-radius:4px 4px 4px 4px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;letter-spacing:5px;cursor:pointer;">CONTACT </div>-->
<!--                </li>-->
                <!-- SLIDE  -->
                <li data-index="rs-2801" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="<?php echo base_url() ?>/assets/landing/assets/plugins/revolution/assets/images/citybg-100x50.jpg"
                    data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="<?php echo base_url() ?>/assets/landing/assets/plugins/revolution/assets/images/citybg.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->
                    <!-- LAYER NR. 8 -->
                    <div class="tp-caption tp-shape tp-shapewrapper " id="slide-2801-layer-7" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-width="full"
                         data-height="full" data-whitespace="nowrap" data-type="shape" data-basealign="slide" data-responsive_offset="off" data-responsive="off" data-frames='[{"from":"opacity:0;","speed":1000,"to":"o:1;","delay":0,"ease":"Power4.easeOut"},{"delay":"wait","speed":1000,"to":"opacity:0;","ease":"Power4.easeOut"}]'
                         data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 12;background-color:rgba(0, 0, 0, 0.50);border-color:rgba(0, 0, 0, 0);border-width:0px;">
                    </div>
                    <!-- LAYER NR. 9 -->
                    <div class="tp-caption   tp-resizeme" id="slide-2801-layer-1" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['-80','-80','-130','-157']" data-fontsize="['130','130','100','80']"
                         data-lineheight="['130','130','100','80']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"from":"y:50px;sX:1;sY:1;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
                         data-textAlign="['center','center','center','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 13; white-space: nowrap; font-size: 130px; line-height: 130px; font-weight: 700; color: rgba(255, 255, 255, 1.00);font-family:Arial, Helvetica, sans-serif;background-color:rgba(0, 0, 0, 0);border-width:0px;letter-spacing:-7px;">R&R Enterprises</div>
                    <!-- LAYER NR. 10 -->
                    <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme" id="slide-2801-layer-3" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['16','16','-54','-89']"
                         data-width="60" data-height="3" data-whitespace="nowrap" data-type="shape" data-responsive_offset="on" data-frames='[{"from":"y:50px;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
                         data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 14;background-color:rgba(3, 169, 244, 1.000);border-color:rgba(0, 0, 0, 0);border-width:0px;">
                    </div>
                    <!-- LAYER NR. 11 -->
                    <div class="tp-caption   tp-resizeme" id="slide-2801-layer-2" data-x="['center','center','center','center']" data-hoffset="['-10','-10','-10','-10']" data-y="['middle','middle','middle','middle']" data-voffset="['65','65','-8','-32']" data-fontsize="['40','40','30','30']"
                         data-width="['640','640','480','360']" data-height="none" data-whitespace="normal" data-type="text" data-typewriter='{"lines":"We%20Are%20DirectDriverApparel.com,We%20Are%20R-RRacewear.com","enabled":"on","speed":"60","delays":"1%7C100","looped":"on","cursorType":"one","blinking":"on","word_delay":"off","sequenced":"on","hide_cursor":"off","start_delay":"1500","newline_delay":"1000","deletion_speed":"20","deletion_delay":"1000","blinking_speed":"500","linebreak_delay":"60","cursor_type":"two","background":"off"}'
                         data-responsive_offset="on" data-frames='[{"from":"y:50px;sX:1;sY:1;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]' data-textAlign="['center','center','center','center']"
                         data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 15; min-width: 640px; max-width: 640px; white-space: normal; font-size: 40px; line-height: 40px; font-weight: 300; color: rgba(255, 255, 255, 1.00);font-family:Roboto, sans-serif;border-width:0px;">We Are R&R Racewear</div>
                    <!-- LAYER NR. 12 -->
<!--                    <div class="tp-caption rev-btn  tp-resizeme" id="slide-2801-layer-4" data-x="['right','right','center','center']" data-hoffset="['660','550','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['240','240','100','83']"-->
<!--                         data-width="none" data-height="none" data-whitespace="nowrap" data-type="button" data-actions='[{"event":"click","action":"scrollbelow","offset":"px","delay":""}]' data-responsive_offset="on" data-frames='[{"from":"x:-50px;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"150","ease":"Power2.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bg:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 0);bw:2px 2px 2px 2px;"}]'-->
<!--                         data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[50,50,50,50]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[50,50,50,50]" style="z-index: 16; white-space: nowrap; font-size: 15px; line-height: 46px; font-weight: 700; color: rgba(255, 255, 255, 1.00);font-family:Arial, Helvetica, sans-serif;background-color:rgba(0, 0, 0, 0);border-color:rgba(255, 255, 255, 0.25);border-style:solid;border-width:2px;border-radius:4px 4px 4px 4px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;letter-spacing:5px;cursor:pointer;">ABOUT US </div>-->
                    <!-- LAYER NR. 13 -->
<!--                    <div class="tp-caption   tp-resizeme" id="slide-2801-layer-6" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['237','237','158','141']" data-width="none"-->
<!--                         data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"from":"y:50px;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'-->
<!--                         data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 17; white-space: nowrap; font-size: 25px; line-height: 25px; font-weight: 300; color: rgba(255, 255, 255, 1.00);font-family:Roboto, sans-serif;border-width:0px;">or </div>-->
                    <!-- LAYER NR. 14 -->
<!--                    <div class="tp-caption rev-btn  tp-resizeme" id="slide-2801-layer-5" data-x="['left','left','center','center']" data-hoffset="['660','550','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['240','240','224','207']"-->
<!--                         data-width="none" data-height="none" data-whitespace="nowrap" data-type="button" data-actions='[{"event":"click","action":"scrollbelow","offset":"px","delay":""}]' data-responsive_offset="on" data-frames='[{"from":"x:50px;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"150","ease":"Power2.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bg:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 0);bw:2px 2px 2px 2px;"}]'-->
<!--                         data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[50,50,50,50]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[50,50,50,50]" style="z-index: 18; white-space: nowrap; font-size: 15px; line-height: 46px; font-weight: 700; color: rgba(255, 255, 255, 1.00);font-family:Arial, Helvetica, sans-serif;background-color:rgba(0, 0, 0, 0);border-color:rgba(255, 255, 255, 0.25);border-style:solid;border-width:2px;border-radius:4px 4px 4px 4px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;letter-spacing:5px;cursor:pointer;">CONTACT </div>-->
                </li>
                <!-- SLIDE  -->
<!--                <li data-index="rs-2802" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="--><?php //echo base_url() ?><!--/assets/landing/assets/plugins/revolution/assets/images/farmbg-100x50.jpg"-->
<!--                    data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">-->
<!--                    <!-- MAIN IMAGE -->
<!--                    <img src="--><?php //echo base_url() ?><!--/assets/landing/assets/img/demo/city_cloud.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>-->
<!--                    <!-- LAYERS -->
<!--                    <!-- LAYER NR. 15 -->
<!--                    <div class="tp-caption tp-shape tp-shapewrapper " id="slide-2802-layer-7" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-width="full"-->
<!--                         data-height="full" data-whitespace="nowrap" data-type="shape" data-basealign="slide" data-responsive_offset="off" data-responsive="off" data-frames='[{"from":"opacity:0;","speed":1000,"to":"o:1;","delay":0,"ease":"Power4.easeOut"},{"delay":"wait","speed":1000,"to":"opacity:0;","ease":"Power4.easeOut"}]'-->
<!--                         data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 19;background-color:rgba(0, 0, 0, 0.50);border-color:rgba(0, 0, 0, 0);border-width:0px;">-->
<!--                    </div>-->
<!--                    <!-- LAYER NR. 16 -->
<!--                    <div class="tp-caption   tp-resizeme" id="slide-2802-layer-1" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['-80','-81','-171','-146']" data-fontsize="['100','100','70','60']"-->
<!--                         data-lineheight="['100','100','70','50']" data-width="['760','760','none','360']" data-height="none" data-whitespace="['normal','normal','nowrap','normal']" data-type="text" data-typewriter='{"lines":"","enabled":"on","speed":"80","delays":"1%7C100","looped":"off","cursorType":"one","blinking":"on","word_delay":"off","sequenced":"off","hide_cursor":"on","start_delay":"1000","newline_delay":"1000","deletion_speed":"20","deletion_delay":"1000","blinking_speed":"500","linebreak_delay":"60","cursor_type":"two","background":"off"}'-->
<!--                         data-responsive_offset="on" data-frames='[{"from":"y:50px;sX:1;sY:1;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]' data-textAlign="['center','center','center','center']"-->
<!--                         data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 20; min-width: 760px; max-width: 760px; white-space: normal; font-size: 100px; line-height: 100px; font-weight: 700; color: rgba(255, 255, 255, 1.00);font-family:Arial, Helvetica, sans-serif;border-width:0px;letter-spacing:-5px;">It Always Seems-->
<!--                        <br/>Impossible-->
<!--                        <br/> Until its Done. </div>-->
<!--                    <!-- LAYER NR. 17 -->
<!--                    <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme" id="slide-2802-layer-3" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['96','96','-35','-19']"-->
<!--                         data-width="60" data-height="3" data-whitespace="nowrap" data-type="shape" data-responsive_offset="on" data-frames='[{"from":"y:50px;opacity:0;","speed":2500,"to":"o:1;","delay":5000,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'-->
<!--                         data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 21;background-color:rgba(3, 169, 244, 1.000);border-color:rgba(0, 0, 0, 0);border-width:0px;">-->
<!--                    </div>-->
<!--                    <!-- LAYER NR. 18 -->
<!--                    <div class="tp-caption   tp-resizeme" id="slide-2802-layer-2" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['144','144','12','18']" data-fontsize="['40','40','30','30']"-->
<!--                         data-width="['640','640','480','360']" data-height="none" data-whitespace="normal" data-type="text" data-responsive_offset="on" data-frames='[{"from":"y:50px;sX:1;sY:1;opacity:0;","speed":2500,"to":"o:1;","delay":5000,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'-->
<!--                         data-textAlign="['center','center','center','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 22; min-width: 640px; max-width: 640px; white-space: normal; font-size: 40px; line-height: 40px; font-weight: 300; color: rgba(255, 255, 255, 1.00);font-family:Roboto, sans-serif;border-width:0px;">Nelson Mandela </div>-->
<!--                    <!-- LAYER NR. 19 -->
<!--                    <div class="tp-caption rev-btn  tp-resizeme" id="slide-2802-layer-4" data-x="['right','right','center','center']" data-hoffset="['660','550','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['240','240','100','93']"-->
<!--                         data-width="none" data-height="none" data-whitespace="nowrap" data-type="button" data-actions='[{"event":"click","action":"scrollbelow","offset":"px","delay":""}]' data-responsive_offset="on" data-frames='[{"from":"x:-50px;opacity:0;","speed":2500,"to":"o:1;","delay":5000,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"150","ease":"Power2.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bg:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 0);bw:2px 2px 2px 2px;"}]'-->
<!--                         data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[50,50,50,50]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[50,50,50,50]" style="z-index: 23; white-space: nowrap; font-size: 15px; line-height: 46px; font-weight: 700; color: rgba(255, 255, 255, 1.00);font-family:Arial, Helvetica, sans-serif;background-color:rgba(0, 0, 0, 0);border-color:rgba(255, 255, 255, 0.25);border-style:solid;border-width:2px;border-radius:4px 4px 4px 4px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;letter-spacing:5px;cursor:pointer;">ABOUT US </div>-->
<!--                    <!-- LAYER NR. 20 -->
<!--                    <div class="tp-caption   tp-resizeme" id="slide-2802-layer-6" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['237','237','158','151']" data-width="none"-->
<!--                         data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"from":"y:50px;opacity:0;","speed":2500,"to":"o:1;","delay":5000,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'-->
<!--                         data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 24; white-space: nowrap; font-size: 25px; line-height: 25px; font-weight: 300; color: rgba(255, 255, 255, 1.00);font-family:Roboto, sans-serif;border-width:0px;">or </div>-->
<!--                    <!-- LAYER NR. 21 -->
<!--                    <div class="tp-caption rev-btn  tp-resizeme" id="slide-2802-layer-5" data-x="['left','left','center','center']" data-hoffset="['660','550','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['240','240','224','217']"-->
<!--                         data-width="none" data-height="none" data-whitespace="nowrap" data-type="button" data-actions='[{"event":"click","action":"scrollbelow","offset":"px","delay":""}]' data-responsive_offset="on" data-frames='[{"from":"x:50px;opacity:0;","speed":2500,"to":"o:1;","delay":5000,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"150","ease":"Power2.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bg:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 0);bw:2px 2px 2px 2px;"}]'-->
<!--                         data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[50,50,50,50]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[50,50,50,50]" style="z-index: 25; white-space: nowrap; font-size: 15px; line-height: 46px; font-weight: 700; color: rgba(255, 255, 255, 1.00);font-family:Arial, Helvetica, sans-serif;background-color:rgba(0, 0, 0, 0);border-color:rgba(255, 255, 255, 0.25);border-style:solid;border-width:2px;border-radius:4px 4px 4px 4px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;letter-spacing:5px;cursor:pointer;">CONTACT </div>-->
<!--                </li>-->
            </ul>
            <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
        </div>
    </div>
    <!-- END REVOLUTION SLIDER -->
    <div class="container">
        <section class="mb-4 services" id="services">
            <h2 class="text-center no-mt mb-6 wow fadeInUp">Our Services</h2>
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6 mb-2">
                    <div class="ms-icon-feature wow flipInX animation-delay-4">
                        <div class="ms-icon-feature-icon">
                  <span class="ms-icon ms-icon-lg ms-icon-inverse">
                    <i class="fa fa-cloud"></i>
                  </span>
                        </div>
                        <div class="ms-icon-feature-content">
                            <h4 class="color-primary">Screen Printing</h4>
                            <p>With state-of-the-art equipment, we are a leader in high end screen printing.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 mb-2">
                    <div class="ms-icon-feature wow flipInX animation-delay-4">
                        <div class="ms-icon-feature-icon">
                  <span class="ms-icon ms-icon-lg ms-icon-inverse">
                    <i class="fa fa-desktop"></i>
                  </span>
                        </div>
                        <div class="ms-icon-feature-content">
                            <h4 class="color-primary">Embroidery</h4>
                            <p>With 22 heads of embroidery we can offer the turn around times needed to meet your deadline.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 mb-2">
                    <div class="ms-icon-feature wow flipInX animation-delay-4">
                        <div class="ms-icon-feature-icon">
                  <span class="ms-icon ms-icon-lg ms-icon-inverse">
                    <i class="fa fa-tablet"></i>
                  </span>
                        </div>
                        <div class="ms-icon-feature-content">
                            <h4 class="color-primary">Promotional Items</h4>
                            <p>We can handle any request with over 300 item in our catalog.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 mb-2">
                    <div class="ms-icon-feature wow flipInX animation-delay-4">
                        <div class="ms-icon-feature-icon">
                  <span class="ms-icon ms-icon-lg ms-icon-inverse">
                    <i class="fa fa-wordpress"></i>
                  </span>
                        </div>
                        <div class="ms-icon-feature-content">
                            <h4 class="color-primary">Die-cast</h4>
                            <p>The industry standard in sprint car and midget die-cast.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 mb-2">
                    <div class="ms-icon-feature wow flipInX animation-delay-4">
                        <div class="ms-icon-feature-icon">
                  <span class="ms-icon ms-icon-lg ms-icon-inverse">
                    <i class="fa fa-graduation-cap"></i>
                  </span>
                        </div>
                        <div class="ms-icon-feature-content">
                            <h4 class="color-primary">Online Fulfillment Stores</h4>
                            <p>Hassle free online stores. We handle everything from inventory to customer service.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 mb-2">
                    <div class="ms-icon-feature wow flipInX animation-delay-4">
                        <div class="ms-icon-feature-icon">
                  <span class="ms-icon ms-icon-lg ms-icon-inverse">
                    <i class="fa fa-send"></i>
                  </span>
                        </div>
                        <div class="ms-icon-feature-content">
                            <h4 class="color-primary">Graphic Design</h4>
                            <p>Our team can handle anything from simple to the most complex artwork.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- container -->
<!--    <div class="wrap ms-hero-bg-info ms-hero-img-keyboard ms-bg-fixed">-->
<!--        <div class="container">-->
<!--            <h1 class="color-white text-center mb-4">Some numerical data</h1>-->
<!--            <div class="row">-->
<!--                <div class="col-lg-3 col-md-6 col-sm-6">-->
<!--                    <div class="card card-royal card-block text-center wow zoomInUp animation-delay-2">-->
<!--                        <h2 class="counter">450</h2>-->
<!--                        <i class="fa fa-4x fa-coffee color-royal"></i>-->
<!--                        <p class="mt-2 no-mb lead small-caps">cups of coffee</p>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-lg-3 col-md-6 col-sm-6">-->
<!--                    <div class="card card-success card-block text-center wow zoomInUp animation-delay-5">-->
<!--                        <h2 class="counter">64</h2>-->
<!--                        <i class="fa fa-4x fa-briefcase color-success"></i>-->
<!--                        <p class="mt-2 no-mb lead small-caps">projects done</p>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-lg-3 col-md-6 col-sm-6">-->
<!--                    <div class="card card-danger card-block text-center wow zoomInUp animation-delay-4">-->
<!--                        <h2 class="counter">600</h2>-->
<!--                        <i class="fa fa-4x fa-comments-o color-danger"></i>-->
<!--                        <p class="mt-2 no-mb lead small-caps">comments</p>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-lg-3 col-md-6 col-sm-6">-->
<!--                    <div class="card card-warning card-block text-center wow zoomInUp animation-delay-3">-->
<!--                        <h2 class="counter">3500</h2>-->
<!--                        <i class="fa fa-4x fa-group color-warning"></i>-->
<!--                        <p class="mt-2 no-mb lead small-caps">happy clients</p>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="text-center color-white mw-800 center-block mt-4">-->
<!--                <p class="lead lead-lg">Discover our projects and the rigorous process of creation. Our principles are creativity, design, experience and knowledge. We are backed by 20 years of research.</p>-->
<!--                <a href="javascript:void(0)" class="btn btn-raised btn-white color-primary wow flipInX animation-delay-8">-->
<!--                    <i class="fa fa-space-shuttle"></i> I have a project</a>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
    <div class="container mt-6 about-us" id="about-us">
        <div class="text-center mb-4">
            <h2>
                <span class="text-normal">About Us</span></h2>
        </div>
        <div class="row">
            <div class="col-lg-6">
<!--                <h3 class="wow fadeInUp animation-delay-2">About Us</h3>-->
                <p class="wow fadeInUp animation-delay-3">
                    From the very beginning, R&R Enterprises has always been a company that evolves to meet the needs and demands of its customers. Because of that, what began as a small retail store under the name of "Awesome Collectibles", in 1995, has grown into a booming apparel, promotional products, and die-cast business.
                    R&R actually began as Awesome Collectibles, an 800 square foot die-cast store, in 1995. After a few years, the company became a distributor for Action Performance before venturing into t-shirt printing and embroidery in 2009.
                </p>
                <p class="wow fadeInUp animation-delay-4">
                    In 2000, R&R moved into a 13,000 square foot location and occupied that space until May, 2013 when it was moved into a new, state-of-the-art 39,000 square foot facility in Valley Park, MO.  Currently, R&R employs over 40 people throughout production, sales, marketing, and administration departments, and offers screen printing, embroidery, sublimation and much more.
                </p>
<!--                    <a href="#">quibusdam odio eius eligendi</a> tenetur! Ea, repudiandae eveniet ab minima laboriosam minima voluptate quaerat sequi harum.</p>-->
            </div>
            <div class="col-lg-6">
                <div class="ms-collapse" id="accordion2" role="tablist" aria-multiselectable="true">
                    <div class="mb-0 card card-primary wow fadeInUp animation-delay-2">
                        <div class="card-header" role="tab" id="headingOne2">
                            <h4 class="card-title">
                                <a class="withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="true" aria-controls="collapseOne2">
                                    <i class="fa fa-lightbulb-o"></i> Talent and creativity </a>
                            </h4>
                        </div>
                        <div id="collapseOne2" class="card-collapse collapse show" role="tabpanel" aria-labelledby="headingOne2">
                            <div class="card-block color-dark">
                                <p>Our art department can handle anything from simple to the most complex.</p>
<!--                                <p>Dolores, corrupti, aliquam doloremque accusantium nemo sunt veniam est incidunt perferendis minima obcaecati ex aperiam voluptatibus blanditiis eum suscipit magnam dolorum in adipisci nihil.</p>-->
                            </div>
                        </div>
                    </div>
                    <div class="mb-0 card card-primary wow fadeInUp animation-delay-5">
                        <div class="card-header" role="tab" id="headingTwo2">
                            <h4 class="card-title">
                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo2" aria-expanded="false" aria-controls="collapseTwo2">
                                    <i class="fa fa-desktop"></i> Equipment </a>
                            </h4>
                        </div>
                        <div id="collapseTwo2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingTwo2">
                            <div class="card-block color-dark">
                                <p>We pride ourselves with having the best equipment. We use state-of-the-art M&R screen printing machines and Tajima embroidery machines. </p>
<!--                                <p>Incidunt, harum itaque voluptatum asperiores recusandae explicabo maiores. Alias, quos, ex impedit at error id laborum fugit architecto qui beatae molestiae dolorum rem veritatis quia aliquam totam.</p>-->
                            </div>
                        </div>
                    </div>
                    <div class="mb-0 card card-primary wow fadeInUp animation-delay-7">
                        <div class="card-header" role="tab" id="headingThree3">
                            <h4 class="card-title">
                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree2" aria-expanded="false" aria-controls="collapseThree2">
                                    <i class="fa fa-user"></i> Quality and Support </a>
                            </h4>
                        </div>
                        <div id="collapseThree2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingThree2">
                            <div class="card-block color-dark">
                                <p>We hold ourselves to high quality products. Along with this comes the best customer service here to assist you.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- container -->
<!--    <div class="wrap ms-hero-bg-dark mt-6 ms-hero-img-room ms-bg-fixed artwork" id="artwork">-->
<!--        <div class="container">-->
<!--            <h2 class="text-center mb-4 color-white">Artwork</h2>-->
<!--            <div class="mw-800 center-block mb-4">-->
<!--                <ul class="nav nav-tabs nav-tabs-transparent color-white indicator-primary nav-tabs-full nav-tabs-4" role="tablist">-->
<!--                    <li role="presentation" class="nav-item filter" data-filter="all">-->
<!--                        <a class="nav-link active withoutripple" href="#home7" aria-controls="home7" role="tab" data-toggle="tab">-->
<!--                            <i class="zmdi zmdi-desktop-mac"></i>-->
<!--                            <span class="d-none d-md-inline">All</span>-->
<!--                        </a>-->
<!--                    </li>-->
<!--                    <li role="presentation" class="nav-item filter" data-filter=".category-1">-->
<!--                        <a class="nav-link withoutripple" href="#profile7" aria-controls="profile7" role="tab" data-toggle="tab">-->
<!--                            <i class="zmdi zmdi-language-html5"></i>-->
<!--                            <span class="d-none d-md-inline">Dirt Racing</span>-->
<!--                        </a>-->
<!--                    </li>-->
<!--                    <li role="presentation" class="nav-item filter" data-filter=".category-2">-->
<!--                        <a class="nav-link withoutripple" href="#messages7" aria-controls="messages7" role="tab" data-toggle="tab">-->
<!--                            <i class="fa fa-wordpress"></i>-->
<!--                            <span class="d-none d-md-inline">Nascar Licensed</span>-->
<!--                        </a>-->
<!--                    </li>-->
<!--                    <li role="presentation" class="nav-item filter" data-filter=".category-3">-->
<!--                        <a class="nav-link withoutripple" href="#settings7" aria-controls="settings7" role="tab" data-toggle="tab">-->
<!--                            <i class="zmdi zmdi-language-javascript"></i>-->
<!--                            <span class="d-none d-md-inline">Other</span>-->
<!--                        </a>-->
<!--                    </li>-->
<!--                </ul>-->
<!--            </div>-->
<!--            <div class="row" id="Container">-->
<!--                <div class="col-lg-4 col-md-6 col-sm-6 mix category-1 category-3">-->
<!--                    <div class="card">-->
<!--                        <figure class="ms-thumbnail">-->
<!--                            <img src="--><?php //echo base_url() ?><!--/assets/landing/assets/img/demo/port1.jpg" alt="" class="img-fluid">-->
<!--                            <figcaption class="ms-thumbnail-caption text-center">-->
<!--                                <div class="ms-thumbnail-caption-content">-->
<!--                                    <h4 class="ms-thumbnail-caption-title mb-2">Lorem ipsum dolor</h4>-->
<!--                                    <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mr-1 btn-circle-white color-danger">-->
<!--                                        <i class="zmdi zmdi-favorite"></i>-->
<!--                                    </a>-->
<!--                                    <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 mr-1 btn-circle-white color-warning">-->
<!--                                        <i class="zmdi zmdi-star"></i>-->
<!--                                    </a>-->
<!--                                    <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 btn-circle-white color-success">-->
<!--                                        <i class="zmdi zmdi-share"></i>-->
<!--                                    </a>-->
<!--                                </div>-->
<!--                            </figcaption>-->
<!--                        </figure>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <!-- item -->
<!--                <div class="col-lg-4 col-md-6 col-sm-6 mix category-2">-->
<!--                    <div class="card">-->
<!--                        <figure class="ms-thumbnail">-->
<!--                            <img src="--><?php //echo base_url() ?><!--/assets/landing/assets/img/demo/port2.jpg" alt="" class="img-fluid">-->
<!--                            <figcaption class="ms-thumbnail-caption text-center">-->
<!--                                <div class="ms-thumbnail-caption-content">-->
<!--                                    <h4 class="ms-thumbnail-caption-title mb-2">Lorem ipsum dolor</h4>-->
<!--                                    <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mr-1 btn-circle-white color-danger">-->
<!--                                        <i class="zmdi zmdi-favorite"></i>-->
<!--                                    </a>-->
<!--                                    <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 mr-1 btn-circle-white color-warning">-->
<!--                                        <i class="zmdi zmdi-star"></i>-->
<!--                                    </a>-->
<!--                                    <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 btn-circle-white color-success">-->
<!--                                        <i class="zmdi zmdi-share"></i>-->
<!--                                    </a>-->
<!--                                </div>-->
<!--                            </figcaption>-->
<!--                        </figure>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <!-- item -->
<!--                <div class="col-lg-4 col-md-6 col-sm-6 mix category-3">-->
<!--                    <div class="card">-->
<!--                        <figure class="ms-thumbnail">-->
<!--                            <img src="--><?php //echo base_url() ?><!--/assets/landing/assets/img/demo/port3.jpg" alt="" class="img-fluid">-->
<!--                            <figcaption class="ms-thumbnail-caption text-center">-->
<!--                                <div class="ms-thumbnail-caption-content">-->
<!--                                    <h4 class="ms-thumbnail-caption-title mb-2">Lorem ipsum dolor</h4>-->
<!--                                    <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mr-1 btn-circle-white color-danger">-->
<!--                                        <i class="zmdi zmdi-favorite"></i>-->
<!--                                    </a>-->
<!--                                    <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 mr-1 btn-circle-white color-warning">-->
<!--                                        <i class="zmdi zmdi-star"></i>-->
<!--                                    </a>-->
<!--                                    <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 btn-circle-white color-success">-->
<!--                                        <i class="zmdi zmdi-share"></i>-->
<!--                                    </a>-->
<!--                                </div>-->
<!--                            </figcaption>-->
<!--                        </figure>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <!-- item -->
<!--                <div class="col-lg-4 col-md-6 col-sm-6 mix category-2">-->
<!--                    <div class="card">-->
<!--                        <figure class="ms-thumbnail">-->
<!--                            <img src="--><?php //echo base_url() ?><!--/assets/landing/assets/img/demo/port4.jpg" alt="" class="img-fluid">-->
<!--                            <figcaption class="ms-thumbnail-caption text-center">-->
<!--                                <div class="ms-thumbnail-caption-content">-->
<!--                                    <h4 class="ms-thumbnail-caption-title mb-2">Lorem ipsum dolor</h4>-->
<!--                                    <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mr-1 btn-circle-white color-danger">-->
<!--                                        <i class="zmdi zmdi-favorite"></i>-->
<!--                                    </a>-->
<!--                                    <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 mr-1 btn-circle-white color-warning">-->
<!--                                        <i class="zmdi zmdi-star"></i>-->
<!--                                    </a>-->
<!--                                    <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 btn-circle-white color-success">-->
<!--                                        <i class="zmdi zmdi-share"></i>-->
<!--                                    </a>-->
<!--                                </div>-->
<!--                            </figcaption>-->
<!--                        </figure>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <!-- item -->
<!--                <div class="col-lg-4 col-md-6 col-sm-6 mix category-1">-->
<!--                    <div class="card">-->
<!--                        <figure class="ms-thumbnail">-->
<!--                            <img src="--><?php //echo base_url() ?><!--/assets/landing/assets/img/demo/port5.jpg" alt="" class="img-fluid">-->
<!--                            <figcaption class="ms-thumbnail-caption text-center">-->
<!--                                <div class="ms-thumbnail-caption-content">-->
<!--                                    <h4 class="ms-thumbnail-caption-title mb-2">Lorem ipsum dolor</h4>-->
<!--                                    <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mr-1 btn-circle-white color-danger">-->
<!--                                        <i class="zmdi zmdi-favorite"></i>-->
<!--                                    </a>-->
<!--                                    <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 mr-1 btn-circle-white color-warning">-->
<!--                                        <i class="zmdi zmdi-star"></i>-->
<!--                                    </a>-->
<!--                                    <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 btn-circle-white color-success">-->
<!--                                        <i class="zmdi zmdi-share"></i>-->
<!--                                    </a>-->
<!--                                </div>-->
<!--                            </figcaption>-->
<!--                        </figure>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <!-- item -->
<!--                <div class="col-lg-4 col-md-6 col-sm-6 mix category-3">-->
<!--                    <div class="card">-->
<!--                        <figure class="ms-thumbnail">-->
<!--                            <img src="--><?php //echo base_url() ?><!--/assets/landing/assets/img/demo/port6.jpg" alt="" class="img-fluid">-->
<!--                            <figcaption class="ms-thumbnail-caption text-center">-->
<!--                                <div class="ms-thumbnail-caption-content">-->
<!--                                    <h4 class="ms-thumbnail-caption-title mb-2">Lorem ipsum dolor</h4>-->
<!--                                    <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mr-1 btn-circle-white color-danger">-->
<!--                                        <i class="zmdi zmdi-favorite"></i>-->
<!--                                    </a>-->
<!--                                    <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 mr-1 btn-circle-white color-warning">-->
<!--                                        <i class="zmdi zmdi-star"></i>-->
<!--                                    </a>-->
<!--                                    <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 btn-circle-white color-success">-->
<!--                                        <i class="zmdi zmdi-share"></i>-->
<!--                                    </a>-->
<!--                                </div>-->
<!--                            </figcaption>-->
<!--                        </figure>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <!-- item -->
<!--                <div class="col-lg-4 col-md-6 col-sm-6 mix category-2">-->
<!--                    <div class="card">-->
<!--                        <figure class="ms-thumbnail">-->
<!--                            <img src="--><?php //echo base_url() ?><!--/assets/landing/assets/img/demo/port7.jpg" alt="" class="img-fluid">-->
<!--                            <figcaption class="ms-thumbnail-caption text-center">-->
<!--                                <div class="ms-thumbnail-caption-content">-->
<!--                                    <h4 class="ms-thumbnail-caption-title mb-2">Lorem ipsum dolor</h4>-->
<!--                                    <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mr-1 btn-circle-white color-danger">-->
<!--                                        <i class="zmdi zmdi-favorite"></i>-->
<!--                                    </a>-->
<!--                                    <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 mr-1 btn-circle-white color-warning">-->
<!--                                        <i class="zmdi zmdi-star"></i>-->
<!--                                    </a>-->
<!--                                    <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 btn-circle-white color-success">-->
<!--                                        <i class="zmdi zmdi-share"></i>-->
<!--                                    </a>-->
<!--                                </div>-->
<!--                            </figcaption>-->
<!--                        </figure>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <!-- item -->
<!--                <div class="col-lg-4 col-md-6 col-sm-6 mix category-1">-->
<!--                    <div class="card">-->
<!--                        <figure class="ms-thumbnail">-->
<!--                            <img src="--><?php //echo base_url() ?><!--/assets/landing/assets/img/demo/port8.jpg" alt="" class="img-fluid">-->
<!--                            <figcaption class="ms-thumbnail-caption text-center">-->
<!--                                <div class="ms-thumbnail-caption-content">-->
<!--                                    <h4 class="ms-thumbnail-caption-title mb-2">Lorem ipsum dolor</h4>-->
<!--                                    <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mr-1 btn-circle-white color-danger">-->
<!--                                        <i class="zmdi zmdi-favorite"></i>-->
<!--                                    </a>-->
<!--                                    <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 mr-1 btn-circle-white color-warning">-->
<!--                                        <i class="zmdi zmdi-star"></i>-->
<!--                                    </a>-->
<!--                                    <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 btn-circle-white color-success">-->
<!--                                        <i class="zmdi zmdi-share"></i>-->
<!--                                    </a>-->
<!--                                </div>-->
<!--                            </figcaption>-->
<!--                        </figure>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <!-- item -->
<!--                <div class="col-lg-4 col-md-6 col-sm-6 mix category-3">-->
<!--                    <div class="card">-->
<!--                        <figure class="ms-thumbnail">-->
<!--                            <img src="--><?php //echo base_url() ?><!--/assets/landing/assets/img/demo/port9.jpg" alt="" class="img-fluid">-->
<!--                            <figcaption class="ms-thumbnail-caption text-center">-->
<!--                                <div class="ms-thumbnail-caption-content">-->
<!--                                    <h4 class="ms-thumbnail-caption-title mb-2">Lorem ipsum dolor</h4>-->
<!--                                    <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mr-1 btn-circle-white color-danger">-->
<!--                                        <i class="zmdi zmdi-favorite"></i>-->
<!--                                    </a>-->
<!--                                    <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 mr-1 btn-circle-white color-warning">-->
<!--                                        <i class="zmdi zmdi-star"></i>-->
<!--                                    </a>-->
<!--                                    <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 btn-circle-white color-success">-->
<!--                                        <i class="zmdi zmdi-share"></i>-->
<!--                                    </a>-->
<!--                                </div>-->
<!--                            </figcaption>-->
<!--                        </figure>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <!-- item -->
<!--                <div class="col-lg-4 col-md-6 col-sm-6 mix category-2">-->
<!--                    <div class="card">-->
<!--                        <figure class="ms-thumbnail">-->
<!--                            <img src="--><?php //echo base_url() ?><!--/assets/landing/assets/img/demo/port10.jpg" alt="" class="img-fluid">-->
<!--                            <figcaption class="ms-thumbnail-caption text-center">-->
<!--                                <div class="ms-thumbnail-caption-content">-->
<!--                                    <h4 class="ms-thumbnail-caption-title mb-2">Lorem ipsum dolor</h4>-->
<!--                                    <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mr-1 btn-circle-white color-danger">-->
<!--                                        <i class="zmdi zmdi-favorite"></i>-->
<!--                                    </a>-->
<!--                                    <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 mr-1 btn-circle-white color-warning">-->
<!--                                        <i class="zmdi zmdi-star"></i>-->
<!--                                    </a>-->
<!--                                    <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 btn-circle-white color-success">-->
<!--                                        <i class="zmdi zmdi-share"></i>-->
<!--                                    </a>-->
<!--                                </div>-->
<!--                            </figcaption>-->
<!--                        </figure>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <!-- item -->
<!--                <div class="col-lg-4 col-md-6 col-sm-6 mix category-1">-->
<!--                    <div class="card">-->
<!--                        <figure class="ms-thumbnail">-->
<!--                            <img src="--><?php //echo base_url() ?><!--/assets/landing/assets/img/demo/port11.jpg" alt="" class="img-fluid">-->
<!--                            <figcaption class="ms-thumbnail-caption text-center">-->
<!--                                <div class="ms-thumbnail-caption-content">-->
<!--                                    <h4 class="ms-thumbnail-caption-title mb-2">Lorem ipsum dolor</h4>-->
<!--                                    <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mr-1 btn-circle-white color-danger">-->
<!--                                        <i class="zmdi zmdi-favorite"></i>-->
<!--                                    </a>-->
<!--                                    <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 mr-1 btn-circle-white color-warning">-->
<!--                                        <i class="zmdi zmdi-star"></i>-->
<!--                                    </a>-->
<!--                                    <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 btn-circle-white color-success">-->
<!--                                        <i class="zmdi zmdi-share"></i>-->
<!--                                    </a>-->
<!--                                </div>-->
<!--                            </figcaption>-->
<!--                        </figure>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <!-- item -->
<!--                <div class="col-lg-4 col-md-6 col-sm-6 mix category-2">-->
<!--                    <div class="card">-->
<!--                        <figure class="ms-thumbnail">-->
<!--                            <img src="--><?php //echo base_url() ?><!--/assets/landing/assets/img/demo/port12.jpg" alt="" class="img-fluid">-->
<!--                            <figcaption class="ms-thumbnail-caption text-center">-->
<!--                                <div class="ms-thumbnail-caption-content">-->
<!--                                    <h4 class="ms-thumbnail-caption-title mb-2">Lorem ipsum dolor</h4>-->
<!--                                    <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mr-1 btn-circle-white color-danger">-->
<!--                                        <i class="zmdi zmdi-favorite"></i>-->
<!--                                    </a>-->
<!--                                    <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 mr-1 btn-circle-white color-warning">-->
<!--                                        <i class="zmdi zmdi-star"></i>-->
<!--                                    </a>-->
<!--                                    <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 btn-circle-white color-success">-->
<!--                                        <i class="zmdi zmdi-share"></i>-->
<!--                                    </a>-->
<!--                                </div>-->
<!--                            </figcaption>-->
<!--                        </figure>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <!-- item -->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
    <!-- container -->
    <aside class="ms-footbar">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 ms-footer-col">
                    <div class="ms-footbar-block">
                        <h3 class="ms-footbar-title">Sitemap</h3>
                        <ul class="list-unstyled ms-icon-list three_cols">
                            <li>
                                <a data-scroll id="back-top" href="#">
                                    <i class="zmdi zmdi-home"></i> Home</a>
                            </li>
<!--                            <li>-->
<!--                                <a href="page-blog.html">-->
<!--                                    <i class="zmdi zmdi-edit"></i> Blog</a>-->
<!--                            </li>-->
                            <li>
                                <a data-scroll href="#artwork">
                                    <i class="zmdi zmdi-image-o"></i> Artwork</a>
                            </li>
<!--                            <li>-->
<!--                                <a href="portfolio-filters_sidebar.html">-->
<!--                                    <i class="zmdi zmdi-case"></i> Works</a>-->
<!--                            </li>-->
<!--                            <li>-->
<!--                                <a href="page-timeline_left2.html">-->
<!--                                    <i class="zmdi zmdi-time"></i> Timeline</a>-->
<!--                            </li>-->
<!--                            <li>-->
<!--                                <a href="page-pricing.html">-->
<!--                                    <i class="zmdi zmdi-money"></i> Pricing</a>-->
<!--                            </li>-->
                            <li>
                                <a data-scroll href="#about-us">
                                    <i class="zmdi zmdi-favorite-outline"></i> About Us</a>
                            </li>
<!--                            <li>-->
<!--                                <a href="page-team2.html">-->
<!--                                    <i class="zmdi zmdi-accounts"></i> Our Team</a>-->
<!--                            </li>-->
                            <li>
                                <a data-scroll href="#services">
                                    <i class="zmdi zmdi-face"></i> Services</a>
                            </li>
                            <li>
                                <a href="page-faq2.html">
                                    <i class="zmdi zmdi-help"></i> FAQ</a>
                            </li>
                            <li>
                                <a href="<? echo base_url()?>/login">
                                    <i class="zmdi zmdi-lock"></i> Login</a>
                            </li>
                            <li>
                                <a href="page-contact.html">
                                    <i class="zmdi zmdi-email"></i> Contact</a>
                            </li>
                        </ul>
                    </div>
<!--                    <div class="ms-footbar-block">-->
<!--                        <h3 class="ms-footbar-title">Subscribe</h3>-->
<!--                        <p class="">Lorem ipsum Amet fugiat elit nisi anim mollit minim labore ut esse Duis ullamco ad dolor veniam velit.</p>-->
<!--                        <form>-->
<!--                            <div class="form-group label-floating mt-2 mb-1">-->
<!--                                <div class="input-group ms-input-subscribe">-->
<!--                                    <label class="control-label" for="ms-subscribe">-->
<!--                                        <i class="zmdi zmdi-email"></i> Email Adress</label>-->
<!--                                    <input type="email" id="ms-subscribe" class="form-control"> </div>-->
<!--                            </div>-->
<!--                            <button class="ms-subscribre-btn" type="button">Subscribe</button>-->
<!--                        </form>-->
<!--                    </div>-->
                </div>
<!--                <div class="col-lg-5 col-md-7 ms-footer-col ms-footer-alt-color">-->
<!--                    <div class="ms-footbar-block">-->
<!--                        <h3 class="ms-footbar-title text-center mb-2">Last Articles</h3>-->
<!--                        <div class="ms-footer-media">-->
<!--                            <div class="media">-->
<!--                                <div class="media-left media-middle">-->
<!--                                    <a href="javascript:void(0)">-->
<!--                                        <img class="media-object media-object-circle" src="--><?php //echo base_url() ?><!--/assets/landing/assets/img/demo/p75.jpg" alt="..."> </a>-->
<!--                                </div>-->
<!--                                <div class="media-body">-->
<!--                                    <h4 class="media-heading">-->
<!--                                        <a href="javascript:void(0)">Lorem ipsum dolor sit expedita cumque amet consectetur adipisicing repellat</a>-->
<!--                                    </h4>-->
<!--                                    <div class="media-footer">-->
<!--                        <span>-->
<!--                          <i class="zmdi zmdi-time color-info-light"></i> August 18, 2016</span>-->
<!--                                        <span>-->
<!--                          <i class="zmdi zmdi-folder-outline color-warning-light"></i>-->
<!--                          <a href="javascript:void(0)">Design</a>-->
<!--                        </span>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="media">-->
<!--                                <div class="media-left media-middle">-->
<!--                                    <a href="javascript:void(0)">-->
<!--                                        <img class="media-object media-object-circle" src="--><?php //echo base_url() ?><!--/assets/landing/assets/img/demo/p75.jpg" alt="..."> </a>-->
<!--                                </div>-->
<!--                                <div class="media-body">-->
<!--                                    <h4 class="media-heading">-->
<!--                                        <a href="javascript:void(0)">Labore ut esse Duis consectetur expedita cumque ullamco ad dolor veniam velit</a>-->
<!--                                    </h4>-->
<!--                                    <div class="media-footer">-->
<!--                        <span>-->
<!--                          <i class="zmdi zmdi-time color-info-light"></i> August 18, 2016</span>-->
<!--                                        <span>-->
<!--                          <i class="zmdi zmdi-folder-outline color-warning-light"></i>-->
<!--                          <a href="javascript:void(0)">News</a>-->
<!--                        </span>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="media">-->
<!--                                <div class="media-left media-middle">-->
<!--                                    <a href="javascript:void(0)">-->
<!--                                        <img class="media-object media-object-circle" src="--><?php //echo base_url() ?><!--/assets/landing/assets/img/demo/p75.jpg" alt="..."> </a>-->
<!--                                </div>-->
<!--                                <div class="media-body">-->
<!--                                    <h4 class="media-heading">-->
<!--                                        <a href="javascript:void(0)">voluptates deserunt ducimus expedita cumque quaerat molestiae labore</a>-->
<!--                                    </h4>-->
<!--                                    <div class="media-footer">-->
<!--                        <span>-->
<!--                          <i class="zmdi zmdi-time color-info-light"></i> August 18, 2016</span>-->
<!--                                        <span>-->
<!--                          <i class="zmdi zmdi-folder-outline color-warning-light"></i>-->
<!--                          <a href="javascript:void(0)">Productivity</a>-->
<!--                        </span>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
                <div class="col-lg-3 col-md-5 ms-footer-col ms-footer">
                    <div class="ms-footbar-block">
<!--                        <div class="ms-footbar-title">-->
<!--                            <span class="ms-logo ms-logo-white ms-logo-sm mr-1">M</span>-->
<!--                            <h3 class="no-m ms-site-title">Material-->
<!--                                <span>Style</span>-->
<!--                            </h3>-->
<!--                        </div>-->
                        <h3 class="ms-footbar-title">Location</h3>
                        <address class="no-mb">
                            <p>
                                <i class="color-danger-light zmdi zmdi-pin mr-1"></i> 400 Valley School Dr</p>
                            <p>
                                <i class="color-warning-light zmdi zmdi-map mr-1"></i> Valley Park, MO 63088</p>
                            <p>
                                <i class="color-info-light zmdi zmdi-email mr-1"></i>
                                <a href="mailto:sales@r-renterprises.com">sales@r-renterprises.com</a>
                            </p>
                            <p>
                                <i class="color-royal-light zmdi zmdi-phone mr-1"></i>(636) 717-0001</p>
                            <p>
                                <i class="color-success-light fa fa-fax mr-1"></i>877-730-6953</p>
                        </address>
                    </div>
<!--                    <div class="ms-footbar-block">-->
<!--                        <h3 class="ms-footbar-title">Social Media</h3>-->
<!--                        <div class="ms-footbar-social">-->
<!--                            <a href="javascript:void(0)" class="btn-circle btn-facebook">-->
<!--                                <i class="zmdi zmdi-facebook"></i>-->
<!--                            </a>-->
<!--                            <a href="javascript:void(0)" class="btn-circle btn-twitter">-->
<!--                                <i class="zmdi zmdi-twitter"></i>-->
<!--                            </a>-->
<!--                            <a href="javascript:void(0)" class="btn-circle btn-youtube">-->
<!--                                <i class="zmdi zmdi-youtube-play"></i>-->
<!--                            </a>-->
<!--                            <br>-->
<!--                            <a href="javascript:void(0)" class="btn-circle btn-google">-->
<!--                                <i class="zmdi zmdi-google"></i>-->
<!--                            </a>-->
<!--                            <a href="javascript:void(0)" class="btn-circle btn-instagram">-->
<!--                                <i class="zmdi zmdi-instagram"></i>-->
<!--                            </a>-->
<!--                            <a href="javascript:void(0)" class="btn-circle btn-github">-->
<!--                                <i class="zmdi zmdi-github"></i>-->
<!--                            </a>-->
<!--                        </div>-->
<!--                    </div>-->
                </div>
                <div class="col-lg-3 col-md-5 ms-footer-col social-media">
<!--                    <div class="ms-footbar-block">-->
<!--                        <div class="ms-footbar-title">-->
<!--                            <span class="ms-logo ms-logo-white ms-logo-sm mr-1">M</span>-->
<!--                            <h3 class="no-m ms-site-title">Material-->
<!--                                <span>Style</span>-->
<!--                            </h3>-->
<!--                        </div>-->
<!--                        <address class="no-mb">-->
<!--                            <p>-->
<!--                                <i class="color-danger-light zmdi zmdi-pin mr-1"></i> 795 Folsom Ave, Suite 600</p>-->
<!--                            <p>-->
<!--                                <i class="color-warning-light zmdi zmdi-map mr-1"></i> San Francisco, CA 94107</p>-->
<!--                            <p>-->
<!--                                <i class="color-info-light zmdi zmdi-email mr-1"></i>-->
<!--                                <a href="mailto:joe@example.com">example@domain.com</a>-->
<!--                            </p>-->
<!--                            <p>-->
<!--                                <i class="color-royal-light zmdi zmdi-phone mr-1"></i>+34 123 456 7890 </p>-->
<!--                            <p>-->
<!--                                <i class="color-success-light fa fa-fax mr-1"></i>+34 123 456 7890 </p>-->
<!--                        </address>-->
<!--                    </div>-->
                    <div class="ms-footbar-block">
                        <h3 class="ms-footbar-title">Social Media</h3>
                        <div class="ms-footbar-social">
                            <a href="https://www.facebook.com/randr95" class="btn-circle btn-facebook">
                                <i class="zmdi zmdi-facebook"></i>
                            </a>
                            <a href="https://twitter.com/randr95" class="btn-circle btn-twitter">
                                <i class="zmdi zmdi-twitter"></i>
                            </a>
                            <br>
                            <a href="https://www.youtube.com/user/RandRRacewear/feed" class="btn-circle btn-youtube">
                                <i class="zmdi zmdi-youtube-play"></i>
                            </a>
<!--                            <a href="javascript:void(0)" class="btn-circle btn-google">-->
<!--                                <i class="zmdi zmdi-google"></i>-->
<!--                            </a>-->
                            <a href="https://www.instagram.com/randr95/" class="btn-circle btn-instagram">
                                <i class="zmdi zmdi-instagram"></i>
                            </a>
<!--                            <a href="javascript:void(0)" class="btn-circle btn-github">-->
<!--                                <i class="zmdi zmdi-github"></i>-->
<!--                            </a>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </aside>
    <footer class="ms-footer">
        <div class="container">
            <p>Copyright &copy; R&R Enterprises 2017</p>
        </div>
    </footer>
    <div class="btn-back-top">
        <a href="#" data-scroll id="back-top" class="btn-circle btn-circle-primary btn-circle-sm btn-circle-raised ">
            <i class="zmdi zmdi-long-arrow-up"></i>
        </a>
    </div>
</div>
<!-- ms-site-container -->
<!--<div class="ms-slidebar sb-slidebar sb-left sb-style-overlay" id="ms-slidebar">-->
<!--    <div class="sb-slidebar-container">-->
<!--        <header class="ms-slidebar-header">-->
<!--            <div class="ms-slidebar-login">-->
<!--                <a href="javascript:void(0)" class="withripple">-->
<!--                    <i class="zmdi zmdi-account"></i> Login</a>-->
<!--                <a href="javascript:void(0)" class="withripple">-->
<!--                    <i class="zmdi zmdi-account-add"></i> Register</a>-->
<!--            </div>-->
<!--            <div class="ms-slidebar-title">-->
<!--                <form class="search-form">-->
<!--                    <input id="search-box-slidebar" type="text" class="search-input" placeholder="Search..." name="q" />-->
<!--                    <label for="search-box-slidebar">-->
<!--                        <i class="zmdi zmdi-search"></i>-->
<!--                    </label>-->
<!--                </form>-->
<!--                <div class="ms-slidebar-t">-->
<!--                    <span class="ms-logo ms-logo-sm">M</span>-->
<!--                    <h3>Material-->
<!--                        <span>Style</span>-->
<!--                    </h3>-->
<!--                </div>-->
<!--            </div>-->
<!--        </header>-->
<!--        <ul class="ms-slidebar-menu" id="slidebar-menu" role="tablist" aria-multiselectable="true">-->
<!--            <li class="card" role="tab" id="sch1">-->
<!--                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#slidebar-menu" href="#sc1" aria-expanded="false" aria-controls="sc1">-->
<!--                    <i class="zmdi zmdi-home"></i> Home </a>-->
<!--                <ul id="sc1" class="card-collapse collapse" role="tabpanel" aria-labelledby="sch1">-->
<!--                    <li>-->
<!--                        <a href="index.html">Default Home</a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="home-generic-2.html">Home Black Slider</a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="home-landing.html">Home Landing Intro</a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="home-landing3.html">Home Landing Video</a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="home-shop.html">Home Shop 1</a>-->
<!--                    </li>-->
<!--                </ul>-->
<!--            </li>-->
<!--            <li class="card" role="tab" id="sch2">-->
<!--                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#slidebar-menu" href="#sc2" aria-expanded="false" aria-controls="sc2">-->
<!--                    <i class="zmdi zmdi-desktop-mac"></i> Pages </a>-->
<!--                <ul id="sc2" class="card-collapse collapse" role="tabpanel" aria-labelledby="sch2">-->
<!--                    <li>-->
<!--                        <a href="page-about.html">About US</a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="page-team.html">Our Team</a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="page-product.html">Products</a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="page-services.html">Services</a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="page-faq.html">FAQ</a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="page-timeline_left.html">Timeline</a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="page-contact.html">Contact Option</a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="page-login.html">Login</a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="page-pricing.html">Pricing</a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="page-coming.html">Coming Soon</a>-->
<!--                    </li>-->
<!--                </ul>-->
<!--            </li>-->
<!--            <li class="card" role="tab" id="sch4">-->
<!--                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#slidebar-menu" href="#sc4" aria-expanded="false" aria-controls="sc4">-->
<!--                    <i class="zmdi zmdi-edit"></i> Blog </a>-->
<!--                <ul id="sc4" class="card-collapse collapse" role="tabpanel" aria-labelledby="sch4">-->
<!--                    <li>-->
<!--                        <a href="blog-sidebar.html">Blog Sidebar 1</a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="blog-sidebar2.html">Blog Sidebar 2</a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="blog-masonry.html">Blog Masonry 1</a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="blog-masonry2.html">Blog Masonry 2</a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="blog-full.html">Blog Full Page 1</a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="blog-full2.html">Blog Full Page 2</a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="blog-post.html">Blog Post 1</a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="blog-post2.html">Blog Post 2</a>-->
<!--                    </li>-->
<!--                </ul>-->
<!--            </li>-->
<!--            <li class="card" role="tab" id="sch5">-->
<!--                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#slidebar-menu" href="#sc5" aria-expanded="false" aria-controls="sc5">-->
<!--                    <i class="zmdi zmdi-shopping-basket"></i> E-Commerce </a>-->
<!--                <ul id="sc5" class="card-collapse collapse" role="tabpanel" aria-labelledby="sch5">-->
<!--                    <li>-->
<!--                        <a href="ecommerce-filters.html">E-Commerce Sidebar</a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="ecommerce-filters-full.html">E-Commerce Sidebar Full</a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="ecommerce-filters-full2.html">E-Commerce Topbar Full</a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="ecommerce-item.html">E-Commerce Item</a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="ecommerce-cart.html">E-Commerce Cart</a>-->
<!--                    </li>-->
<!--                </ul>-->
<!--            </li>-->
<!--            <li class="card" role="tab" id="sch6">-->
<!--                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#slidebar-menu" href="#sc6" aria-expanded="false" aria-controls="sc6">-->
<!--                    <i class="zmdi zmdi-collection-image-o"></i> Portfolio </a>-->
<!--                <ul id="sc6" class="card-collapse collapse" role="tabpanel" aria-labelledby="sch6">-->
<!--                    <li>-->
<!--                        <a href="portfolio-filters_sidebar.html">Portfolio Sidebar Filters</a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="portfolio-filters_topbar.html">Portfolio Topbar Filters</a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="portfolio-filters_sidebar_fluid.html">Portfolio Sidebar Fluid</a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="portfolio-filters_topbar_fluid.html">Portfolio Topbar Fluid</a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="portfolio-cards.html">Porfolio Cards</a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="portfolio-masonry.html">Porfolio Masonry</a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="portfolio-item.html">Portfolio Item 1</a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="portfolio-item2.html">Portfolio Item 2</a>-->
<!--                    </li>-->
<!--                </ul>-->
<!--            </li>-->
<!--            <li>-->
<!--                <a class="link" href="component-typography.html">-->
<!--                    <i class="zmdi zmdi-view-compact"></i> UI Elements</a>-->
<!--            </li>-->
<!--            <li>-->
<!--                <a class="link" href="page-all.html">-->
<!--                    <i class="zmdi zmdi-link"></i> All Pages</a>-->
<!--            </li>-->
<!--        </ul>-->
<!--        <div class="ms-slidebar-social ms-slidebar-block">-->
<!--            <h4 class="ms-slidebar-block-title">Social Links</h4>-->
<!--            <div class="ms-slidebar-social">-->
<!--                <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-facebook">-->
<!--                    <i class="zmdi zmdi-facebook"></i>-->
<!--                    <span class="badge-pill badge-pill-pink">12</span>-->
<!--                    <div class="ripple-container"></div>-->
<!--                </a>-->
<!--                <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-twitter">-->
<!--                    <i class="zmdi zmdi-twitter"></i>-->
<!--                    <span class="badge-pill badge-pill-pink">4</span>-->
<!--                    <div class="ripple-container"></div>-->
<!--                </a>-->
<!--<!--                <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-google">-->
<!--<!--                    <i class="zmdi zmdi-google"></i>-->
<!--<!--                    <div class="ripple-container"></div>-->
<!--<!--                </a>-->
<!--                <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-instagram">-->
<!--                    <i class="zmdi zmdi-instagram"></i>-->
<!--                    <div class="ripple-container"></div>-->
<!--                </a>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<script src="<?php echo base_url() ?>/assets/landing/assets/js/plugins.min.js"></script>
<!-- REVOLUTION JS FILES -->
<script type="text/javascript" src="<?php echo base_url() ?>/assets/landing/assets/plugins/revolution/revolution/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>/assets/landing/assets/plugins/revolution/revolution/js/jquery.themepunch.revolution.min.js"></script>
<script src="<?php echo base_url() ?>/assets/landing/assets/js/app.min.js"></script>
<script type="text/javascript">
    $(function()
    {
        $('#Container').mixItUp();
    });
    var tpj = jQuery;
    var revapi1014;
    tpj(document).ready(function()
    {
        if (tpj("#rev_slider_1014_1").revolution == undefined)
        {
            revslider_showDoubleJqueryError("#rev_slider_1014_1");
        }
        else
        {
            revapi1014 = tpj("#rev_slider_1014_1").show().revolution(
                {
                    sliderType: "standard",
                    jsFileLocation: "assets/plugins/revolution/revolution/js/",
                    sliderLayout: "fullscreen",
                    dottedOverlay: "none",
                    delay: 9000,
                    navigation:
                        {
                            keyboardNavigation: "off",
                            keyboard_direction: "horizontal",
                            mouseScrollNavigation: "off",
                            mouseScrollReverse: "default",
                            onHoverStop: "off",
                            touch:
                                {
                                    touchenabled: "on",
                                    swipe_threshold: 75,
                                    swipe_min_touches: 1,
                                    swipe_direction: "horizontal",
                                    drag_block_vertical: false
                                },
                            arrows:
                                {
                                    style: "uranus",
                                    enable: true,
                                    hide_onmobile: true,
                                    hide_under: 768,
                                    hide_onleave: false,
                                    tmp: '',
                                    left:
                                        {
                                            h_align: "left",
                                            v_align: "center",
                                            h_offset: 20,
                                            v_offset: 0
                                        },
                                    right:
                                        {
                                            h_align: "right",
                                            v_align: "center",
                                            h_offset: 20,
                                            v_offset: 0
                                        }
                                }
                        },
                    responsiveLevels: [1240, 1024, 778, 480],
                    visibilityLevels: [1240, 1024, 778, 480],
                    gridwidth: [1240, 1024, 778, 480],
                    gridheight: [868, 768, 960, 600],
                    lazyType: "none",
                    shadow: 0,
                    spinner: "off",
                    stopLoop: "on",
                    stopAfterLoops: 0,
                    stopAtSlide: 1,
                    shuffle: "off",
                    autoHeight: "off",
                    fullScreenAutoWidth: "off",
                    fullScreenAlignForce: "off",
                    fullScreenOffsetContainer: "",
                    fullScreenOffset: "60px",
                    disableProgressBar: "on",
                    hideThumbsOnMobile: "off",
                    hideSliderAtLimit: 0,
                    hideCaptionAtLimit: 0,
                    hideAllCaptionAtLilmit: 0,
                    debugMode: false,
                    fallbacks:
                        {
                            simplifyAll: "off",
                            nextSlideOnWindowFocus: "off",
                            disableFocusListener: false,
                        }
                });
        }
        RsTypewriterAddOn(tpj, revapi1014);
    }); /*ready*/
</script>
</body>
</html>
