<?php
/**
 * Created by PhpStorm.
 * User: Marketing101
 * Date: 12/17/17
 * Time: 12:53 PM
 */?>

<style>

    div.ibox-footer {
        border-bottom-left-radius: 5px;
        border-bottom-right-radius: 5px;
    }
    #title {
        text-align: center;
        color: green;
    }
    #inputOrder {
        text-align: center;
        margin-top: 20px;
    }
    .bottomBtn {
        margin-top: 15px;
    }
    #actionButtons {
        margin-top: 15px;
    }
    h2 {
        text-align: center;
    }
    .dateTime {
        font-size: 15px;
        margin-left: 10px;
    }
    .badge {
        margin-left: 10px;
    }
    .title{
        padding-bottom: 20px;
        padding-left: 12px;
    }
    label {
        font-weight: normal;
        font-size: 14px;
    }
    .selection {
        background-color: transparent;
    }
    .bootstrap-select .dropdown-menu {
        margin: 15px 0 0;
    }
    .productImage {
        /*display: block;*/
        max-width:55px;
        max-height:55px;
        width: auto;
        height: auto;
    }
    a {
        color: #000000;
    }

    .bulky-radio-button {
        cursor: pointer;
        padding: 1.6rem;
        border-radius: 3px;
        border: 1px solid #c4cdd5
    }

    .bulky-radio + .bulky-radio {
        margin-top: 0.8rem
    }

    .bulky-radio {
        display: block;
        width: auto;
    }

    .bulky-radio > input:checked + .bulky-radio-button, .bulky-radio > .autocomplete-field:checked + .bulky-radio-button {
        border: 1px solid #5c6ac4;
        background: #fafbff;
        -webkit-box-shadow: 0 0 0 1px #5c6ac4;
        box-shadow: 0 0 0 1px #5c6ac4
    }
    hr {
        border-color: #dcdcdc;
    }

    .shipperImage {
        /*display: block;*/
        max-width:80px;
        max-height:50px;
        width: auto;
        height: auto;
    }

</style>
</head>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div id="shipTable"></div>
        </div>
    </div>
</div>
<? $this->load->view("navigation/footer");?>
<script>
    $( document ).ready(function() {
        $("#shipTable").html('<img height="50" width="50" style=" position: absolute; top: 50%; left: 50%;" src="https://thewrapapp.net/assets/images/loader.gif">');
        var orderData = {orderNumber: "<?php echo $orderNumber; ?>", shipPack: '<?php echo $shipPack ?>', storeName: '<?php echo $storeName ?>'};
        $.ajax({
            url  	: base_url+"fulfillment/orderActions",
            type 	: 'POST',
            data    : orderData,
            success : function(data){
                $("#shipTable").html(data);
                setCountryCode('<? echo $shippingInfo->province_code;?>');
                <? if(!in_array("shippingOverview", $this->session->tour)) { ?>
                var enjoyhint_instance = new EnjoyHint({
                    onEnd: function () {
                        markTourDone("shippingOverview");
                    },
                    onSkip: function () {
                        markTourDone("shippingOverview");
                    }
                });
                var enjoyhint_script_steps = [
                    {
                        'next .tourItems': "The order lines items appear here.",
                        'skipButton': {className: "mySkip", text: "End tour"}
                    },
                    {
                        'next .tourShipping': 'Each line item has a shipping qty. This is the number of items shipping in this shipment.',
                        'skipButton': {className: "mySkip", text: "End tour"}
                    },
                    {
                        'next .tourPackage': "You can select your box type, enter the dimensions, and set the weight",
                        'skipButton': {className: "mySkip", text: "End tour"}
                    },
                    {
                        'next .tourLocation': "You can set the location the items are shipping from.",
                        'skipButton': {className: "mySkip", text: "End tour"}
                    },
                    {
                        'next .tourShippingOptions': "The service and amount paid for shipping by the customer is here.",
                        'skipButton': {className: "mySkip", text: "End tour"}
                    },
                    {
                        'next .tourShippingDate': "Enter the date you will be shipping the package.",
                        'skipButton': {className: "mySkip", text: "End tour"}
                    },
                    {
                        'next .tourAddress': "This is where the package is shipping. Make sure the address is verified.",
                        'skipButton': {className: "mySkip", text: "End tour"}
                    },
                    {
                        'next .tourSummary': 'Finally you can purchase the shipping label here.',
                        'nextButton': {className: "myNext", text: "Got it!"},
                        'showSkip': false
                    }
                ];
                enjoyhint_instance.set(enjoyhint_script_steps);
                enjoyhint_instance.run();
                <?}?>
            }
        });
    });
</script>
<!-- Mainly scripts -->
<script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"  type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/jeditable/jquery.jeditable.js"></script>
<script href="https://cdn.datatables.net/plug-ins/1.10.16/api/sum().js"></script>
<!-- Data picker -->
<script src="<?php echo base_url() ?>assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/dataTables/datatables.min.js"></script>
<!--<link href="--><?php //echo base_url() ?><!--assets/css/plugins/datapicker/datepicker3.css" rel="stylesheet">-->

<!-- Sweet alert -->
<script src="<?php echo base_url() ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>

<!-- iCheck -->
<script src="<? echo base_url()?>assets/js/plugins/iCheck/icheck.min.js"></script>

<!-- EnjoyHint -->
<script src="<?php echo base_url() ?>assets/js/plugins/enjoyhint/enjoyhint.min.js"></script>

<!-- Ladda -->
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.jquery.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo base_url() ?>assets/js/inspinia.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/pace/pace.min.js"></script>
</body>
</html>
<!-- Address Change Modal -->
<div class="modal inmodal" id="addressModal" name="addressModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Update Shipping Address</h4>
            </div>
            <div class="modal-body">
                <form id="addressForm" name="addressForm" method="post" class="form-horizontal">
                    <input type="hidden" id="orderNumber" name="orderNumber" value="<?php echo $orderNumber?>" />
                    <input type="hidden" id="orderId" name="orderId" value="<?php echo $orderInfo->get("orderId")?>" />
                    <input type="hidden" id="storeName" name="storeName" value="<?php echo $storeName?>" />
                    <input type="hidden" id="countryAddress" name="countryAddress" value="<?php echo $shippingInfo->country?>" />
                    <input type="hidden" id="provinceAddress" name="provinceAddress" value="<?php echo $shippingInfo->province?>" />
                    <div class="form-group">
                        <label autocomplete="false" class="col-lg-2 control-label">First Name</label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" id="first_name" name="first_name" value="<? echo $shippingInfo->first_name?>">
                        </div>
                        <label autocomplete="false" class="col-lg-2 control-label">Last Name</label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" id="last_name" name="last_name" value="<? echo $shippingInfo->last_name?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label autocomplete="false" class="col-lg-2 control-label">Company Name</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" id="company" name="company" value="<? if(isset($shippingInfo->company)) { echo $shippingInfo->company; }?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label autocomplete="false" class="col-lg-2 control-label">Address</label>
                        <div class="col-lg-7">
                            <input type="text" class="form-control" id="address1" name="address1" value="<? echo $shippingInfo->address1?>">
                        </div>
                        <div class="col-lg-3">
                            <input type="text" class="form-control" id="address2" name="address2" value="<? echo $shippingInfo->address2?>">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label style="font-size:14px; font-weight: bolder;" for="inputLast" class="col-md-2 control-label">City</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="city" name="city" value="<? echo $shippingInfo->city?>">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label style="font-size:14px; font-weight: bolder;" class="col-md-2 control-label">State/Province</label>
                        <div class="col-md-3">
                            <select class="form-control m-b minimal ui-select" name="province_code" id="province_code" onchange="setProvinceCode()">
                                <option selected disabled>Select State</option>
                            </select>
                        </div>
                        <label style="font-size:14px; font-weight: bolder;" for="inputLast" class="col-md-1 control-label">Zip</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control" id="zip" name="zip" value="<? echo $shippingInfo->zip?>">
                        </div>
                        <label style="font-size:14px; font-weight: bolder;" class="col-md-1 control-label">Country</label>
                        <div class="col-md-3">
                            <select class="form-control m-b minimal ui-select" name="country_code" id="country_code" onchange="setCountryCode('<? echo $shippingInfo->province_code;?>')">
                                <option selected disabled>Select Country</option>
                                <? foreach ($countryCodes as $code => $name) {?>
                                    <option <? if($shippingInfo->country_code == $code) {?> selected <?}?> value="<? echo $code?>"><? echo $name?></option>
                                <?}?>
                            </select>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label style="font-size:14px; font-weight: bolder;" for="inputLast" class="col-md-2 control-label">Phone Number</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="phone" name="phone" value="<? echo $shippingInfo->phone?>">
                        </div>
                    </div>
<!--                    <div class="form-group">-->
<!--                        <div id="actionButtons" class="col-lg-2 col-lg-offset-5">-->
<!--                            <button class="bottomBtn btn btn-primary" name="validateBtn" id="validateBtn" style="" onclick="validateAddress(); return false;">Validate Address</button>-->
<!--                        </div>-->
<!--                    </div>-->
                    <div class="modal-footer">
                        <div class="col-sm-12 col-sm-12 pull-right">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="button" class="ladda-button btn btn-primary" data-style="zoom-out" id="save" value="Save" name="save" onclick="saveCustomerAddress('shipping')"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

