<?php
/**
 * Created by PhpStorm.
 * User: Marketing101
 * Date: 7/11/18
 * Time: 7:41 AM
 */?>
<!-- Badges -->
    <div class="printOrder">
        <h4><?php if($this->session->shopifyStoresInfo[$storeName]["shopifyDisplayName"] != null) { echo "Store ". $this->session->shopifyStoresInfo[$storeName]["shopifyDisplayName"];} else { echo "Store ". $storeName;}; ?></h4>
    </div>
    <form method="post" id="shippingLabelForm" name="shippingLabelForm" action="viewLabelConfirmation">
        <input id="labelCost" name="labelCost" type="hidden" value=""/>
        <input id="trackingNumber" name="trackingNumber" type="hidden" value=""/>
        <input id="labelLink" name="labelLink" type="hidden" value=""/>
    </form>
<div class="row">
    <form class="form-horizontal" id="shipForm" name="shipForm" method="post">
        <div class="col-lg-9">
            <div class="ibox float-e-margins">
                <div class="ibox-content img-rounded">
                    <h1 id="title">Create Shipping Label</h1>
                    <input type="hidden" id="orderObjectId" name="orderObjectId" value="<? echo $orderObjectId; ?>"/>
                    <input type="hidden" id="orderId" name="orderId" value="<? echo $orderId; ?>"/>
                    <input type="hidden" id="packedStatus" name="packedStatus" value="<? echo $packedStatus; ?>"/>
                    <input type="hidden" id="orderNumber" name="orderNumber" value="<? echo $orderNumber; ?>"/>
                    <input type="hidden" id="location" name="location" value="<? if($shippingInfo->country_code != $companyInfo[0]->get("countryCode")) { echo "international";} else {echo "domestic";} ?>"/>
                    <input type="hidden" id="firstName" name="firstName" value="<? echo $shippingInfo->first_name?>">
                    <input type="hidden" id="lastName" name="lastName" value="<? echo $shippingInfo->last_name?>">
                    <input type="hidden" id="companyName" name="companyName" value="<? if(isset($shippingInfo->company)) { echo $shippingInfo->company; }?>">
                    <input type="hidden" id="address1" name="address1" value="<? echo $shippingInfo->address1?>">
                    <input type="hidden" id="address2" name="address2" value="<? echo $shippingInfo->address2?>">
                    <input type="hidden" id="city" name="city" value="<? echo $shippingInfo->city?>">
                    <input type="hidden" id="state" name="state" value="<? echo $shippingInfo->province_code?>">
                    <input type="hidden" id="zip" name="zip" value="<? echo $shippingInfo->zip?>">
                    <input type="hidden" id="country" name="country" value="<? echo $shippingInfo->country_code?>">
                    <input type="hidden" id="phone" name="phone" value="<? echo $shippingInfo->phone?>">
                    <input type="hidden" id="packaging" name="packaging" value="">
                    <input type="hidden" id="storeName" name="storeName" value="<? echo $storeName?>">
                    <input type="hidden" name="packageCarrier" id="packageCarrier" value="">
                    <input type="hidden" name="carrierId" id="carrierId" value="">
                    <input type="hidden" name="serviceCode" id="serviceCode" value="">
                    <input type="hidden" name="carrierName" id="carrierName" value="">
                    <input type="hidden" id="weightHidden" name="weightHidden" class="form-control" value="<?php if($totalWeight != "") {if($this->session->weightMeasurement == "oz") { $weight = $totalWeight / 28.3495; } else if($this->session->weightMeasurement == "lb") { $weight = $totalWeight * .0022; } else if($this->session->weightMeasurement == "g") { $weight = $totalWeight;} else if($this->session->weightMeasurement == "kg") { $weight = $totalWeight / 1000;} echo round($weight,2) ; } else { echo 9; }?>"/>
                    <div class="row tourItems">
                        <hr>
                        <h3 class="title">Package Contents</h3>
                        <div id="packageInfo" class="col-lg-12">
                            <div class="table-responsive" style="">
                                <table class="table" id="inventory" >
                                    <thead>
                                    <tr>
                                        <th style="text-align: center">Image</th>
                                        <th>Product Info</th>
                                        <th style="text-align: center">Quantity Needed</th>
                                        <th style="text-align: center">Picked</th>
                                        <th style="text-align: center">Shipped</th>
                                        <? if(count(json_decode($orderInfo->get("refunds"))) > 0) {?>
                                            <th style="text-align: center">Refunded</th>
                                        <?}?>
                                        <th style="text-align: center">Shipping</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $x = 0;
                                    foreach ($lineItems as $lineItem) {
                                        $numberShipped = 0;
                                        $numberPicked = 0; ?>
                                        <input type="hidden" name="productId[<? echo $x?>]" id="productId[<? echo $x?>]" value="<?php echo $lineItem->id;?>"/>
                                        <input type="hidden" name="productSku[<? echo $x?>]" id="productSku[<? echo $x?>]" value="<?php echo $lineItem->sku;?>"/>
                                        <input type="hidden" name="qtyNeeded[<? echo $x?>]" id="qtyNeeded[<? echo $x?>]" value="<?php echo $lineItem->quantity;?>"/>
                                        <tr>
                                            <td style="width: 10%; text-align: center"><img class="productImage" src="<?php echo $images[$x]?>" alt="Product Image" height="55" width="55"></td>
                                            <td style="vertical-align: middle"><?php echo $lineItem->title;?><br><?php echo $lineItem->variant_title;?><br>SKU: <?php echo $lineItem->sku;?></td>
                                            <td style="vertical-align: middle; width: 5%;text-align: center"><?php echo $lineItem->quantity;?></td>
                                            <? if(intval($packedStatus) > 0) { ?>
                                                <td style="vertical-align: middle;width: 5%;text-align: center"> <?
                                                    if(isset($productIdsPacked->{$lineItem->sku})) {
                                                        echo count($productIdsPacked->{$lineItem->sku});
                                                        $numberPicked = $numberPicked + count($productIdsPacked->{$lineItem->sku});
                                                    } else {
                                                        echo 0;
                                                    } ?>
                                                </td>
                                            <? } else { ?>
                                                <td style="vertical-align: middle;width: 5%;text-align: center">0</td>
                                            <?php } ?>
                                            <? if(intval($shippedStatus) > 0) { ?>
                                                <td style="vertical-align: middle; width: 5%"> <?
                                                    if(isset($productIdsShipped->{$lineItem->sku})) {
                                                        echo count($productIdsShipped->{$lineItem->sku});
                                                        $numberShipped = $numberShipped + count($productIdsShipped->{$lineItem->sku});
                                                    } else {
                                                        echo 0;
                                                    }
                                                    ?>
                                                </td>
                                            <? } else { ?>
                                                <td style="vertical-align: middle;width: 5%;text-align: center">0</td>
                                            <?php } ?>
                                            <? if(count(json_decode($orderInfo->get("refunds"))) > 0) {?>
                                                <td style="vertical-align: middle; width: 5%; text-align: center">
                                                    <? $qtyRefunded = 0;
                                                    foreach (json_decode($orderInfo->get("refunds")) as $refunds) { //Could be multiple refunds
                                                        foreach ($refunds->refund_line_items as $lineItemsRefund) { //Could have multiple line items refunded
                                                            if(isset($lineItemsRefund->line_item->sku) && $lineItemsRefund->line_item->sku == $lineItem->sku) {
                                                                $qtyRefunded = $qtyRefunded + $lineItemsRefund->quantity;
                                                            } else {
                                                                $qtyRefunded = $qtyRefunded + 0;
                                                            }
                                                        } ?>
                                                    <?} echo $qtyRefunded;?>
                                                </td>
                                            <?}?>
                                            <td style="vertical-align: middle; width: 13%;text-align: center">
                                                <div class="input-group">
                                                    <input style="text-align: center" type="number" class="form-control tourShipping" id="shipping[<? echo $x?>]" value="<?php echo max($numberPicked - $numberShipped,0)?>" min="0" name="shipping[<? echo $x?>]"> <span id="basic-addon1" class="input-group-addon">of <?php echo $lineItem->quantity?></span>
                                                </div>
                                            </td>
                                        </tr>
                                        <input type="hidden" name="qtyShipped[<? echo $x?>]" id="qtyShipped[<? echo $x?>]" value="<?php echo $numberShipped?>"/>
                                        <?php $x++; } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row tourPackage">
                        <hr>
                        <h3 class="title">Package and Weight</h3>
                        <div id="packageInfo" class="col-lg-12">
                            <div class="row form-group">
                                <label class="col-sm-2 col-md-2 col-lg-2 control-label">Package Type</label>
                                <div class="col-sm-9 col-md-9 col-lg-9">
                                    <select class="form-control selectpicker minimal ui-select" name="packageType" id="packageType" onchange="packageChange(true, false)">
                                        <option selected disabled="" value="">Choose Box</option>
                                        <?php foreach ($packages as $package) { ?>
                                            <option <? if($companyInfo[0]->get("defaultBox") == $package->getObjectId()) {?> selected <? }?> value="<?php echo $package->getObjectId()?>"><?php echo $package->get("name")?></option>
                                        <?php } ?>
                                        <option value="other">Other</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-md-2 control-label">Dimensions<? if($this->session->unitSystem == "imperial") { echo " (in)"; } else { echo " (cm)";}?></label>
                                <div class="col-lg-3">
                                    <div class="input-group">
                                        <span class="input-group-addon">Length</span><input type="number" min="0" class="form-control" id="length" name="length" value="" onchange="hideShippingMethod()">
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="input-group">
                                        <span class="input-group-addon">Width</span> <input type="number" min="0" class="form-control" id="width" name="width" value="" onchange="hideShippingMethod()">
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="input-group">
                                        <span class="input-group-addon">Height</span> <input type="number" min="0" class="form-control" id="height" name="height" value="" onchange="hideShippingMethod()">
                                    </div>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-md-2 control-label">Weight</label>
                                <div class="col-md-6 col-sm-6">
                                    <input type="number" onchange="hideShippingMethod()" id="weight" name="weight" class="form-control" value="<?php if($totalWeight != "") {if($this->session->weightMeasurement == "oz") { $weight = $totalWeight / 28.3495; } else if($this->session->weightMeasurement == "lb") { $weight = $totalWeight * .0022; } else if($this->session->weightMeasurement == "g") { $weight = $totalWeight;} else if($this->session->weightMeasurement == "kg") { $weight = $totalWeight / 1000;} echo round($weight,2) ; } else { echo 9; }?>"/>
                                </div>
                                <div class="col-md-3 col-sm-3">
                                    <select id="weightMeasurement" name="weightMeasurement" class="form-control minimal ui-select" onchange="hideShippingMethod()">
                                        <? if($this->session->unitSystem == "imperial") {?>
                                            <option <?php if($this->session->weightMeasurement == "oz") { ?> selected <?php } ?> value="oz">oz</option>
                                            <option <?php if($this->session->weightMeasurement == "lb") { ?> selected <?php } ?> value="lb">lb</option>
                                        <?} else {?>
                                            <option <?php if($this->session->weightMeasurement == "g") { ?> selected <?php } ?> value="g">g</option>
                                            <option <?php if($this->session->weightMeasurement == "kg") { ?> selected <?php } ?> value="kg">kg</option>
                                        <?}?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <? if($shippingInfo->country_code != $companyInfo[0]->get("countryCode")) { ?>
                    <div class="row">
                        <hr>
                        <h3 class="title">Customs Declaration</h3>
                        <div id="packageInfo" class="col-lg-11">
                            <? foreach ($lineItems as $item) { ?>
                                <div class="row form-group">
                                    <label style="font-size:14px; font-weight: bolder;" class="col-md-2 control-label">Description</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" id="customsDescription[]" name="customsDescription[]" value="<? echo $item->title ?>">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label style="font-size:14px; font-weight: bolder;"class="col-md-2 control-label">Value</label>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control" id="customsValue[]" name="customsValue[]" value="<? echo $item->price?>" onchange="hideShippingMethod()">
                                    </div>
                                    <label style="font-size:14px; font-weight: bolder;" class="col-md-1 control-label">Quantity</label>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control" id="customsQty[]" name="customsQty[]" value="<? echo $item->quantity?>" onchange="hideShippingMethod()">
                                    </div>
                                    <label style="font-size:14px; font-weight: bolder;" class="col-md-2 control-label">Country Of Origin</label>
                                    <div class="col-md-3">
    <!--                                    <input type="text" class="form-control" id="coo[]" name="coo[]" value="" onchange="hideShippingMethod()">-->
                                        <select class="form-control m-b minimal ui-select" name="coo[]" id="coo[]" onchange="hideShippingMethod()">
                                            <option selected disabled>Select Country</option>
                                            <? foreach ($coo as $code => $name) {?>
                                                <option value="<? echo $code?>"><? echo $name?></option>
                                            <?}?>
                                        </select>
                                    </div>
    <!--                                <label style="font-size:14px; font-weight: bolder;" class="col-md-2 control-label">HS Tariff Code</label>-->
    <!--                                <div class="col-md-3">-->
    <!--                                    <input type="text" class="form-control" id="tariffCode[]" name="tariffCode[]" value="" onchange="hideShippingMethod()">-->
    <!--                                </div>-->
                                </div>
    <!--                            <div class="row form-group">-->
    <!--                                -->
    <!--                            </div>-->
                            <? } ?>
                        </div>
                    </div>
                    <? } ?>
                    <div class="row tourLocation">
                        <hr>
                        <h3 class="title">Shipping Location</h3>
                        <div class="col-lg-12">
                            <div class="row form-group">
                                <label style="font-size:14px" class="col-sm-2 col-md-2 col-lg-2 control-label">Location</label>
                                <div class="col-sm-9 col-md-9 col-lg-9">
                                    <select class="form-control m-b minimal ui-select" name="warehouse" id="warehouse" onchange="hideShippingMethod()">
                                        <? foreach ($locations as $location) {?>
                                        <option <? if($location->get("default") == "true") {?> selected <?}?> value="<? echo $location->get("warehouseId")?>"><? echo $location->get("name")?></option>
                                        <?}?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <hr>
                        <h3 class="title">Shipping Service</h3>
                        <div class="col-lg-12">
                            <div style="font-size: 14px" class="alert alert-success col-lg-9 col-md-9 col-lg-offset-2 col-md-offset-2 tourShippingOptions" role="alert">
                                <? if(isset($shippingLines[0]->title)) {?>
                                The customer paid $<?php echo $shippingLines[0]->price?> for <?php echo $shippingLines[0]->title?>
                                <?} else {?>
                                    The customer did not select a shipping method.
                                <?}?>
                            </div>
                            <div class="row form-group">
                                <label style="font-size:14px" class="col-sm-2 col-md-2 col-lg-2 control-label">Confirmation</label>
                                <div class="col-sm-9 col-md-9 col-lg-9">
                                    <select class="form-control m-b minimal ui-select" name="confirmation" id="confirmation" onchange="hideShippingMethod()">
                                        <option value="none">None</option>
                                        <option value="signature">Signature</option>
                                        <option value="adult_signature">Adult Signature</option>
                                    </select>
                                </div>
                            </div>
                            <div id="shippingMethodsButton" class="col-lg-2 col-md-2 col-lg-offset-5 col-md-offset-5">
                                <button class="bottomBtn btn btn-primary pull-right ladda-button" data-style="zoom-out" style="" onclick="getRate(); return false;">Get Shipping Methods</button>
                            </div>
                            <div id="shippingPackage">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 pull-right">
            <div class="ibox float-e-margins tourShippingDate">
                <div class="ibox-content img-rounded">
                    <h3>Shipping Date</h3>
                    <div class="input-group date col-lg-10" id="datepicker">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" id="dateShipped" name="dateShipped" value="<? echo date("m/d/Y")?>">
                    </div>
                    <span>The date you are going to send the<br>shipment out.</span>
                </div>
            </div>
        </div>
        <div class="col-lg-3 pull-right">
            <div class="ibox float-e-margins tourAddress">
                <div class="ibox-content img-rounded">
                    <h3>Shipping Address <span class="pull-right"><a href="#" data-toggle="modal" data-target="#addressModal" style="color: #0a9fe2">Edit</a></span></h3>
                    <span class="customerName secondaryText"><?php echo $shippingInfo->first_name." ".$shippingInfo->last_name; ?><br></span>
                    <?php if ($shippingInfo->company != "") { ?> <span class="secondaryText"> <?php echo $shippingInfo->company; ?> </span><br> <?php }?>
                    <span class="secondaryText"><?php echo $shippingInfo->address1;?><br></span>
                    <?php if ($shippingInfo->address2 != "") { ?> <span class="secondaryText"> <?php echo $shippingInfo->address2; ?> </span><br> <?php }?>
                    <span class="secondaryText"><?php echo $shippingInfo->city." ".$shippingInfo->province_code." ".$shippingInfo->zip;?><br></span>
                    <span class="secondaryText"><?php echo $shippingInfo->country;?></span><br>
                    <span class="secondaryText"><?php echo $shippingInfo->phone;?></span>
                    <hr>
                    <h3>Address Verification</h3>
                    <span id="addressVerify">Verifing</span>
                </div>
            </div>
        </div>
        <div class="col-lg-3 pull-right">
            <div class="ibox float-e-margins tourSummary">
                <div class="ibox-content img-rounded">
                    <h3 style="padding-bottom: 10px">Summary</h3>
    <!--                <span class="customerName secondaryText">Label is ready<br></span>-->
                    <span  class="customerName secondaryText">Customer Paid <span id="customerPaid" name="customerPaid" class="pull-right">$<? if(isset($shippingLines[0]->title)) {echo $shippingLines[0]->price;} else {echo "0.00";}?></span><br></span>
                    <span  class="customerName secondaryText">Your Cost <span id="rate" name="rate" class="pull-right"></span> <br></span>
                    <hr>
                    <button class="btn btn-primary block full-width ladda-button" name="downloadBtn" data-style="zoom-out" disabled="" id="downloadBtn" style="" onclick="getLabel(); return false;">Select shipping method</button>
                    <p class="secondaryText" style="padding-top: 10px; text-align: center">Buying shipping labels will mark<br>items as fulfilled</p>
                </div>
            </div>
        </div>
    </form>
</div>
<script>
    $( document ).ready(function() {
        <? if( $shippingInfo->country_code == $companyInfo[0]->get("countryCode")) { ?>
            //Domestic Order
            packageChange(false, false);
        <?} else {?>
            //International Order
            packageChange(true, false);
        <?}?>
        $('#datepicker').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,

        });
        <? if($shippedStatus == 2) {
            if($note != "" && $note != " " && $note != null) { ?>
            swal({
                    title: "Order Already Shipped",
                    text: "<? echo $note;?>",
                    showCancelButton: true,
                    confirmButtonColor: "#0d8ddb",
                    confirmButtonText: "Ok",
                    cancelButtonText: "Go Back",
                    closeOnConfirm: true,
                    closeOnCancel: false
                },
                function (isConfirm) {
                    if(isConfirm) {

                    } else {
                        window.location = base_url+"fulfillment"
                    }
                });
            <? } ?>
        <? } ?>
        <? if($this->session->labelsRemaining < 2) {?>
            swal({
                    title: "Your labels remaining are low",
                    text: "Your remaining label count is "+<? echo $this->session->labelsRemaining?>,
                    showCancelButton: true,
                    confirmButtonColor: "#0d8ddb",
                    confirmButtonText: "Purchase Labels",
                    cancelButtonText: "Ok",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if(isConfirm) {
                        window.location = base_url+"settings/buyLables"
                    }
                });
        <?}?>
        validateAddress('#shipForm');
    });
</script>