<?php
/**
 * Created by PhpStorm.
 * User: Marketing101
 * Date: 9/25/18
 * Time: 12:43 PM
 */?>

<?php
/**
 * Created by PhpStorm.
 * User: Marketing101
 * Date: 12/17/17
 * Time: 12:53 PM
 */?>

<style>
    div.ibox {
        border: solid 1px #d9d9d9;
        /*box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);*/
        /*margin-top: 20px;*/
        border-radius: 5px;
        /*box-shadow: #3D3D3D;*/
    }
    div.ibox-footer {
        border-bottom-left-radius: 5px;
        border-bottom-right-radius: 5px;
    }
    #title {
        text-align: center;
        color: green;
    }
    #inputOrder {
        text-align: center;
        margin-top: 20px;
    }
    .bottomBtn {
        margin-top: 15px;
    }
    #actionButtons {
        margin-top: 15px;
    }
    h2 {
        text-align: center;
    }
    .dateTime {
        font-size: 15px;
        margin-left: 10px;
    }
    .badge {
        margin-left: 10px;
    }
    .title{
        padding-bottom: 20px;
        padding-left: 12px;
    }
    label {
        font-weight: normal;
        font-size: 14px;
    }
    .selection {
        background-color: transparent;
    }
    .bootstrap-select .dropdown-menu {
        margin: 15px 0 0;
    }
    a {
        color: #000000;
    }
    hr {
        border-color: #dcdcdc;
    }


</style>
</head>
<div class="wrapper wrapper-content">
    <div class="col-lg-8 col-md-8 col-lg-offset-2 col-mg-offset-2">
        <div class="row">
            <h1>Print shipping label</h1>
        </div>
        <hr>
        <div class="row">
            <div class=" col-lg-6 col-md-6 col-lg-offset-3 col-md-offset-3">
                <div class="ibox-content img-rounded">
                    <h3>Label information</h3>
                    <br>
                    <span>Tracking number<span class="pull-right"><? echo $trackingNumber?></span></span><br>
                    <span>Purchased 1 shipping label<span class="pull-right">$<? echo number_format($cost,2)?></span></span><br>
                    <span>Total paid<span class="pull-right">$<? echo number_format($cost,2)?></span> </span><br><br>
                    <button style="margin-top: 10px" class="btn btn-primary" onclick="download('<? echo $link?>')">Print shipping label</button>
                    <a style="margin-top: 10px" class="btn btn-default pull-right" href="<? echo base_url()?>fulfillment">Done</a>
                </div>
            </div>
        </div>
    </div>
</div>
<? $this->load->view("navigation/footer");?>
<!-- Mainly scripts -->
<script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"  type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/jeditable/jquery.jeditable.js"></script>
<script href="https://cdn.datatables.net/plug-ins/1.10.16/api/sum().js"></script>
<!-- Data picker -->
<script src="<?php echo base_url() ?>assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/dataTables/datatables.min.js"></script>
<!--<link href="--><?php //echo base_url() ?><!--assets/css/plugins/datapicker/datepicker3.css" rel="stylesheet">-->

<!-- Sweet alert -->
<script src="<?php echo base_url() ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>

<!-- iCheck -->
<script src="<? echo base_url()?>assets/js/plugins/iCheck/icheck.min.js"></script>

<!-- Ladda -->
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.jquery.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo base_url() ?>assets/js/inspinia.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/pace/pace.min.js"></script>
</body>
</html>