<?php
/**
 * Created by PhpStorm.
 * User: Marketing101
 * Date: 12/17/17
 * Time: 12:46 PM
 */?>
<div class="table-responsive">
    <table class="table dataTables-example" >
        <thead>
        <tr>
            <th style="width: 10%">Order</th>
            <th style="width: 15%">Store</th>
<!--            <th style="width: 15%">Order Id</th>-->
            <th style="width: 10%">Date</th>
            <th style="width: 10%">Customer</th>
            <th style="width: 10%">Payment</th>
            <th style="width: 10%">Picked</th>
            <th style="width: 10%">Shipped</th>
<!--            <th style="width: 15%">Note</th>-->
            <th style="width: 15%">Timestamp</th>
<!--            <th style="width: 15%">Shipping Type</th>-->
<!--            <th style="width: 15%">Action</th>-->
        </tr>
        </thead>
        <tbody>
        <?php $x = 0;
        foreach ( $results as $result ) { ?>
            <tr class='clickable-row' id="row<? echo $x?>" data-href='<?php echo base_url()?>fulfillment/orderOverview/<?php echo $result->get("storeName")?>/<? echo $result->get("orderNumber")?>'>
                <td style="vertical-align: middle"><?php echo $result->get('orderNumber');?></td>
                <td style="vertical-align: middle"><?php if($this->session->shopifyStoresInfo[$result->get("storeName")]["shopifyDisplayName"] != null) { echo $this->session->shopifyStoresInfo[$result->get("storeName")]["shopifyDisplayName"];} else { echo $result->get("storeName");}; ?></td>
<!--                <td>--><?php //echo $result->get('orderId');?><!--</td>-->
                <td style="vertical-align: middle"><?php
                    if(strtotime($result->get('dateOrderedTime')) > strtotime('1 day ago')) {
                        $dateNow = new DateTime($result->get('dateOrderedTime'));
                        echo "Today".$dateNow->format(' h:ia');
                    } else if(strtotime($result->get('dateOrderedTime')) > strtotime('2 days ago')) {
                        $dateNow = new DateTime($result->get('dateOrderedTime'));
                        echo "Yesterday". $dateNow->format(' h:ia');
                    } else if(strtotime($result->get('dateOrderedTime')) > strtotime('7 days ago')) {
                        $dateNow = new DateTime($result->get('dateOrderedTime'));
                        echo $dateNow->format('l h:ia');
                    } else {
                        $dateNow = new DateTime($result->get("dateOrderedTime"));
                        echo $dateNow->format('M d h:ia');
                    } ?>
                </td>
                <td style="vertical-align: middle"><? $customerName = json_decode($result->get("customer")); if(count($customerName) > 0) { echo $customerName->first_name;?> <?php echo $customerName->last_name; } else { echo "No Customer";}?></td>
                <td style="vertical-align: middle"><?php
                    if($result->get("financialStatus") == "paid") {?>
                        <span class="badge badge-primary"> Paid </span>
                    <?php } else if($result->get("financialStatus") == "pending") {?>
                        <span class="badge badge-warning"> Pending </span>
                    <?php } else if($result->get("financialStatus") == "authorized") { ?>
                        <span class="badge badge-warning"> Authorized </span>
                    <?php } else if ($result->get("financialStatus") == "partially_paid") { ?>
                        <span class="badge badge-warning"> Partially Paid </span>
                    <?php } else if($result->get("financialStatus") == "partially_refunded") { ?>
                        <span class="badge badge-warning"> Partially Refunded </span>
                    <?php } else if($result->get("financialStatus") == "refunded") { ?>
                        <span class="badge badge-danger"> Refunded </span>
                    <?php } else if($result->get("financialStatus") == "voided") { ?>
                        <span class="badge badge-danger"> Voided </span>
                    <?php } ?>
                </td>
                <td style="vertical-align: middle"><?php if($result->get("packed") == "2") {?> <span class="badge badge-primary"> Yes </span> <?php } else if($result->get("packed") == "1") {?> <span class="badge badge-warning"> Partial </span> <?php } else { ?> <span class="badge badge-danger"> No </span> <?php } ?></td>
                <td style="vertical-align: middle"><?php if($result->get("shipped") == "2") {?><span class="badge badge-primary"> Yes </span> <?php } else if($result->get("shipped") == "1") {?> <span class="badge badge-warning"> Partial </span> <?php } else { ?> <span class="badge badge-danger"> No </span> <?php } ?></span></td>
<!--                <td style="vertical-align: middle">--><?php //echo $result->get('note');?><!--</td>-->
                <td style="vertical-align: middle"><?php
                    if($result->get('dateOrderedTime') != "") {
                        $dateNow = new DateTime($result->get('dateOrderedTime'), new DateTimeZone("America/Chicago"));
                        echo $dateNow->format('U');
                    } else {
                        $dateNow = new DateTime($result->get('dateOrdered'), new DateTimeZone("America/Chicago"));
                        echo $dateNow->format('U');
                    }?></td>
<!--                <td> --><?php
//                    if(count($result->get("shippingLines")) > 0) {
//                        if ($result->get("shippingLines")[0] != "no") {
//                            if ($result->get("shippingLines")[0]["title"] == "Priority Mail Express") {
//                                echo 1;
//                            } else {
//                                echo 2;
//                            }
//                        } else {
//                            echo 3;
//                        }
//                    } else {
//                        echo 3;
//                    }?><!--</td>-->
            </tr>
        <?php $x++; } ?>
        </tbody>
    </table>
</div>

<script>
    jQuery(document).ready(function($) {
        $(".clickable-row").click(function() {
            window.location = $(this).data("href");
            // console.log($(this).data("href"));
        });
    });
</script>

