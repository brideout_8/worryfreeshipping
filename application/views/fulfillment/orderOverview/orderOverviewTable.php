<?php
/**
 * Created by PhpStorm.
 * User: Marketing101
 * Date: 7/19/18
 * Time: 3:35 PM
 */?>
<!-- Badges -->
    <div class="printOrder">
<!--            <a href="--><?// echo base_url()?><!--fulfillment/fulfillmentPrint?orderNumber=--><?// echo $orderNumber?><!--&storeName=--><?//echo $storeName?><!--" target="_blank">​​​​​​​​​​​​​​​​​print pdf</a>-->
        <div class="dropdown">
            <a href="<? echo base_url()?>fulfillment/invoicePrint?orderNumber=<? echo $orderNumber?>&storeName=<?echo $storeName?>" target="_blank" ><i class="fa fa-print"></i> Print order</a>
            <button data-toggle="dropdown" class="moreActions btn-sm">More actions <span class="caret"></span></button>
            <ul class="dropdown-menu">
                <li><a href="#"  data-toggle="modal" data-target="#holdModal">Place on hold</a></li>
                <? if(in_array("archiveOrders", $this->session->userdata("permissions")) || in_array("fullAccess", $this->session->userdata("permissions"))) {?>
                    <li><a onclick="archiveOrder('<? echo $storeName?>', '<? echo $orderInfo->getObjectId() ?>','<? echo $orderInfo->get("orderId")?>')" >Archive</a></li>
                <?}?>
            </ul>
        </div>
    </div>
<div class="row">
    <div class="col-lg-8">
        <div class="ibox float-e-margins img-rounded">
            <div class="ibox-heading">
                <p class="storeName"><?php if($this->session->shopifyStoresInfo[$storeName]["shopifyDisplayName"] != null) { echo $this->session->shopifyStoresInfo[$storeName]["shopifyDisplayName"];} else { echo $storeName;}; ?></p>
            </div>
            <div class="ibox-content">
                <h3>Order Items</h3>
                <div id="inputOrder">
                    <form class="form-horizontal" id="message_form" name="message_form" type="post">
                        <div class="table-responsive" style="margin-top: 20px">
                            <table class="table" id="inventory" >
                                <thead>
                                <tr style="text-align: center">
                                    <th style="text-align: center">Image</th>
                                    <th>Product Info</th>
                                    <!--                                            <th>Size/Color</th>-->
                                    <!--                                        <th>Sku</th>-->
                                    <!--                                        <th>Quantity On Hand</th>-->
                                    <th style="text-align: center">Quantity Needed</th>
                                    <th style="text-align: center">Picked</th>
                                    <th style="text-align: center">Shipped</th>
                                    <? if(count(json_decode($orderInfo->get("refunds"))) > 0) {?>
                                    <th style="text-align: center">Refunded</th>
                                    <?}?>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $x = 0;
                                $mystery = false;
                                foreach ($lineItems as $lineItem) { ?>
                                    <input type="hidden" name="productId[<? echo $x?>]" id="productId[<? echo $x?>]" value="<?php echo $lineItem->id;?>"/>
                                    <input type="hidden" name="iDsPacked[<? echo $x?>]" id="iDsPacked[<? echo $x?>]" value=""/>
                                    <tr>
                                        <td style="width: 10%"><img class="productImage" src="<?php echo $images[$x]?>" alt="Product Image" height="55" width="55"></td>
                                        <td style="vertical-align: middle; text-align: left"><?php echo $lineItem->title;?><br><?php echo $lineItem->variant_title;?><br>SKU: <?php echo $lineItem->sku;?></td>
                                        <td style="vertical-align: middle; width: 5%"><?php echo $lineItem->quantity;?></td>
                                        <? if(intval($packedStatus) > 0) { ?>
                                            <td style="vertical-align: middle; width: 5%"> <?
                                                if(isset($productIdsPacked->{$lineItem->sku})) {
                                                    echo count($productIdsPacked->{$lineItem->sku});
                                                } else {
                                                    echo 0;
                                                } ?>
                                            </td>
                                        <? } else { ?>
                                            <td style="vertical-align: middle; width: 5%;">0</td>
                                        <?php } ?>
                                        <? if(intval($shippedStatus) > 0) { ?>
                                            <td style="vertical-align: middle; width: 5%"> <?
                                                if(isset($productIdsShipped->{$lineItem->sku})) {
                                                    echo count($productIdsShipped->{$lineItem->sku});
                                                } else {
                                                    echo 0;
                                                }
                                                ?>
                                            </td>
                                        <? } else { ?>
                                            <td style="vertical-align: middle; width: 5%;">0</td>
                                        <?php } ?>
                                        <? if(count(json_decode($orderInfo->get("refunds"))) > 0) {?>
                                            <td style="vertical-align: middle; width: 5%">
                                                <? $qtyRefunded = 0;
                                                foreach (json_decode($orderInfo->get("refunds")) as $refunds) { //Could be multiple refunds
                                                    foreach ($refunds->refund_line_items as $lineItemsRefund) { //Could have multiple line items refunded
                                                        if(isset($lineItemsRefund->line_item->sku) && $lineItemsRefund->line_item->sku == $lineItem->sku) {
                                                            $qtyRefunded = $qtyRefunded + $lineItemsRefund->quantity;
                                                        } else {
                                                            $qtyRefunded = $qtyRefunded + 0;
                                                        }
                                                    } ?>
                                                <?} echo $qtyRefunded;?>
                                            </td>
                                        <?}?>
                                    </tr>
                                    <? $x++; } ?>
                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>
            </div>
            <div class="ibox-footer">
                <div class="row">
                    <div class="clearButton">
                        <? if(count($shippingAddress) == 0) {?>
                            <button class="btn btn-primary pull-right shipButton" <? if(!in_array("shipOrders", $this->session->permissions) && !in_array("fullAccess", $this->session->permissions)) {?> disabled="" <?}?> <? if($orderInfo->get("hold") == 2) { ?> style="background-color: red; border-color: red" <? } ?> onclick="shippingAddressError()">Ship Order <?php if($orderInfo->get("hold") == 2) { ?>On Hold <?php } ?></button>
                        <?} else {?>
                            <button class="btn btn-primary pull-right shipButton" <? if(!in_array("shipOrders", $this->session->permissions) && !in_array("fullAccess", $this->session->permissions)) {?> disabled="" <?}?> <? if($orderInfo->get("hold") == 2) { ?> style="background-color: red; border-color: red" <? } ?> onclick="location.href='<? echo base_url()?>fulfillment/shipOrder?orderNumber=<? echo $orderNumber?>&storeName=<? echo $storeName;?>';">Ship Order <?php if($orderInfo->get("hold") == 2) { ?>On Hold <?php } ?></button>
                        <?}?>
                        <button class="btn btn-primary pull-right packButton" <? if(!in_array("pickOrders", $this->session->permissions) && !in_array("fullAccess", $this->session->permissions)) {?> disabled="" <?}?> <? if($orderInfo->get("hold") == 2) { ?> style="background-color: red; border-color: red" <? } ?> onclick="location.href='<? echo base_url()?>fulfillment/packOrder?orderNumber=<? echo $orderNumber?>&storeName=<? echo $storeName;?>';">Pick Order <?php if($orderInfo->get("hold") == 2) { ?>On Hold <?php } ?></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="ibox float-e-margins">
            <div class="ibox-content img-rounded">
                <h3>Internal Notes <span class="pull-right"><a href="#" data-toggle="modal" data-target="#notesModal" style="color: #0a9fe2">Edit</a></span></h3>
                <span class="secondaryText"><?php if($orderInfo->get("internalNote") == "") { echo "No internal notes";} else { echo $orderInfo->get("internalNote");}?></span>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
    <!-- Tracking -->
    <? if ($orderInfo->get("shipped") == "2" || $orderInfo->get("shipped") == "1") { ?>
        <div class="ibox float-e-margins">
            <div class="ibox-content img-rounded">
                <h3>Tracking Information</h3>
                <? foreach ($orderInfo->get("labelData") as $data) {?>
                    <button class="buttonLink"  <? if(isset($data["voided"])) {?> disabled="" <?}?> onclick="trackPackage('<? echo $data["trackingNumber"];?>')"><? if(isset($data["voided"])) {?> <s> <?}?><? echo $data["trackingNumber"]?><? if(isset($data["voided"])) {?> </s> <?}?></button>
                    <button class="btn btn-primary btn-xs labelDownload"  <? if(isset($data["voided"])) {?> disabled="" <?}?> onclick="download('<? echo $data["labelLink"]?>?download=0')"><? echo "Download Label"?></button>
                    <button id="voidButton" <? if(!in_array("voidLabels", $this->session->permissions) && !in_array("fullAccess", $this->session->permissions)) {?> disabled="" <?}?> <? if(isset($data["voided"])) {?> disabled="" <?}?> class="btn btn-danger btn-xs pull-right ladda-button" data-style="zoom-out" onclick="voidLabel('<? echo $data["id"];?>','<? echo $orderInfo->getObjectId()?>')"><? if(isset($data["voided"])) {?> Voided <?} else {?> Void Label <?}?></button>
                    <br>
                    <? }?>
            </div>
        </div>
    <? } ?>
    <!--  End of tracking -->
    <? if($orderInfo->get("hold") == 2) {?>
        <div class="ibox rounded float-e-margins">
            <div class="ibox-content img-rounded">
                <h3>Hold <span class="pull-right"><a href="#" data-toggle="modal" data-target="#holdModal" style="color: #0a9fe2">Edit</a></span></h3>
                <span class="secondaryText"><?php echo $holdReason->note;?></span><br><br><hr><span class="thirdText"><? $dateNow = new DateTime($holdReason->time, new DateTimeZone("America/Chicago")); echo $holdReason->user?> - <? echo $dateNow->format('M d Y h:ia')?></span>
            </div>
        </div>
    <? } ?>
        <!-- Customer notes -->
        <div class="ibox float-e-margins">
            <div class="ibox-content img-rounded">
                <h3>Customer Notes</h3>
                <span class="secondaryText"><?php if($note == "" ) { echo "No notes from customer";} else { echo json_decode($note);}?></span>
            </div>
        </div>
        <!-- End of customer notes -->
        <!-- Customer -->
        <div class="ibox float-e-margins tourCustomer">
            <div class="ibox-content img-rounded">
                <h3>Customer</h3>
                <? $customer = json_decode($orderInfo->get("customer")); if(count($customer) > 0) {?>
                    <span class="customerName secondaryText"><? echo $customer->first_name." ".$customer->last_name; ?><br></span>
                    <span class="secondaryText"><? if($customer->orders_count == "1") { echo $customer->orders_count." Order"; } else { echo $customer->orders_count." Orders";} ?><br></span>
                <?} else {?>
                    <span class="customerName secondaryText">No Customer</span>
                <?}?>
                <hr>
                <h3>Shipping Address<span class="pull-right"><a href="#" data-toggle="modal" data-target="#orderAddressModal" style="color: #0a9fe2">Edit</a></span></h3>
                <? if(count($shippingAddress) > 0) {?>
                    <span class="customerName secondaryText"><?php echo $shippingAddress->name; ?><br></span>
                    <?php if ($shippingAddress->company != "") { ?> <span class="secondaryText"> <?php echo $shippingAddress->company; ?> </span><br> <?php }?>
                    <span class="secondaryText"><?php echo $shippingAddress->address1;?><br></span>
                    <? if ($shippingAddress->address2 != "") { ?> <span class="secondaryText"> <? echo $shippingAddress->address2; ?> </span><br> <? }?>
                    <span class="secondaryText"><? echo $shippingAddress->city." ".$shippingAddress->province_code." ".$shippingAddress->zip;?><br></span>
                    <span class="secondaryText"><? echo $shippingAddress->country;?></span><br>
                    <span class="secondaryText"><? echo $shippingAddress->phone;?></span>
                <?} else {?>
                    <span class="customerName secondaryText">No shipping address</span>
                <?}?>
            </div>
        </div>
        <!-- End of customer -->
        <!-- Returns -->
        <div class="ibox float-e-margins tourReturns">
            <div class="ibox-content img-rounded">
                <h3>Return labels<span class="pull-right"><a <? if($this->session->subscriptionLevel == "1") {?> href="<? echo base_url()?>register/changePlan"<?} else {?> href="<? echo base_url()?>/returns/newReturn?orderId=<? echo $orderInfo->getObjectId()?>" <?}?> style="color: #0a9fe2">Issue return label</a></span> </h3>
                <? if(count($returns) > 0) {?>
                    <?php foreach ($returns as $return) { ?>
                        <button class="buttonLink" <? if($this->session->subscriptionLevel == "1") {?> disabled="" <?}?> onclick="trackPackage('<?php echo $return->get("trackingLink");?>')"><?php echo $return->get("trackingNumbers")?></button>
                        <button class="btn btn-primary btn-xs labelDownload" <? if($this->session->subscriptionLevel == "1") {?> disabled="" <?}?> onclick="download('<?php echo $return->get("labelLink");?>?download=0')" ><?php echo "Download Label"?></button>
                        <br>
                    <?php }?>
                <?} else {?>
                    <span class="secondaryText">No return labels issued</span>
                <?}?>
            </div>
        </div>
        <!-- End of returns -->
    </div>
</div>
<script>
    $( document ).ready(function() {
        <?php if($orderInfo->get("hold") == 2) {?>
            swal({
                title: "Order On Hold",
                text: '<? echo $holdReason->note;?>'
            });
        <?php } ?>
    });
</script>
