<?php
/**
 * Created by PhpStorm.
 * User: rapidware
 * Date: 6/30/18
 * Time: 4:49 PM
 */?>

<style>
    div.ibox {
        border: solid 1px #d9d9d9;
        /*box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);*/
        /*margin-top: 20px;*/
        border-radius: 5px;
        /*box-shadow: #3D3D3D;*/
    }
    div.ibox-footer {
        border-bottom-left-radius: 5px;
        border-bottom-right-radius: 5px;
    }
    div.ibox-content {
        border: 0px;
    }
    #title {
        text-align: center;
        color: green;
    }
    .dateTime {
        font-size: 15px;
        margin-left: 10px;
    }
    p {
        text-align: center;
    }
    #inputOrder {
        text-align: center;
        margin-top: 20px;
    }
    .badge {
        margin-left: 10px;
    }
    .printOrder {
        font-size: 15px;
    }
    a {
        color: #000000;
    }
    .secondaryText {
        /*font-size: 14px;*/
        /*font-weight: normal;*/
    }
    .thirdText {
        /*font-size: 12px;*/
        /*font-weight: normal;*/
    }
    .customerName {
        padding-top: 20px;
    }
    .pull {
        margin-left: 75%;
        margin-right: 10px;
    }
    div.ibox-heading {
        background-color: white;
    }
    .storeName {
        font-size: 22px;
    }
    p {
        margin:-2px 0 -2px 0;
    }
    hr {
        border-color: #dcdcdc;
    }
    .buttonLink {
        background:none!important;
        color: steelblue;
        border:none;
        padding:0!important;
        font: inherit;
        /*border is optional*/
        cursor: pointer;
    }
    .labelDownload {
        margin-left: 10px;
    }
    .packButton {
        margin-right: 8px;
    }
    .shipButton {
        margin-right: 8px;
    }
    .moreActions {
        background-color: transparent;
        border-color: transparent;
        font-size: 15px;
        font-weight: inherit;
        padding-top: 1px;
    }
    .moreActions:active,
    .moreActions:hover,
    .moreActions:focus{
        background-color: transparent;
        border-color: transparent;
        box-shadow: transparent;
        outline: transparent;
    }
    .productImage {
        /*display: block;*/
        max-width:55px;
        max-height:55px;
        width: auto;
        height: auto;
    }
</style>
</head>

<div class="wrapper wrapper-content animated">
    <div class="row">
        <div class="col-lg-12">
            <div id="orderOverviewTable"></div>
        </div>
    </div>
</div>
<? $this->load->view("navigation/footer");?>
<script>
    $( document ).ready(function() {
        $("#orderOverviewTable").html('<img height="50" width="50" style=" position: absolute; top: 50%; left: 50%;" src="https://thewrapapp.net/assets/images/loader.gif">');
        var orderData = {orderNumber: "<?php echo $orderNumber; ?>", storeName: '<?php echo $storeName ?>'};
        $.ajax({
            url  	: base_url+"fulfillment/orderOverviewTable",
            type 	: 'POST',
            data    : orderData,
            success : function(data){
                $("#orderOverviewTable").html(data);
                setCountryCode('<? if(isset($shippingInfo->province_code)) { echo $shippingInfo->province_code;}?>');
                <? if(!in_array("orderOverview", $this->session->tour)) { ?>
                var enjoyhint_instance = new EnjoyHint({
                    onEnd: function () {
                        markTourDone("orderOverview");
                    },
                    onSkip: function () {
                        markTourDone("orderOverview");
                    }
                });
                var enjoyhint_script_steps = [
                    {
                        'next .tourBadges': "Here is a quick look into the order information and the current statues.",
                        'skipButton': {className: "mySkip", text: "End tour"}
                    },
                    {
                        'next #inputOrder': 'The order items are listed here. You can also see if you have picked or shipped any of the items.',
                        'skipButton': {className: "mySkip", text: "End tour"}
                    },
                    {
                        'next .tourCustomer': "On the side will have your order's customer's information.",
                        'skipButton': {className: "mySkip", text: "End tour"}
                    },
                    {
                        'next .tourReturns': "Below the customer box you have your current returns.",
                        'skipButton': {className: "mySkip", text: "End tour"}
                    },
                    {
                        'next .ibox-footer': 'When you are ready you can pick or ship your order.',
                        'nextButton': {className: "myNext", text: "Got it!"},
                        'showSkip': false
                    }
                ];
                enjoyhint_instance.set(enjoyhint_script_steps);
                enjoyhint_instance.run();
                <?}?>
            }
        });
    });
</script>
<!-- Mainly scripts -->
<script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"  type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/jeditable/jquery.jeditable.js"></script>
<script href="https://cdn.datatables.net/plug-ins/1.10.16/api/sum().js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/dataTables/datatables.min.js"></script>

<!-- Sweet alert -->
<script src="<?php echo base_url() ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>

<!-- EnjoyHint -->
<script src="<?php echo base_url() ?>assets/js/plugins/enjoyhint/enjoyhint.min.js"></script>


<!-- Custom and plugin javascript -->
<script src="<?php echo base_url() ?>assets/js/inspinia.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/pace/pace.min.js"></script>
<!-- Ladda -->
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.jquery.min.js"></script>

<!-- Hold Modal -->
<div class="modal inmodal" id="holdModal" name="holdModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Hold</h4>
            </div>
            <div class="modal-body">
                <form id="holdForm" name="holdForm" method="post" class="form-horizontal">
                    <input type="hidden" id="orderNumber" name="orderNumber" value="<?php echo $orderNumber?>" />
                    <input type="hidden" id="storeName" name="storeName" value="<?php echo $storeName?>" />
                    <div class="form-group">
                        <label class="col-sm-2 col-md-2 control-label">Hold?</label>
                        <div class="col-sm-10 col-md-10">
                            <label class="checkbox-inline i-checks"> <input type="radio" value="2" id="yes" name="hold" checked=""> Yes</label>
                            <label class="checkbox-inline i-checks"> <input type="radio" value="" id="no" name="hold"> No</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 col-md-2 control-label">Reason</label>
                        <div class="col-sm-10 col-md-10">
                            <textarea class="form-control input_me" name="reason" id="reason" placeholder="Reason for hold"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="col-sm-12 col-sm-12 pull-right">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="button" class="ladda-button btn btn-primary" data-style="zoom-out" id="save" value="Save" name="save" onclick="saveHold()"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Notes Modal -->
<div class="modal inmodal" id="notesModal" name="notesModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Internal Note</h4>
            </div>
            <div class="modal-body">
                <form id="notesForm" name="notesForm" method="post" class="form-horizontal">
                    <input type="hidden" id="orderNumber" name="orderNumber" value="<?php echo $orderNumber?>" />
                    <input type="hidden" id="storeName" name="storeName" value="<?php echo $storeName?>" />
                    <div class="form-group">
                        <label class="col-sm-2 col-md-2 control-label">Note</label>
                        <div class="col-sm-10 col-md-10">
                            <textarea class="form-control input_me" name="note" id="note" placeholder="Your note"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="col-sm-12 col-sm-12 pull-right">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="button" class="ladda-button btn btn-primary" data-style="zoom-out" id="save" value="Save" name="save" onclick="saveInternalNote()"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Address Change Modal -->
<div class="modal inmodal" id="orderAddressModal" name="orderAddressModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Update Shipping Address</h4>
            </div>
            <div class="modal-body">
                <form id="orderAddressForm" name="orderAddressForm" method="post" class="form-horizontal">
                    <input type="hidden" id="orderNumber" name="orderNumber" value="<?php echo $orderNumber?>" />
                    <input type="hidden" id="orderId" name="orderId" value="<?php echo $orderInfo->get("orderId")?>" />
                    <input type="hidden" id="storeName" name="storeName" value="<?php echo $storeName?>" />
                    <input type="hidden" id="countryAddress" name="countryAddress" value="<? if(isset($shippingInfo->country)) { echo $shippingInfo->country;}?>" />
                    <input type="hidden" id="provinceAddress" name="provinceAddress" value="<? if(isset($shippingInfo->province)) { echo $shippingInfo->province;}?>" />
                    <div class="form-group">
                        <label autocomplete="false" class="col-lg-2 control-label">First Name</label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" id="first_name" name="first_name" value="<? if(isset($shippingInfo->first_name)) { echo $shippingInfo->first_name;}?>">
                        </div>
                        <label autocomplete="false" class="col-lg-2 control-label">Last Name</label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" id="last_name" name="last_name" value="<? if(isset($shippingInfo->last_name)) { echo $shippingInfo->last_name;}?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label autocomplete="false" class="col-lg-2 control-label">Company Name</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" id="company" name="company" value="<? if(isset($shippingInfo->company)) { echo $shippingInfo->company; }?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label autocomplete="false" class="col-lg-2 control-label">Address</label>
                        <div class="col-lg-7">
                            <input type="text" class="form-control" id="address1" name="address1" value="<? if(isset($shippingInfo->address1)) { echo $shippingInfo->address1;}?>">
                        </div>
                        <div class="col-lg-3">
                            <input type="text" class="form-control" id="address2" name="address2" value="<? if(isset($shippingInfo->address2)) { echo $shippingInfo->address2;}?>">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label style="font-size:14px; font-weight: bolder;" for="inputLast" class="col-md-2 control-label">City</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="city" name="city" value="<? if(isset($shippingInfo->city)) { echo $shippingInfo->city;}?>">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label style="font-size:14px; font-weight: bolder;" class="col-md-2 control-label">State/Province</label>
                        <div class="col-md-3">
                            <select class="form-control m-b minimal ui-select" name="province_code" id="province_code" onchange="setProvinceCode()">
                            </select>
                        </div>
                        <label style="font-size:14px; font-weight: bolder;" for="inputLast" class="col-md-1 control-label">Zip</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control" id="zip" name="zip" value="<? if(isset($shippingInfo->first_name)) { echo $shippingInfo->zip;}?>">
                        </div>
                        <label style="font-size:14px; font-weight: bolder;" class="col-md-1 control-label">Country</label>
                        <div class="col-md-3">
                            <select class="form-control m-b minimal ui-select" name="country_code" id="country_code" onchange="setCountryCode('<? if(isset($shippingInfo->province_code)) { echo $shippingInfo->province_code;}?>')">
                                <option selected disabled value=""></option>
                                <? foreach ($countryCodes as $code => $name) {?>
                                    <option <? if(isset($shippingInfo->country_code)) { if($shippingInfo->country_code == $code) {?> selected <?}}?> value="<? echo $code?>"><? echo $name?></option>
                                <?}?>
                            </select>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label style="font-size:14px; font-weight: bolder;" for="inputLast" class="col-md-2 control-label">Phone Number</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="phone" name="phone" value="<? if(isset($shippingInfo->phone)) { echo $shippingInfo->phone;}?>">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="col-sm-12 col-sm-12 pull-right">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="button" class="ladda-button btn btn-primary" data-style="zoom-out" id="save" value="Save" name="save" onclick="saveCustomerAddress('overview')"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

