<?php
/**
 * Created by PhpStorm.
 * User: Marketing101
 * Date: 7/11/18
 * Time: 7:23 AM
 */?>


<div class="printOrder">
    <h4><?php if($this->session->shopifyStoresInfo[$storeName]["shopifyDisplayName"] != null) { echo "Store ". $this->session->shopifyStoresInfo[$storeName]["shopifyDisplayName"];} else { echo "Store ". $storeName;}; ?></h4>
</div>
<div class="ibox float-e-margins">
    <div class="ibox-content">
        <h1 id="title">Scan Products</h1>
        <p id="tour" class="tour">Scan the product barcode or enter the barcode number below and hit enter.</p>
        <div id="inputOrder">
            <div id="orderNumberD" class="orderNumberD">
                <input class="tourOrderNumber" id="orderNumber" name="orderNumber" onpaste="return false;" autofocus size="50" type="text" onkeypress="if (event.keyCode ==13){ itemScanned(this.value, '<? echo $orderObjectId?>','<? echo strtoupper($orderNumber)?>','<? echo $storeName?>');}"/>
            </div>
            <form class="form-horizontal" id="message_form" name="message_form" type="post">
                <div class="table-responsive" style="margin-top: 20px">
                    <table class="table" id="inventory" >
                        <thead>
                        <tr>
                            <th style="text-align: center">Image</th>
                            <th>Product Info</th>
                            <!--                                            <th>Size/Color</th>-->
                            <th>Barcode</th>
<!--                            <th style="text-align: center">Quantity On Hand</th>-->
                            <th style="text-align: center">Quantity Needed</th>
                            <th style="text-align: center">Scanned</th>
                            <? if(count(json_decode($orderInfo->get("refunds"))) > 0) {?>
                                <th style="text-align: center">Refunded</th>
                            <?}?>
                            <? if(intval($packedStatus) > 0) { ?>
                                <th style="text-align: center">Packed</th>
                            <? } ?>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $x = 0;
                        $mystery = false;
                        foreach ($lineItems as $lineItem) { ?>
                            <input type="hidden" name="productId[<? echo $x?>]" id="productId[<? echo $x?>]" value="<?php echo $lineItem->id;?>"/>
                            <input type="hidden" name="productSku[<? echo $x?>]" id="productSku[<? echo $x?>]" value="<?php echo $lineItem->sku;?>"/>
                            <input type="hidden" name="iDsPacked[<? echo $x?>]" id="iDsPacked[<? echo $x?>]" value=""/>
                            <tr>
                                <td style="width: 10%"><img class="productImage" src="<?php echo $images[$x]?>" alt="Product Image" height="100" width="100"></td>
                                <td style="vertical-align: middle; text-align: left; width: 20%"><?php echo $lineItem->title;?><br><?php echo $lineItem->variant_title;?><br>SKU: <? echo $lineItem->sku;?></td>
                                <!--                                                <td>--><?php //echo $lineItem["variant_title"];?><!--</td>-->
                                <td style="vertical-align: middle; text-align: left" class="sku"><?php echo $barcodes[$x];?></td>
<!--                                <td style="vertical-align: middle; width: 8%" class="inv">--><?php //echo $inventory[$x];?><!--</td>-->
                                <td style="vertical-align: middle; width: 8%"><?php echo $lineItem->quantity;?></td>
                                <td id="tourScanned" style="vertical-align: middle; width: 8%">0</td>
                                <? if(count(json_decode($orderInfo->get("refunds"))) > 0) {?>
                                    <td style="vertical-align: middle; width: 8%; text-align: center">
                                        <? $qtyRefunded = 0;
                                        foreach (json_decode($orderInfo->get("refunds")) as $refunds) { //Could be multiple refunds
                                            foreach ($refunds->refund_line_items as $lineItemsRefund) { //Could have multiple line items refunded
                                                if(isset($lineItemsRefund->line_item->sku) && $lineItemsRefund->line_item->sku == $lineItem->sku) {
                                                    $qtyRefunded = $qtyRefunded + $lineItemsRefund->quantity;
                                                } else {
                                                    $qtyRefunded = $qtyRefunded + 0;
                                                }
                                            } ?>
                                        <?} echo $qtyRefunded;?>
                                    </td>
                                <?}?>
                                <? if(intval($packedStatus) > 0) { ?>
                                    <td style="vertical-align: middle; width: 8%"> <?
                                        if(isset($productIdsPacked->{$lineItem->sku})) {
                                            echo count($productIdsPacked->{$lineItem->sku});
                                        } else {
                                            echo 0;
                                        } ?>
                                    </td> <?
                                } ?>
                            </tr>
                            <?php $x++; } ?>
                        </tbody>
                    </table>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="clearButton">
    <button id="clear" class="btn btn-danger pull-right" name="clear" onclick="clearScanned()">Clear Scanned Items</button>
    <button id="tourPartial" style="margin-right: 10px" class="btn btn-warning pull-right" name="partial" onclick="markedPartial('<? echo $orderObjectId;?>','<? echo $orderObjectId?>','<? echo strtoupper($orderNumber)?>','<? echo $storeName?>')">Mark Partially Packed</button>
    <!--                <button class="btn btn-primary" style="" onclick="history.back()">Scan Different Order</button>-->
<!--    <a class="btn btn-primary" style="" href="--><?php //echo base_url() ?><!--fulfillment">Scan Different Order</a>-->
</div>
<script>
$( document ).ready(function() {
    checkPartiallyPacked();
});

</script>

