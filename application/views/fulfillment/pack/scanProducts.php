<?php
/**
 * Created by PhpStorm.
 * User: Marketing101
 * Date: 12/15/17
 * Time: 5:00 PM
 */?>

<style>
    div.ibox-content{
        border: solid 1px #d9d9d9;
        border-radius: 5px;
    }
    #title {
        text-align: center;
        color: green;
    }
    p {
        text-align: center;
    }
    .dateTime {
        font-size: 15px;
        margin-left: 10px;
    }
    #inputOrder {
        text-align: center;
        margin-top: 20px;
    }
    .badge {
        margin-left: 10px;
    }
    a {
        color: #000000;
    }
    .table th {
        /*text-align: center;*/
        /*font-weight: normal;*/
    }
    .table td {
        /*font-weight: bold;*/
        /*font-size: 15px;*/
    }
    .productImage {
        /*display: block;*/
        max-width:80px;
        max-height:80px;
        width: auto;
        height: auto;
    }
</style>
</head>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div id="products"></div>
        </div>
    </div>
</div>
<? $this->load->view("navigation/footer");?>

<!-- Mainly scripts -->
<script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"  type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/jeditable/jquery.jeditable.js"></script>
<script href="https://cdn.datatables.net/plug-ins/1.10.16/api/sum().js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/dataTables/datatables.min.js"></script>

<!-- Sweet alert -->
<script src="<?php echo base_url() ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>

<!-- EnjoyHint -->
<script src="<?php echo base_url() ?>assets/js/plugins/enjoyhint/enjoyhint.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo base_url() ?>assets/js/inspinia.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/pace/pace.min.js"></script>
<script>
    $( document ).ready(function() {
        $("#products").html('<img height="50" width="50" style=" position: absolute; top: 50%; left: 50%;" src="https://thewrapapp.net/assets/images/loader.gif">');
        var orderData = {orderNumber: "<?php echo $orderNumber; ?>", shipPack: '<?php echo $shipPack ?>', storeName: '<?php echo $storeName?>'};
        $.ajax({
            url  	: base_url+"fulfillment/orderActions",
            type 	: 'POST',
            data    : orderData,
            success : function(data){
                $("#products").html(data);
                <? if(!in_array("scanProducts", $this->session->tour)) { ?>
                var enjoyhint_instance = new EnjoyHint({
                    onEnd: function () {
                        markTourDone("scanProducts");
                    },
                    onSkip: function () {
                        markTourDone("scanProducts");
                    }
                });
                var enjoyhint_script_steps = [
                    {
                        'next .tourOrderNumber': "You can scan or type your barcode number here. To scan the barcode, hook up a barcode scanner to your computer",
                        'skipButton': {className: "mySkip", text: "End tour"}
                    },
                    {
                        'next #tourScanned': "As you scan your products, this number will increment by one.",
                        'radius': 90,
                        'shape': 'circle',
                        'skipButton': {className: "mySkip", text: "End tour"}
                    },
                    {
                        'next #tourPartial': 'If you are not packing all of the products, you can click Mark Partial when you have finished scanning the products.',
                        'nextButton': {className: "myNext", text: "Got it!"},
                        'showSkip': false
                    }
                ];
                enjoyhint_instance.set(enjoyhint_script_steps);
                enjoyhint_instance.run();
                <?}?>
            }
        });
    });
</script>
</body>
</html>
