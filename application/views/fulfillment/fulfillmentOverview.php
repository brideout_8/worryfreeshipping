<?php
/**
 * Created by PhpStorm.
 * User: rapidware
 * Date: 11/23/17
 * Time: 12:00 PM
 */?>

<style>
    .body {
        min-height: 0 !important;
        margin-bottom: 500px;
    }
    td {
        height: 35px;
    }

    tr {
        cursor: pointer;
    }
    tr:hover td {
        background: #f4f4f4
    }

    .rounded {
        border-radius: 5px;
    }
    #title {
        text-align: center;
        color: green;
    }
    p {
        text-align: center;
    }
    #inputOrder {
        text-align: center;
        margin-top: 20px;
    }
    .bottomBtn {
        margin-top: 15px;
    }
    #actionButtons {
        margin-top: 15px;
    }
    h2 {
        text-align: center;
    }
    .dateTime {
        font-size: 15px;
        margin-left: 10px;
    }
    .badge {
        margin-left: 10px;
    }
    .title{
        padding-bottom: 20px;
        padding-left: 12px;
    }
    label {
        font-weight: normal;
        font-size: 14px;
    }
    .selection {
        background-color: transparent;
    }
    .bootstrap-select .dropdown-menu {
        margin: 15px 0 0;
    }
    .fa {
        font-size: 18px;
        color: grey;
    }
    .tour-step-background,
    .tour-backdrop {
        position: fixed;
    }

    .tour-step-background {
        background: #fff;
    }
    /*.tour-step-backdrop>td*/
    /*{*/
        /*position: relative;*/
        /*z-index: 1101;*/
    /*}*/

</style>
</head>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
<!--            <h1><b>Fulfillment</b></h1>-->
<!--            <div class="ibox float-e-margins">-->
<!--                <div class="ibox-content img-rounded">-->
<!--                    <div id="listings2" class="listings2">-->
<!--                        <h1 id="title"><b>Scan Order</b></h1>-->
<!--                        <p>Scan the order barcode.</p>-->
<!--<!--                        <button class="btn btn-raised btn-block btn-primary" onClick="updateOrders();">Send Password</button>-->
<!--                        <div id="inputOrder">-->
<!--                            <form id="orderSearch" name="orderSearch" type="post" action="--><?// base_url()?><!--/fulfillment/orderOverview/">-->
<!--                                <input id="orderNumber" autofocus name="orderNumber" size="35" type="text" onkeypress="if(event.keyCode==13){form.submit(); loader(); }" />-->
<!--                            </form>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
            <h1 class="tourTitle">Fulfillment</h1>
            <div class="ibox float-e-margins">
                <div class="ibox-content img-rounded">
                    <div class="row">
                        <div class="col-lg-12">
                            <? if($this->session->importedOrder != "yes") {?>
                                <div class="tourFirstImport">
                                    <!-- New User. Show prompt to get first order -->
                                    <h2 style="font-size: 15px">To get started, enter the order number of the first order you would like to import.<br>
                                        Once the first order is imported, any additional imports will only import newer orders.<br>
                                        For example, if your most recent order is #1200 and you want to import orders from #1150 to #1200, you will enter #1150 below.</h2>
                                    <form method="post" id="addStoreNumberForm" name="addStoreNumberForm" class="form">
                                        <input type="hidden" id="shopifyToken" name="shopifyToken" value="<? echo $this->session->shopifyToken?>">
                                        <input type="hidden" id="storeName" name="storeName" value="<? echo $this->session->shopifyStore?>">
                                        <div class="col-lg-4 col-md-4 col-lg-offset-4 col-md-offset-4">
                                            <div style="margin: 10px">
                                                <label>Starting Order Number</label>
                                                <input class="form-control" type="text" id="startingOrderNumber" placeholder="#1001" name="startingOrderNumber" />
                                            </div>
                                        </div>
                                        <button style="margin-top: 36px" type="button" id="connect" name="connect" class="ladda-button btn btn-primary" data-style="zoom-out" onclick="addStartingOrder()">Import Order</button>
                                    </form>
                                </div>
                            <?} else {?>
<!--                            <hr>-->
                                <div class="tabs-container">
                                    <a  href="#" data-toggle="modal" data-target="#addOrderModal" class="fa fa-question-circle pull-right" ></a>
                                    <button <? if(!in_array("importOrders", $this->session->permissions) && !in_array("fullAccess", $this->session->permissions)) {?> disabled="" <?}?> class="ladda-button btn btn-primary pull-right" style="" data-style="zoom-out" id="importNewOrders" onclick="getNewOrders()">Import New Orders</button>
                                    <ul id="myTabs" class="nav nav-tabs nav-fill">
    <!--                                    <li><a style="color: black" data-toggle="tab" href="#tab-1">New Orders</a></li>-->
                                        <li><a style="color: black" data-toggle="tab" href="#tab-2">Pending Orders</a></li>
                                        <li><a style="color: black" data-toggle="tab" href="#tab-3">Orders To Pull</a></li>
                                        <li><a style="color: black" data-toggle="tab" href="#tab-4">Orders To Ship</a></li>
                                        <li><a style="color: black" data-toggle="tab" href="#tab-5">Orders On Hold</a></li>
                                        <li><a style="color: black" data-toggle="tab" href="#tab-6">All Orders</a></li>
                                    </ul>
                                    <div class="tab-content boxShadow">
    <!--                                    <div id="tab-1" class="tab-pane">-->
    <!--                                        <div class="panel-body">-->
    <!--                                            <div id="newOrders"></div>-->
    <!--                                        </div>-->
    <!--                                    </div>-->
                                        <div id="tab-2" class="tab-pane">
                                            <div class="panel-body">
                                                <div id="pendingOrders"></div>
                                            </div>
                                        </div>
                                        <div id="tab-3" class="tab-pane">
                                            <div class="panel-body">
                                                <div id="ordersToPull"></div>
                                            </div>
                                        </div>
                                        <div id="tab-4" class="tab-pane">
                                            <div class="panel-body">
                                                <div id="ordersToShip"></div>
                                            </div>
                                        </div>
                                        <div id="tab-5" class="tab-pane">
                                            <div class="panel-body">
                                                <div id="ordersOnHold"></div>
                                            </div>
                                        </div>
                                        <div id="tab-6" class="tab-pane">
                                            <div class="panel-body">
                                                <div id="allOrders"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?}?>
                        </div>
                    </div>
                </div>
            </div>
<!--            <a class="btn btn-primary pull-right" href="--><?//=base_url();?><!--fulfillment/ordersToShip">Orders To Ship</a>-->
            <a <? if(!in_array("pickOrders", $this->session->permissions) && !in_array("fullAccess", $this->session->permissions)) {?> disabled="" <?} if ($this->session->subscriptionLevel == "1") {?> disabled="" <?}?>  class="btn btn-primary pull-right" style="margin-right: 10px" href="<?=base_url();?>fulfillment/shipNonOrder">Ship Non Order</a>
<!--            <button class="ladda-button btn btn-primary" style="" onclick="createScanForm()">Get Scan Form</button>-->
<!--            <button class="ladda-button btn btn-primary" style="" onclick="createScanForm2()">Scope</button>-->
<!--            <button class="ladda-button btn btn-primary" style="" onclick="createScanForm3()">Scope3</button>-->
        </div>
    </div>
</div>
<? $this->load->view("navigation/footer");?>

<script>
    function loader() {
        $('#listings2').html('<img height="50" width="50" style=" position: absolute; top: 50%; left: 50%;" src="'+base_url+'assets/img/loader.gif">');
    }

    $(document).ready(function() {
        //If first time user open intro modal
        // $('#firstTimeUserModal').modal('show');
        var newOrders = false;
        var ordersToShip = false;
        var ordersOnHold = false;
        var allOrders = false;
        $('[data-toggle="tooltip"]').tooltip();
        // $('.nav-tabs a[href="#tab-1"]').on('show.bs.tab', function(event) {
        //     if(newOrders == false) {
        //         localStorage.setItem('activeTab', $(event.target).attr('href'));
        //         tableInfo('newOrders');
        //     }
            // newOrders = true;
        // });
        $('.nav-tabs a[href="#tab-2"]').on('show.bs.tab', function(event) {
            if(newOrders == false) {
                localStorage.setItem('activeTab', $(event.target).attr('href'));
                tableInfo('pendingOrders');
            }
            // newOrders = true;
        });
         $('.nav-tabs a[href="#tab-3"]').on('show.bs.tab', function(event) {
                    if(newOrders == false) {
                        localStorage.setItem('activeTab', $(event.target).attr('href'));
                        tableInfo('ordersToPull');
                        // newOrders = true;
                    }
                });

        $('.nav-tabs a[href="#tab-4"]').on('show.bs.tab', function(event) {
            if(ordersToShip == false) {
                localStorage.setItem('activeTab', $(event.target).attr('href'));
                tableInfo('ordersToShip');
                // ordersToShip = true;
            }
        })
        $('.nav-tabs a[href="#tab-5"]').on('shown.bs.tab', function(event) {
            if(ordersOnHold == false) {
                localStorage.setItem('activeTab', $(event.target).attr('href'));
                console.log($(event.target).attr('href'));
                tableInfo('ordersOnHold');
                // ordersOnHold = true
            }
        })
        $('.nav-tabs a[href="#tab-6"]').on('shown.bs.tab', function(event) {
            if(allOrders == false) {
                localStorage.setItem('activeTab', $(event.target).attr('href'));
                tableInfo('allOrders');
                // allOrders = true;
            }
        });
        var activeTab = localStorage.getItem('activeTab');
        if(activeTab){
            $('.nav-tabs a[href="' + activeTab + '"]').tab('show');
            console.log(activeTab);
        } else {
            $('.nav-tabs a[href="#tab-2"]').tab('show');
        }
        jQuery(document).ready(function($) {
            $(".clickable-row").click(function() {
                window.location = $(this).data("href");
                console.log($(this).data("href"));
            });
        });
        <? if($this->session->importedOrder != "yes" && !in_array("firstOrderImport", $this->session->tour)) {?>
        console.log("1");
            var enjoyhint_instance1 = new EnjoyHint({
                onEnd: function () {
                    markTourDone("firstOrderImport");
                },
                onSkip: function () {
                    markTourDone("firstOrderImport");
                }
            });
            var enjoyhint_script_steps1 = [
                {
                    'next .tourFirstImport': 'It looks like you have not imported an order yet. Enter your first order number to get started.',
                    // 'nextButton': {className: "myNext", text: "Done"},
                    'nextButton': {className: "myNext", text: "Ok"},
                    'showSkip': false
                }
            ];
            enjoyhint_instance1.set(enjoyhint_script_steps1);
            enjoyhint_instance1.run();
        <? } else {?>
            <? if(!in_array("fulfillment", $this->session->tour)) { ?>
                var enjoyhint_instance = new EnjoyHint({
                    onEnd: function () {
                        markTourDone("fulfillment");
                    },
                    onSkip: function () {
                        markTourDone("fulfillment");
                    }
                });
                var enjoyhint_script_steps = [
                    {
                        'next .tourTitle': "Your setup is complete. Let me guide you through our features.",
                        'nextButton': {className: "myNext", text: "Sure"},
                        'skipButton': {className: "mySkip", text: "No thanks"}
                    },
                    {
                        'next #importNewOrders': 'Here you can import new orders from your connected stores. Archived and cancelled orders will not import.',
                        'skipButton': {className: "mySkip", text: "End tour"}
                    },
                    {
                        'next .footer': 'At the bottom of every page you can use our quick find feature to find any order for any connected store.',
                        'skipButton': {className: "mySkip", text: "End tour"}
                    },
                    {
                        'click table tbody tr:first-of-type': 'This is where all of your imported orders will be displayed. Just select an order when you are ready to fulfill it.',
                        // 'nextButton': {className: "myNext", text: "Done"},
                        // 'nextButton': {className: "myNext", text: "Ok"},
                        'skipButton': {className: "mySkip", text: "Ok"}
                    }
                ];
                enjoyhint_instance.set(enjoyhint_script_steps);
                enjoyhint_instance.run();
            <?}?>
        <?}?>
    });
</script>
<!-- Mainly scripts -->
<script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"></script>
<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/jeditable/jquery.jeditable.js"></script>

<script src="<?php echo base_url() ?>assets/js/plugins/dataTables/datatables.min.js"></script>

<!-- Sweet alert -->
<script src="<?php echo base_url() ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>
<!-- Bootstrap Tour -->
<script src="<?php echo base_url() ?>assets/js/plugins/bootstrapTour/bootstrap-tour.min.js"></script>

<!-- Ladda -->
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.jquery.min.js"></script>

<!-- EnjoyHint -->
<script src="<?php echo base_url() ?>assets/js/plugins/enjoyhint/enjoyhint.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo base_url() ?>assets/js/inspinia.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/pace/pace.min.js"></script>

</body>

</html>
<!-- Import orders info Modal -->
<div class="modal fade" id="addOrderModal" name="addOrderModal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Import order explanation</h4>
            </div>
            <div class="modal-body">
                <h2 style="font-size: 15px">This will import any new orders on all of your connected stores. Archived and cancelled orders will not be imported. Each import is limited to 250 orders per store. If you need to import more orders, click import orders again once the first import is completed. Please be patient when importing orders.</h2>
            </div>
            <div class="modal-footer">
                <div class="col-sm-12 col-sm-12 pull-right">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>

