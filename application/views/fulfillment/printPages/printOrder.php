<?php
/**
 * Created by PhpStorm.
 * User: Marketing101
 * Date: 9/12/18
 * Time: 7:30 AM
 */?>

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--    --><?php //include_once(APPPATH."helpers/analyticstracking.php") ?><!--<!-- Google Analylitics -->

    <title>Worry Free Shipping</title>

    <script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"></script>
    <script src="<?php echo base_url() ?>assets/js/functionsV1.js" type="text/javascript" charset="utf-8"></script>

    <link href="<?php echo base_url() ?>assets/css/plugins/chosen/chosen.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/css/plugins/iCheck/custom.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/css/animate.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/css/plugins/datapicker/datepicker3.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/css/plugins/dataTables/datatables.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/css/shopify/shopifyTable.css" rel="stylesheet" media="all">
    <!-- Toastr style -->
    <link href="<?php echo base_url() ?>assets/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo base_url()?>assets/js/validationEngine.jquery.css"/>
    <script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.validationEngine.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>

    <!-- Sweet Alert -->
    <link href="<?php echo base_url() ?>assets/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

    <link href="<?php echo base_url() ?>assets/css/plugins/fullcalendar/fullcalendar.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/plugins/fullcalendar/fullcalendar.print.css" rel='stylesheet' media='print'>

    <link href="<?php echo base_url() ?>assets/css/plugins/dropzone/dropzone.css" rel="stylesheet">

    <!-- qTip -->
    <link href="<?php echo base_url() ?>assets/css/plugins/qtip/jquery.qtip.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/plugins/qtip/jquery.qtip.css" rel="stylesheet">

    <!-- Ladda style -->
    <link href="<?php echo base_url() ?>assets/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">
    <!-- orris -->
    <link href="<?php echo base_url() ?>assets/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">

    <!--     <link href="css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet"> -->
</head>
<style>

    html, body { height: auto; }
    #logo {
        /*display: block;*/
        max-width:100%;
        max-height:100%;
        width: auto;
        height: auto;
    }

</style>
<body class="white-bg">

<!--<div id="wrapper">-->
    <div class="gray-bg">
        <div id="printableArea">
            <p style="float: right; text-align: right; margin: 0;">
                <?php $dateNow = new DateTime($orderInfo->get('dateOrderedTime'), new DateTimeZone("America/Chicago")); echo $dateNow->format('M d Y');?><br />
                Invoice for #<? echo $orderNumber; ?>
            </p>

            <div style="float: left; margin: 0 0 1.5em 0;" >
                <? if($logo != "") {?>
                    <img id="logo" src="<? echo $logo->getURL();?>"</img>
                <?}?>
                <br>
                <br>
                <? echo $companyInfo[0]->get("address1");?><br/>
                <? if($companyInfo[0]->get("address2") != "") {
                    echo $companyInfo[0]->get("address2");
                }?>
                <? echo $companyInfo[0]->get("city")." ".$companyInfo[0]->get("stateCode")." ".$companyInfo[0]->get("zip");?><br/>
                <? echo $companyInfo[0]->get("country")?>
            </div>
            <br>
            <hr />

            <h3 style="margin: 0 0 1em 0;">Item Details</h3>

            <table class="table-tabular" style="margin: 0 0 1.5em 0;">
                <thead>
                <tr>
                    <th>Image</th>
                    <th>Qty</th>
                    <th>Item</th>
                    <th>SKU</th>
                    <th>Price</th>
                </tr>
                </thead>
                <tbody>
                <? foreach ($lineItems as $lineItem) { $y=0;?>
                    <tr>
                        <td><img class="productImage" src="<?php echo $images[$y]?>" alt="Product Image" height="55" width="55"></td>
                        <td style="font-size: 13px"><?php echo $lineItem->quantity;?> x</td>
                        <td style="font-size: 13px"><b><?php echo $lineItem->variant_title;?></b></td>
                        <td style="font-size: 13px"><b><?php echo $lineItem->sku;?></b></td>
                        <td style="font-size: 13px"><?php echo $lineItem->price;?></td>
                    </tr>
                    <? $y++;}?>
                </tbody>
            </table>



            <h3 style="margin: 0 0 1em 0;">Payment Details</h3>

            <table class="table-tabular" style="margin: 0 0 1.5em 0;">
                <tr>
                    <td>Subtotal price:</td>
                    <td><?php echo $orderInfo->get("subTotal");?></td>
                </tr>
                <? if($orderInfo->get("totalDiscounts") != "0.00") {?>
                    <tr>
                        <!--            <td>Includes discount "{{ discount.code }}"</td>-->
                        <td><?php echo $orderInfo->get("totalDiscounts");?></td>
                    </tr>
                <?}?>
                <tr>
                    <td>Total tax:</td>
                    <td><?php echo $orderInfo->get("totalTax");?></td>
                </tr>
                <? if(count(json_decode($orderInfo->get("shippingLines"))) > 0){?>
                    <tr>
                        <td>Shipping:</td>
                        <td><? $shipping = json_decode($orderInfo->get("shippingLines")); echo $shipping[0]->price?></td>
                    </tr>
                <?}?>
                <tr>
                    <td><strong>Total price:</strong></td>
                    <td><strong><? echo $orderInfo->get("totalPrice")?></strong></td>
                </tr>
            </table>

            <? if(count(json_decode($orderInfo->get("shippingAddress"))) > 0){
                $shippingAddress = json_decode(($orderInfo->get("shippingAddress")))?>
                <h3 style="margin: 0 0 1em 0;">Shipping Details</h3>

                <div style="margin: 0 0 1em 0; padding: 1em; border: 1px solid black;">
                    <strong><? echo $shippingAddress->name?></strong><br/>
                    <? if($shippingAddress->company != null) {?>
                        <? echo $shippingAddress->company?><br/>
                    <?}?>
                    <? echo $shippingAddress->address1?><br/>
                    <? if($shippingAddress->address2 != null) {?>
                        <? echo $shippingAddress->address2?><br/>
                    <?}?>
                    <? echo $shippingAddress->city?>
                    <? echo $shippingAddress->province_code?>
                    <? echo $shippingAddress->zip?><br/>
                    <? echo $shippingAddress->country?>
                </div>
            <?}?>
        </div>
    </div>
<!--</div>-->
<script type="text/javascript">
    window.print();
</script>
<script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"  type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/jeditable/jquery.jeditable.js"></script>
<script href="https://cdn.datatables.net/plug-ins/1.10.16/api/sum().js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/dataTables/datatables.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo base_url() ?>assets/js/inspinia.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/pace/pace.min.js"></script>


</body>
