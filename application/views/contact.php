<?php
/**
 * Created by PhpStorm.
 * User: Marketing101
 * Date: 12/13/17
 * Time: 5:25 PM
 */?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#333">
    <title>R&R Enterprises</title>
    <meta name="description" content="Material Style Theme">
    <link rel="shortcut icon" href="<?php echo base_url() ?>/assets/landing/assets/img/demo/RRRacewear.png">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>/assets/landing/assets/plugins/revolution/revolution/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css">
    <script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"></script>
    <script src="<?php echo base_url() ?>assets/js/functionsLandingV1.js" type="text/javascript" charset="utf-8"></script>
    <!-- REVOLUTION STYLE SHEETS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>/assets/landing/assets/plugins/revolution/revolution/css/settings.css">
    <!-- REVOLUTION LAYERS STYLES -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>/assets/landing/assets/plugins/revolution/revolution/css/layers.css">
    <!-- REVOLUTION NAVIGATION STYLES -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>/assets/landing/assets/plugins/revolution/revolution/css/navigation.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>/assets/landing/assets/css/preload.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>/assets/landing/assets/css/plugins.min.css">
    <!--     TYPEWRITER ADDON-->
    <script type="text/javascript" src="<?php echo base_url() ?>/assets/landing/assets/plugins/revolution/revolution-addons/typewriter/js/revolution.addon.typewriter.min.js"></script>
    <!--    <link rel="stylesheet" type="text/css" href="--><?php //echo base_url() ?><!--/assets/landing/assets/plugins/revolution/revolution-addons/typewriter/css/typewriter.css">-->
    <link rel="stylesheet" href="<?php echo base_url() ?>/assets/landing/assets/css/style.light-blue-500.min.css">
    <!-- Sweet Alert -->
    <link href="<?php echo base_url() ?>assets/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <!-- Ladda style -->
    <link href="<?php echo base_url() ?>assets/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">
<!--    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>-->
</head>
<style>
    #emailBtn:disabled {
        background-color: lightskyblue;
    }
</style>
<body>
<div id="ms-preload" class="ms-preload">
    <div id="status">
        <div class="spinner">
            <div class="dot1"></div>
            <div class="dot2"></div>
        </div>
    </div>
</div>
<div class="ms-site-container">
    <header class="ms-header ms-header-white">
        <!--ms-header-white-->
        <div class="container container-full home" id="home">
            <div class="ms-title">
                <a href="index.html">
                    <!-- <img src="assets/img/demo/logo-header.png" alt=""> -->
                    <!--                    <span class="ms-logo animated zoomInDown animation-delay-5">M</span>-->
                    <img src="<?php echo base_url() ?>/assets/landing/assets/img/demo/RRRacewear.png" height="125px" width="218px">
                    <!--                    <h1 class="animated fadeInRight animation-delay-6">Material-->
                    <!--                        <span>Style</span>-->
                    <!--                    </h1>-->
                </a>
            </div>
        </div>
    </header>
    <nav class="navbar navbar-expand-md  navbar-static ms-navbar ms-navbar-white">
        <div class="container container-full">
            <div class="navbar-header">
                <a class="navbar-brand" href="index.html">
                    <!-- <img src="assets/img/demo/logo-navbar.png" alt=""> -->
                    <!--                    <span class="ms-logo ms-logo-sm">M</span>-->
                    <img src="<?php echo base_url() ?>/assets/landing/assets/img/demo/RRRacewear.png" height="35px" width="63px">
                    <span class="ms-title">R&R Enterprises
                        <!--                <strong>Style</strong>-->
              </span>
                </a>
            </div>
            <div class="collapse navbar-collapse" id="ms-navbar">
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a href="<?php base_url() ?>/landing" data-scroll class="nav-link dropdown-toggle animated fadeIn animation-delay-7" role="button">Home</a>
                    </li>
                    <li class="nav-item dropdown active">
                        <a href="<? base_url()?>/landing/contact" data-scroll class="nav-link dropdown-toggle animated fadeIn animation-delay-7" role="button"> Contact</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="<?php base_url() ?>/login" class="nav-link dropdown-toggle animated fadeIn animation-delay-7" role="button">Client Login</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="<?php base_url() ?>/landing/customerProfile" class="nav-link dropdown-toggle animated fadeIn animation-delay-7" role="button">Customer Profile</a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- container -->
    </nav>
    <div class="ms-hero-page-override ms-hero-img-team ms-hero-bg-primary">
        <div class="container">
            <div class="text-center">
                <h1 class="no-m ms-site-title color-white center-block ms-site-title-lg mt-2 animated zoomInDown animation-delay-5">Contact Us</h1>
                <p class="lead lead-lg color-light text-center center-block mt-2 mw-800 text-uppercase fw-300 animated fadeInUp animation-delay-7">We will answer your email as soon as possible.
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xl-8 col-lg-7">
                <div class="card card-hero animated fadeInUp animation-delay-7 ">
                    <form class="form-horizontal" id="message_form" name="message_form" type="post">
                        <fieldset class="container">
                            <div class="form-group row">
                                <label for="inputEmail" autocomplete="false" class="col-lg-2 control-label">Name</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Name"> </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail" autocomplete="false" class="col-lg-2 control-label">Email</label>
                                <div class="col-lg-9">
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Email"> </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail" autocomplete="false" class="col-lg-2 control-label">Subject</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" id="subject" name="subject" placeholder="Subject"> </div>
                            </div>
                            <div class="form-group row">
                                <label for="textArea" class="col-lg-2 control-label">Message</label>
                                <div class="col-lg-9">
                                    <textarea class="form-control" rows="3" id="message" name="message" placeholder="Your message..."></textarea>
                                </div>
                            </div>
                            <div class="form-group row justify-content-end">
                                <div class="col-lg-10">
<!--                                    <input type="button" id="loginButton" name="loginButton" value = "Send Message" class="btn btn-raised btn-primary" onClick="emailUs(); return false">-->
                                    <button class="ladda-button btn btn-primary btn-raised" id="emailBtn" data-style="zoom-out" onclick="emailUs(); return false;">Send Message</button>
<!--                                    <button type="button" class="btn btn-danger">Cancel</button>-->
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
            <div class="col-xl-4 col-lg-5">
                <div class="card card-hero animated fadeInUp animation-delay-7">
                    <div class="card-block">
                        <div class="text-center mb-2">
                            <h3 class="no-m ms-site-title">Location</h3>
                            </h3>
                        </div>
                        <address class="no-mb">
                            <p>
                                <i class="color-danger-light zmdi zmdi-pin mr-1"></i> 400 Valley School Dr</p>
                            <p>
                                <i class="color-warning-light zmdi zmdi-map mr-1"></i> Valley Park, MO 63088</p>
                            <p>
                                <i class="color-info-light zmdi zmdi-email mr-1"></i>
                                <a href="mailto:sales@r-renterprises.com">sales@r-renterprises.com</a>
                            </p>
                            <p>
                                <i class="color-royal-light zmdi zmdi-phone mr-1"></i>(636) 717-0001</p>
                            <p>
                                <i class="color-success-light fa fa-fax mr-1"></i>877-730-6953</p>
                        </address>
                    </div>
                </div>
<!--                <div class="card card-primary animated fadeInUp animation-delay-7">-->
<!--                    <div class="card-header">-->
<!--                        <h3 class="card-title">-->
<!--                            <i class="zmdi zmdi-map"></i>Map</h3>-->
<!--                    </div>-->
<!--                    <iframe width="100%" height="340" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d48342.06480344582!2d-73.980069429762!3d40.775680208459505!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c2589a018531e3%3A0xb9df1f7387a94119!2sCentral+Park!5e0!3m2!1sen!2sus!4v1491233314840"></iframe>-->
<!--                </div>-->
            </div>
        </div>
        <footer class="ms-footer ">
            <div class="container">
                <p>Copyright &copy; R&R Enterprises 2018</p>
            </div>
        </footer>
        <div class="btn-back-top">
            <a href="#" data-scroll id="back-top" class="btn-circle btn-circle-primary btn-circle-sm btn-circle-raised ">
                <i class="zmdi zmdi-long-arrow-up"></i>
            </a>
        </div>
        <script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"></script>
        <script src="<?php echo base_url() ?>assets/js/functionsV3.js" type="text/javascript" charset="utf-8"></script>
        <!-- Sweet alert -->
        <script src="<?php echo base_url() ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/landing/assets/js/plugins.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/landing/assets/js/app.min.js"></script>
        <!-- Ladda -->
        <script src="<?php echo base_url() ?>assets/js/plugins/ladda/spin.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.jquery.min.js"></script>

</div>
</body>
</html>
