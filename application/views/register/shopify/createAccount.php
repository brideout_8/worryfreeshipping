<?php
/**
 * Created by PhpStorm.
 * User: Marketing101
 * Date: 7/16/18
 * Time: 9:19 AM
 */?>

<html>

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-126911005-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-126911005-1');
    </script>
    <!-- End of Google Analytics -->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Worry Free Shipping - Sign Up</title>

    <script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"></script>
    <!-- 	<link href="<?php echo base_url() ?>assets/css/plugins/chosen/chosen.css" rel="stylesheet"> -->
    <!-- Ladda style -->
    <link href="<?php echo base_url() ?>assets/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
    <!-- Sweet Alert -->
    <link href="<?php echo base_url() ?>assets/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo base_url()?>assets/js/validationEngine.jquery.css"/>
    <script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.validationEngine.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>

    <script type="text/javascript">
        function createShopifyAccount() {
            var base_url = window.location.origin+"/";
            var c = $('.ladda-button').ladda();
            c.ladda('start');
            var formArray = $('#createShopifyAccountForm').serialize();
            jQuery.post(base_url+'register/createAccount/', formArray, function(data) {
                if (data.success) {
                    var form = document.getElementById('createShopifyAccountForm');
                    form.submit();
                } else {
                    c.ladda('stop');
                    swal({
                        title: "Error",
                        text: data.message
                    });
                }
            }, 'json');

            return false;
        }
    </script>
    <style>
        img {
            max-width: 60%;
            max-height: 60%;
            display: block;
            margin-left: auto;
            margin-right: auto
        }

        .signUpColumns {
            max-width: 1100px;
            margin: 0 auto;
            padding: 100px 20px 20px 20px;
        }
        .custom-bg {
            background-color: #f6f8fa;
        }
        div.ibox-content {
            border: solid 1px #d9d9d9;
            /*box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);*/
            /*margin-top: 20px;*/
            border-radius: 5px;
            /*box-shadow: #3D3D3D;*/
        }
        .withUs {
            padding-top: 5px;
            padding-bottom: 5px;
            font-size: 15px;
        }
        .form-group {
            padding-left: 0px;
            padding-right: 8px;
        }
        .form-control {
            border-radius: 5px;
        }
        label {
            font-weight: normal;
            color: black;
        }
        ::placeholder {
            font-weight: lighter;
        }
        .signUpForm {
            padding-right: 7px;
            padding-left: 12px;
        }
        .createAccount {
            margin-top: 15px;
        }
        h1 {
            font-size: 40px;
        }
        .ui-select {
            font-size: 13px;
            font-weight: 400;
            line-height: 2.4rem;
            text-transform: initial;
            letter-spacing: initial;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            display: block;
            height: 3.4rem;
            width: 100%;
            padding: 0.4rem 0.8rem;
            padding-right: 2.8rem;
            padding-left: 1.6rem;
            background: #fefefe;
            border: 1px solid #c4cdd5;
            border-radius: 3px;
            max-width: none;
            -webkit-transition-property: background, border, -webkit-box-shadow;
            transition-property: background, border, -webkit-box-shadow;
            transition-property: background, border, box-shadow;
            transition-property: background, border, box-shadow, -webkit-box-shadow;
            -webkit-transition-timing-function: cubic-bezier(0.64, 0, 0.35, 1);
            transition-timing-function: cubic-bezier(0.64, 0, 0.35, 1);
            -webkit-transition-duration: 200ms;
            transition-duration: 200ms
        }

        select.minimal {
            background-image:
                    linear-gradient(45deg, transparent 50%, gray 50%),
                    linear-gradient(135deg, gray 50%, transparent 50%),
                    linear-gradient(to right, #ccc, #ccc);
            background-position:
                    calc(100% - 20px) calc(1em + 2px),
                    calc(100% - 15px) calc(1em + 2px),
                    calc(100% - 2.5em) 0.5em;
            background-size:
                    5px 5px,
                    5px 5px,
                    1px 1.5em;
            background-repeat: no-repeat;
        }
    </style>
</head>

<body class="custom-bg">
<div class="signUpColumns">
    <div class="row">
        <div class="col-md-6 animated fadeInDown">
            <img src="<? echo base_url()?>/assets/img/WorryFreeShipping.png">
        </div>
        <div class="col-md-6">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-md-12">
                        <h1 style="color: black; font-weight: normal; text-align: center">Unite With Us</h1>
                    </div>
                    <div class="row">
                        <div style="font-weight: lighter" class="col-md-12">
<!--                            <p class="withUs" style="text-align: center"> Already with us? <a href="--><?php //echo base_url()?><!--login">Login</a></p>-->
                        </div>
                    </div>
                </div>
                <form method="post" id="createShopifyAccountForm" name="createShopifyAccountForm" action="plan">
                    <input id="weightUnit" name="weightUnit" type="hidden" value="<?php echo $shopInfo->weight_unit;?>"/>
                    <input id="timeZone" name="timeZone" type="hidden" value="<?php echo $shopInfo->iana_timezone;?>"/>
                    <input id="primaryLocationId" name="primaryLocationId" type="hidden" value="<?php echo $shopInfo->primary_location_id;?>"/>
                    <input id="shopifyToken" name="shopifyToken" type="hidden" value="<?php echo $accessToken;?>"/>
                    <input id="shopifyStore" name="shopifyStore" type="hidden" value="<?php echo $shopifyStore;?>"/>
                    <input id="shopifyStoreId" name="shopifyStoreId" type="hidden" value="<?php echo $shopInfo->id;?>"/>
                    <input id="newCompany" name="newCompany" type="hidden" value="yes"/>
                    <input id="newStore" name="newStore" type="hidden" value="yes"/>
                    <div class="row signUpForm">
                        <div class="form-group col-md-6">
                            <label>First Name</label>
                            <input class="form-control" type="text" id="firstName" placeholder="First Name" name="firstName" value="<? echo $firstName?>"/>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Last Name</label>
                            <input class="form-control" type="text" id="lastName" placeholder="Last Name" name="lastName" value="<? echo $lastName?>"/>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Email</label>
                            <input class="form-control" type="email" placeholder="Email" name="email" id="email" value="<? echo $shopInfo->email?>"/>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Username</label>
                            <input class="form-control" type="email" name="username" id="username" value=""/>
                        </div>
                        <div class="form-group col-md-12">
                            <label>Password</label>
                            <input class="form-control" type="password" id="password" name="password" />
                        </div>
                        <div class="form-group col-md-6">
                            <label>Company Name</label>
                            <input class="form-control" type="text" id="companyName" placeholder="Company Name" name="companyName" value="<? echo $shopInfo->name?>"/>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Phone Number</label>
                            <input class="form-control" type="text" placeholder="Phone Number" name="phoneNumber" id="phoneNumber" value="<? echo $shopInfo->phone?>"/>
                        </div>
                        <div class="form-group col-md-8">
                            <label>Address</label>
                            <input class="form-control" type="text" name="address1" id="address1" placeholder="Address" value="<? echo $shopInfo->address1?>"/>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Address (cont)</label>
                            <input class="form-control" type="text" name="address2" id="address2" placeholder="Optional" value="<? echo $shopInfo->address2?>"/>
                        </div>
                        <div class="form-group col-md-6">
                            <label>City</label>
                            <input class="form-control" type="text" name="city" id="city" placeholder="City" value="<? echo $shopInfo->city?>"/>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Zip</label>
                            <input class="form-control" type="text" name="zip" id="zip" placeholder="Zip" value="<? echo $shopInfo->zip?>"/>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Country</label>
                            <select id="country" name="country" class="form-control minimal ui-select">
                                <option <? if($shopInfo->country_name == "United States") {?> selected <?}?> value="United States">United Sates</option>
                                <option <? if($shopInfo->country_name == "Canada") {?> selected <?}?> value="Canada">Canada</option>
                                <option <? if($shopInfo->country_name == "United Kingdom") {?> selected <?}?> value="United Kingdom">United Kingdom</option>
                                <option <? if($shopInfo->country_name == "Australia") {?> selected <?}?> value="Australia">Australia</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>State/Province</label>
                            <input class="form-control" type="text" name="state" id="state" placeholder="State/Province" value="<? echo $shopInfo->province?>"/>
                            <input class="form-control" type="hidden" name="stateCode" id="stateCode" value="<? echo $shopInfo->province_code?>"/>
<!--                            <input class="form-control" type="hidden" name="countryCode" id="countryCode" value="--><?// echo $shopInfo->country_code?><!--"/>-->
                        </div>
                    </div>
                    <button type="button" data-style="zoom-out" class="ladda-button btn btn-primary block full-width m-b createAccount" onclick="createShopifyAccount()">Create Account</button>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-2.1.1.js"></script>
<!-- Sweet alert -->
<script src="<?php echo base_url() ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo base_url()?>assets/js/functions.js" type="text/javascript" charset="utf-8"></script>
<!-- Ladda -->
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.jquery.min.js"></script>
</body>

</html>


