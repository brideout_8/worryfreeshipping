<?php
/**
 * Created by PhpStorm.
 * User: Marketing101
 * Date: 9/20/18
 * Time: 11:05 AM
 */?>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Worry Free Shipping</title>

    <script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"></script>
    <script src="<?php echo base_url() ?>assets/js/stripeCheckout.js"></script>
    <script src="<?php echo base_url() ?>assets/js/functionsV20.js" type="text/javascript" charset="utf-8"></script>
    <script src="https://js.stripe.com/v3/"></script>
    <!-- 	<link href="<?php echo base_url() ?>assets/css/plugins/chosen/chosen.css" rel="stylesheet"> -->
    <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
    <!-- Sweet Alert -->
    <link href="<?php echo base_url() ?>assets/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo base_url()?>assets/js/validationEngine.jquery.css"/>
    <script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.validationEngine.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>

    <script type="text/javascript">

    </script>
    <style>
        img {
            max-width: 60%;
            max-height: 60%;
            display: block;
            margin-left: auto;
            margin-right: auto
        }

        .signUpColumns {
            max-width: 1100px;
            margin: 0 auto;
            padding: 100px 20px 20px 20px;
        }
        .custom-bg {
            background-color: #f6f8fa;
        }
        div.ibox-content{
            border: solid 1px #d9d9d9;
            /*box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);*/
            /*margin-top: 20px;*/
            border-radius: 5px;
            /*box-shadow: #3D3D3D;*/
            /*box-shadow: #3D3D3D;*/
        }
        .withUs {
            padding-top: 10px;
            font-size: 12px;
        }
        .form-group {
            padding-left: 0px;
            padding-right: 8px;
        }
        .form-control {
            border-radius: 5px;
        }
        label {
            font-weight: normal;
            color: black;
        }
        ::placeholder {
            font-weight: lighter;
        }
        .signUpForm {
            padding-right: 7px;
            padding-left: 12px;
        }
        .createAccount {
            margin-top: 15px;
        }
    </style>
</head>

<body class="custom-bg">
<div class="signUpColumns animated fadeInDown">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-md-12">
                        <h2 style="color: black; font-weight: normal; text-align: center">Reconnect With Us</h2>
                        <p style="text-align: center">Your credit card on file with Shopify declined. <br>
                            Please reach out to shopify to find out why and how to correct the card.</p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-2.1.1.js"></script>
<!-- Sweet alert -->
<script src="<?php echo base_url() ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo base_url()?>assets/js/functions.js" type="text/javascript" charset="utf-8"></script>
</body>

</html>





