<?php
/**
 * Created by PhpStorm.
 * User: Marketing101
 * Date: 9/20/18
 * Time: 3:27 PM
 */?>

<!DOCTYPE html>
<html>

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-126911005-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-126911005-1');
    </script>
    <!-- End of Google Analytics -->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--    --><?php //include_once(APPPATH."helpers/analyticstracking.php") ?><!--<!-- Google Analylitics -->

    <title>Worry Free Shipping</title>
    <link rel="shortcut icon" href="<?php echo base_url() ?>/assets/img/Favicon.png">
    <script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"></script>
    <script src="<?php echo base_url() ?>assets/js/functionsV1.js" type="text/javascript" charset="utf-8"></script>

    <link href="<?php echo base_url() ?>assets/css/plugins/chosen/chosen.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/css/plugins/iCheck/custom.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/css/animate.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/css/plugins/datapicker/datepicker3.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/css/plugins/dataTables/datatables.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/css/shopify/shopifyTable.css" rel="stylesheet" media="all">
    <!-- Toastr style -->
    <link href="<?php echo base_url() ?>assets/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo base_url()?>assets/js/validationEngine.jquery.css"/>
    <script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.validationEngine.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>

    <!-- Sweet Alert -->
    <link href="<?php echo base_url() ?>assets/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

    <link href="<?php echo base_url() ?>assets/css/plugins/fullcalendar/fullcalendar.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/plugins/fullcalendar/fullcalendar.print.css" rel='stylesheet' media='print'>

    <link href="<?php echo base_url() ?>assets/css/plugins/dropzone/dropzone.css" rel="stylesheet">

    <!-- qTip -->
    <link href="<?php echo base_url() ?>assets/css/plugins/qtip/jquery.qtip.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/plugins/qtip/jquery.qtip.css" rel="stylesheet">

    <!-- Ladda style -->
    <link href="<?php echo base_url() ?>assets/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">
    <!-- orris -->
    <link href="<?php echo base_url() ?>assets/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">

    <!--     <link href="css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet"> -->

    <script type="text/javascript">
        function createShopifyAccount(cost, name, cappedAmount, terms) {
            var c = $('.ladda-button').ladda();
            c.ladda('start');
            $('#planCost').val(cost);
            $('#planName').val(name);
            $('#cappedAmount').val(cappedAmount);
            $('#terms').val(terms);
            var form = document.getElementById('createShopifyAccountForm');
            form.submit();
        }
    </script>

    <style>

        #navLink {
            color: black;
        }
        div.ibox-content {
            border: solid 1px #d9d9d9;
            /*box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);*/
            /*margin-top: 20px;*/
            border-radius: 5px;
            /*box-shadow: #3D3D3D;*/
        }
        div.ibox-footer {
            background-color: #f1f1f1;
            border-radius: 5px;
            border: solid 1px #d9d9d9;
            padding-top: 28px;
            text-align: center;
        }
        .table th {
            text-align: center;
            font-weight: normal;
            border: 0;
        }
        .table td {
            /*font-weight: bold;*/
            /*font-size: 15px;*/
        }
        table tr:first-child td {
            border-top: 0;
        }
        /*a {*/
        /*color: #717171;*/
        /*}*/
        .normalLink {
            font-size: 15px;
            font-weight: normal;
        }
        p {
            margin:-2px 0 -2px 0;
        }
        .buttonLink {
            background:none!important;
            color: steelblue;
            border:none;
            padding:0!important;
            font: inherit;
            /*border is optional*/
            cursor: pointer;
        }
        .explainText {
            padding-top: 10px;
            color: #9a9a9a;
        }
        .titles {
            padding-top: 17px;
        }
        .dataText p {
            margin-bottom: 3px;
        }
        hr {
            border-color: #dcdcdc;
        }
        label {
            font-weight: normal;
        }
        .pricing__tag {
            font-family: -apple-system, BlinkMacSystemFont, San Francisco, Segoe UI, Roboto, Helvetica Neue, sans-serif;
            font-size: 60px;
            color: #212b36;
            font-weight: normal;
            /*margin: 1.6rem 0;*/
        }
        .pricing__tag--small {
            font-size: 16px;
            vertical-align: middle;
            margin: 0px 5px;
        }
        .next-heading--1 {
            font-size: 22px;
            font-weight: 600;
        }
        .ui-heading {
            font-size: 1.9rem;
            font-weight: 500;
            line-height: 2.8rem;
        }
        .tooltip-wordy {

            min-width: 180px;
            white-space: normal;
            line-height: 1.4;
            font-size: 16px;
            margin-top: -10px;
            margin-bottom: 15px;
            font-weight: 300;
        }
        .pricing__description {
            max-width: 250px;
            min-height: 85px;
            margin: 0 auto;
            font-size: 14px;
        }
        .info {
            margin-top: 40px;
        }
        .fa {
            font-size: 14px;
            color: grey;
        }
        .btn-large {
            margin: .5rem 0;
            padding: .8rem 2.4rem;
            font-size: 1.7rem
        }
        .next-heading--3 {
            font-size: 18px;
            padding-bottom: 20px;
            color: #6e6e6e;
        }
        .notPopular {
            padding-top: 82px;
        }

    </style>
</head>

<body class="top-navigation">
<div id="wrapper">
    <div id="page-wrapper" class="gray-bg">
        <div class="wrapper wrapper-content">
            <form method="post" id="createShopifyAccountForm" name="createShopifyAccountForm" action="createShopifyChargeAgain">
                <input id="shopifyStore" name="shopifyStore" type="hidden" value="<?php echo $shopifyStore;?>"/>
                <input id="accessToken" name="accessToken" type="hidden" value="<?php echo $accessToken;?>"/>
                <input id="planCost" name="planCost" type="hidden" value=""/>
                <input id="planName" name="planName" type="hidden" value=""/>
                <input id="cappedAmount" name="cappedAmount" type="hidden" value=""/>
                <input id="terms" name="terms" type="hidden" value=""/>
            </form>
            <div class="row">
                <div class="col-lg-12">
                    <div class="col-lg-10 col-md-10 col-lg-offset-1 col-mg-offset-1">
                        <h1>Pick a plan</h1>
                        <hr>
                        <div class="row">
                            <div class="row">
                                <div class="col-lg-3 col-md-12 notPopular">
                                    <div class="ibox-content img-rounded">
                                        <div class="row">
                                            <div class="text-center">
                                                <p class="pricing__tag">
                                                    <span class="pricing__tag--small">$</span> 40<span class="pricing__tag--small">/month</span>
                                                </p>
                                                <button id="saveBtn" name="saveBtn" class="btn btn-primary btn-large ladda-button" data-style="zoom-out" onclick="createShopifyAccount(40,'Lite', 110, '0.18 for every label over 200')" >Choose this plan</button>
                                                <h2 class="next-heading--1" style="margin-top: 30px; margin-bottom: 20px">LITE</h2>
                                                <p class="pricing__description">
                                                    Start reducing returns and increase productivity with scanning products, putting orders on hold, and choosing the best shipping option.
                                                </p>
                                                <div class="row info">
                                                    <h2 class="ui-heading">Labels</h2>
                                                    <p class="tooltip-wordy">200 <i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="The number of labels included per month. This includes shipping, returns, and non order labels."></i></p>
                                                    <h2 class="ui-heading">Users</h2>
                                                    <p class="tooltip-wordy">1 <i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="How many users are allowed."></i></p>
                                                    <h2 class="ui-heading">Stores</h2>
                                                    <p class="tooltip-wordy">Unlimited <i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="How many shopify stores you can connect."></i></p>
                                                    <h2 class="ui-heading">Additional Labels</h2>
                                                    <p class="tooltip-wordy">18 cents <i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="How much you are charged per label if you go over 1,000 in a month. This includes shipping, returns, and non order labels."></i></p>
                                                    <h2 class="ui-heading">Additional Features</h2>
                                                    <p class="tooltip-wordy"><s>Return labels</s> <i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Create return labels right from the order. Not included at this level"></i></p>
                                                    <p class="tooltip-wordy"><s>Non order labels</s> <i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Create labels for orders that did not process through Shopify. Not included at this level"></i></p>
                                                    <p class="tooltip-wordy">Hold orders <i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Put orders on hold"></i></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class=" col-lg-3 col-md-12 ">
                                    <div class="ibox-footer">
                                        <p class="next-heading--3">Most Popular</p>
                                    </div>
                                    <div class="ibox-content img-rounded">
                                        <div class="row">
                                            <div class="text-center">
                                                <p class="pricing__tag">
                                                    <span class="pricing__tag--small">$</span> 130<span class="pricing__tag--small">/month</span>
                                                </p>
                                                <button id="saveBtn" name="saveBtn" class="btn btn-primary btn-large ladda-button" data-style="zoom-out" onclick="createShopifyAccount(130,'Standard', 140, '0.15 for every label over 1,000')" >Choose this plan</button>
                                                <h2 class="next-heading--1" style="margin-top: 30px; margin-bottom: 20px">STANDARD</h2>
                                                <p class="pricing__description">
                                                    Start reducing returns and increase productivity with scanning products, putting orders on hold, and choosing the best shipping option.
                                                </p>
                                                <div class="row info">
                                                    <h2 class="ui-heading">Labels</h2>
                                                    <p class="tooltip-wordy">1,000 <i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="The number of labels included per month. This includes shipping, returns, and non order labels."></i></p>
                                                    <h2 class="ui-heading">Users</h2>
                                                    <p class="tooltip-wordy">Unlimited <i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="How many users are allowed."></i></p>
                                                    <h2 class="ui-heading">Stores</h2>
                                                    <p class="tooltip-wordy">Unlimited <i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="How many shopify stores you can connect."></i></p>
                                                    <h2 class="ui-heading">Additional Labels</h2>
                                                    <p class="tooltip-wordy">15 cents <i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="How much you are charged per label if you go over 2,500 in a month. This includes shipping, returns, and non order labels."></i></p>
                                                    <h2 class="ui-heading">Additional Features</h2>
                                                    <p class="tooltip-wordy">Return labels <i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Create return labels right from the order"></i></p>
                                                    <p class="tooltip-wordy">Non order labels <i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Create labels for orders that did not process through Shopify"></i></p>
                                                    <p class="tooltip-wordy">Hold orders <i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Put orders on hold"></i></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class=" col-lg-3 col-md-12 notPopular">
                                    <div class="ibox-content img-rounded">
                                        <div class="row">
                                            <div class="text-center">
                                                <p class="pricing__tag">
                                                    <span class="pricing__tag--small">$</span> 250<span class="pricing__tag--small">/month</span>
                                                </p>
                                                <button id="saveBtn" name="saveBtn" class="btn btn-primary btn-large ladda-button" data-style="zoom-out" onclick="createShopifyAccount(250,'Pro',250 ,'0.13 for every label over 2,500')" >Choose this plan</button>
                                                <h2 class="next-heading--1" style="margin-top: 30px; margin-bottom: 20px">PRO</h2>
                                                <p class="pricing__description">
                                                    Start reducing returns and increase productivity with scanning products, putting orders on hold, and choosing the best shipping option.
                                                </p>
                                                <div class="row info">
                                                    <h2 class="ui-heading">Labels</h2>
                                                    <p class="tooltip-wordy">2,500 <i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="The number of labels included per month. This includes shipping, returns, and non order labels."></i></p>
                                                    <h2 class="ui-heading">Users</h2>
                                                    <p class="tooltip-wordy">Unlimited <i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="How many users are allowed."></i></p>
                                                    <h2 class="ui-heading">Stores</h2>
                                                    <p class="tooltip-wordy">Unlimited <i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="How many shopify stores you can connect."></i></p>
                                                    <h2 class="ui-heading">Additional Labels</h2>
                                                    <p class="tooltip-wordy">13 cents <i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="How much you are charged per label if you go over 5,000 in a month. This includes shipping, returns, and non order labels."></i></p>
                                                    <h2 class="ui-heading">Additional Features</h2>
                                                    <p class="tooltip-wordy">Return labels <i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Create return labels right from the order"></i></p>
                                                    <p class="tooltip-wordy">Non order labels <i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Create labels for orders that did not process through Shopify"></i></p>
                                                    <p class="tooltip-wordy">Hold orders <i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Put orders on hold"></i></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class=" col-lg-3 col-md-12 notPopular">
                                    <div class="ibox-content img-rounded">
                                        <div class="row">
                                            <div class="text-center">
                                                <p class="pricing__tag">
                                                    <span class="pricing__tag--small">$</span> 400<span class="pricing__tag--small">/month</span>
                                                </p>
                                                <button id="saveBtn" name="saveBtn" class="btn btn-primary btn-large ladda-button" data-style="zoom-out" onclick="createShopifyAccount(400,'Enterprise',400,'0.11 for every label over 5,000')" >Choose this plan</button>
                                                <h2 class="next-heading--1" style="margin-top: 30px; margin-bottom: 20px">ENTERPRISE</h2>
                                                <p class="pricing__description">
                                                    Start reducing returns and increase productivity with scanning products, putting orders on hold, and choosing the best shipping option.
                                                </p>
                                                <div class="row info">
                                                    <h2 class="ui-heading">Labels</h2>
                                                    <p class="tooltip-wordy">5,000 <i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="The number of labels included per month. This includes shipping, returns, and non order labels."></i></p>
                                                    <h2 class="ui-heading">Users</h2>
                                                    <p class="tooltip-wordy">Unlimited <i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="How many users are allowed."></i></p>
                                                    <h2 class="ui-heading">Stores</h2>
                                                    <p class="tooltip-wordy">Unlimited <i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="How many shopify stores you can connect."></i></p>
                                                    <h2 class="ui-heading">Additional Labels</h2>
                                                    <p class="tooltip-wordy">11 cents <i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="How much you are charged per label if you go over 100 in a month. This includes shipping, returns, and non order labels."></i></p>
                                                    <h2 class="ui-heading">Additional Features</h2>
                                                    <p class="tooltip-wordy">Return labels <i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Create return labels right from the order"></i></p>
                                                    <p class="tooltip-wordy">Non order labels <i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Create labels for orders that did not process through Shopify"></i></p>
                                                    <p class="tooltip-wordy">Hold orders <i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Put orders on hold"></i></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip()
        });
    </script>
    <!-- Mainly scripts -->
    <script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"></script>
    <script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?php echo base_url() ?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/plugins/jeditable/jquery.jeditable.js"></script>

    <script src="<?php echo base_url() ?>assets/js/plugins/dataTables/datatables.min.js"></script>

    <!-- Sweet alert -->
    <script src="<?php echo base_url() ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url() ?>assets/js/inspinia.js"></script>
    <script src="<?php echo base_url() ?>assets/js/plugins/pace/pace.min.js"></script>

    <!-- iCheck -->
    <script src="<? echo base_url()?>assets/js/plugins/iCheck/icheck.min.js"></script>

    <!-- Ladda -->
    <script src="<?php echo base_url() ?>assets/js/plugins/ladda/spin.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.jquery.min.js"></script>

</body>

</html>


