<?php
/**
 * Created by PhpStorm.
 * User: Marketing101
 * Date: 7/16/18
 * Time: 2:34 PM
 */?>

<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Worry Free Shipping - Sign Up</title>
    <link rel="shortcut icon" href="<?php echo base_url() ?>/assets/img/Favicon.png">
    <script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"></script>
    <script src="<?php echo base_url() ?>assets/js/functionsV1.js" type="text/javascript" charset="utf-8"></script>
    <!-- 	<link href="<?php echo base_url() ?>assets/css/plugins/chosen/chosen.css" rel="stylesheet"> -->
    <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
    <!-- Sweet Alert -->
    <link href="<?php echo base_url() ?>assets/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo base_url()?>assets/js/validationEngine.jquery.css"/>
    <script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.validationEngine.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>

    <script type="text/javascript">


    </script>
    <style>

        .signUpColumns {
            max-width: 1100px;
            margin: 0 auto;
            padding: 30px 20px 20px 20px;
        }
        .custom-bg {
            background-color: #f6f8fa;
        }
        div.ibox-content{
            border: solid 1px #d9d9d9;
            /*box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);*/
            /*margin-top: 20px;*/
            border-radius: 5px;
            /*box-shadow: #3D3D3D;*/
        }
        label {
            font-weight: normal;
            color: black;
        }
        ::placeholder {
            font-weight: lighter;
        }
        .createAccount {
            margin-top: 15px;
        }
        h1 {
            font-size: 40px;
        }
        .productImage {
            /*display: block;*/
            max-width:200px;
            max-height:200px;
            display: block;
            margin-left: auto;
            margin-right: auto;
            width: auto;
            height: auto;
            margin-bottom: 20px;
        }
    </style>
</head>

<body class="custom-bg">
<div class="signUpColumns animated fadeInDown">
    <div class="row">
        <img class="productImage" src="<?php echo base_url()?>/assets/img/WorryFreeShippingIcon.png" alt="Product Image" height="200" width="200">
        <div class="col-md-6 col-md-offset-3">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1">
                        <div class="row">
                            <div class="col-md-12">
                                <h1 style="color: black; font-weight: normal; text-align: center; padding-bottom: 25px">Welcome To<br>Worry Free Shipping</h1>
                            </div>
                        </div>
                        <form method="post" id="login_form" name="login_form">
                            <div class="row">
                                <p style="text-align: center">We are glad to have you on board.<br><br>You can always reach out to us with questions at <a href="mailto:onboarding@worryfreeshipping.com">onboarding@worryfreeshipping.com</a>.<br><br>In the future you can go to www.worryfreeshipping.com to access your account or inside of Shopify under the apps section.
                                <br><br>We have two easy steps to get your account up and going.</p>
                            </div>
                            <a class="ladda-button btn btn-primary block full-width m-b createAccount" href="<?php echo base_url()?>fulfillment/importFirstOrder">Get Started</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-2.1.1.js"></script>
<!-- Sweet alert -->
<script src="<?php echo base_url() ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo base_url()?>assets/js/functions.js" type="text/javascript" charset="utf-8"></script>
</body>

</html>



