<?php
/**
 * Created by PhpStorm.
 * User: rapidware
 * Date: 10/1/18
 * Time: 6:13 PM
 */?>
<html>
<head>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-126911005-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-126911005-1');
    </script>
    <!-- End of Google Analytics -->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Worry Free Shipping</title>
    <link rel="shortcut icon" href="<?php echo base_url() ?>/assets/img/Favicon.png">
    <script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"></script>
    <link href="<?php echo base_url() ?>assets/css/shopify/shopifyTable.css" rel="stylesheet" media="all">
    <script src="<?php echo base_url() ?>assets/js/functionsV1.js" type="text/javascript" charset="utf-8"></script>
    <!-- 	<link href="<?php echo base_url() ?>assets/css/plugins/chosen/chosen.css" rel="stylesheet"> -->
    <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
    <!-- Sweet Alert -->
    <link href="<?php echo base_url() ?>assets/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo base_url()?>assets/js/validationEngine.jquery.css"/>
    <script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.validationEngine.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>

    <script type="text/javascript">


    </script>
    <style>
        .signUpColumns {
            max-width: 1100px;
            margin: 0 auto;
            padding: 30px 20px 20px 20px;
        }
        .custom-bg {
            background-color: #f6f8fa;
        }
        label {
            font-weight: normal;
            color: black;
        }
        ::placeholder {
            font-weight: lighter;
        }
        .createAccount {
            margin-top: 15px;
        }
        h1 {
            font-size: 40px;
        }
        .form-control {
            border-radius: 5px;
        }
        .productImage {
            /*display: block;*/
            max-width:200px;
            max-height:200px;
            display: block;
            margin-left: auto;
            margin-right: auto;
            width: auto;
            height: auto;
            margin-bottom: 20px;
        }
    </style>
</head>

<script>
    function checkEnter(e)
    {
        var keyCode = (e.keyCode ? e.keyCode : e.which);
        if(keyCode == 13 )
        {
            e.preventDefault();

            addStartingOrder();
        }
    }
</script>

<body class="custom-bg">
<div class="signUpColumns">
    <div class="row">
        <img class="productImage" src="<?php echo base_url()?>/assets/img/WorryFreeShippingIcon.png" alt="Product Image" height="200" width="200">
        <div class="col-md-6 col-md-offset-3">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1">
                        <div class="row">
                            <div class="col-md-12">
                                <h1 style="color: black; font-weight: normal; text-align: center; padding-bottom: 25px">Import your first order</h1>
                            </div>
                        </div>
                        <h2 style="font-size: 15px; text-align: center; font-weight: 400">To get started, enter the order number of the first order you would like to import.<br><br>
                            Once the first order is imported, any additional imports will only import newer orders.<br><br>
                            For example, if your most recent order is #1200 and you want to import orders from #1150 to #1200, you will enter #1150 below.</h2>
                        <hr style="margin-top: 34px">
                        <form method="post" id="addStoreNumberForm" name="addStoreNumberForm" class="form">
                            <input type="hidden" id="shopifyToken" name="shopifyToken" value="<? echo $shopifyToken?>">
                            <input type="hidden" id="storeName" name="storeName" value="<? echo $storeName?>">
                            <div style="margin-top: 10px" class="col-lg-8 col-md-8">
                                <div style="margin: 10px">
                                    <label>Starting Order Number</label>
                                    <input class="form-control" type="text" id="startingOrderNumber" onkeypress="checkEnter(event);" placeholder="#1001" name="startingOrderNumber" />
                                </div>
                            </div>
                            <button style="margin-top: 43px" type="button" id="connect" name="connect" class="ladda-button btn btn-primary" data-style="zoom-out" onclick="addStartingOrder()">Import Order</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-2.1.1.js"></script>
<!-- Sweet alert -->
<script src="<?php echo base_url() ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo base_url()?>assets/js/functions.js" type="text/javascript" charset="utf-8"></script>

<!-- Ladda -->
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.jquery.min.js"></script>
</body>
</html>
