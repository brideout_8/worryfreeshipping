<?php
/**
 * Created by PhpStorm.
 * User: rapidware
 * Date: 10/7/18
 * Time: 5:14 PM
 */?>

<html>
<head>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-126911005-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-126911005-1');
    </script>
    <!-- End of Google Analytics -->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Worry Free Shipping</title>
    <link rel="shortcut icon" href="<?php echo base_url() ?>/assets/img/Favicon.png">
    <script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"></script>
    <link href="<?php echo base_url() ?>assets/css/shopify/shopifyTable.css" rel="stylesheet" media="all">
    <!--    <script src="--><?php //echo base_url() ?><!--assets/js/functionsV1.js" type="text/javascript" charset="utf-8"></script>-->
    <!-- 	<link href="<?php echo base_url() ?>assets/css/plugins/chosen/chosen.css" rel="stylesheet"> -->
    <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
    <!-- Sweet Alert -->
    <link href="<?php echo base_url() ?>assets/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo base_url()?>assets/js/validationEngine.jquery.css"/>
    <script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.validationEngine.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>

    <script type="text/javascript">

        function connectEndiciaAccount() {
            var base_url  = "<?php echo base_url(); ?>";
            var c = $('.ladda-button').ladda();
            c.ladda('start');
            var formArray = $('#endiciaUserForm').serialize();
            jQuery.post(base_url+'settings/connectEndicia' , formArray, function(data) {
                c.ladda('stop');
                if (data.success) {
                    swal({
                            title: "Account Added",
                            text: "To start using this account, you will need to enable it on in the settings tab under shipping. When you click ok, you will be automatically sent to the page is enable the carrier.",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#0d8ddb",
                            confirmButtonText: "Ok",
                            cancelButtonText: "Close",
                            closeOnConfirm: true,
                            closeOnCancel: true
                        },
                        function (isConfirm) {
                            if (isConfirm) {
                                top.location = base_url+"fulfillment";
                            }
                        });
                } else {
                    swal({
                        title: "Error",
                        text: data.message
                    });
                }
            }, 'json');

            return false;
        }
    </script>
    <style>
        div.ibox-content {
            border: solid 1px #d9d9d9;
            /*box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);*/
            /*margin-top: 20px;*/
            border-radius: 5px;
            /*box-shadow: #3D3D3D;*/
        }
        .custom-bg {
            background-color: #f6f8fa;
        }
        .table th {
            /*text-align: center;*/
            font-weight: normal;
            border: 0;
        }
        .table td {
            /*font-weight: bold;*/
            /*font-size: 15px;*/
        }
        table tr:first-child td {
            border-top: 0;
        }
        /*a {*/
        /*color: #717171;*/
        /*}*/
        .normalLink {
            font-size: 15px;
            font-weight: normal;
        }
        p {
            margin:-2px 0 -2px 0;
        }
        .buttonLink {
            background:none!important;
            color: steelblue;
            border:none;
            padding:0!important;
            font: inherit;
            /*border is optional*/
            cursor: pointer;
        }
        .explainText {
            padding-top: 10px;
            color: #9a9a9a;
        }
        .titles {
            padding-top: 17px;
        }
        .dataText p {
            margin-bottom: 3px;
        }
        hr {
            border-color: #dcdcdc;
        }
        .modal-backdrop {
            background-color: #c7c7c7
        }
        .explainText {
            /*padding-top: 10px;*/
            color: #9a9a9a;
        }
        .ui-select {
            font-size: 13px;
            font-weight: 400;
            line-height: 2.4rem;
            text-transform: initial;
            letter-spacing: initial;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            display: block;
            height: 3.4rem;
            width: 100%;
            padding: 0.4rem 0.8rem;
            padding-right: 2.8rem;
            padding-left: 1.6rem;
            background: #fefefe;
            border: 1px solid #c4cdd5;
            border-radius: 3px;
            max-width: none;
            -webkit-transition-property: background, border, -webkit-box-shadow;
            transition-property: background, border, -webkit-box-shadow;
            transition-property: background, border, box-shadow;
            transition-property: background, border, box-shadow, -webkit-box-shadow;
            -webkit-transition-timing-function: cubic-bezier(0.64, 0, 0.35, 1);
            transition-timing-function: cubic-bezier(0.64, 0, 0.35, 1);
            -webkit-transition-duration: 200ms;
            transition-duration: 200ms
        }

        select.minimal {
            background-image:
                linear-gradient(45deg, transparent 50%, gray 50%),
                linear-gradient(135deg, gray 50%, transparent 50%),
                linear-gradient(to right, #ccc, #ccc);
            background-position:
                calc(100% - 20px) calc(1em + 2px),
                calc(100% - 15px) calc(1em + 2px),
                calc(100% - 2.5em) 0.5em;
            background-size:
                5px 5px,
                5px 5px,
                1px 1.5em;
            background-repeat: no-repeat;
        }
    </style>
</head>

<body class="custom-bg">
<div class="signUpColumns">
    <div class="row">
        <div class="col-lg-12">
            <div class="col-lg-8 col-md-8 col-lg-offset-2 col-mg-offset-2">
                <h1 style="font-weight: bold">Connect Endicia Account</h1>
                <hr>
                <div class="row">
                    <div class="col-lg-4 col-md-4">
                        <h3 class="titles">Connect Account</h3>
                        <!--                        <p class="explainText">The address used to calculate shipping rates and the return address on your labels</p>-->
                    </div>
                    <div class=" col-lg-8 col-md-8">
                        <div class="ibox-content img-rounded">
                            <h3 style="padding-bottom: 8px">Account Information</h3>
                            <form id="endiciaUserForm" name="endiciaUserForm" method="post" class="form-horizontal">
                                <div class="form-group">
                                    <label autocomplete="false" class="col-lg-2 control-label">Account Nickname</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" id="nickname" name="nickname" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label autocomplete="false" class="col-lg-2 control-label">Username or Account Number</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" id="username" name="username" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label autocomplete="false" class="col-lg-2 control-label">Passphrase</label>
                                    <div class="col-lg-10">
                                        <input type="password" class="form-control" id="passphrase" name="passphrase" value="">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="row">
                            <button style="margin-top: 10px" id="createStamps" name="createStamps" class="btn btn-primary pull-right ladda-button" data-style="zoom-out" onclick="connectEndiciaAccount()" >Connect Account</button>
                            <a style="margin-top: 10px; margin-right: 10px" href="<? echo base_url()?>register/connectCarrier" class="btn btn-default pull-right ladda-button"  >Cancel</a>
                        </div>
                            <div class="row">
                            <h4 class="pull-right">Connecting the account can take up to one minute.</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-2.1.1.js"></script>
<!-- Sweet alert -->
<script src="<?php echo base_url() ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo base_url()?>assets/js/functions.js" type="text/javascript" charset="utf-8"></script>

<!-- Ladda -->
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.jquery.min.js"></script>
</body>
</html>


