<?php
/**
 * Created by PhpStorm.
 * User: rapidware
 * Date: 10/7/18
 * Time: 1:54 PM
 */?>
<html>
<head>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-126911005-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-126911005-1');
    </script>
    <!-- End of Google Analytics -->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Worry Free Shipping</title>
    <link rel="shortcut icon" href="<?php echo base_url() ?>/assets/img/Favicon.png">
    <script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"></script>
    <link href="<?php echo base_url() ?>assets/css/shopify/shopifyTable.css" rel="stylesheet" media="all">
    <script src="<?php echo base_url() ?>assets/js/functionsV1.js" type="text/javascript" charset="utf-8"></script>
    <!-- 	<link href="<?php echo base_url() ?>assets/css/plugins/chosen/chosen.css" rel="stylesheet"> -->
    <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
    <!-- Sweet Alert -->
    <link href="<?php echo base_url() ?>assets/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo base_url()?>assets/js/validationEngine.jquery.css"/>
    <script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.validationEngine.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>

    <script type="text/javascript">


    </script>
    <style>
        .signUpColumns {
            max-width: 1100px;
            margin: 0 auto;
            padding: 30px 20px 20px 20px;
        }
        .custom-bg {
            background-color: #f6f8fa;
        }
        label {
            font-weight: normal;
            color: black;
        }
        ::placeholder {
            font-weight: lighter;
        }
        .createAccount {
            margin-top: 15px;
        }
        h1 {
            font-size: 40px;
        }
        .form-control {
            border-radius: 5px;
        }
        .productImage {
            /*display: block;*/
            max-width:200px;
            max-height:200px;
            display: block;
            margin-left: auto;
            margin-right: auto;
            width: auto;
            height: auto;
            margin-bottom: 20px;
        }
        .carrierImage {
            /*display: block;*/
            max-width:180px;
            max-height: 130px;
            width: auto;
            height: auto;
            margin-left: auto;
            margin-right: auto;
            display: block;
        }
    </style>
</head>

<body class="custom-bg">
<div class="signUpColumns">
    <div class="row">
        <img class="productImage" src="<?php echo base_url()?>/assets/img/WorryFreeShippingIcon.png" alt="Product Image" height="200" width="200">
        <div class="col-md-6 col-md-offset-3">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1">
                        <div class="row">
                            <div class="col-md-12">
                                <h1 style="color: black; font-weight: normal; text-align: center; padding-bottom: 25px">Connect a shipping carrier</h1>
                            </div>
                        </div>
                        <h2 style="font-size: 15px; text-align: center; font-weight: 400">We offer a free stamps.com account.</h2>
                        <a style="display: block; margin-left: auto; margin-right: auto" href="<? echo base_url()?>register/stampsSignUp" class="btn btn-primary">Connect free Stamps account</a>
                        <hr style="margin-top: 15px">
                        <h2 style="font-size: 15px; text-align: center; font-weight: 400; margin-top: 15px;">Or connect your own account.</h2>
                        <form id="addCarrierForm" name="addCarrierForm" method="post" class="form-horizontal">
                            <input type="hidden" id="carrierId" name="carrierId" value="" />
                            <div class="row">
                                <a href="<? echo base_url()?>register/addEndicia">
                                    <img class="carrierImage" src="<? echo base_url()?>/assets/img/endicia-vector-logo.png"/>
                                </a>
                            </div>
                            <div style="margin-top: 15px" class="row">
                                <a href="<? echo base_url()?>register/addUps">
                                    <img class="carrierImage" src="<? echo base_url()?>/assets/img/ups.png"/>
                                </a>
                            </div>
                            <div style="margin-top: 30px" class="row">
                                <a href="<? echo base_url()?>register/addFedex">
                                    <img class="carrierImage" src="<? echo base_url()?>/assets/img/fedex.png"/>
                                </a>
                            </div>
                            <div style="margin-bottom: -32px" class="row">
                                <a href="<? echo base_url()?>register/addStamps">
                                    <img class="carrierImage" src="<? echo base_url()?>/assets/img/stamps_com.png" />
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <a style="margin-top: 15px" class="btn btn-primary pull-right" href="<? echo base_url()?>/fulfillment">Set up later</a>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-2.1.1.js"></script>
<!-- Sweet alert -->
<script src="<?php echo base_url() ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo base_url()?>assets/js/functions.js" type="text/javascript" charset="utf-8"></script>

<!-- Ladda -->
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.jquery.min.js"></script>
</body>
</html>


