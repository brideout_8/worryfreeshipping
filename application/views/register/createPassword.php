<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Worry Free Shipping</title>
<!--    --><?php //include_once(APPPATH."helpers/analyticstracking.php") ?><!--<!-- Google Analylitics -->

    <script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"></script>
    <script src="<?php echo base_url() ?>assets/js/functionsLandingV1.js" type="text/javascript" charset="utf-8"></script>

    <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo base_url() ?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
    
    <link rel="stylesheet" href="<?php echo base_url()?>assets/js/validationEngine.jquery.css"/>
	<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.validationEngine.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
    <!-- Sweet Alert -->
    <link href="<?php echo base_url() ?>assets/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    
<script type="text/javascript">
	
	$(function () {
			var $switcher = $(".signup-switcher a");
			$switcher.click(function (e) {
				e.preventDefault();
				$switcher.removeClass("active");
				$(this).addClass("active");
				$("body").attr("class", $(this).data("class"));
			});
		});

function checkEnter(e)
{
	var keyCode = (e.keyCode ? e.keyCode : e.which);
	if(keyCode == 13 )
	{
   		e.preventDefault();
	 	changepassword();
	}
}


</script>

    <style>
        #productImage {
            /*display: block;*/
            max-width:200px;
            max-height:400px;
            width: auto;
            height: auto;
            display:block;
            margin:auto;
            position: relative;
            top: 50px;
        }
        /*#photo{*/
            /*display:block;*/
            /*margin:auto;*/
            /*position: relative;*/
            /*top: 50px;*/
        /*}*/
        .middle-box {
            margin-top: 50px;
        }
    </style>

</head>

<body class="gray-bg">
<div class="loginscreen animated fadeInDown row">
    <img id="productImage" src="<?php echo base_url() ?>assets/img/WorryFreeShipping.png" alt="Logo" />
</div>
    <div class="passwordBox animated fadeInDown">
        <div class="row">
            <div class="col-md-12">
                <div class="ibox-content">
                    <h2 class="font-bold">Create A Password</h2>
                    <hr>
                    <form name="setPasswordForm" id="setPasswordForm" class="form-signin" method="post" action="">
	                    <div class="form-group">
                            <input type="hidden" id="setPasswordToken" name="setPasswordToken" value="<?php echo @$token; ?>" />
			                <div class="fields">
				                <strong>New Password</strong>
				                <input class="form-control" type="password" placeholder="Password" id="password" name="password" />
			                </div><br>
                            <div class="fields">
				                <strong>Confirm Password</strong>
				                <input class="form-control" type="password" placeholder="Confirm Password" id="passwordMatch" name="passwordMatch" />
			                </div><br>
			                <div class="actions">
				            <input type="button" class="btn btn-primary block full-width m-b" id="changeBtn" name="changeBtn" value="Set Password"  onClick="setPassword();return false;"/>
			                </div>
	                    </div>
		            </form>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-6">
                Worry Free Shipping
            </div>
            <div class="col-md-6 text-right">
               <small>© 2018</small>
            </div>
        </div>
    </div>
</body>
</html>

<!-- Sweet alert -->
<script src="<?php echo base_url() ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>