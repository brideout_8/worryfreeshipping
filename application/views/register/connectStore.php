<?php
/**
 * Created by PhpStorm.
 * User: Marketing101
 * Date: 10/3/18
 * Time: 3:12 PM
 */?>

<html>
<head>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-126911005-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-126911005-1');
    </script>
    <!-- End of Google Analytics -->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Worry Free Shipping - Sign Up</title>

    <script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"></script>
    <script src="<?php echo base_url() ?>assets/js/functions.js" type="text/javascript" charset="utf-8"></script>
    <link href="<?php echo base_url() ?>assets/css/shopify/shopifyTable.css" rel="stylesheet" media="all">
    <!-- 	<link href="<?php echo base_url() ?>assets/css/plugins/chosen/chosen.css" rel="stylesheet"> -->
    <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
    <!-- Sweet Alert -->
    <link href="<?php echo base_url() ?>assets/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo base_url()?>assets/js/validationEngine.jquery.css"/>
    <script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.validationEngine.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>

    <script type="text/javascript">

        function checkEnter(e) {
            var keyCode = (e.keyCode ? e.keyCode : e.which);
            if(keyCode == 13 ) {
                e.preventDefault();
                connectToStore();
            }
        }

        function connectToStore() {
            // var formArray = $('#createShopifyAccountForm').serialize();
            var base_url  = "<?php echo base_url(); ?>";
            var c = $('.ladda-button').ladda();
            c.ladda('start');
            var shopName = $("#shopName").val();
            if(shopName != "") {
                c.ladda('stop');
                top.location = base_url + "register/newPayingStore?shop="+shopName;
            } else {
                c.ladda('stop');
                swal({
                    title: "Error",
                    text: "Please enter a store name"
                });
            }
        }
    </script>
    <style>
        img {
            max-width: 60%;
            max-height: 60%;
            display: block;
            margin-left: auto;
            margin-right: auto
        }

        .signUpColumns {
            max-width: 1100px;
            margin: 0 auto;
            padding: 100px 20px 20px 20px;
        }
        .custom-bg {
            background-color: #f6f8fa;
        }
        div.ibox-content {
            border: solid 1px #d9d9d9;
            /*box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);*/
            /*margin-top: 20px;*/
            border-radius: 5px;
            /*box-shadow: #3D3D3D;*/
        }
        .withUs {
            padding-top: 5px;
            padding-bottom: 5px;
            font-size: 15px;
        }
        .form-group {
            padding-left: 0px;
            padding-right: 8px;
        }
        .form-control {
            border-radius: 5px;
        }
        label {
            font-weight: normal;
            color: black;
        }
        ::placeholder {
            font-weight: lighter;
        }
        .signUpForm {
            padding-right: 7px;
            padding-left: 12px;
        }
        .createAccount {
            margin-top: 15px;
        }
        h1 {
            font-size: 40px;
        }
    </style>
</head>

<body class="custom-bg">
<div class="signUpColumns">
    <div class="row">
        <div class="col-md-6 animated fadeInDown">
            <img src="<? echo base_url()?>/assets/img/WorryFreeShipping.png">
        </div>
        <div class="col-md-6">
            <div style="margin-top: 45px" class="ibox-content">
                <div class="row">
                    <div class="col-md-12">
                        <h1 style="color: black; font-weight: normal; text-align: center">Reunite With Us</h1>
                    </div>
                    <div class="row">
                        <div style="font-weight: lighter" class="col-md-12">
<!--                            <p class="withUs" style="text-align: center"> Already with us? <a href="--><?php //echo base_url()?><!--login">Login</a></p>-->
                        </div>
                        <div class="col-md-8 col-md-offset-2">
                            <p class="withUs" style="text-align: center">The store you were being billed with was deleted from your Shopify Admin account. You will need to add the monthly billing to a new store to access your account.</p>
                        </div>
                    </div>
                </div>
                <form method="post" id="newPayingStoreForm" name="newPayingStoreFormForm" action="newPayingStore">
                    <div class="row signUpForm">
                        <div class="form-group col-md-12">
                            <label>Shopify Store Name</label>
                            <select id="shopName" name="shopName" class="form-control minimal ui-select">
                                <?php foreach ($stores as $store) {
                                    if($store->get("status") != "inactive") { ?>
                                        <option value="<? echo $store->get("shopifyName")?>"><? echo $store->get("shopifyName") ?></option>
                                    <?}?>
                                <? } ?>
                            </select>
                        </div>
                    </div>
                    <button type="button" data-style="zoom-out" class="ladda-button btn btn-primary block full-width m-b createAccount" onclick="connectToStore()">Select plan</button>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-2.1.1.js"></script>
<!-- Sweet alert -->
<script src="<?php echo base_url() ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo base_url()?>assets/js/functions.js" type="text/javascript" charset="utf-8"></script>
<!-- Ladda -->
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.jquery.min.js"></script>
</body>

</html>
