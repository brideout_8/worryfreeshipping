<?php
/**
 * Created by PhpStorm.
 * User: rapidware
 * Date: 7/13/18
 * Time: 1:23 PM
 */?>

<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Worry Free Shipping - Sign Up</title>

    <script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"></script>
    <script src="<?php echo base_url() ?>assets/js/stripeCheckout.js"></script>
    <script src="<?php echo base_url() ?>assets/js/functions.js" type="text/javascript" charset="utf-8"></script>
    <script src="https://js.stripe.com/v3/"></script>
    <!-- 	<link href="<?php echo base_url() ?>assets/css/plugins/chosen/chosen.css" rel="stylesheet"> -->
    <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
    <!-- Sweet Alert -->
    <link href="<?php echo base_url() ?>assets/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo base_url()?>assets/js/validationEngine.jquery.css"/>
    <script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.validationEngine.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>

    <script type="text/javascript">


        function checkEnter(e)
        {
            var keyCode = (e.keyCode ? e.keyCode : e.which);
            if(keyCode == 13 )
            {
                e.preventDefault();

                login();
            }
        }
    </script>
    <style>
        img {
            max-width: 60%;
            max-height: 60%;
            display: block;
            margin-left: auto;
            margin-right: auto
        }

        .signUpColumns {
            max-width: 1100px;
            margin: 0 auto;
            padding: 100px 20px 20px 20px;
        }
        .custom-bg {
            background-color: #f6f8fa;
        }
        div.ibox-content{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            /*box-shadow: #3D3D3D;*/
        }
        .withUs {
            padding-top: 10px;
            font-size: 12px;
        }
        .form-group {
            padding-left: 0px;
            padding-right: 8px;
        }
        .form-control {
            border-radius: 5px;
        }
        label {
            font-weight: normal;
            color: black;
        }
        ::placeholder {
            font-weight: lighter;
        }
        .signUpForm {
            padding-right: 7px;
            padding-left: 12px;
        }
        .createAccount {
            margin-top: 15px;
        }
        .StripeElement {
            background-color: white;
            height: 40px;
            padding: 10px 12px;
            border-radius: 5px;
            /*border: 1px solid;*/
            border: 1px solid #E5E6E7;
            /*box-shadow: 0 1px 3px 0 #e6ebf1;*/
            -webkit-transition: box-shadow 150ms ease;
            transition: box-shadow 150ms ease;
        }

        .StripeElement--focus {
            box-shadow: 0 1px 3px 0 #cfd7df;
        }

        .StripeElement--invalid {
            border-color: #fa755a;
        }

        .StripeElement--webkit-autofill {
            background-color: #fefde5 !important;
        }
    </style>
</head>

<body class="custom-bg">
    <div class="signUpColumns animated fadeInDown">
        <div class="row">
            <div class="col-md-6">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-6">
                            <h2 style="color: black; font-weight: normal">Unite With Us</h2>
                        </div>
                        <div style="font-weight: lighter" class="col-md-6">
                            <p class="pull-right withUs"> Already with us? <a href="<?php echo base_url()?>login">Login</a></p>
                        </div>
                    </div>
                    <form method="post" id="payment-form">
                        <div class="row signUpForm">
                            <div class="form-group col-md-6">
                                <label>Company Name</label>
                                <input class="form-control" type="text" id="companyName" placeholder="Company Name" name="companyName" />
                            </div>
                            <div class="form-group col-md-6">
                                <label>Phone Number</label>
                                <input class="form-control" onkeypress="checkEnter(event);" type="text" placeholder="Phone Number" name="phoneNumber" id="phoneNumber" />
                            </div>
                            <div class="form-group col-md-6">
                                <label>Full Name</label>
                                <input class="form-control" type="text" id="fullName" placeholder="Full Name" name="fullName" />
                            </div>
                            <div class="form-group col-md-6">
                                <label>Email</label>
                                <input class="form-control" onkeypress="checkEnter(event);" type="email" placeholder="Email" name="email" id="email" />
                            </div>
                            <div class="form-group col-md-6">
                                <label>Password</label>
                                <input class="form-control" type="password" id="password" name="password" />
                            </div>
                        </div>
<!--                        <div class="form-row">-->
<!--                            <label for="card-element">-->
<!--                                Card Information-->
<!--                            </label>-->
<!--                            <div id="card-element">-->
<!--                                <!-- A Stripe Element will be inserted here. -->
<!--                            </div>-->
<!---->
<!--                            <!-- Used to display form errors. -->
<!--                            <div id="card-errors" role="alert"></div>-->
<!--                        </div>-->
                        <button type="submit" class="btn btn-primary block full-width m-b createAccount">Create Account</button>
                    </form>
<!--                    <form action="/charge" method="post" class="m-t" id="payment-form">-->
<!--                        <div class="form-row">-->
<!--                            <div class="row signUpForm">-->
<!--                                <div class="form-group col-md-6">-->
<!--                                    <label>Company Name</label>-->
<!--                                    <input class="form-control" type="text" id="companyName" placeholder="Company Name" name="companyName" />-->
<!--                                </div>-->
<!--                                <div class="form-group col-md-6">-->
<!--                                    <label>Phone Number</label>-->
<!--                                    <input class="form-control" onkeypress="checkEnter(event);" type="text" placeholder="Phone Number" name="phoneNumber" id="phoneNumber" />-->
<!--                                </div>-->
<!--                                <div class="form-group col-md-6">-->
<!--                                    <label>Full Name</label>-->
<!--                                    <input class="form-control" type="text" id="fullName" placeholder="Full Name" name="fullName" />-->
<!--                                </div>-->
<!--                                <div class="form-group col-md-6">-->
<!--                                    <label>Email</label>-->
<!--                                    <input class="form-control" onkeypress="checkEnter(event);" type="email" placeholder="Email" name="email" id="email" />-->
<!--                                </div>-->
<!--                                <div class="form-group col-md-6">-->
<!--                                    <label>Password</label>-->
<!--                                    <input class="form-control" type="password" id="password" name="password" />-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="row signUpForm">-->
<!--                                <div class="form-group col-md-6">-->
<!--                                    <label>Credit Card Number</label>-->
<!--                                    <input class="form-control" onkeypress="checkEnter(event);" type="text" placeholder="Credit Card Number" name="ccnumber" id="ccnumber" />-->
<!--                                </div>-->
<!--                                <div class="form-group col-md-3">-->
<!--                                    <label>CVV</label>-->
<!--                                    <input class="form-control" type="text" id="cvv" placeholder="CVV" name="cvv" />-->
<!--                                </div>-->
<!--                                <div class="form-group col-md-6">-->
<!--                                    <label>Name On Credit Card</label>-->
<!--                                    <input class="form-control" onkeypress="checkEnter(event);" type="text" placeholder="Name On Credit Card" name="nameCard" id="nameCard" />-->
<!--                                </div>-->
<!--                                <div class="form-group col-md-6">-->
<!--                                    <label>CVV</label>-->
<!--                                    <input class="form-control" type="text" id="cvv" placeholder="CVV" name="cvv" />-->
<!--                                </div>-->
<!--                            </div>-->
<!---->
<!--                            <!-- Used to display form errors -->
<!--                            <div id="card-errors" role="alert"></div>-->
<!--                        </div>-->
<!---->
<!--<!--                        <input type="submit" class="submit" value="Submit Payment">-->
<!--                        <input type="submit" id="loginButton" name="loginButton" value = "Submit Payment" class="btn btn-primary block full-width m-b">-->
<!--                    </form>-->
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-2.1.1.js"></script>
<!-- Sweet alert -->
<script src="<?php echo base_url() ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo base_url()?>assets/js/functions.js" type="text/javascript" charset="utf-8"></script>
</body>

</html>

