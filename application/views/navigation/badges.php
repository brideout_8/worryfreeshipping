<?php
/**
 * Created by PhpStorm.
 * User: Marketing101
 * Date: 9/12/18
 * Time: 6:47 AM
 */?>

<? if($badge == "shipPack") {?>
    <a href="<?php echo base_url()?>fulfillment/orderOverview/<?php echo $storeName ?>/<?php echo $orderNumber?>">< #<?php echo $orderNumber?></a>
<?} else {?>
    <a href="<?php echo base_url()?>fulfillment">< Orders</a>
<?}?>
<h1 class="tourBadges">#<? echo $orderNumber;?><span class="dateTime"> <?php $dateNow = new DateTime($orderInfo->get('dateOrderedTime'), new DateTimeZone("America/Chicago")); echo $dateNow->format('M d Y h:ia');?>
        <!--Payment Status Badge-->
        <?php if($orderInfo->get("financialStatus") == "paid") {?>
            <span class="badge badge-primary"> Paid </span>
        <?php } else if($orderInfo->get("financialStatus") == "pending") {?>
            <span class="badge badge-warning"> Pending </span>
        <?php } else if($orderInfo->get("financialStatus") == "authorized") { ?>
            <span class="badge badge-warning"> Authorized </span>
        <?php } else if ($orderInfo->get("financialStatus") == "partially_paid") { ?>
            <span class="badge badge-warning"> Partially Paid </span>
        <?php } else if($orderInfo->get("financialStatus") == "partially_refunded") { ?>
            <span class="badge badge-warning"> Partially Refunded </span>
        <?php } else if($orderInfo->get("financialStatus") == "refunded") { ?>
            <span class="badge badge-danger"> Refunded </span>
        <?php } else if($orderInfo->get("financialStatus") == "voided") { ?>
            <span class="badge badge-danger"> Voided </span>
        <?php } ?>
        <!-- Packed Status Badge -->
        <?php if($orderInfo->get("packed") == "2") {?>
            <span class="badge badge-primary"> Picked </span>
        <?php } else if($orderInfo->get("packed") == "1") {?>
            <span class="badge badge-warning"> Partially Picked </span>
        <?php } else { ?>
            <span class="badge badge-danger"> Not Picked </span>
        <?php } ?>
        <!--Shipped Status Badge-->
        <?php if($orderInfo->get("shipped") == "2") {?>
            <span class="badge badge-primary"> Shipped </span>
        <?php } else if($orderInfo->get("shipped") == "1") {?>
            <span class="badge badge-warning"> Partially Shipped </span>
        <?php } else { ?>
            <span class="badge badge-danger"> Not Shipped </span>
        <?php } ?>
        <!--Hold Status Badge-->
        <?php if($orderInfo->get("hold") == "2") {?>
            <span class="badge badge-danger"> On Hold </span>
        <?php } ?>
        <!--Archive Status Badge-->
        <?php if($orderInfo->get("archived") == "2") {?>
            <span class="badge badge-primary"> Archived </span>
        <?php } ?>
    </span>
</h1>
