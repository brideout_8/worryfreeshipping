<?php
/**
 * Created by PhpStorm.
 * User: rapidware
 * Date: 9/26/18
 * Time: 8:24 PM
 */?>
<div class="footer fixed-footer gray-bg">
    <form id="findOrderForm" name="findOrderForm" method="post" class="form">
        <div class="col-lg-1">
            <label style="background-color: transparent; border-color: transparent;font-weight: bold" class="form-control" >Quick Find</label>
        </div>
        <div class="col-lg-4">
            <input type="text" autocomplete="off" id="orderNumber" name="orderNumber" placeholder="Order Number" class="form-control" value=""/>
        </div>
        <div class="col-lg-5">
            <select id="storeName" name="storeName" class="form-control minimal ui-select">
                <?php foreach ($this->session->shopifyStoresInfo as $store) {?>
                    <option value="<? echo $store["shopifyName"]?>"><? if($store["shopifyDisplayName"] != null) { echo $store["shopifyDisplayName"];} else { echo $store["shopifyName"];} ?></option>
                <? } ?>
            </select>
        </div>
        <input type="button" class="ladda-button btn btn-primary" data-style="zoom-out" id="findOrder" value="Find Order" name="findOrder" onclick="quickOrderFind()"/>
    </form>
</div>
