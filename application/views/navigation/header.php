<!DOCTYPE html>
<html>

<head>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-126911005-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-126911005-1');
    </script>
    <!-- End of Google Analytics -->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--    --><?php //include_once(APPPATH."helpers/analyticstracking.php") ?><!--<!-- Google Analylitics -->

    <title>Worry Free Shipping</title>
    <link rel="shortcut icon" href="<?php echo base_url() ?>/assets/img/Favicon.png">
    
    <script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"></script>
    <script src="<?php echo base_url() ?>assets/js/functionsV1.js" type="text/javascript" charset="utf-8"></script>

    <!-- Bootstrap Tour -->
    <link href="<?php echo base_url() ?>assets/css/plugins/bootstrapTour/bootstrap-tour.min.css" rel="stylesheet">

	<link href="<?php echo base_url() ?>assets/css/plugins/chosen/chosen.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/css/plugins/iCheck/custom.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/css/animate.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/css/plugins/datapicker/datepicker3.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/css/plugins/dataTables/datatables.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/css/shopify/shopifyTable.css" rel="stylesheet" media="all">
    <!-- Toastr style -->
    <link href="<?php echo base_url() ?>assets/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo base_url()?>assets/js/validationEngine.jquery.css"/>
	<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.validationEngine.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
    
    <!-- Sweet Alert -->
    <link href="<?php echo base_url() ?>assets/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <!-- EnjoyHint -->
    <link href="<?php echo base_url() ?>assets/css/plugins/enjoyhint/enjoyhint.css" rel="stylesheet">
    
    <link href="<?php echo base_url() ?>assets/css/plugins/fullcalendar/fullcalendar.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/plugins/fullcalendar/fullcalendar.print.css" rel='stylesheet' media='print'>

    <!-- Dropzone -->
    <link href="<?php echo base_url() ?>assets/css/plugins/dropzone/dropzone.css" rel="stylesheet">

    <!-- qTip -->
    <link href="<?php echo base_url() ?>assets/css/plugins/qtip/jquery.qtip.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/plugins/qtip/jquery.qtip.css" rel="stylesheet">

    <!-- Ladda style -->
    <link href="<?php echo base_url() ?>assets/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">
    <!-- orris -->
    <link href="<?php echo base_url() ?>assets/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">

<!--     <link href="css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet"> -->

<body class="top-navigation">

<div id="wrapper">
    <div id="page-wrapper" class="gray-bg">

        <?php if($this->session->userdata("role") == "SuperAdmin" || $this->session->userdata("role") == "Admin") { ?>
            <div class="row border-bottom white-bg">
                <nav class="navbar navbar-static-top" role="navigation">
                    <div class="navbar-header">
                        <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                            <i class="fa fa-reorder"></i>
                        </button>
                        <a href="#" class="navbar-brand">Worry Free Shipping</a>
                    </div>
                    <div class="navbar-collapse collapse" id="navbar">
                        <ul class="nav navbar-nav">
    <!--                        <li class="active">-->
    <!--                            <a aria-expanded="false" role="button" href="layouts.html"> Back to main Layout page</a>-->
    <!--                        </li>-->
                            <li class="dropdown">
                                <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"> Menu item <span class="caret"></span></a>
                                <ul role="menu" class="dropdown-menu">
                                    <li><a href="">Menu item</a></li>
                                    <li><a href="">Menu item</a></li>
                                    <li><a href="">Menu item</a></li>
                                    <li><a href="">Menu item</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"> Menu item <span class="caret"></span></a>
                                <ul role="menu" class="dropdown-menu">
                                    <li><a href="">Menu item</a></li>
                                    <li><a href="">Menu item</a></li>
                                    <li><a href="">Menu item</a></li>
                                    <li><a href="">Menu item</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"> Menu item <span class="caret"></span></a>
                                <ul role="menu" class="dropdown-menu">
                                    <li><a href="">Menu item</a></li>
                                    <li><a href="">Menu item</a></li>
                                    <li><a href="">Menu item</a></li>
                                    <li><a href="">Menu item</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"> Menu item <span class="caret"></span></a>
                                <ul role="menu" class="dropdown-menu">
                                    <li><a href="">Menu item</a></li>
                                    <li><a href="">Menu item</a></li>
                                    <li><a href="">Menu item</a></li>
                                    <li><a href="">Menu item</a></li>
                                </ul>
                            </li>

                        </ul>
                        <ul class="nav navbar-top-links navbar-right">
                            <li>
                                <?php echo $this->session->userdata["name"] .' '. $this->session->userdata["role"]; ?>
                            </li>
                            <li>
                                <a href="<?php echo base_url();?>login/logout">
                                    <i class="fa fa-sign-out"></i> Log out
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        <?php } else if ($this->session->userdata("role") == "user") { ?>
            <div class="row border-bottom white-bg">
                <nav class="navbar navbar-static-top" role="navigation">
                    <div class="navbar-header">
                        <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                            <i class="fa fa-reorder"></i>
                        </button>
                        <a href="#" class="navbar-brand">Worry Free Shipping</a>
                    </div>
                    <div class="navbar-collapse collapse" id="navbar">
                        <ul class="nav navbar-nav">
                            <? if(in_array("pickOrders", $this->session->userdata("permissions")) || in_array("shipOrders", $this->session->userdata("permissions")) || in_array("voidLabels", $this->session->userdata("permissions")) || in_array("fullAccess", $this->session->userdata("permissions")) || in_array("nonOrders", $this->session->userdata("permissions")) || in_array("importOrders", $this->session->userdata("permissions")) || in_array("archiveOrders", $this->session->userdata("permissions"))) {?>
                                <li <?php if(isset($menu) && $menu=='fulfillment') { ?>class="active" <?php } ?>>
                                    <a id="navLink" href="<?=base_url();?>fulfillment">Fulfillment</a>
                                </li>
                            <?php } ?>
                            <? if(in_array("inventoryLevels", $this->session->userdata("permissions"))) { ?>
                                <li <?php if(isset($menu) && $menu=='Inventory') { ?>class="active" <?php } ?>>
                                    <a id="navLink" href="<?=base_url();?>inventory">Inventory Levels</a>
                                </li>
                            <? } ?>
                            <? if(in_array("createOrderReturns", $this->session->userdata("permissions")) || in_array("fullAccess", $this->session->userdata("permissions")) || in_array("createGeneralReturns", $this->session->userdata("permissions")) || in_array("receiveReturns", $this->session->userdata("permissions")) || in_array("archiveReturns", $this->session->userdata("permissions"))) {?>
                                <li><?php if(isset($menu) && $menu=='returnsManager') { ?><li class="active"> <?php } ?>
                                    <a id="navLink" href="<?php base_url() ?>/returns">Returns Manager</a>
                                </li>
                            <?}?>
                            <li><?php if(isset($menu) && $menu=='profile') { ?><li class="active"> <?php } ?>
                                <a id="navLink" href="<?php base_url() ?>/client/profile">Profile</a>
                            </li>
                            <? if(in_array("addUsers", $this->session->userdata("permissions")) || in_array("addStores", $this->session->userdata("permissions")) || in_array("fullAccess", $this->session->userdata("permissions")) || in_array("editStores", $this->session->userdata("permissions")) || in_array("addPackages", $this->session->userdata("permissions")) || in_array("editPackages", $this->session->userdata("permissions")) || in_array("changeShippingLabels", $this->session->userdata("permissions"))) {?>
                                <li <?php if(isset($menu) && $menu=="stores" || $menu=="account" || $menu=="billing" || $menu=="shipping" || $menu=="returns") { ?>class="active dropdown" <?php } else { ?> class="dropdown" <? } ?>>
                                    <a id="navLink" aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown">Settings<span class="caret"></span></a>
                                    <ul role="menu" class="dropdown-menu">
                                        <? if(in_array("addUsers", $this->session->userdata("permissions")) || in_array("fullAccess", $this->session->userdata("permissions")) ) { ?>
                                            <li <?php if(isset($menu) && $menu=='account') { ?>class="active" <?php } ?>>
                                                <a id="navLink" href="<?=base_url();?>settings/account">Account</a>
                                            </li>
                                        <?}?>
                                        <? if(in_array("addStores", $this->session->userdata("permissions")) || in_array("fullAccess", $this->session->userdata("permissions")) || in_array("editStores", $this->session->userdata("permissions")) ) { ?>
                                            <li <?php if(isset($menu) && $menu=='stores') { ?>class="active" <?php } ?>>
                                                <a id="navLink" href="<?=base_url();?>settings/stores">Stores</a>
                                            </li>
                                        <?}?>
                                        <? if(in_array("addPackages", $this->session->userdata("permissions")) || in_array("fullAccess", $this->session->userdata("permissions")) || in_array("editPackages", $this->session->userdata("permissions"))|| in_array("changeShippingLabels", $this->session->userdata("permissions")) ) { ?>
                                            <li <?php if(isset($menu) && $menu=='shipping') { ?>class="active" <?php } ?>>
                                                <a id="navLink" href="<?=base_url();?>settings/shipping">Shipping</a>
                                            </li>
                                        <?}?>
                                        <? if(in_array("fullAccess", $this->session->userdata("permissions")) ) { ?>
                                            <li <?php if(isset($menu) && $menu=='billing') { ?>class="active" <?php } ?>>
                                                <a id="navLink" href="<?=base_url();?>settings/billing">Billing</a>
                                            </li>
                                            <li <?php if(isset($menu) && $menu=='returnsSettings') { ?>class="active" <?php } ?>>
                                                <a id="navLink" href="<?=base_url();?>settings/returns">Returns</a>
                                            </li>
                                        <?}?>
                                    </ul>
                                </li>
                            <?}?>
                            <li <?php if(isset($menu) && $menu=="support" || $menu=="faq" ) { ?>class="active dropdown" <?php } else { ?> class="dropdown" <? } ?>>
                                <a id="navLink" aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown">Support Center<span class="caret"></span></a>
                                <ul role="menu" class="dropdown-menu">
                                    <li <?php if(isset($menu) && $menu=='support') { ?>class="active" <?php } ?>>
                                        <a id="navLink" href="<?=base_url();?>support">Support Tickets</a>
                                    </li>
                                    <li <?php if(isset($menu) && $menu=='faq') { ?>class="active" <?php } ?>>
                                        <a id="navLink" href="<?=base_url();?>support/faq">FAQ</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="nav navbar-top-links navbar-right">
                            <li>
                                <?php echo $this->session->name; ?>
                            </li>
                            <li>
                                <a id="navLink" href="<?php echo base_url();?>login/logout">
                                    <i class="fa fa-sign-out"></i> Log out
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        <?php } ?>