<?php
/**
 * Created by PhpStorm.
 * User: Marketing101
 * Date: 12/17/17
 * Time: 12:53 PM
 */?>

<style>
    div.ibox-content{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        /*box-shadow: #3D3D3D;*/
    }
    #title {
        text-align: center;
        color: green;
    }
    p {
        text-align: center;
    }
    #inputOrder {
        text-align: center;
        margin-top: 20px;
    }
    .bottomBtn {
        margin-top: 15px;
    }
    fieldset{
        margin-top: 15px;
    }
    #actionButtons {
        margin-top: 15px;
    }
    h2 {
        text-align: center;
    }
</style>
</head>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <h1><b>Return Label</b></h1>
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form class="form-horizontal" id="shipForm" name="shipForm" method="post">
                        <input type="hidden" id="location" name="location" value="domestic"/>
                        <h1 id="title"><b>Return Label</b></h1>
                        <fieldset>
                            <legend>Customer Info</legend>
                            <div id="packageInfo" class="col-lg-8 col-lg-offset-2">
                                <div class="form-group">
                                    <label autocomplete="false" class="col-lg-2 control-label">Ship Date</label>
<!--                                    <div class="col-lg-9">-->
<!--                                        <input name="dateShipped" id="dateShipped" data-provide="datepicker">-->
<!--                                    </div>-->
                                    <div class="input-group date col-lg-10" id="datepicker">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" id="dateShipped" name="dateShipped" value="<? echo date("m/d/Y");?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label autocomplete="false" class="col-lg-2 control-label">First Name</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" id="firstName" name="firstName" value="">
                                    </div>
                                    <label autocomplete="false" class="col-lg-2 control-label">Last Name</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" id="lastName" name="lastName" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label autocomplete="false" class="col-lg-2 control-label">Company Name</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" id="companyName" name="companyName">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label autocomplete="false" class="col-lg-2 control-label">Address</label>
                                    <div class="col-lg-7">
                                        <input type="text" class="form-control" id="address1" name="address1" value="">
                                    </div>
                                    <div class="col-lg-3">
                                        <input type="text" class="form-control" id="address2" name="address2" value="">
                                    </div>
                                </div>
    <!--                            <div class="form-group">-->
    <!--                                <label autocomplete="false" class="col-lg-2 control-label">Address 2</label>-->
    <!--                                <div class="col-lg-9">-->
    <!--                                    <input type="text" class="form-control" id="address2" name="address2">-->
    <!--                                </div>-->
    <!--                            </div>-->
                                <div class="row form-group">
                                    <label style="font-size:14px; font-weight: bolder;" for="inputLast" class="col-md-2 control-label">City</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" id="city" name="city" value="">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label style="font-size:14px; font-weight: bolder;" for="inputLast" class="col-md-2 control-label">State</label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" id="state" name="state" value="">
                                    </div>
                                    <label style="font-size:14px; font-weight: bolder;" for="inputLast" class="col-md-1 control-label">Zip</label>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control" id="zip" name="zip" value="">
                                    </div>
                                    <label style="font-size:14px; font-weight: bolder;" for="inputLast" class="col-md-1 control-label">Country</label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" id="country" name="country" value="US">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label style="font-size:14px; font-weight: bolder;" for="inputLast" class="col-md-2 control-label">Phone Number</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" id="phone" name="phone" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label style="font-size:14px; font-weight: bolder;" class="col-lg-2 control-label">Email Address</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" id="emailAddress" name="emailAddress">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div id="actionButtons" class="col-lg-2 col-lg-offset-5">
                                        <button class="bottomBtn btn btn-primary" name="validateBtn" id="validateBtn" style="" onclick="validateAddress(); return false;">Validate Address</button>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend>Package Info</legend>
                            <div id="packageInfo" class="col-lg-8 col-lg-offset-2">
                                <div class="row form-group">
                                    <div class="col-lg-8 col-lg-offset-2">
                                        <label style="font-size:14px; font-weight: bolder;" class="col-md-2 control-label">Weight</label>
                                        <div class="col-lg-5">
                                            <div class="input-group">
                                                <input type="number" class="form-control" id="lbs" name="lbs"> <span  id="basic-addon1" class="input-group-addon">lbs</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-5">
                                            <div class="input-group">
                                                <input type="number" class="form-control" id="ounce" name="ounce"> <span class="input-group-addon">oz</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label style="font-size:14px"; class="col-sm-2 col-md-2 col-lg-2 control-label">Package Type</label>
                                    <div class="col-sm-9 col-md-9 col-lg-9">
                                        <select class="form-control m-b" name="packageType" id="packageType">
                                            <option selected disabled>Select package type</option>
                                            <option value="whiteBag">White Bag</option>
                                            <option value="smallBox">Small Box</option>
                                            <option value="mediumBox">Medium Box</option>
                                            <option value="largeBox">Large Box</option>
                                            <option value="other">Other</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label style="font-size:14px; font-weight: bolder;" class="col-md-2 control-label">Dimensions</label>
                                    <div class="col-lg-3">
                                        <div class="input-group">
                                            <span class="input-group-addon">Length</span><input type="number" class="form-control" id="length" name="length">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="input-group">
                                            <span class="input-group-addon">Width</span> <input type="number" class="form-control" id="width" name="width">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="input-group">
                                            <span class="input-group-addon">Height</span> <input type="number" class="form-control" id="height" name="height">
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label style="font-size:14px"; class="col-sm-2 col-md-2 col-lg-2 control-label">Ship Method</label>
                                    <div class="col-sm-9 col-md-9 col-lg-9">
                                        <select class="form-control m-b" name="shippingType" id="shippingType">
                                            <option selected disabled>Select shipping type</option>
                                            <option value="usps_first_class_mail">USPS First Class Mail</option>
                                            <option value="usps_priority_mail">USPS Priority Mail</option>
                                            <option value="usps_priority_mail_express">USPS Priority Mail Express</option>
                                            <option value="usps_first_class_package_international">USPS First Class Mail Intl</option>
                                            <option value="usps_priority_mail_international">USPS Priority Mail Intl</option>
                                            <option value="usps_priority_mail_express_international">USPS Priority Mail Express Intl</option>
                                        </select>
                                    </div>
                                </div>

<!--                            </div>-->
<!--                        </fieldset>-->
<!--                                    <fieldset>-->
<!--                                        <legend>Customs Declaration</legend>-->
<!--                                        <div id="packageInfo" class="col-lg-8 col-lg-offset-2">-->
<!--                                            --><?// foreach ($lineItems as $item) { ?>
<!--                                                <div class="row form-group">-->
<!--                                                    <label style="font-size:14px; font-weight: bolder;" class="col-md-2 control-label">Description</label>-->
<!--                                                    <div class="col-md-10">-->
<!--                                                        <input type="text" class="form-control" id="customsDescription[]" name="customsDescription[]" value="--><?// echo $item["title"] ?><!--">-->
<!--                                                    </div>-->
<!--                                                </div>-->
<!--                                                <div class="row form-group">-->
<!--                                                    <label style="font-size:14px; font-weight: bolder;"class="col-md-2 control-label">Value</label>-->
<!--                                                    <div class="col-md-2">-->
<!--                                                        <input type="text" class="form-control" id="customsValue[]" name="customsValue[]" value="--><?// echo $item["price"]?><!--">-->
<!--                                                    </div>-->
<!--                                                    <label style="font-size:14px; font-weight: bolder;" class="col-md-1 control-label">Quantity</label>-->
<!--                                                    <div class="col-md-2">-->
<!--                                                        <input type="text" class="form-control" id="customsQty[]" name="customsQty[]" value="--><?// echo $item["quantity"]?><!--">-->
<!--                                                    </div>-->
<!--                                                    <label style="font-size:14px; font-weight: bolder;" class="col-md-2 control-label">HS Tariff Code</label>-->
<!--                                                    <div class="col-md-3">-->
<!--                                                        <select class="form-control m-b" name="tariffCode[]" id="tariffCode[]">-->
<!--                                                            <option selected disabled>Select Product</option>-->
<!--                                                            <option value="6109.1004">T-Shirt</option>-->
<!--                                                            <option value="6110.20.1010">Hoodie</option>-->
<!--                                                            <option value="6504.00.3000">Hat</option>-->
<!--                                                            <option value="9503.00.0090">Die-cast</option>-->
<!--                                                            <option value="6911.10.5200">Coozie</option>-->
<!--                                                            <option value="6911.10.5800">Mug/Stein</option>-->
<!--                                                            <option value="3924.10.2000">Shot/Shooter Glass</option>-->
<!--                                                        </select>-->
<!--                                                    </div>-->
<!--                                                </div>-->
<!--                                                <div class="row form-group">-->
<!--                                                    <label style="font-size:14px; font-weight: bolder;" class="col-md-2 control-label">Country Of Origin</label>-->
<!--                                                    <div class="col-md-10">-->
<!--                                                        <select class="form-control m-b" name="coo[]" id="coo[]">-->
<!--                                                            <option selected disabled>Select Country</option>-->
<!--                                                            <option value="SV">El Salvador</option>-->
<!--                                                            <option value="CN">China</option>-->
<!--                                                            <option value="HT">Haiti</option>-->
<!--                                                            <option value="HN">Honduras</option>-->
<!--                                                            <option value="ID">Indonesia</option>-->
<!--                                                            <option value="MX">Mexico</option>-->
<!--                                                            <option value="NI">Nicaragua</option>-->
<!--                                                            <option value="US">United States</option>-->
<!--                                                        </select>-->
<!--                                                    </div>-->
<!--                                                </div>-->
<!--                                                <hr>-->
<!--                                                <br>-->
<!--                                            --><?// } ?>
<!--                                            <div class="row">-->
<!--                                                <div class="col-lg-6 col-lg-offset-3">-->
<!--                                                    <div class="input-group" id="actionButtons">-->
<!--                                                      <span class="input-group-btn">-->
<!--                                                        <button class="btn " name="getRateBtn" id="getRateBtn" onclick="getRate(); return false;" type="button">Get Rate</button>-->
<!--                                                      </span>-->
<!--                                                        <input type="text" id="rate" name="rate" class="form-control"/>-->
<!--                                                        <span class="input-group-addon">-->
<!--                                                            --><?// echo $shippingLines["price"]; ?>
<!--                                                        </span>-->
<!--                                                    </div>-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </fieldset>-->
<!--                                --><?// } else { ?>
                                <div class="row">
                                    <div class="col-lg-6 col-lg-offset-3">
                                        <div class="input-group">
                                          <span class="input-group-btn">
                                            <button class="btn " name="getRateBtn" id="getRateBtn" onclick="getRate(); return false;" type="button">Get Rate</button>
                                          </span>
                                            <input type="text" id="rate" name="rate" class="form-control"/>
                                            <span class="input-group-addon">
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>


                        <div class="form-group">
                            <div id="actionButtons" class="col-lg-2 col-lg-offset-5">
                                <button class="bottomBtn btn btn-primary" name="downloadBtn" id="downloadBtn" style="" onclick="returnLabel(); return false;">Download Label</button>
<!--                                <button class="bottomBtn btn btn-primary pull-right" style="" onclick="getRate(); return false;">Get Rate</button>-->
                                <!--                                    <button type="button" class="btn btn-danger">Cancel</button>-->
                            </div>
                        </div>
                    </form>
<!--                        <label class="col-sm-3 col-md-3 control-label">New Macro Date</label>-->
<!--                        <div class="col-sm-8 col-md-8">-->
<!--                            <div class="input-group date" id="newDatePicker">-->
<!--                                <input data-provide="datepicker"/>-->
<!--                            </div>-->
<!--                        </div>-->
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $( document ).ready(function() {
        $('#datepicker').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,

        });
    });
</script>
<!-- Mainly scripts -->
<script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"  type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/jeditable/jquery.jeditable.js"></script>
<script href="https://cdn.datatables.net/plug-ins/1.10.16/api/sum().js"></script>
<!-- Data picker -->
<script src="<?php echo base_url() ?>assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/dataTables/datatables.min.js"></script>
<!--<link href="--><?php //echo base_url() ?><!--assets/css/plugins/datapicker/datepicker3.css" rel="stylesheet">-->

<!-- Sweet alert -->
<script src="<?php echo base_url() ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo base_url() ?>assets/js/inspinia.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/pace/pace.min.js"></script>
</body>
</html>

