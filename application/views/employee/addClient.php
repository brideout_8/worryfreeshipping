<?php
/**
 * Created by PhpStorm.
 * User: Marketing101
 * Date: 12/15/17
 * Time: 10:01 AM
 */?>
<script type="text/javascript">

    $(document).ready(function(){

    });
</script>
<style>
    div.ibox-content{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        /*box-shadow: #3D3D3D;*/
    }
    #title {
        text-align: center;
        color: green;
    }
    p {
        text-align: center;
    }
    #inputOrder {
        text-align: center;
        margin-top: 20px;
    }
    .bottomBtn {
        margin-top: 15px;
    }
</style>
</head>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <h1><b>Add User</b></h1>
            <form class="form-horizontal" id="newClientForm" name="newClientForm" method="post">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="row">
                                <div id="packageInfo" class="col-lg-8 col-lg-offset-2">
                                    <div class="row form-group">
                                        <label style="font-size:14px; font-weight: bolder;" class="col-md-2 control-label">Username</label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" id="username" name="username" value="">
                                        </div>
                                        <label style="font-size:14px; font-weight: bolder;" class="col-md-2 control-label">Password</label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" id="password" name="password" value="">
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label style="font-size:14px; font-weight: bolder;" class="col-md-2 control-label">Email</label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" id="email" name="email" value="">
                                        </div>
                                        <label style="font-size:14px; font-weight: bolder;" class="col-md-2 control-label">Role</label>
                                        <div class="col-md-4">
                                            <select class="form-control m-b" name="role" id="role">
                                                <option selected disabled>Select Role</option>
                                                <option value="client">Client</option>
                                                <option value="driver">Driver</option>
                                                <option value="team">Team</option>
                                                <option value="employee">Employee</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label style="font-size:14px; font-weight: bolder;" class="col-md-2 control-label">First Name</label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" id="firstName" name="firstName" value="">
                                        </div>
                                        <label style="font-size:14px; font-weight: bolder;" class="col-md-2 control-label">Last Name</label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" id="lastName" name="lastName" value="">
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div id="actionButtons" class="col-lg-2 col-lg-offset-5">
                                    <input type="button" class="btn btn-primary block full-width m-b" id="changeBtn" name="changeBtn" value="Create User" onClick="createUser();return false;"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Mainly scripts -->
<script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"></script>
<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/jeditable/jquery.jeditable.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/dataTables/datatables.min.js"></script>
<!-- DROPZONE -->
<script src="<?php echo base_url() ?>assets/js/plugins/dropzone/dropzone.js"></script>
<!-- Sweet alert -->
<script src="<?php echo base_url() ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo base_url() ?>assets/js/inspinia.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/pace/pace.min.js"></script>

</body>

</html>



