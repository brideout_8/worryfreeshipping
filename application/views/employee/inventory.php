<?php
/**
 * Created by PhpStorm.
 * User: Marketing101
 * Date: 12/15/17
 * Time: 10:01 AM
 */?>
<script type="text/javascript">
    $(document).ready(function(){
        GetRRInventoryPage();
        GetDDAInventoryPage();
    });
</script>
<style>
    div.tab-content{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }
</style>
</head>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="tabs-container">
                <ul id="myTabs" class="nav nav-tabs">
                    <li class=""><a style="color: black" data-toggle="tab" href="#tab-1">R&R Racewear</a></li>
                    <li class=""><a style="color: black" data-toggle="tab" href="#tab-2">Direct Driver Apparel</a></li>
                </ul>
                <div class="tab-content boxShadow">
                    <div id="tab-1" class="tab-pane">
                        <div class="panel-body">
                            <div id="rr"></div>
                        </div>
                    </div>
                    <div id="tab-2" class="tab-pane">
                        <div class="panel-body">
                            <div id="dda"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function() {
    $('.nav-tabs a[href="#tab-1"]').tab('show');
});

</script>
<!-- Mainly scripts -->
<script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"></script>
<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/jeditable/jquery.jeditable.js"></script>

<script src="<?php echo base_url() ?>assets/js/plugins/dataTables/datatables.min.js"></script>

<!-- Sweet alert -->
<script src="<?php echo base_url() ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo base_url() ?>assets/js/inspinia.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/pace/pace.min.js"></script>

</body>

</html>



