<?php
/**
 * Created by PhpStorm.
 * User: Marketing101
 * Date: 12/17/17
 * Time: 12:46 PM
 */?>
<div class="table-responsive">
    <table class="table dataTables-example" >
        <thead>
        <tr>
            <th style="width: 10%">Order Number</th>
            <th style="width: 15%">Store</th>
<!--            <th style="width: 15%">Order Id</th>-->
            <th style="width: 10%">Date Ordered</th>
            <th style="width: 10%">Customer</th>
            <th style="width: 15%">Note</th>
            <th style="width: 10%">Picked</th>
            <th style="width: 10%">Shipped</th>
<!--            <th style="width: 15%">Action</th>-->
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ( $results as $result ) { ?>
            <tr>
                <td style="vertical-align: middle"><?php echo $result->get('orderNumber');?></td>
                <td style="vertical-align: middle"><?php if($result->get('storeName') == "RR") {echo "R&RRacewear.com"; } else if($result->get("storeName") == "DDA") { echo "DirectDriverApparel.com";};?></td>
<!--                <td>--><?php //echo $result->get('orderId');?><!--</td>-->
                <td style="vertical-align: middle"><?php echo $result->get('dateOrdered');?></td>
                <td style="vertical-align: middle"><?php echo $result->get('customer')["first_name"];?> <?php echo $result->get('customer')["last_name"];?></td>
                <td style="vertical-align: middle"><?php echo $result->get('note');?></td>
                <td style="vertical-align: middle"><?php if($result->get("packed") == "2") {?> Yes <?php } else if($result->get("packed") == "1") {?> Partial <?php } else { ?> No <?php } ?></td>
                <td style="vertical-align: middle"><?php if($result->get("shipped") == "2") {?> Yes <?php } else if($result->get("shipped") == "1") {?> Partial <?php } else { ?> No <?php } ?></td>
<!--                <td style="vertical-align: middle">--><?php //echo $result->get('note');?><!--</td>-->
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>

