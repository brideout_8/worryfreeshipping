<?php
/**
 * Created by PhpStorm.
 * User: rapidware
 * Date: 12/17/17
 * Time: 2:18 AM
 */?>

<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover dataTables-example" >
        <thead>
        <tr>
            <th>Order Number</th>
            <th>Store</th>
            <th>Order Id</th>
            <th>Date Ordered</th>
            <th>Line Item Count</th>
            <th>Note</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ( $results as $result ) { ?>
            <tr>
                <td><?php echo $result->get('orderNumber');?></td>
                <td><?php if($result->get('storeName') == "RR") {echo "R&RRacewear.com"; } else if($result->get("storeName") == "DDA") { echo "DirectDriverApparel.com";};?></td>
                <td><?php echo $result->get('orderId');?></td>
                <td><?php echo $result->get('dateOrdered');?></td>
                <td><?php echo count($result->get('lineItems'));?></td>
                <td><?php echo $result->get('note');?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
