<?php
/**
 * Created by PhpStorm.
 * User: Marketing101
 * Date: 12/14/17
 * Time: 5:08 PM
 */?>
<style>
    div.ibox-content{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        /*box-shadow: #3D3D3D;*/
    }
    #title {
        text-align: center;
        color: green;
    }
    p {
        text-align: center;
    }
    #inputOrder {
        text-align: center;
        margin-top: 20px;
    }
</style>
<script>
    function loader() {
        $('#listings2').html('<img height="50" width="50" style=" position: absolute; top: 50%; left: 50%;" src="'+base_url+'assets/img/loader.gif">');
    }
</script>
</head>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <h1><b>Orders To Ship <span class="pull-right">$<? echo $funds?></span> </b></h1>
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div id="listings2" class="listings">
                        <h1 id="title"><b>Scan Order</b></h1>
                        <p>Scan the order barcode or enter the order number below and hit enter.</p>
                        <div id="inputOrder">
                            <form id="orderSearch" name="orderSearch" type="post" action="<? base_url()?>/employee/findOrder/ship">
                                <input id="orderNumber" autofocus name="orderNumber" size="35" type="text" onkeypress="if(event.keyCode==13){form.submit(); loader(); }" />
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <h1><b>Pending Orders</b></h1>
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="text-center" id="ship">
                        <button class="btn btn-primary" onclick="getFulfillmentOverviewShip()">Get Orders</button>
<!--                        <button class="btn btn-primary" onclick="download('https://api.shipengine.com/v1/downloads/6/oJwbX5IeiEuZC_Xo6MXP6w/label-87778285.pdf')">Order 11302</button>-->
                    </div>
                </div>
            </div>
        </div>
        <button class="btn btn-primary" style="" onclick="createScanForm()">Get Scan Form</button>
        <a class="btn btn-primary pull-right" href="<?=base_url();?>employee/ordersToPack">Orders To Pack</a>
<!--        <a class="btn btn-primary pull-right" onclick="addNote()">Test Note</a>-->
        <a class="btn btn-primary pull-right" style="margin-right: 10px" href="<?=base_url();?>employee/shipNonOrder">Stamps.com</a>
    </div>
</div>
<!-- Mainly scripts -->
<script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"  type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/jeditable/jquery.jeditable.js"></script>

<script src="<?php echo base_url() ?>assets/js/plugins/dataTables/datatables.min.js"></script>

<!-- Sweet alert -->
<script src="<?php echo base_url() ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/swalExtend/swalExtend.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo base_url() ?>assets/js/inspinia.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/pace/pace.min.js"></script>
</body>
</html>