<?php
/**
 * Created by PhpStorm.
 * User: Marketing101
 * Date: 12/15/17
 * Time: 10:06 AM
 */?>
<!--<h1><b>Orders Not In Shopworks - --><?php //echo $store ?><!--</b></h1>-->
<form id="ordersForm" name="ordersForm" method="post" action="<? base_url() ?>/index.php/fulfillment/downloadOrders/" >
    <!--            <input type="submit" id="submitBtn" name="submitBtn" class="btn btn-primary pull-right" onclick="this.disabled=true; form.submit()" value="Download Orders" />-->
    <!--            <input type="hidden" id="orders[]" name="orders[]" value="--><?// echo $results?><!--"/>-->
    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover dataTables-example" id="inventory" >
            <thead>
            <tr>
                <!--                    <th>#</th>-->
                <th>Product Name</th>
                <th>Size</th>
                <th>Color</th>
                <th>Quantity</th>
                <th>Date Published</th>
                <!--                        <th>Grand Total</th>-->
                <!--                        <th>Note</th>-->
                <!--                        <th>Gift Card Payments</th>-->
                <!--                        <th>Gift Card</th>-->
                <!--                    <th>Action</th>-->
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>Product Name</th>
                <th>Size</th>
                <th>Color</th>
                <th>Quantity</th>
                <th>Date Published</th>
            </tr>
            </tfoot>
            <tbody>
            <?php
            for($x=0;$x<count($results);$x++) {
                foreach ( $results[$x]["variants"] as $result ) {
                    if($result["option1"] != "Default Title" && intval($result["inventory_quantity"]) < 4) { ?>
                        <tr>
                            <td><?php echo $results[$x]['title'];?></td>

                            <td><?php echo $result["option1"];?></td>
                            <td><?php echo $result["option2"];?></td>
                            <td><?php echo $result["inventory_quantity"];?></td>
                            <td><?php echo $newDate = date("m-d-Y", strtotime($results[$x]["published_at"]));?></td>
                            <!--                            <td>--><?php //echo $result->get('grandTotal');?><!--</td>-->
                            <!--                            <td>--><?php //echo $result->get('note');?><!--</td>-->
                            <!--                            <td>--><?php //if(in_array("gift_card",$result->get('gateway'))) { echo "Yes";};?><!--</td>-->
                            <!--                            <td>--><?php //echo $result->get('giftCard');?><!--</td>-->
                            <!--                        <td><a href="--><?php //base_url() ?><!--coach/editAthlete/--><?php //echo $result->getObjectId() ?><!--">View</a></td>-->
                        </tr>
                    <?php } ?>
                <?php } ?>
            <?php } ?>
            </tbody>
        </table>
    </div>
</form>
