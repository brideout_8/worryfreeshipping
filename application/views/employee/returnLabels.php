<?php
/**
 * Created by PhpStorm.
 * User: Marketing101
 * Date: 12/15/17
 * Time: 10:01 AM
 */?>
<script type="text/javascript">

    $(document).ready(function(){
        var oTable = $('.dataTables-example').DataTable({
            "aLengthMenu": [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"] ],
            "iDisplayLength" : 25,
            "order": [[ 5, "desc" ]],
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: 'Documents'},
                {extend: 'pdf', title: 'Documents'},

                {extend: 'print',
                    customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });
    });
</script>
<style>
    div.ibox-content{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        /*box-shadow: #3D3D3D;*/
    }
    #title {
        text-align: center;
        color: green;
    }
    p {
        text-align: center;
    }
    #inputOrder {
        text-align: center;
        margin-top: 20px;
    }
    .bottomBtn {
        margin-top: 15px;
    }
</style>
</head>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <h1><b>Return Labels<? if(in_array("addReturnLabel",$this->session->userdata("permissions"))) {?> <a class="pull-right" href="<?=base_url();?>fulfillment/returnLabel">New Return Label</a> <?}?></b></h1>
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                            <tr>
                                <th>Label Id</th>
                                <th>Created At</th>
                                <th>Cost</th>
                                <th>Tracking Number</th>
                                <th>Service</th>
                                <th>Email Address</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($labels as $label) { ?>
                                <tr>
                                    <td><?php echo $label->get("labelId");?></td>
                                    <td><?$time = strtotime($label->get("dateShipped")); date_default_timezone_set('America/Chicago'); echo date('m-d-Y H:i:s', $time)?></td>
                                    <td><?php echo $label->get("shipmentCost");?></td>
                                    <td><?php echo $label->get("trackingNumbers");?></td>
                                    <td><?php echo $label->get("service");?></td>
                                    <td><?php echo $label->get("emailAddress");?></td>
                                    <td><? if(in_array("downloadDocuments",$this->session->userdata("permissions"))) {?> <a class="btn btn-xs btn-primary" href="<? echo $label->get("labelLink")?>">Download</a> <?}?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Mainly scripts -->
<script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"></script>
<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/jeditable/jquery.jeditable.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/dataTables/datatables.min.js"></script>
<!-- DROPZONE -->
<script src="<?php echo base_url() ?>assets/js/plugins/dropzone/dropzone.js"></script>
<!-- Sweet alert -->
<script src="<?php echo base_url() ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo base_url() ?>assets/js/inspinia.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/pace/pace.min.js"></script>

</body>

</html>



