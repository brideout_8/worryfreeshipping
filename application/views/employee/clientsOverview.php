<?php
/**
 * Created by PhpStorm.
 * User: Marketing101
 * Date: 3/28/18
 * Time: 12:02 PM
 */
?>

<style>
    div.ibox-content{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        /*box-shadow: #3D3D3D;*/
    }
    #title {
        text-align: center;
        color: green;
    }
    p {
        text-align: center;
    }
    #inputOrder {
        text-align: center;
        margin-top: 20px;
    }
</style>
<script>
    // getToPackPage();
    function loader() {
        $('#listings').html('<img height="50" width="50" style=" position: absolute; top: 50%; left: 50%;" src="'+base_url+'assets/img/loader.gif">');
    }

    function selectedClient() {
        $('#getButton').prop('disabled',false);
    }
</script>
</head>

<div class="wrapper wrapper-content animated fadeInRight">
    <form id="clientM" name="clientM" type="post">
        <div class="row">
            <div class="col-lg-12">
                <h1><b>Client Overview</b></h1>
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <h1 id="title"><b>Select Client<? echo $message; ?></b></h1>
                        <div class="row">
                            <div class="col-sm-4 col-md-4 col-lg-4 col-sm-offset-4 col-md-offset-4 col-lg-offset-4">
                                <select class="form-control m-b" name="client" id="client" onchange="selectedClient()">
                                    <option selected disabled>Select Client</option>
                                    <? foreach ($clients as $client) { ?>
                                        <option value="<? echo $client->getObjectId()?>"><? echo $client->get("username")." - ".$client->get("firstName")." ".$client->get("lastName")?></option>
                                    <? } ?>
                                </select>
                                <div class="text-center" id="pack">
<!--                                    <input type="button" id="geButton" name="geButton" class=" btn btn-primary" onclick="fixOrders()" value="Fix"">-->
                                    <button name="getButton" id="getButton" class="ladda-button btn btn-primary" data-style="zoom-out" onclick="GetClientsOverviewPage();return false;">Get Metrics</button>
<!--                                    <input type="button" name="getButton" id="getButton" class="ladda-button btn btn-primary" data-style="zoom-out" onclick="fixOrders()" value="fix">-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="listings" class="listings">
                    <h1><b>Client Metrics</b></h1>
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- Mainly scripts -->
<script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"  type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/jeditable/jquery.jeditable.js"></script>

<script src="<?php echo base_url() ?>assets/js/plugins/dataTables/datatables.min.js"></script>

<!-- Sweet alert -->
<script src="<?php echo base_url() ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>

<!-- Morris -->
<script src="<?php echo base_url() ?>assets/js/plugins/morris/raphael-2.1.0.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/morris/morris.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo base_url() ?>assets/js/inspinia.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/pace/pace.min.js"></script>
<!-- Ladda -->
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.jquery.min.js"></script>
</body>
</html>


