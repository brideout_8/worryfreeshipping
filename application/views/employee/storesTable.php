<?php
/**
 * Created by PhpStorm.
 * User: rapidware
 * Date: 7/20/18
 * Time: 9:57 AM
 */?>
<div class="col-lg-8 col-md-8 col-lg-offset-2 col-mg-offset-2">
    <h1>Stores</h1>
    <hr>
    <div class="row">
        <div class="col-lg-3 col-md-3">
            <h3 class="titles">Connected stores</h3>
            <p class="explainText">These are the stores that supply orders and inventory to your account.</p>
        </div>
        <div class=" col-lg-9 col-md-9">
            <div class="ibox-content img-rounded">
                <? if(in_array("addStores", $this->session->permissions) || in_array("fullAccess", $this->session->permissions)) {?>
                <h3 style="padding-bottom: 8px">Stores <span class="pull-right"><a href="#" data-toggle="modal" data-target="#addStoreModal" class="pull-right normalLink" style="font-weight: bold">Add Store</a></span></h3>
<!--                <h3 style="padding-bottom: 8px">Stores <span class="pull-right"><a href="--><?// if($this->session->subscriptionLevel == "1" || $this->session->subscriptionLevel == "0") { base_url()?><!--/register/changePlan--><?//} else { base_url()?><!--/settings/addStore --><?//}?><!--" class="pull-right normalLink" style="font-weight: bold">Add Store</a></span></h3>-->
                <?} else {?>
                    <h3 style="padding-bottom: 8px">Stores</h3>
                <?}?>
                <table class="table" >
                    <thead>
                    <tr style="text-align: center">
                        <th>Name</th>
                        <th>Display Name</th>
                        <th>Date Added</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($stores as $store) { ?>
                        <tr>
                            <td><?php echo $store->get("shopifyName")?></td>
                            <td><?php echo $store->get("displayName")?></td>
                            <td><?php $date = $store->getUpdatedAt(); echo $date->format('m/d/Y');?></td>
                            <td><a type="button" class="btn btn-primary btn-xs" href="<? base_url()?>/settings/editStore?storeId=<? echo $store->getObjectId();?>" <? if(!in_array("editStores", $this->session->permissions) && !in_array("fullAccess", $this->session->permissions)) {?> disabled="" <?}?>>Edit</a></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


