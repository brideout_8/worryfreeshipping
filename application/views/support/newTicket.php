<?php
/**
 * Created by PhpStorm.
 * User: rapidware
 * Date: 10/8/18
 * Time: 12:59 PM
 */?>

<style>
    div.ibox-content {
        border: solid 1px #d9d9d9;
        /*box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);*/
        /*margin-top: 20px;*/
        border-radius: 5px;
        /*box-shadow: #3D3D3D;*/
    }
    .table th {
        /*text-align: center;*/
        font-weight: normal;
        border: 0;
    }
    .table td {
        /*font-weight: bold;*/
        /*font-size: 15px;*/
    }
    table tr:first-child td {
        border-top: 0;
    }
    /*a {*/
    /*color: #717171;*/
    /*}*/
    .normalLink {
        font-size: 15px;
        font-weight: normal;
    }
    p {
        margin:-2px 0 -2px 0;
    }
    .buttonLink {
        background:none!important;
        color: steelblue;
        border:none;
        padding:0!important;
        font: inherit;
        /*border is optional*/
        cursor: pointer;
    }
    .titles {
        padding-top: 17px;
    }
    .dataText p {
        margin-bottom: 3px;
    }
    hr {
        border-color: #dcdcdc;
    }
    .form-control {
        border-radius: 5px;
    }
    .ui-select {
        font-size: 13px;
        font-weight: 400;
        line-height: 2.4rem;
        text-transform: initial;
        letter-spacing: initial;
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
        display: block;
        height: 3.4rem;
        width: 100%;
        padding: 0.4rem 0.8rem;
        padding-right: 2.8rem;
        padding-left: 1.6rem;
        background: #fefefe;
        border: 1px solid #c4cdd5;
        border-radius: 3px;
        max-width: none;
        -webkit-transition-property: background, border, -webkit-box-shadow;
        transition-property: background, border, -webkit-box-shadow;
        transition-property: background, border, box-shadow;
        transition-property: background, border, box-shadow, -webkit-box-shadow;
        -webkit-transition-timing-function: cubic-bezier(0.64, 0, 0.35, 1);
        transition-timing-function: cubic-bezier(0.64, 0, 0.35, 1);
        -webkit-transition-duration: 200ms;
        transition-duration: 200ms
    }

    select.minimal {
        background-image:
                linear-gradient(45deg, transparent 50%, gray 50%),
                linear-gradient(135deg, gray 50%, transparent 50%),
                linear-gradient(to right, #ccc, #ccc);
        background-position:
                calc(100% - 20px) calc(1em + 2px),
                calc(100% - 15px) calc(1em + 2px),
                calc(100% - 2.5em) 0.5em;
        background-size:
                5px 5px,
                5px 5px,
                1px 1.5em;
        background-repeat: no-repeat;
    }
</style>
</head>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="col-lg-8 col-md-8 col-lg-offset-2 col-mg-offset-2">
                <h1 style="font-weight: bold">New support ticket</h1>
                <hr>
                <div class="row">
                    <div class="col-lg-4 col-md-4">
                        <h3 class="titles">Ticket information</h3>
                        <!--                        <p class="explainText">The address used to calculate shipping rates and the return address on your labels</p>-->
                    </div>
                    <div class=" col-lg-8 col-md-8">
                        <div class="ibox-content img-rounded">
<!--                            <h3 style="padding-bottom: 8px">Account Information</h3>-->
                            <form id="addTicketForm" name="addTicketForm" method="post" class="form-horizontal">
                                <div class="form-group">
                                    <label autocomplete="false" class="col-lg-2 control-label">Name</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" id="name" name="name" value="<? echo $this->session->name;?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label autocomplete="false" class="col-lg-2 control-label">Email</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" id="email" name="email" value="<? echo $this->session->email;?>">
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <label autocomplete="false" class="col-lg-2 control-label">Issue type</label>
                                    <div class="col-lg-10">
                                        <select class="form-control minimal ui-select" id="typeOfIssue" name="typeOfIssue">
                                            <option value="Order Import">Order Import</option>
                                            <option value="Product Scanning">Product Scanning</option>
                                            <option value="Order Shipping">Order Shipping</option>
                                            <option value="Creating Non Order">Creating Non Order</option>
                                            <option value="Creating A Return">Creating A Return</option>
                                            <option value="Adding A User">Adding A User</option>
                                            <option value="Changing Plans">Changing Plans</option>
                                            <option value="Purchasing Labels">Purchasing Labels</option>
                                            <option value="Adding A Carrier">Adding A Carrier</option>
                                            <option value="Other">Other</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label autocomplete="false" class="col-lg-2 control-label">Details</label>
                                    <div class="col-lg-10">
                                        <textarea class="form-control" rows="10" id="issue" name="issue"></textarea>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <button style="margin-top: 10px" id="fullAccess" name="fullAccess" class="btn btn-primary pull-right ladda-button" data-style="zoom-out" onclick="createTicket()">Create Ticket</button>
                        <a style="margin-top: 10px; margin-right: 10px" href="<? echo base_url()?>support" class="btn btn-default pull-right ladda-button"  >Cancel</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<? $this->load->view("navigation/footer");?>
<!-- Mainly scripts -->
<script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"></script>
<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/jeditable/jquery.jeditable.js"></script>

<script src="<?php echo base_url() ?>assets/js/plugins/dataTables/datatables.min.js"></script>

<!-- Sweet alert -->
<script src="<?php echo base_url() ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo base_url() ?>assets/js/inspinia.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/pace/pace.min.js"></script>

<!-- iCheck -->
<script src="<? echo base_url()?>assets/js/plugins/iCheck/icheck.min.js"></script>

<!-- Toastr script -->
<script src="<? echo base_url()?>assets/js/plugins/toastr/toastr.min.js"></script>

<!-- Ladda -->
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.jquery.min.js"></script>

</body>

</html>






