<?php
/**
 * Created by PhpStorm.
 * User: rapidware
 * Date: 10/8/18
 * Time: 11:25 AM
 */?>

<style>

    td {
        height: 35px;
    }

    .rounded {
        border-radius: 5px;
    }
    #title {
        text-align: center;
        color: green;
    }
    p {
        text-align: center;
    }
    #inputOrder {
        text-align: center;
        margin-top: 20px;
    }
    .bottomBtn {
        margin-top: 15px;
    }
    #actionButtons {
        margin-top: 15px;
    }
    h2 {
        text-align: center;
    }
    .dateTime {
        font-size: 15px;
        margin-left: 10px;
    }
    .badge {
        margin-left: 10px;
    }
    .title{
        padding-bottom: 20px;
        padding-left: 12px;
    }
    label {
        font-weight: normal;
        font-size: 14px;
    }
    .selection {
        background-color: transparent;
    }
    .bootstrap-select .dropdown-menu {
        margin: 15px 0 0;
    }
    hr {
        border-color: #dcdcdc;
    }

</style>
</head>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="tourTitle">Support center</h1>
            <div class="ibox float-e-margins tourReturns">
                <div class="ibox-content img-rounded">
                    <div class="row">
                        <div class="col-lg-12">
                            <div id="support"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<? $this->load->view("navigation/footer");?>
<script>
    function loader() {
        $('#support').html('<img height="50" width="50" style=" position: absolute; top: 50%; left: 50%;" src="'+base_url+'assets/img/loader.gif">');
    }
    $(document).ready(function() {
        $("#support").html('<img height="50" width="50" style=" position: absolute; top: 50%; left: 50%;" src="https://thewrapapp.net/assets/images/loader.gif">');
        $.ajax({
            url  	: base_url+"support/supportTable",
            type 	: 'POST',
            success : function(data){
                $("#support").html(data);
                var oTable = $('.dataTables-example').DataTable({
                    retrieve: true,
                    "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                    "iDisplayLength": 25,
                    // "order": [[ 7, "asc" ]],
                    "order": [[2,'desc']]

                });
            }
        });
    });
</script>
<!-- Mainly scripts -->
<script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"></script>
<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/jeditable/jquery.jeditable.js"></script>

<script src="<?php echo base_url() ?>assets/js/plugins/dataTables/datatables.min.js"></script>

<!-- Sweet alert -->
<script src="<?php echo base_url() ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>

<!-- EnjoyHint -->
<script src="<?php echo base_url() ?>assets/js/plugins/enjoyhint/enjoyhint.min.js"></script>

<!-- Ladda -->
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.jquery.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo base_url() ?>assets/js/inspinia.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/pace/pace.min.js"></script>

</body>

</html>








