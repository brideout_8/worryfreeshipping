<?php
/**
 * Created by PhpStorm.
 * User: rapidware
 * Date: 10/8/18
 * Time: 11:41 AM
 */?>

<div class="row">
    <h2>Welcome to the support ticket section of the support center</h2>
    <h3 style="text-align: center">If you are unable to find the answers to your questions in the <a href="<? echo base_url()?>/support/faq">FAQ</a> section,<br>feel free to create a support ticket below.</h3>
</div>
<div style="margin-top: 15px" class="row">
    <div class="col-md-2 col-lg-2 col-md-offset-5 col-lg-offset-5">
        <a type="button" class="btn btn-primary" href="<? echo base_url()?>/support/newTicket">New support ticket</a>
    </div>
</div>
<hr>
<div style="margin-top: 30px; margin-left: 10px; margin-right: 10px" class="row">
<!--    <h3 style="text-align: center">Tickets</h3>-->
    <div class="table-responsive">
        <table class="table dataTables-example" >
            <thead>
            <tr>
                <th style="width: 10%">Ticket Id</th>
                <th style="width: 15%">Submitted By</th>
                <th style="width: 10%">Date Submitted</th>
                <th style="width: 10%">Issue</th>
                <th style="width: 10%">Message</th>
                <th style="width: 10%">Status</th>
            </tr>
            </thead>
            <tbody>
            <?php $x = 0;
            foreach ( $tickets as $ticket ) { ?>
                <tr>
                    <td style="vertical-align: middle"><?php echo $ticket->getObjectId();?></td>
                    <td style="vertical-align: middle"><? echo $ticket->get("name")?></td>
                    <td style="vertical-align: middle"><? $UTCdate = $ticket->getCreatedAt(); $date = $UTCdate->setTimezone(new DateTimeZone($this->session->timeZone)); echo date_format($date, "m/d/Y")?></td>
                    <td style="vertical-align: middle"><?php echo $ticket->get("typeOfIssue");?></td>
                    <td style="vertical-align: middle"><?php echo $ticket->get("issue");?></td>
                    <td style="vertical-align: middle"><?php echo $ticket->get("status");?></td>
                </tr>
                <?php $x++; } ?>
            </tbody>
        </table>
    </div>
</div>
