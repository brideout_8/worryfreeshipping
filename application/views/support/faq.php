<?php
/**
 * Created by PhpStorm.
 * User: rapidware
 * Date: 10/8/18
 * Time: 1:52 PM
 */?>

<?php
/**
 * Created by PhpStorm.
 * User: rapidware
 * Date: 10/8/18
 * Time: 11:25 AM
 */?>

<style>

    td {
        height: 35px;
    }

    .rounded {
        border-radius: 5px;
    }
    #title {
        text-align: center;
        color: green;
    }
    p {
        text-align: center;
    }
    #inputOrder {
        text-align: center;
        margin-top: 20px;
    }
    .bottomBtn {
        margin-top: 15px;
    }
    #actionButtons {
        margin-top: 15px;
    }
    h2 {
        text-align: center;
    }
    .dateTime {
        font-size: 15px;
        margin-left: 10px;
    }
    .badge {
        margin-left: 10px;
    }
    .title{
        padding-bottom: 20px;
        padding-left: 12px;
    }
    label {
        font-weight: normal;
        font-size: 14px;
    }
    .selection {
        background-color: transparent;
    }
    .bootstrap-select .dropdown-menu {
        margin: 15px 0 0;
    }
    #questions {
        font-weight: bold;
        font-size: 15px;
    }
    #answers{
        font-size: 14px;
        text-align: center;
        margin-bottom: 25px;
    }
    hr {
        border-color: #dcdcdc;
    }
    #sectionTitles {
        font-weight: bold;
        margin-bottom: 25px;
    }

</style>
</head>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="tourTitle">Support center</h1>
            <div class="ibox float-e-margins tourReturns">
                <div class="ibox-content img-rounded">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row">
                                <h2>Welcome to the FAQ section of the support center</h2>
                                <h3 style="text-align: center">If you are unable to find the answers to your questions in the FAQ section below,<br>
                                    feel free to create a support ticket <a href="<? echo base_url()?>/support">here</a>.</h3>
                            </div>
                            <hr>
                            <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2">
                                <h2 id="sectionTitles">Fulfillment</h2>
                                <p id="questions">How can I import my store's orders?</p>
                                <p id="answers">On the fulfillment page, you will see an import orders button on the top right hand side of the page. Any connected store will have their newest orders imported.</p>
                                <p id="questions">How can I mark an order on hold?</p>
                                <p id="answers">Inside of the order, near the top left, you will see an actions drop down. Under the drop down is a button to place the order on hold.</p>
                                <p id="questions">How can I archive an order?</p>
                                <p id="answers">Inside of the order, near the top left, you will see an actions drop down. Under the drop down is a button to archive the order.</p>
                                <p id="questions">How do I use a barcode scanner?</p>
                                <p id="answers">Almost any USB or bluetooth scanner will work. First install any drivers that come with the scanner. Once installed the scanner will automatically scan the barcode into
                                our software. Just make sure the scan barcode field is selected before scanning.</p>
                                <p id="questions">What happens if I update the order information inside of the Shopify Admin?</p>
                                <p id="answers">Our software will get an update automatically from Shopify when a change is made. If your change is not showing up, please create a support ticket.</p>
                                <p id="questions">If I update an order's shipping address, will it update in Shopify?</p>
                                <p id="answers">Yes, updating an order's shipping address will sync with Shopify to keep everything up to date.</p>
                                <p id="questions">I am getting the error saying "All shipping quantities are 0. Please enter a quantity." when trying to purchase a label.</p>
                                <p id="answers">This is from none of the line items having a shipping quantity. Our system believes you are trying to ship an empty box. On the far right side of each line
                                item, you will see a box to enter a number. At least one line item needs a number higher than 0 to create a label.</p>
                                <p id="questions">The shipping address is not verified.</p>
                                <p id="answers">You can still purchase a label but the carrier cannot guarantee the address exists. Worry Free Shipping is not responsible if the package gets lost.</p>
                            </div>
                            <hr>
                            <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2">
                                <h2 id="sectionTitles">Returns</h2>
                                <p id="questions">Can I create a return from the order?</p>
                                <p id="answers">Yes, you can create a return from the order. On the bottom right side you will see the returns section of the order.
                                Here you can issue a new return label. This will automatically pull in the order's address.</p>
                                <p id="questions">Can I create a return for an order in the return's manager?</p>
                                <p id="answers">Yes. To create an order return inside the return's manager, click the button "New Order Return". This will allow you to search for an order. Once found, all of the order's
                                information will populate. This return can also be seen inside the order.</p>
                                <p id="questions">Can I create a return that is not attached to an order?</p>
                                <p id="answers">Yes. To create a return that is not attached to an order, use the "New General Return" button inside the return's manager page. This will allow you
                                to add any shipping information.</p>
                            </div>
                            <hr>
                            <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2">
                                <h2 id="sectionTitles">Settings</h2>
                                <p id="questions">How do I add an user?</p>
                                <p id="answers">Under the settings section there is an account tab. On this page your users will be listed. You can only add or see users if you are the owner
                                of the company or your account has access.</p>
                                <p id="questions">How do I add a package?</p>
                                <p id="answers">Under the settings section there is a shipping tab. On this page you can add your own custom packaging or add a carrier's packaging.</p>
                                <p id="questions">How do I purchase more shipping labels?</p>
                                <p id="answers">You can purchase more shipping labels in two locations. One is under settings, billing and the other is under settings, account. You have to have access to
                                purchase more labels. You can also purchase more here. <a href="<? echo base_url()?>settings/buyLabels">Purchase Labels</a></p>
                                <p id="questions">Will I lose my unused purchased labels?</p>
                                <p id="answers">No, any unused purcahsed shipping labels will roll over each month. If you upgrade or downgrade your plan, you will keep your labels as well. Any leftover
                                labels that were not purchased will not rollover.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<? $this->load->view("navigation/footer");?>
<!-- Mainly scripts -->
<script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"></script>
<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/jeditable/jquery.jeditable.js"></script>

<script src="<?php echo base_url() ?>assets/js/plugins/dataTables/datatables.min.js"></script>

<!-- Sweet alert -->
<script src="<?php echo base_url() ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>

<!-- EnjoyHint -->
<script src="<?php echo base_url() ?>assets/js/plugins/enjoyhint/enjoyhint.min.js"></script>

<!-- Ladda -->
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.jquery.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo base_url() ?>assets/js/inspinia.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/pace/pace.min.js"></script>

</body>

</html>









