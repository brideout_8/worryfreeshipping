<?php
/**
 * Created by PhpStorm.
 * User: Marketing101
 * Date: 7/16/18
 * Time: 3:45 PM
 */?>

<html>

<head>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-126911005-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-126911005-1');
    </script>
    <!-- End of Google Analytics -->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Worry Free Shipping - Login</title>
    <link rel="shortcut icon" href="<?php echo base_url() ?>/assets/img/Favicon.png">
    <script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"></script>
    <!-- 	<link href="<?php echo base_url() ?>assets/css/plugins/chosen/chosen.css" rel="stylesheet"> -->
    <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
    <!-- Sweet Alert -->
    <link href="<?php echo base_url() ?>assets/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <!-- Ladda style -->
    <link href="<?php echo base_url() ?>assets/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo base_url()?>assets/js/validationEngine.jquery.css"/>
    <script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.validationEngine.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>

    <script type="text/javascript">


        function checkEnter(e)
        {
            var keyCode = (e.keyCode ? e.keyCode : e.which);
            if(keyCode == 13 )
            {
                e.preventDefault();

                login();
            }
        }

        function login() {
            var formArray = $('#login_form').serialize();
            var base_url  = "<?php echo base_url(); ?>";
            var c = $('.ladda-button').ladda();
            c.ladda('start');
            jQuery.post('<?php echo base_url(); ?>login/login_validation' , formArray, function(data) {
                c.ladda("stop");

                if (data.success) {
                    var url = data.url;
                    top.location = base_url+url;
                } else if(data.newStore) {
                    var form = document.getElementById('login_form');
                    form.submit();
                } else {
                    if(data.error == 1){
                        swal({
                            title: "Error",
                            text: data.message,
                        });
                    } else if (data.error == 2){
                        swal({
                            title: "Error",
                            text: "Your subscription is past due. Please contact support at bret@rapidwarellc.com"
                        });
                    } else {
                        swal({
                            title: "Error",
                            text: "Please contact support at bret@rapidwarellc.com",
                        });
                    }
                }
            }, 'json');

            return false;
        }
    </script>
    <style>
        img {
            max-width: 60%;
            max-height: 60%;
            display: block;
            margin-left: auto;
            margin-right: auto
        }

        .signUpColumns {
            max-width: 1100px;
            margin: 0 auto;
            padding: 250px 20px 20px 20px;
        }
        .custom-bg {
            background-color: #f6f8fa;
        }
        div.ibox-content {
            border: solid 1px #d9d9d9;
            /*box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);*/
            margin-top: 20px;
            border-radius: 5px;
            /*box-shadow: #3D3D3D;*/
        }
        .withUs {
            padding-top: 10px;
            font-size: 15px;
        }
        .form-group {
            padding-left: 0px;
            padding-right: 8px;
        }
        .form-control {
            border-radius: 5px;
        }
        label {
            font-weight: normal;
            color: black;
        }
        ::placeholder {
            font-weight: lighter;
        }
        .signUpForm {
            padding-right: 7px;
            padding-left: 12px;
        }
        .createAccount {
            margin-top: 15px;
        }
        h1 {
            font-size: 45px;
        }
        .rounded {
            border-radius: 5px;
        }
        .signUpColumns {
            padding-top: 100px;
        }
        .productImage {
            /*display: block;*/
            max-width:200px;
            max-height:200px;
            width: auto;
            height: auto;
        }
    </style>
</head>

<body class="custom-bg">
<div class="signUpColumns animated fadeInDown">
    <div class="row">
        <img class="productImage" src="<?php echo base_url()?>/assets/img/WorryFreeShippingIcon.png" alt="Product Image" height="200" width="200">
        <div class="col-md-6 col-md-offset-3">
            <div class="ibox-content rounded">
                <div class="row">
                    <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2">
                        <div class="row">
                            <div class="col-md-12" style="margin-bottom: 30px">
                                <h1 style="color: black; font-weight: normal; text-align: center">Login</h1>
                                <?if(isset($payingStore)) {?>
                                    <h2 style="color: black; font-weight: normal; text-align: center">to continue</h2>
                                <?}?>
                            </div>
                        </div>
<!--                        <div class="row">-->
<!--                            <div style="font-weight: lighter" class="col-md-12">-->
<!--                                <p class="withUs" style="text-align: center"> Need an account?<a href="--><?php //echo base_url()?><!--register/signUp">Sign up</a></p>-->
<!--                            </div>-->
<!--                        </div>-->
                        <form method="post" id="login_form" name="login_form" action=newStore>
                            <? if(isset($payingStore)) {?>
                                <input type="hidden" id="shopifyToken" name="shopifyToken" value="<? echo $shopifyToken?>">
                                <input type="hidden" id="storeName" name="storeName" value="<? echo $storeName?>">
                                <input type="hidden" id="payingStore" name="payingStore" value="<? echo $payingStore?>">
                                <input type="hidden" id="storeId" name="storeId" value="<? echo $storeId?>">
                            <? }?>
                            <div class="row signUpForm">
                                <div class="form-group col-md-12">
                                    <label>Username</label>
                                    <input class="form-control" onkeypress="checkEnter(event);" type="username" placeholder="Username" name="username" id="username" />
                                </div>
                                <div class="form-group col-md-12">
                                    <label>Password</label>
                                    <input class="form-control" type="password" id="password" name="password" onkeypress="checkEnter(event);" placeholder="Password" />
                                </div>
                            </div>
                            <button data-style="zoom-out" type="button" class="ladda-button btn btn-primary block full-width m-b createAccount" onclick="login()">Login</button>
                            <div class="row">
                                <div style="font-weight: lighter" class="col-md-12">
                                    <p class="withUs" style="text-align: center"> <a href="<?php echo base_url()?>forgotpassword">Forgot password/username?</a></p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-2.1.1.js"></script>
<!-- Sweet alert -->
<script src="<?php echo base_url() ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo base_url()?>assets/js/functions.js" type="text/javascript" charset="utf-8"></script>
<!-- Ladda -->
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.jquery.min.js"></script>
</body>

</html>


