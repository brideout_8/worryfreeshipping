<?php
/**
 * Created by PhpStorm.
 * User: Marketing101
 * Date: 12/13/17
 * Time: 3:54 PM
 */?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#333">
    <title>R&R Enterprises</title>
    <meta name="description" content="Material Style Theme">
    <link rel="shortcut icon" href="<?php echo base_url() ?>/assets/landing/assets/img/demo/RRRacewear.png">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="<?php echo base_url() ?>/assets/landing/assets/css/preload.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>/assets/landing/assets/css/plugins.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>/assets/landing/assets/css/style.light-blue-500.min.css">
    <!-- Sweet Alert -->
    <link href="<?php echo base_url() ?>assets/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <!-- Ladda style -->
    <link href="<?php echo base_url() ?>assets/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <!--<script src="<?php echo base_url() ?>assets/js/html5shiv.min.js"></script>-->
<!--    <script src="--><?php //echo base_url() ?><!--assets/js/respond.min.js"></script>-->
    <![endif]-->
</head>
<script type="text/javascript">
    function checkEnter(e) {
        var keyCode = (e.keyCode ? e.keyCode : e.which);
        if(keyCode == 13 )
        {
            e.preventDefault();

            login();
        }
    }

    function forgotpassword() {
        var formArray = $('#reset_form').serialize();
        var base_url  = '<?php echo base_url(); ?>';
        // document.reset_form.subBtn.disabled = true;
        jQuery.post(base_url+'forgotpassword/forgotpassword_validation/' , formArray, function(data) {
            //alert('here');

            if (data.success) {
                // window.location = base_url+"forgotpassword/success_email";
                swal({
                    title: "Success",
                    text: data.message,
                    type: "success"
                },
                function (isConfirm) {
                    if(isConfirm) {
                        window.location = base_url+"login";
                    }

                })
            } else {
                swal({
                    title: "Error",
                    text: data.message,
                    type: "warning"
                })
            }
        }, 'json');

        return false;
    }

    function login() {

        var formArray = $('#login_form').serialize();
        var base_url  = "<?php echo base_url(); ?>";
        // document.login_form.loginButton.value="Logging In...";
        var c = $('.ladda-button').ladda();
        c.ladda('start');
        // document.login_form.loginButton.disabled = true;
//        $('#loader').show();
//        $('#logInBox').hide();
        jQuery.post('<?php echo base_url(); ?>login/login_validation' , formArray, function(data) {
            c.ladda("stop");
            //alert('here');

            if (data.success)
            {
                var url = data.url;
                top.location = base_url+url;
            }
            else
            {
//                $('#loader').hide();
//                $('#logInBox').show();
//                 document.login_form.loginButton.disabled = false;
//                 document.login_form.loginButton.value="Login";
                if(data.error == 1){
                    swal({
                        title: "Error",
                        text: data.message,
//                 type: "success"
                    });
                } else if (data.error == 2){
                    swal({
                        title: "Error",
                        text: "Your subscription is past due. Please contact your administrator to update your account."
//                 type: "success"
                    });
                } else if (data.error == 3) {
                    swal({
                        title: "Error",
                        text: "Your subscription has been cancelled. Please contact your administrator to activate your account."
//                 type: "success"
                    });
                } else {
                    swal({
                        title: "Error",
                        text: "Please contact support at 636-717-0001",
//                 type: "success"
                    });
                }
            }
        }, 'json');

        return false;
    }
</script>
<style>
    #testBtn:disabled {
        background-color: lightskyblue;
    }
</style>
<body>
<div id="ms-preload" class="ms-preload">
    <div id="status">
        <div class="spinner">
            <div class="dot1"></div>
            <div class="dot2"></div>
        </div>
    </div>
</div>
<div class="bg-full-page ms-hero-bg-dark ms-hero-img-airplane back-fixed">
    <div class="mw-500 absolute-center">
        <div class="card color-dark shadow-6dp animated fadeIn animation-delay-7">
            <div class="ms-hero-bg-primary ms-hero-img-mountain">
                <h2 class="text-center no-m pt-4 pb-4 color-white index-1">Login Form </h2>
            </div>
            <ul class="nav nav-tabs nav-tabs-full nav-tabs-3 nav-tabs-transparent indicator-primary" role="tablist">
                <li class="nav-item" role="presentation">
                    <a href="#ms-login-tab" aria-controls="ms-login-tab" role="tab" data-toggle="tab" class="nav-link withoutripple active">
                        <i class="zmdi zmdi-account"></i> Login</a>
                </li>
<!--                <li class="nav-item" role="presentation">-->
<!--                    <a href="#ms-register-tab" aria-controls="ms-register-tab" role="tab" data-toggle="tab" class="nav-link withoutripple">-->
<!--                        <i class="zmdi zmdi-account-add"></i> Register</a>-->
<!--                </li>-->
                <li class="nav-item" role="presentation">
                    <a href="#ms-recovery-tab" aria-controls="ms-recovery-tab" role="tab" data-toggle="tab" class="nav-link withoutripple">
                        <i class="zmdi zmdi-key"></i> Recovery</a>
                </li>
            </ul>
            <div class="card-block">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade active show" id="ms-login-tab">
                        <form name="login_form" id="login_form" method="post">
                            <fieldset>
                                <div class="form-group label-floating">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                          <i class="zmdi zmdi-account"></i>
                                        </span>
                                        <label class="control-label" for="ms-form-user">Username<?php var_dump($rede)?></label>
                                        <input type="text" id="username" name="username" onkeypress="checkEnter(event);" class="form-control"> </div>
                                </div>
                                <div class="form-group label-floating">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                          <i class="zmdi zmdi-lock"></i>
                                        </span>
                                        <label class="control-label" for="ms-form-pass">Password</label>
                                        <input type="password" id="password" name="password" onkeypress="checkEnter(event);" class="form-control"> </div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-5">
                                    </div>
                                    <div class="col-7">
<!--                                        <input type="button" id="loginButton" name="loginButton" value = "Login" class="btn btn-raised btn-primary pull-right" onClick="login();">-->
                                        <button class="ladda-button btn btn-primary pull-right btn-raised" id="testBtn" data-style="zoom-out" onclick="login();">Login</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="ms-recovery-tab">
                        <form name="reset_form" id="reset_form" method="post">
                        <fieldset>
                            <div class="form-group label-floating">
                            <div class="form-group label-floating">
                                    <div class="input-group">
                                      <span class="input-group-addon">
                                        <i class="zmdi zmdi-email"></i>
                                      </span>
                                        <label class="control-label" >Email</label>
                                        <input type="email" name="email" id="email" class="form-control"> </div>
                                </div>
<!--                            <button class="btn btn-raised btn-block btn-primary" onClick="updateOrders();">Send Password</button>-->
                            <input class="btn btn-raised btn-block btn-primary" type="button" id="subBtn" name="subBtn" value="Get Reset Link" onClick="this.value = 'Submitting...'; return forgotpassword();"/>
                        </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center animated fadeInUp animation-delay-7">
            <a href="<? echo base_url()?>" class="btn btn-white">
                <i class="zmdi zmdi-home"></i> Go Home</a>
        </div>
    </div>
</div>
<script src="<?php echo base_url() ?>/assets/landing/assets/js/plugins.min.js"></script>
<script src="<?php echo base_url() ?>/assets/landing/assets/js/app.min.js"></script>
<!-- Sweet alert -->
<script src="<?php echo base_url() ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>
<!-- Ladda -->
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.jquery.min.js"></script>

</body>
</html>
