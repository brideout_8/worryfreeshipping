<?php
/**
 * Created by PhpStorm.
 * User: Marketing101
 * Date: 12/17/17
 * Time: 12:53 PM
 */?>
<script type="text/javascript">
    $(document).ready(function(){
        Ladda.bind( '.ladda-button',{  });
    });
</script>
<h1><b>Profile</b></h1>
<div class="ibox float-e-margins">
    <div class="ibox-content">
        <form class="form-horizontal" id="profileForm" name="profileForm" method="post">
            <input type="hidden" id="clientId" name="clientId" value="<? echo $clientId?>">
            <fieldset>
                <legend>Personal Information</legend>
                <div class="row">
                    <div id="packageInfo" class="col-lg-6 col-lg-offset-3">
                        <div class="form-group">
                            <label class="col-lg-2 control-label">First Name</label>
                            <div class="col-lg-5">
                               <input type="text" class="form-control" id="firstName" name="firstName" value="<? echo $results[0]->get("firstName")?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Last Name</label>
                            <div class="col-lg-5">
                               <input type="text" class="form-control" id="lastName" name="lastName" value="<? echo $results[0]->get("lastName")?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Email Address</label>
                            <div class="col-lg-5">
                                <input type="text" class="form-control" id="email" name="email" value="<? echo $results[0]->get("email")?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Username</label>
                            <div class="col-lg-5">
                                <input type="text" class="form-control" id="username" name="username" value="<? echo $results[0]->get("username")?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Phone Number</label>
                            <div class="col-lg-5">
                                <input type="text" class="form-control" id="phoneNumber" name="phoneNumber" value="<? echo $results[0]->get("phoneNumber")?>">
                            </div>
                        </div>
<!--                        <div class="form-group">-->
<!--                            <label class="col-lg-2 control-label">Password</label>-->
<!--                            <div class="col-lg-5">-->
<!--                               <input type="text" class="form-control" id="password" name="password" value="">-->
<!--                                <p class="pull-right">Leave blank if you do not want to change your password</p>-->
<!--                            </div>-->
<!--                        </div>-->
                    </div>
                </div>
            </fieldset>
            <div class="form-group">
                <div id="actionButtons" class="col-lg-2 col-lg-offset-5">
<!--                    <input type="button" value="Update Profile" class="bottomBtn btn btn-primary" name="updateBtn" id="updateBtn" onclick="updateClientProfile(); return false;"/>-->
                    <button class="ladda-button btn btn-primary" data-style="zoom-out" onclick="updateClientProfile(); return false">Update Profile</button>
                </div>
            </div>
        </form>
    </div>
</div>

