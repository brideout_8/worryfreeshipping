<?php
/**
 * Created by PhpStorm.
 * User: Marketing101
 * Date: 9/27/18
 * Time: 3:25 PM
 */?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Worry Free Shipping</title>

    <script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"></script>
    <script src="<?php echo base_url() ?>assets/js/functionsV1.js" type="text/javascript" charset="utf-8"></script>

    <link href="<?php echo base_url() ?>assets/css/plugins/chosen/chosen.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/css/plugins/iCheck/custom.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/css/animate.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/css/plugins/datapicker/datepicker3.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/css/plugins/dataTables/datatables.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/css/shopify/shopifyTable.css" rel="stylesheet" media="all">
    <!-- Toastr style -->
    <link href="<?php echo base_url() ?>assets/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo base_url()?>assets/js/validationEngine.jquery.css"/>
    <script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.validationEngine.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>

    <!-- Sweet Alert -->
    <link href="<?php echo base_url() ?>assets/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

    <!-- Ladda style -->
    <link href="<?php echo base_url() ?>assets/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">

</head>
<style>
    div.ibox-content {
        border: solid 1px #d9d9d9;
        /*box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);*/
        margin-top: 20px;
        border-radius: 5px;
        /*box-shadow: #3D3D3D;*/
    }
    .form-control {
        border-radius: 5px;
    }
</style>
<body class="gray-bg">
    <div class="passwordBox">
        <div class="row">
            <div class="col-md-12">
                <div class="ibox-content">
                    <h2>Success</h2>
                    <p>Your username and password reset link have been emailed to you.</p>
                    <div class="row">
                            <a href="<?php echo base_url() ?>login" class="btn btn-primary block full-width m-b" >Login</a>
                    </div>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-6">
                Worry Free Shipping
            </div>
            <div class="col-md-6 text-right">
                <small>© 2018</small>
            </div>
        </div>
    </div>
</body>
</html>


