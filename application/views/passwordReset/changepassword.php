<!DOCTYPE html>
<html>

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-126911005-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-126911005-1');
    </script>
    <!-- End of Google Analytics -->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Worry Free Shipping</title>

    <script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"></script>
    <script src="<?php echo base_url() ?>assets/js/functionsV1.js" type="text/javascript" charset="utf-8"></script>
    <link rel="shortcut icon" href="<?php echo base_url() ?>/assets/img/Favicon.png">
    <link href="<?php echo base_url() ?>assets/css/plugins/chosen/chosen.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/css/plugins/iCheck/custom.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/css/animate.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/css/plugins/datapicker/datepicker3.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/css/plugins/dataTables/datatables.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/css/shopify/shopifyTable.css" rel="stylesheet" media="all">
    <!-- Toastr style -->
    <link href="<?php echo base_url() ?>assets/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo base_url()?>assets/js/validationEngine.jquery.css"/>
    <script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.validationEngine.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>

    <!-- Sweet Alert -->
    <link href="<?php echo base_url() ?>assets/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

    <!-- Ladda style -->
    <link href="<?php echo base_url() ?>assets/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">
    
<script type="text/javascript">
	
	$(function () {
			var $switcher = $(".signup-switcher a");
			$switcher.click(function (e) {
				e.preventDefault();
				$switcher.removeClass("active");
				$(this).addClass("active");
				$("body").attr("class", $(this).data("class"));
			});
		});

function checkEnter(e)
{
	var keyCode = (e.keyCode ? e.keyCode : e.which);
	if(keyCode == 13 )
	{
   		e.preventDefault();
	 	changepassword();
	}
}

    function changepassword() {
        var formArray = $('#changepassword_form').serialize();
        var base_url  = '<?php echo base_url(); ?>';
        var l = $('.ladda-button').ladda();
        l.ladda('start');
        jQuery.post('<?php echo base_url(); ?>password_reset/changepassword_validation/' , formArray, function(data) {
            l.ladda('stop');
            if (data.success) {
                window.location = base_url+"password_reset/success";
            } else {
                swal({
                    title: "Error",
                    text: data.message,
                    type: "warning"
                })
            }
        }, 'json');

        return false;
    }
</script>

    <style>
        div.ibox-content {
            border: solid 1px #d9d9d9;
            /*box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);*/
            margin-top: 20px;
            border-radius: 5px;
            /*box-shadow: #3D3D3D;*/
        }
        .form-control {
            border-radius: 5px;
        }
        .middle-box {
            margin-top: 50px;
        }
        .productImage {
            /*display: block;*/
            max-width:200px;
            max-height:200px;
            width: auto;
            height: auto;
        }
    </style>

</head>

<body class="gray-bg">
    <div class="loginscreen row">
    <!--    <img class="productImage" src="--><?php //echo base_url() ?><!--assets/img/WorryFreeShippingIcon.png" alt="Logo" width="200" height="200" />-->
        <div class="passwordBox">
            <div class="row">
                <img class="productImage" src="<?php echo base_url() ?>assets/img/WorryFreeShippingIcon.png" alt="Logo" width="200" height="200" />
                <div class="col-md-12">
                    <div class="ibox-content">
                        <h2 class="font-bold">Create A New Password</h2>
                        <hr>
                        <form name="changepassword_form" id="changepassword_form" class="form-signin" method="post" action="">
                            <div class="form-group">
                                <input type="hidden" id="reset_token" name="reset_token" value="<?php echo @$token; ?>" />
                                <div class="fields">
                                    <strong>New Password</strong>
                                    <input class="form-control" type="password" placeholder="Password" id="password" name="password" />
                                </div><br>
                                <div class="fields">
                                    <strong>Confirm Password</strong>
                                    <input class="form-control" type="password" placeholder="Confirm Password" id="cpassword" name="cpassword" />
                                </div><br>
                                <div class="actions">
    <!--				<a href="javascript:void(0)" class="btn btn-primary block full-width m-b"  onClick="changepassword();return false;">Change Password</a>-->
                                    <button type="button" class="btn btn-primary block full-width m-b ladda-button" data-style="zoom-out" id="changeBtn" name="changeBtn" onClick="changepassword();return false;">Change Password</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <hr/>
            <div class="row">
                <div class="col-md-6">
                    Worry Free Shipping
                </div>
                <div class="col-md-6 text-right">
                   <small>© 2018</small>
                </div>
            </div>
        </div>
    </div>
</body>

</html>

<!-- Sweet alert -->
<script src="<?php echo base_url() ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>
<!-- Sweet alert -->
<script src="<?php echo base_url() ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>
<!--<script src="--><?php //echo base_url()?><!--assets/js/functions.js" type="text/javascript" charset="utf-8"></script>-->
<!-- Ladda -->
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.jquery.min.js"></script>