<?php
/**
 * Created by PhpStorm.
 * User: rapidware
 * Date: 9/2/18
 * Time: 1:48 PM
 */?>

<style>
    div.ibox-content {
        border: solid 1px #d9d9d9;
        /*box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);*/
        /*margin-top: 20px;*/
        border-radius: 5px;
        /*box-shadow: #3D3D3D;*/
    }
    .table th {
        text-align: center;
        font-weight: normal;
        border: 0;
    }
    .table td {
        /*font-weight: bold;*/
        /*font-size: 15px;*/
    }
    table tr:first-child td {
        border-top: 0;
    }
    /*a {*/
    /*color: #717171;*/
    /*}*/
    .normalLink {
        font-size: 15px;
        font-weight: normal;
    }
    p {
        margin:-2px 0 -2px 0;
    }
    .buttonLink {
        background:none!important;
        color: steelblue;
        border:none;
        padding:0!important;
        font: inherit;
        /*border is optional*/
        cursor: pointer;
    }
    .explainText {
        padding-top: 10px;
        color: #9a9a9a;
    }
    .titles {
        padding-top: 17px;
    }
    .dataText p {
        margin-bottom: 3px;
    }
    hr {
        border-color: #dcdcdc;
    }
    .form-control {
        border-radius: 5px;
    }
    label {
        font-weight: normal;
    }
    #title {
        text-align: center;
        color: green;
    }
    .bulky-radio-button {
        cursor: pointer;
        padding: 1.6rem;
        border-radius: 3px;
        border: 1px solid #c4cdd5
    }

    .bulky-radio + .bulky-radio {
        margin-top: 0.8rem
    }

    .bulky-radio {
        display: block;
        width: auto;
    }

    .bulky-radio > input:checked + .bulky-radio-button, .bulky-radio > .autocomplete-field:checked + .bulky-radio-button {
        border: 1px solid #5c6ac4;
        background: #fafbff;
        -webkit-box-shadow: 0 0 0 1px #5c6ac4;
        box-shadow: 0 0 0 1px #5c6ac4
    }
</style>
</head>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div id="nonOrder"></div>
        </div>
    </div>
</div>
<? $this->load->view("navigation/footer");?>
<script type="text/javascript">
    $(document).ready(function(){
        $('#nonOrder').html('<img height="50" width="50" style=" position: absolute; top: 50%; left: 50%;" src="'+loaderImage+'">');
        //var orderData = {orderId: "<?php //echo $orderId; ?>//"};
        $.ajax({
            url  	: base_url+'fulfillment/getNonOrderTable',
            type 	: 'POST',
            // data    : orderData,
            success : function(data){
                $("#nonOrder").html(data);
            }
        });
    });
</script>
<!-- Mainly scripts -->
<script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"></script>
<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/jeditable/jquery.jeditable.js"></script>

<script src="<?php echo base_url() ?>assets/js/plugins/dataTables/datatables.min.js"></script>

<!-- Sweet alert -->
<script src="<?php echo base_url() ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo base_url() ?>assets/js/inspinia.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/pace/pace.min.js"></script>

<!-- iCheck -->
<script src="<? echo base_url()?>assets/js/plugins/iCheck/icheck.min.js"></script>

<!-- Ladda -->
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.jquery.min.js"></script>

</body>

</html>








