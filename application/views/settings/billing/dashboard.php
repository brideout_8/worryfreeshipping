<?php
/**
 * Created by PhpStorm.
 * User: rapidware
 * Date: 8/19/18
 * Time: 11:00 AM
 */?>

<style>
    .table th {
        text-align: center;
        font-weight: bold;
        border: 0;
    }
    .table td {
        text-align: center;
        /*font-weight: bold;*/
        /*font-size: 15px;*/
    }
    table tr:first-child td {
        border-top: 0;
    }
    /*a {*/
    /*color: #717171;*/
    /*}*/
    .normalLink {
        font-size: 15px;
        font-weight: normal;
    }
    p {
        margin:-2px 0 -2px 0;
    }
    .buttonLink {
        background:none!important;
        color: steelblue;
        border:none;
        padding:0!important;
        font: inherit;
        /*border is optional*/
        cursor: pointer;
    }
    .explainText {
        padding-bottom: 3px;
        color: #9a9a9a;
    }
    .titles {
        padding-top: 17px;
    }
    .dataText p {
        margin-bottom: 3px;
    }
    hr {
        border-color: #dcdcdc;
    }
    .form-control {
        border-radius: 5px;
    }
    label {
        font-weight: normal;
    }
    tr {
        cursor: pointer;
    }
    tr:hover td {
        background: #f4f4f4
    }
</style>
</head>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div id="billing"></div>
        </div>
    </div>
</div>
<? $this->load->view("navigation/footer");?>
<script type="text/javascript">
    $(document).ready(function(){
        $('#billing').html('<img height="50" width="50" style=" position: absolute; top: 50%; left: 50%;" src="'+loaderImage+'">');
        $.ajax({
            url  	: base_url+'settings/getBillingInfo',
            type 	: 'POST',
            success : function(data){
                $("#billing").html(data);
            }
        });
    });
</script>
<!-- Mainly scripts -->
<script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"></script>
<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/jeditable/jquery.jeditable.js"></script>

<script src="<?php echo base_url() ?>assets/js/plugins/dataTables/datatables.min.js"></script>

<!-- Sweet alert -->
<script src="<?php echo base_url() ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo base_url() ?>assets/js/inspinia.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/pace/pace.min.js"></script>

<!-- iCheck -->
<script src="<? echo base_url()?>assets/js/plugins/iCheck/icheck.min.js"></script>

<!-- Ladda -->
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.jquery.min.js"></script>

</body>

</html>






