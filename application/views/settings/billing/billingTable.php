<?php
/**
 * Created by PhpStorm.
 * User: rapidware
 * Date: 8/19/18
 * Time: 11:02 AM
 */?>

<script type="text/javascript">
    $(document).ready(function(){
        Ladda.bind( '.ladda-button',{  });
    });
</script>
<div class="col-lg-8 col-md-8 col-lg-offset-2 col-mg-offset-2">
    <h1>Billing</h1>
    <hr>
<!--    <div class="row">-->
<!--        <div class="col-lg-4 col-md-4">-->
<!--            <h3 class="titles">Store address</h3>-->
<!--            <p class="explainText">This address will appear on your invoices. You can edit the address used to calculate shipping rates in your <a href="--><?// echo base_url()?><!--/settings/shipping" >shipping settings</a></p>-->
<!--        </div>-->
<!--        <div class=" col-lg-8 col-md-8">-->
<!--            <div class="ibox-content img-rounded">-->
<!--                <form id="companyAddressForm" name="companyAddressForm" method="post">-->
<!--                    <input type="hidden" name="companyId" id="companyId" value="--><?// echo $billingInfo[0]->getObjectId();?><!--">-->
<!--                    <div class="row">-->
<!--                        <div class="form-group col-md-12">-->
<!--                            <label>Legal name of business</label>-->
<!--                            <input class="form-control" type="text" name="name" id="name" value="--><?// echo $billingInfo[0]->name;?><!--" oninput="enableSave()" />-->
<!--                        </div>-->
<!--                        <div class="form-group col-md-12">-->
<!--                            <label>Phone</label>-->
<!--                            <input class="form-control" type="text" name="phone" id="phone" value="--><?// echo $billingInfo[0]->phone;?><!--" oninput="enableSave()"/>-->
<!--                        </div>-->
<!--                        <div class="form-group col-md-12">-->
<!--                            <label>Address</label>-->
<!--                            <input class="form-control" type="text" name="address" id="address" value="--><?// echo $billingInfo[0]->address;?><!--" oninput="enableSave()"/>-->
<!--                        </div>-->
<!--                        <div class="form-group col-md-6">-->
<!--                            <label>City</label>-->
<!--                            <input class="form-control" type="text" name="city" id="city" value="--><?// echo $billingInfo[0]->city;?><!--" oninput="enableSave()"/>-->
<!--                        </div>-->
<!--                        <div class="form-group col-md-6">-->
<!--                            <label>Zip</label>-->
<!--                            <input class="form-control" type="text" name="zip" id="zip" value="--><?// echo $billingInfo[0]->zip;?><!--" oninput="enableSave()"/>-->
<!--                        </div>-->
<!--                        <div class="form-group col-md-6">-->
<!--                            <label>Country</label>-->
<!--                            <select onchange="enableSave()" id="country" name="country" class="form-control minimal ui-select">-->
<!--                                <option--><?// if($billingInfo[0]->get("country") == "United States") {?><!-- selected --><?//}?><!-- value="United States">United Sates</option>-->
<!--                                <option --><?// if($billingInfo[0]->get("country") == "Canada") {?><!-- selected --><?//}?><!-- value="Canada">Canada</option>-->
<!--                                <option --><?// if($billingInfo[0]->get("country") == "United Kingdom") {?><!-- selected --><?//}?><!-- value="United Kingdom">United Kingdom</option>-->
<!--                                <option  --><?// if($billingInfo[0]->get("country") == "Australia") {?><!-- selected --><?//}?><!--value="Australia">Australia</option>-->
<!--                            </select>-->
<!--                        </div>-->
<!--                        <div class="form-group col-md-6">-->
<!--                            <label>State</label>-->
<!--                            <input class="form-control" type="text" name="state" id="state" value="--><?// echo $billingInfo[0]->state;?><!--" oninput="enableSave()"/>-->
<!--                        </div>-->
<!--                        <button id="saveBtn" name="saveBtn" disabled class="btn btn-primary pull-right ladda-button" data-style="zoom-out" onclick="updateBilling()" >Save</button>-->
<!--                    </div>-->
<!--                </form>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--    <hr>-->
    <div class="row">
        <div class="col-lg-4 col-md-4">
            <h3 class="titles">Current usage</h3>
<!--            <p class="explainText">This address will appear on your invoices. You can edit the address used to calculate shipping rates in your <a href="<?// echo base_url()?><!--/settings/shipping" >shipping settings</a></p> -->
        </div>
        <div class=" col-lg-8 col-md-8">
            <div class="ibox-content img-rounded">
                <div class="row">
                    <div class="col-lg-3 col-md-3">
                        <h3 style="padding-top: 8px">Total Labels Per Plan</h3>
                        <span><? echo $labelsPerPlan["labelsRemaining"]?></span>
                    </div>
                    <div class="col-lg-3 col-md-3">
                        <h3 style="padding-top: 8px">Labels Remaining</h3>
                        <span><? echo $billingInfo[0]->get("labelsRemaining")?></span>
                    </div>
                    <div class="col-lg-3 col-md-3">
                        <h3 style="padding-top: 8px">Starts On</h3>
                        <span><? echo $startDate?></span>
                    </div>
                    <div class="col-lg-3 col-md-3">
                        <h3 style="padding-top: 8px">Ends On</h3>
                        <span><? echo $endDate?></span>
                    </div>
                </div>
                <hr>
                <? if($this->session->subscriptionLevel != "0") {?>
                    <p class="explainText">Your labels will reset on <? echo $endDate?></p>
                    <p class="explainText"><a href="<? echo base_url()?>settings/buyLabels">Purchase</a> more shipping labels.</p>
                <?}?>
                <p class="explainText"><a href="<? echo base_url()?>register/changePlan">Compare plans</a> with different features and rates.</p>
            </div>
        </div>
    </div>
    <? if(!isset($error) && count($invoices) > 0) {?>
        <hr>
        <div class="row">
            <div class="col-lg-4 col-md-4">
                <h3 class="titles">Invoices</h3>
    <!--            <p class="explainText">Your invoices are paid using your active payment method.</p>-->
            </div>
            <div class=" col-lg-8 col-md-8">
                <div class="ibox-content img-rounded">
                    <table class="table" >
                    <thead>
                    <tr style="text-align: center">
                        <th>Invoice Number</th>
                        <th>Invoice Date</th>
                        <th>Billing Period</th>
                        <th>Total</th>
                        <!--                        <th>Owner</th>-->
    <!--                    <th>Action</th>-->
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($invoices as $invoice) { ?>
                        <tr class='clickable-rowBilling' data-href='<?php echo base_url()?>settings/invoice?invoice=<?php echo $invoice->getObjectId()?>'>
                            <td><? echo $invoice->getObjectId()?></td>
                            <td><? $date = strtotime($invoice->get("billingDate")); echo date('M d, Y',$date);?></td>
                            <td><? if($invoice->get("billingPeriodStartDate") != "") { $startDate = strtotime($invoice->get("billingPeriodStartDate")); $endDate = strtotime($invoice->get("billingPeriodEndDate")); echo date('M d, Y', $startDate)." - ".date('M d, Y', $endDate);} else { $date = strtotime($invoice->get("billingDate")); echo date('M d, Y', $date). " - ".date('M d, Y', $date); }?></td>
                            <td>$<? if($invoice->get("price") != "") { echo number_format($invoice->get("price"),2); } else { $price = $invoice->get("chargePerOrder") * $invoice->get("orderCountToCharge"); echo number_format($price,2);} ?></td>
    <!--                        <td><a type="button" class="btn btn-danger btn-xs" href="--><?// base_url()?><!--/settings/stampsSignUp?id=--><?// echo $carrier->getObjectId()?><!--">Set Up</a></td>-->
                        </tr>
                    <?php } ?>
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    <? } else if(!isset($error) && count($invoices) < 1) { ?>
        <hr>
        <div class="row">
            <div class="col-lg-4 col-md-4">
                <h3 class="titles">Invoices</h3>
                <!--            <p class="explainText">Your invoices are paid using your active payment method.</p>-->
            </div>
            <div class=" col-lg-8 col-md-8">
                <div class="ibox-content img-rounded">
                    <h3 style="text-align: center">You don't have any invoices yet</h3>
                    <p style="text-align: center">Your invoices will be shown here once you have paid your first bill.</p>
                    <!--                <h3>Credit card</h3>-->
                </div>
            </div>
        </div>
    <? } else {?>
        <hr>
        <div class="row">
            <div class="col-lg-4 col-md-4">
                <h3 class="titles">Invoices</h3>
                <!--            <p class="explainText">Your invoices are paid using your active payment method.</p>-->
            </div>
            <div class=" col-lg-8 col-md-8">
                <div class="ibox-content img-rounded">
                    <h3 style="text-align: center">Error</h3>
                    <p style="text-align: center">We encountered an error. Please try again or contact support at support@worryfreeshipping.com with error: <? var_dump($error)?></p>
                    <!--                <h3>Credit card</h3>-->
                </div>
            </div>
        </div>
    <? }?>
</div>
<script>
    jQuery(document).ready(function($) {
        $(".clickable-rowBilling").click(function() {
            window.location = $(this).data("href");
            console.log($(this).data("href"));
        });
    });
</script>
