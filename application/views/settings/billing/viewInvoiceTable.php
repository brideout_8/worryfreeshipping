<?php
/**
 * Created by PhpStorm.
 * User: Marketing101
 * Date: 9/24/18
 * Time: 3:37 PM
 */?>

<div class="col-lg-6 col-md-6 col-lg-offset-3 col-mg-offset-3">
    <a href="<?php echo base_url()?>settings/billing/dashboard">< Invoices</a>
    <h1>Invoice</h1>
    <div class="row">
        <div class=" col-lg-12 col-md-12">
            <div class="ibox-content img-rounded">
                <div class="row">
                    <div class="col-lg-4 col-md-4">
                        <h3 style="text-align: center" style="padding-top: 8px">Invoice Number:</h3>
                        <p id="topInfo"><? echo $invoice[0]->getObjectId();?></p>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <h3 style="text-align: center" style="padding-top: 8px">Billing Period:</h3>
                        <p id="topInfo"><? if($invoice[0]->get("billingPeriodStartDate") != "") { $startDate = strtotime($invoice[0]->get("billingPeriodStartDate")); $endDate = strtotime($invoice[0]->get("billingPeriodEndDate")); echo date('M d, Y', $startDate)." - ".date('M d, Y', $endDate);} else { $date = strtotime($invoice[0]->get("billingDate")); echo date('M d, Y', $date). " - ".date('M d, Y', $date); }?></p>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <h3 style="text-align: center" style="padding-top: 8px">Invoice Date:</h3>
                        <p id="topInfo"><? $date = strtotime($invoice[0]->get("billingDate")); echo date('M d, Y',$date);?></p>
                    </div>
                </div>
                <hr>
                <div class="row" style="margin-top: 50px; margin-right: 25px; margin-left: 25px">
                    <table class="table" >
                        <thead>
                        <tr>
                            <th>Service</th>
                            <th style="text-align: right">Quantity</th>
                            <th style="text-align: right">Amount</th>
                            <th style="text-align: right">Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><? if($invoice[0]->get("chargeType") == "usage") { echo "Labels"; } else { echo "Monthly Subscription";}?></td>
                            <td style="text-align: right"><? if($invoice[0]->get("chargeType") == "usage") { echo $invoice[0]->get("orderCountToCharge"); } else { echo "1";}?></td>
                            <td style="text-align: right">$<? if($invoice[0]->get("chargeType") == "usage") { echo $invoice[0]->get("chargePerOrder"); } else { echo $invoice[0]->get("price");}?></td>
                            <td style="text-align: right">$<? if($invoice[0]->get("chargeType") == "usage") { echo number_format($invoice[0]->get("orderCountToCharge") * $invoice[0]->get("chargePerOrder"),2); } else { echo $invoice[0]->get("price");}?></td>
                        </tr>
                        </tbody>
                    </table>
                    <hr>
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-lg-offset-9 col-md-offset-9">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <td>Subtotal:</td>
                                        <td style="text-align: right">$<? if($invoice[0]->get("chargeType") == "usage") { echo number_format($invoice[0]->get("orderCountToCharge") * $invoice[0]->get("chargePerOrder"),2); } else { echo $invoice[0]->get("price");}?></td>
                                    </tr>
                                    <tr>
                                        <td>Total:</td>
                                        <td style="text-align: right">$<? if($invoice[0]->get("chargeType") == "usage") { echo number_format($invoice[0]->get("orderCountToCharge") * $invoice[0]->get("chargePerOrder"),2); } else { echo $invoice[0]->get("price");}?></td>
                                    </tr>
                                    <tr>
                                        <td>Amount Due:</td>
                                        <td style="text-align: right">$0.00</td>
                                    </tr>
                                </table>
                            </tbody>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

