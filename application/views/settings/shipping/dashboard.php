<?php
/**
 * Created by PhpStorm.
 * User: rapidware
 * Date: 8/18/18
 * Time: 8:33 AM
 */?>

<style>
    .table th {
        /*text-align: center;*/
        font-weight: normal;
        border: 0;
    }
    .table td {
        /*font-weight: bold;*/
        /*font-size: 15px;*/
    }
    table tr:first-child td {
        border-top: 0;
    }
    /*a {*/
        /*color: #717171;*/
    /*}*/
    .normalLink {
        font-size: 15px;
        font-weight: normal;
    }
    p {
        margin:-2px 0 -2px 0;
    }
    .buttonLink {
        background:none!important;
        color: steelblue;
        border:none;
        padding:0!important;
        font: inherit;
        /*border is optional*/
        cursor: pointer;
    }
    .explainText {
        padding-top: 10px;
        color: #9a9a9a;
    }
    .titles {
        padding-top: 17px;
    }
    .dataText p {
        margin-bottom: 3px;
    }
    hr {
        border-color: #dcdcdc;
    }
    .modal-backdrop {
        background-color: #c7c7c7
    }
    .explainText {
        /*padding-top: 10px;*/
        color: #9a9a9a;
    }
    #toast-container>.toast {
        background-image: none !important;
    }
    .carrierImage {
        /*display: block;*/
        max-width:180px;
        max-height: 130px;
        width: auto;
        height: auto;
        margin-left: 20px
    }
</style>
</head>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div id="shipping"></div>
        </div>
    </div>
</div>
<? $this->load->view("navigation/footer");?>
<script type="text/javascript">
    $(document).ready(function(){
        $('#shipping').html('<img height="50" width="50" style=" position: absolute; top: 50%; left: 50%;" src="'+loaderImage+'">');
        $.ajax({
            url  	: base_url+'settings/getShippingInfo',
            type 	: 'POST',
            success : function(data){
                $("#shipping").html(data);
                $('.i-checks').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green'
                });
                $('.labelFormat').on('ifChecked', function () {
                    var formArray = $('#shippingLabelForm').serialize();
                    jQuery.post(base_url+'settings/updateLabel' , formArray, function(data) {
                        if (data.success) {
                            toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "progressBar": false,
                                "preventDuplicates": true,
                                "positionClass": "toast-bottom-center",
                                "onclick": null,
                                "showDuration": "400",
                                "hideDuration": "1000",
                                "timeOut": "3000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            };
                            toastr.success('Successfully updated shipping label format');
                        } else {
                            swal({
                                title: "Error",
                                text: data.message
                            });
                        }
                    }, 'json');
                    return false;
                });
            }
        });

        //On load of edit box modal
        $('#editPackageModal').on('show.bs.modal', function(e) {
            var packagename = e.relatedTarget.dataset.packagename;
            var length = e.relatedTarget.dataset.length;
            var width = e.relatedTarget.dataset.width;
            var height = e.relatedTarget.dataset.height;
            var sizeMeasurement = e.relatedTarget.dataset.sizemeasurement;
            var packageWeight = e.relatedTarget.dataset.packageweight;
            var weightMeasurement = e.relatedTarget.dataset.weightmeasurement;
            var defaultBox = e.relatedTarget.dataset.default;
            var packageId = e.relatedTarget.dataset.packageid;
            var weightUnits = e.relatedTarget.dataset.weightunits;
            var sizeUnits = e.relatedTarget.dataset.sizeunits;
            $(".modal-body #packageName").val(packagename);
            $(".modal-body #length").val(length);
            $(".modal-body #width").val(width);
            $(".modal-body #height").val(height);
            $(".modal-body #sizeMeasurement").val(sizeMeasurement);
            $(".modal-body #packageWeight").val(packageWeight);
            $(".modal-body #weightMeasurement").val(weightMeasurement);
            $(".modal-body #packageId").val(packageId);
            $("#sizeUnitsEdit").val(sizeUnits);
            $("#weightUnitsEdit").val(weightUnits);
            if(defaultBox === "1") {
                $('#defaultPackage').hide();
                $('#delete').prop("disabled", true)
            }
            // $('.i-checks').iCheck({
            //     checkboxClass: 'icheckbox_square-green',
            //     radioClass: 'iradio_square-orange'
            // });
        });
        //On close of edit box modal
        $('#editPackageModal').on('hidden.bs.modal', function(e) {
            $('#delete').prop("disabled", false);
            $('#defaultPackage').show();
            $(".modal-body #packageName").val("");
            $(".modal-body #length").val("");
            $(".modal-body #width").val("");
            $(".modal-body #height").val("");
            $(".modal-body #sizeMeasurement").val("");
            $(".modal-body #packageWeight").val("");
            $(".modal-body #weightMeasurement").val("");
            $(".modal-body #packageId").val("");
            $("#sizeUnitsEdit").val("");
            $("#weightUnitsEdit").val("");
        });
        //On load of edit carrier box modal
        $('#editCarrierPackageModal').on('show.bs.modal', function(e) {
            var packagename = e.relatedTarget.dataset.packagename2;
            var defaultBox = e.relatedTarget.dataset.default2;
            var packageId = e.relatedTarget.dataset.packageid2;
            $(".modal-body #carrierPackageName").val(packagename);
            $(".modal-body #carrierPackageId").val(packageId);
            if(defaultBox === "1") {
                $('#defaultCarrierPackage').hide();
                $('#deleteCarrier').prop("disabled", true)
            }
        });
        //On close of edit carrier box modal
        $('#editCarrierPackageModal').on('hidden.bs.modal', function(e) {
            $('#deleteCarrier').prop("disabled", false);
            $('#defaultCarrierPackage').show();
            $(".modal-body #carrierPackageName").val("");
            $(".modal-body #carrierPackageId").val("");
        });
        //On load of add funds modal
        $('#addFundsModal').on('show.bs.modal', function(e) {
            var carrierId = e.relatedTarget.dataset.carrierid;
            $(".modal-body #carrierId").val(carrierId);
        });
    });
</script>
<!-- Mainly scripts -->
<script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"></script>
<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/jeditable/jquery.jeditable.js"></script>

<script src="<?php echo base_url() ?>assets/js/plugins/dataTables/datatables.min.js"></script>

<!-- Sweet alert -->
<script src="<?php echo base_url() ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo base_url() ?>assets/js/inspinia.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/pace/pace.min.js"></script>

<!-- iCheck -->
<script src="<? echo base_url()?>assets/js/plugins/iCheck/icheck.min.js"></script>

<!-- Toastr script -->
<script src="<? echo base_url()?>assets/js/plugins/toastr/toastr.min.js"></script>

<!-- Ladda -->
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.jquery.min.js"></script>

</body>

</html>





