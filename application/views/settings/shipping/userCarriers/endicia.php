<?php
/**
 * Created by PhpStorm.
 * User: Marketing101
 * Date: 9/11/18
 * Time: 2:00 PM
 */?>
<style>
    div.ibox-content {
        border: solid 1px #d9d9d9;
        /*box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);*/
        /*margin-top: 20px;*/
        border-radius: 5px;
        /*box-shadow: #3D3D3D;*/
    }
    .table th {
        /*text-align: center;*/
        font-weight: normal;
        border: 0;
    }
    .table td {
        /*font-weight: bold;*/
        /*font-size: 15px;*/
    }
    table tr:first-child td {
        border-top: 0;
    }
    /*a {*/
    /*color: #717171;*/
    /*}*/
    .normalLink {
        font-size: 15px;
        font-weight: normal;
    }
    p {
        margin:-2px 0 -2px 0;
    }
    .buttonLink {
        background:none!important;
        color: steelblue;
        border:none;
        padding:0!important;
        font: inherit;
        /*border is optional*/
        cursor: pointer;
    }
    .explainText {
        padding-top: 10px;
        color: #9a9a9a;
    }
    .titles {
        padding-top: 17px;
    }
    .dataText p {
        margin-bottom: 3px;
    }
    hr {
        border-color: #dcdcdc;
    }
    .modal-backdrop {
        background-color: #c7c7c7
    }
    .explainText {
        /*padding-top: 10px;*/
        color: #9a9a9a;
    }
    #toast-container>.toast {
        background-image: none !important;
    }
</style>
</head>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="col-lg-8 col-md-8 col-lg-offset-2 col-mg-offset-2">
                <h1 style="font-weight: bold">Connect Endicia Account</h1>
                <hr>
                <div class="row">
                    <div class="col-lg-4 col-md-4">
                        <h3 class="titles">Connect Account</h3>
                        <!--                        <p class="explainText">The address used to calculate shipping rates and the return address on your labels</p>-->
                    </div>
                    <div class=" col-lg-8 col-md-8">
                        <div class="ibox-content img-rounded">
                            <h3 style="padding-bottom: 8px">Account Information</h3>
                            <form id="endiciaUserForm" name="endiciaUserForm" method="post" class="form-horizontal">
                                <div class="form-group">
                                    <label autocomplete="false" class="col-lg-2 control-label">Account Nickname</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" id="nickname" name="nickname" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label autocomplete="false" class="col-lg-2 control-label">Username or Account Number</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" id="username" name="username" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label autocomplete="false" class="col-lg-2 control-label">Passphrase</label>
                                    <div class="col-lg-10">
                                        <input type="password" class="form-control" id="passphrase" name="passphrase" value="">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <button style="margin-top: 10px" id="createStamps" name="createStamps" class="btn btn-primary pull-right ladda-button" data-style="zoom-out" onclick="connectEndiciaAccount()" >Connect Account</button>
                        <a style="margin-top: 10px; margin-right: 10px" href="<? echo base_url()?>/settings/shipping" class="btn btn-default pull-right ladda-button"  >Cancel</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<? $this->load->view("navigation/footer");?>
<!-- Mainly scripts -->
<script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"></script>
<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/jeditable/jquery.jeditable.js"></script>

<script src="<?php echo base_url() ?>assets/js/plugins/dataTables/datatables.min.js"></script>

<!-- Sweet alert -->
<script src="<?php echo base_url() ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo base_url() ?>assets/js/inspinia.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/pace/pace.min.js"></script>

<!-- iCheck -->
<script src="<? echo base_url()?>assets/js/plugins/iCheck/icheck.min.js"></script>

<!-- Toastr script -->
<script src="<? echo base_url()?>assets/js/plugins/toastr/toastr.min.js"></script>

<!-- Ladda -->
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.jquery.min.js"></script>

</body>

</html>


