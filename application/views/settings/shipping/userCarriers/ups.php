<?php
/**
 * Created by PhpStorm.
 * User: Marketing101
 * Date: 9/11/18
 * Time: 2:06 PM
 */?>

<style>
    div.ibox-content {
        border: solid 1px #d9d9d9;
        /*box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);*/
        /*margin-top: 20px;*/
        border-radius: 5px;
        /*box-shadow: #3D3D3D;*/
    }
    .table th {
        /*text-align: center;*/
        font-weight: normal;
        border: 0;
    }
    .table td {
        /*font-weight: bold;*/
        /*font-size: 15px;*/
    }
    table tr:first-child td {
        border-top: 0;
    }
    /*a {*/
    /*color: #717171;*/
    /*}*/
    .normalLink {
        font-size: 15px;
        font-weight: normal;
    }
    p {
        margin:-2px 0 -2px 0;
    }
    .buttonLink {
        background:none!important;
        color: steelblue;
        border:none;
        padding:0!important;
        font: inherit;
        /*border is optional*/
        cursor: pointer;
    }
    .explainText {
        padding-top: 10px;
        color: #9a9a9a;
    }
    .titles {
        padding-top: 17px;
    }
    .dataText p {
        margin-bottom: 3px;
    }
    hr {
        border-color: #dcdcdc;
    }
    .modal-backdrop {
        background-color: #c7c7c7
    }
    .explainText {
        /*padding-top: 10px;*/
        color: #9a9a9a;
    }
    #toast-container>.toast {
        background-image: none !important;
    }
</style>
</head>

<script>
    function setCountryCode(provinceCode) {
        var country = $('#countryCode').find(":selected").text();
        $('#countryAddress').val(country);//Set for address change
        var formArray = {"country":country};
        jQuery.post(base_url+'fulfillment/getProvince/', formArray, function(data) {
            $('#state').children().remove();
            $.each( data.provinceCodes, function( key, value ) {
                $('#state').append($('<option>', {
                    value: key,
                    text: value
                }));
            });
            $('#state').val(provinceCode);
            // console.log(provinceCode);
            // console.log($('#province_code').val())

        }, 'json');

    }
</script>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="col-lg-8 col-md-8 col-lg-offset-2 col-mg-offset-2">
                <h1 style="font-weight: bold">Connect UPS Account</h1>
                <hr>
                <div class="row">
                    <div class="col-lg-4 col-md-4">
                        <h3 class="titles">Connect Account</h3>
                        <!--                        <p class="explainText">The address used to calculate shipping rates and the return address on your labels</p>-->
                    </div>
                    <div class=" col-lg-8 col-md-8">
                        <div class="ibox-content img-rounded">
                            <h3 style="padding-bottom: 8px">Account Information</h3>
                            <form id="upsUserForm" name="upsUserForm" method="post" class="form-horizontal">
                                <div class="form-group">
                                    <label autocomplete="false" class="col-lg-2 control-label">Account Nickname</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" id="nickname" name="nickname" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label autocomplete="false" class="col-lg-2 control-label">Account Number</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" id="accountNumber" name="accountNumber" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Account Country Code</label>
                                    <div class="col-lg-4">
                                        <select class="form-control m-b minimal ui-select" name="accountCountryCode" id="accountCountryCode">
                                            <? foreach ($countryCodes as $code => $name) {?>
                                                <option <? if($companyInfo[0]->get("countryCode") == $code) {?> selected <?}?> value="<? echo $code?>"><? echo $name?></option>
                                            <?}?>
                                        </select>
                                    </div>
                                    <label autocomplete="false" class="col-lg-2 control-label">Account Postal Code</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" id="accountPostalCode" name="accountPostalCode" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label autocomplete="false" class="col-lg-2 control-label">First Name</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" id="firstName" name="firstName" value="">
                                    </div>
                                    <label autocomplete="false" class="col-lg-2 control-label">Last Name</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" id="lastName" name="lastName" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label autocomplete="false" class="col-lg-2 control-label">Company</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" id="company" name="company" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label autocomplete="false" class="col-lg-2 control-label">Address</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" id="address1" name="address1" value="">
                                    </div>
                                    <label autocomplete="false" class="col-lg-2 control-label">Address (APT/SUITE)</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" id="address2" name="address2" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label autocomplete="false" class="col-lg-2 control-label">City</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" id="city" name="city" value="">
                                    </div>
                                    <label class="col-lg-2 control-label">State/Province</label>
                                    <div class="col-lg-4">
                                        <select class="form-control m-b minimal ui-select" name="state" id="state">
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label autocomplete="false" class="col-lg-2 control-label">Postal Code</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" id="postalCode" name="postalCode" value="">
                                    </div>
                                    <label class="col-lg-2 control-label">Country</label>
                                    <div class="col-lg-4">
                                        <select class="form-control m-b minimal ui-select" name="countryCode" id="countryCode" onchange="setCountryCode('<? echo $companyInfo[0]->get("stateCode");?>')">
                                            <option selected disabled value=""></option>
                                            <? foreach ($countryCodes as $code => $name) {?>
                                                <option <? if($companyInfo[0]->get("countryCode") == $code) {?> selected <?}?> value="<? echo $code?>"><? echo $name?></option>
                                            <?}?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label autocomplete="false" class="col-lg-2 control-label">Email</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" id="email" name="email" value="">
                                    </div>
                                    <label autocomplete="false" class="col-lg-2 control-label">Phone</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" id="phone" name="phone" value="">
                                    </div>
                                </div>
                                <hr>
                                <h3>The remaining section is only required if you have recieved an invoice in the past 90 days from UPS.</h3>
                                <div class="form-group">
                                    <label autocomplete="false" class="col-lg-2 control-label">Control Id</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" id="controlId" name="controlId" value="">
                                    </div>
                                    <label autocomplete="false" class="col-lg-2 control-label">Invoice Number</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" id="invoiceNumber" name="invoiceNumber" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label autocomplete="false" class="col-lg-2 control-label">Invoice Amount</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" id="invoiceAmount" name="invoiceAmount" value="">
                                    </div>
                                    <label autocomplete="false" class="col-lg-2 control-label">Invoice Date</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" id="invoiceDate" name="invoiceDate" placeholder="EX: 2017-04-01" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-4 col-lg-offset-1">
                                        <div class="i-checks">
                                            <label style="font-weight: normal"> <input class="agreeToEULA form-control" type="checkbox" value="agree" name="agreeUPS" id="agreeUPS"> Agree to <a href="https://www.ups.com/media/en/UTA_with_EUR.pdf">UPS Technology Agreement.</a></label>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <button style="margin-top: 10px" id="createUPS" name="createUPS" disabled="" class="btn btn-primary pull-right ladda-button" data-style="zoom-out" onclick="connectUPSAccount()" >Connect Account</button>
                        <a style="margin-top: 10px; margin-right: 10px" href="<? echo base_url()?>/settings/shipping" class="btn btn-default pull-right ladda-button"  >Cancel</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<? $this->load->view("navigation/footer");?>
<script>
    $(document).ready(function(){
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green'
        });
        setCountryCode('<? echo $companyInfo[0]->get("stateCode");?>')

        $('#agreeUPS').on('ifChecked', function(event){
            $('#createUPS').prop("disabled", false);
        });
        $('#agreeUPS').on('ifUnchecked', function(event){
            $('#createUPS').prop("disabled", true);
        });
    });
</script>

<!-- Mainly scripts -->
<script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"></script>
<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/jeditable/jquery.jeditable.js"></script>

<script src="<?php echo base_url() ?>assets/js/plugins/dataTables/datatables.min.js"></script>

<!-- Sweet alert -->
<script src="<?php echo base_url() ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo base_url() ?>assets/js/inspinia.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/pace/pace.min.js"></script>

<!-- iCheck -->
<script src="<? echo base_url()?>assets/js/plugins/iCheck/icheck.min.js"></script>

<!-- Toastr script -->
<script src="<? echo base_url()?>assets/js/plugins/toastr/toastr.min.js"></script>

<!-- Ladda -->
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.jquery.min.js"></script>

</body>

</html>



