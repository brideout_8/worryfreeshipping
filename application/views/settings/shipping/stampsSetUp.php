<?php
/**
 * Created by PhpStorm.
 * User: Marketing101
 * Date: 9/11/18
 * Time: 7:42 AM
 */?>

<style>
    div.ibox-content {
        border: solid 1px #d9d9d9;
        /*box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);*/
        /*margin-top: 20px;*/
        border-radius: 5px;
        /*box-shadow: #3D3D3D;*/
    }
    .table th {
        /*text-align: center;*/
        font-weight: normal;
        border: 0;
    }
    .table td {
        /*font-weight: bold;*/
        /*font-size: 15px;*/
    }
    table tr:first-child td {
        border-top: 0;
    }
    /*a {*/
    /*color: #717171;*/
    /*}*/
    .normalLink {
        font-size: 15px;
        font-weight: normal;
    }
    p {
        margin:-2px 0 -2px 0;
    }
    .buttonLink {
        background:none!important;
        color: steelblue;
        border:none;
        padding:0!important;
        font: inherit;
        /*border is optional*/
        cursor: pointer;
    }
    .explainText {
        padding-top: 10px;
        color: #9a9a9a;
    }
    .titles {
        padding-top: 17px;
    }
    .dataText p {
        margin-bottom: 3px;
    }
    hr {
        border-color: #dcdcdc;
    }
    .modal-backdrop {
        background-color: #c7c7c7
    }
    .explainText {
        /*padding-top: 10px;*/
        color: #9a9a9a;
    }
    #toast-container>.toast {
        background-image: none !important;
    }
</style>
</head>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="col-lg-8 col-md-8 col-lg-offset-2 col-mg-offset-2">
                <h1 style="font-weight: bold">Create Stamps.com Account</h1>
                <hr>
                <div class="row">
                    <div class="col-lg-3 col-md-3">
                        <h3 class="titles">Create Account</h3>
<!--                        <p class="explainText">The address used to calculate shipping rates and the return address on your labels</p>-->
                    </div>
                    <div class=" col-lg-9 col-md-9">
                        <div class="ibox-content img-rounded">
                            <h3 style="padding-bottom: 8px">Company Information</h3>
                            <form id="stampsForm" name="stampsForm" method="post" class="form-horizontal">
                                <input type="hidden" id="carrierObjectId" name="carrierObjectId" value="<?php echo $carrierObjectId?>" />
                                <div class="form-group">
                                    <label autocomplete="false" class="col-lg-2 control-label">Account Nickname</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" id="nickname" name="nickname" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label autocomplete="false" class="col-lg-2 control-label">Email</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" id="email" name="email" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label autocomplete="false" class="col-lg-2 control-label">Name</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" id="name" name="name" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label autocomplete="false" class="col-lg-2 control-label">Phone</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" id="phone" name="phone" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label autocomplete="false" class="col-lg-2 control-label">Company Name</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" id="company" placeholder="optional" name="company" value="<? echo $shippingFrom->get("name");?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label autocomplete="false" class="col-lg-2 control-label">Address</label>
                                    <div class="col-lg-7">
                                        <input type="text" class="form-control" id="address1" name="address1" value="<? echo $shippingFrom->get("address1")?>">
                                    </div>
                                    <div class="col-lg-3">
                                        <input type="text" class="form-control" placeholder="optional" id="address2" name="address2" value="<? echo $shippingFrom->get("address2")?>">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label style="font-size:14px; font-weight: bolder;" for="inputLast" class="col-md-2 control-label">City</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" id="city" name="city" value="<? echo $shippingFrom->get("city")?>">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label style="font-size:14px; font-weight: bolder;" for="inputLast" class="col-md-2 control-label">State/Province</label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" id="state" name="state" value="<? echo $shippingFrom->get("stateCode")?>">
                                    </div>
                                    <label style="font-size:14px; font-weight: bolder;" for="inputLast" class="col-md-1 control-label">Zip</label>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control" id="zip" name="zip" value="<? echo $shippingFrom->get("zip")?>">
                                    </div>
                                    <label style="font-size:14px; font-weight: bolder;" for="inputLast" class="col-md-1 control-label">Country</label>
                                    <div class="col-md-3">
                                        <select id="country" name="country" class="form-control">
                                            <option<? if($shippingFrom->get("country") == "United States") {?> selected <?}?> value="United States">United Sates</option>
                                            <option <? if($shippingFrom->get("country") == "Canada") {?> selected <?}?> value="Canada">Canada</option>
                                            <option <? if($shippingFrom->get("country") == "United Kingdom") {?> selected <?}?> value="United Kingdom">United Kingdom</option>
                                            <option  <? if($shippingFrom->get("country") == "Australia") {?> selected <?}?>value="Australia">Australia</option>
                                        </select>
                                    </div>
                                </div>
                                <hr>
                                <h3 style="padding-bottom: 8px">Billing Information</h3>
                                <div class="form-group">
                                    <label autocomplete="false" class="col-lg-2 control-label">Name On Card</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" id="cardName" name="cardName" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label autocomplete="false" class="col-lg-2 control-label">Card Number</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" id="cardNumber" name="cardNumber" value="">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label style="font-size:14px; font-weight: bolder;" for="inputLast" class="col-md-2 control-label">Type</label>
                                    <div class="col-md-3">
                                        <select id="type" name="type" class="form-control">
                                            <option value="Visa">Visa</option>
                                            <option value="Discover">Discover</option>
                                            <option value="Mastercard">Mastercard</option>
                                            <option value="American Express">American Express</option>
                                        </select>
                                    </div>
                                    <label style="font-size:14px; font-weight: bolder;" for="inputLast" class="col-md-1 control-label">Exp Year</label>
                                    <div class="col-md-2">
                                        <select id="expirationYear" name="expirationYear" class="form-control">
                                            <option value="2028">2028</option>
                                            <option value="2027">2027</option>
                                            <option value="2026">2026</option>
                                            <option value="2025">2025</option>
                                            <option value="2024">2024</option>
                                            <option value="2023">2023</option>
                                            <option value="2022">2022</option>
                                            <option value="2021">2021</option>
                                            <option value="2020">2020</option>
                                            <option value="2019">2019</option>
                                            <option value="2018">2018</option>
                                        </select>
                                    </div>
                                    <label style="font-size:14px; font-weight: bolder;" for="inputLast" class="col-md-1 control-label">Exp Month</label>
                                    <div class="col-md-2">
                                        <select id="expirationMonth" name="expirationMonth" class="form-control">
                                            <option value="01">01</option>
                                            <option value="02">02</option>
                                            <option value="03">03</option>
                                            <option value="04">04</option>
                                            <option value="05">05</option>
                                            <option value="06">06</option>
                                            <option value="07">07</option>
                                            <option value="08">08</option>
                                            <option value="09">09</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <button style="margin-top: 10px" id="creaeteStamps" name="createStamps" class="btn btn-primary pull-right ladda-button" data-style="zoom-out" onclick="createStampsAccount()" >Create Account</button>
                        <a style="margin-top: 10px; margin-right: 10px" id="creaeteStamps2" name="createStamps2" class="btn btn-default pull-right ladda-button" href="<? echo base_url()?>settings/shipping" >Cancel</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<? $this->load->view("navigation/footer");?>
<!-- Mainly scripts -->
<script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"></script>
<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/jeditable/jquery.jeditable.js"></script>

<script src="<?php echo base_url() ?>assets/js/plugins/dataTables/datatables.min.js"></script>

<!-- Sweet alert -->
<script src="<?php echo base_url() ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo base_url() ?>assets/js/inspinia.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/pace/pace.min.js"></script>

<!-- iCheck -->
<script src="<? echo base_url()?>assets/js/plugins/iCheck/icheck.min.js"></script>

<!-- Toastr script -->
<script src="<? echo base_url()?>assets/js/plugins/toastr/toastr.min.js"></script>

<!-- Ladda -->
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.jquery.min.js"></script>

</body>

</html>
