<?php
/**
 * Created by PhpStorm.
 * User: rapidware
 * Date: 8/18/18
 * Time: 11:06 AM
 */?>
<div class="col-lg-8 col-md-8 col-lg-offset-2 col-mg-offset-2">
    <h1 >Shipping</h1>
    <hr>
    <? if(in_array("admin", $this->session->permissions)) {?>
        <div class="row">
            <div class="col-lg-4 col-md-4">
                <h3 class="titles">Locations</h3>
                <p class="explainText">The address used to calculate shipping rates and the return address on your labels</p>
            </div>
            <div class=" col-lg-8 col-md-8">
                <div class="ibox-content img-rounded">
                    <h3 style="padding-bottom: 8px">Locations<span class="pull-right"><button class="normalLink ladda-buttonLocation btn btn-primary btn-xs"  data-style="zoom-out" onclick="refreshLocations()">Refresh locations</button></span></h3>
                    <table class="table" >
                        <thead>
                        <tr style="text-align: center">
                            <th>Name</th>
                            <th>Address</th>
                            <th>City</th>
                            <th>State</th>
                            <th>Zip</th>
                            <th>Country</th>
                            <th>Store</th>
                            <th size="5%">Default</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($locations as $location) { ?>
                            <tr>
                                <td><? echo $location->get("name"); ?></td>
                                <td><? echo $location->get("address1")." ".$location->get("address2")?></td>
                                <td><? echo $location->get("city") ?></td>
                                <td><? echo $location->get("state") ?></td>
                                <td><? echo $location->get("zip") ?></td>
                                <td><? echo $location->get("country") ?></td>
                                <td><? echo $location->get("shopifyName") ?></td>
                                <? if($location->get("default") == "true") {?>
                                    <td><span>Yes</span></td>
                                <?} else {?>
                                    <td>No</td>
                                <?}?>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-lg-4 col-md-4">
                <h3 class="titles">Shipping accounts</h3>
                <p class="explainText">The current shipping accounts available when creating labels</p>
            </div>
            <div class=" col-lg-8 col-md-8">
                <div class="ibox-content img-rounded">
                    <h3 style="padding-bottom: 8px">Carrier accounts</h3>
                    <table class="table" >
                        <thead>
                        <tr style="text-align: center">
                            <th>Name</th>
                            <th>Carrier Type</th>
                            <th>Funds</th>
                            <th>Add Funds</th>
    <!--                        <th>Owner</th>-->
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($carriers as $carrier) { ?>
                            <tr>
                                <td><?php if($carrier->get("status") != "enabled") {?> <s> <?} echo $carrier->get("name"); if($carrier->get("status") != "enabled") {?> </s> <?}?></td>
                                <td><? echo $carrier->get("service")?></td>
                                <td><?php if($carrier->get("funds") != -1) { echo "$".number_format($carrier->get("funds"),2); } else {echo "NA";}?></td>
    <!--                            <td>--><?php //echo $carrier->get("accountOwner")?><!--</td>-->
                                <? if($carrier->get("status") == "pending" && $carrier->get("name") == "Stamps.com") {?>
                                    <td><a type="button" disabled="" class="btn btn-primary btn-xs" href="#" data-toggle="modal" data-target="#addFundsModal">Add Funds</a></td>
                                <?} else if($carrier->get("name") == "Stamps.com" || $carrier->get("name") == "Endicia") {?>
                                    <td><a type="button" <? if($carrier->get("status") == "disabled") {?> disabled="" <?}?> class="btn btn-primary btn-xs" href="#" data-id="<? echo $carrier->get("carrierId")?>" data-toggle="modal" data-target="#addFundsModal">Add Funds</a></td>
                                <?} else {?>
                                    <td><a type="button" disabled="" class="btn btn-primary btn-xs" href="<? base_url()?>/settings/editStore?storeId=<? echo $carrier->getObjectId();?>">N/A</a></td>
                                <?}?>
                                <? if($carrier->get("status") == "pending") {?>
                                    <td><a type="button" class="btn btn-danger btn-xs" href="<? base_url()?>/settings/stampsSignUp?id=<? echo $carrier->getObjectId()?>">Set Up</a></td>
                                <?} else if($carrier->get("status") == "enabled") {?>
                                    <td><button type="button" onclick="shippingAccountStatus('disabled','<? echo $carrier->get("service")?>','<? echo $carrier->getObjectId()?>')" class="btn btn-danger btn-xs ladda-button">Disable</button></td>
                                <?} else {?>
                                    <td><button type="button" onclick="shippingAccountStatus('enabled','<? echo $carrier->get("service")?>','<? echo $carrier->getObjectId()?>')" class="btn btn-primary btn-xs ladda-button">Enable</button></td>
                                <?}?>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                    <hr>
                    <div class="dataText">
                        <p>If you have an existing account with a shipping carrier, connect it to your store to use your own rates.</p>
                    </div>
                    <button style="margin-top: 18px; margin-left: -3px" type="button" class="btn btn-default"  href="#" data-toggle="modal" data-target="#addCarrierModal">Connect carrier account</button>
                </div>
            </div>
        </div>
        <hr>
    <?}?>
    <div class="row">
        <div class="col-lg-4 col-md-4">
            <h3 class="titles">Packages</h3>
            <p class="explainText">The package sizes you use to ship your products. Weight and dimensions of the default package are used when calculating shipping rates at during fulfillment.</p>
        </div>
        <div class=" col-lg-8 col-md-8">
            <div class="ibox-content img-rounded">
                <? if(in_array("addPackages", $this->session->permissions) || in_array("fullAccess", $this->session->permissions)) {?>
                    <h3 style="padding-bottom: 8px">Saved packages <span class="pull-right"><a class="normalLink" href="#" data-toggle="modal" data-target="#addPackageModal">Add custom package</a><a style="margin-left: 15px" class="normalLink" href="#" data-toggle="modal" data-target="#addCarrierPackageModal">Add carrier package</a></span></h3>
                <?} else {?>
                    <h3 style="padding-bottom: 8px">Saved packages</h3>
                <?}?>
                <table class="table" >
                    <thead>
                        <tr>
                        </tr>
                    </thead>
                    <tbody>
                    <? foreach ($packages as $package) {
                        $default = $shippingInfo[0]->defaultBox;
                        //Convert from grams and cm
                        $weightUnits = $package->get("weightUnits");
                        $sizeUnits = $package->get("sizeUnits");
                        if($weightUnits == "oz") { $unit = 28.34952;} else if($weightUnits == "lb") { $unit = 453.59237;} else if($weightUnits == "kg") { $unit = 1000;} else {$unit = 1;}
                        if($sizeUnits == "in") { $size = 2.54;} else { $size = 1;} ?>
                        <tr>
                            <td><? echo $package->get("name")?>
                                <? if(in_array("editPackages", $this->session->permissions) || in_array("fullAccess", $this->session->permissions)) {?>
                                    <? if($package->get("packageType") == "package") {?>
                                        <span class="pull-right">
                                            <a class="normalLink" href="#"
                                              data-packagename="<?php echo $package->get("name") ?>"
                                              data-length="<?php echo round($package->get("length") / $size,2) ?>"
                                              data-width="<?php echo round($package->get("width") / $size,2) ?>"
                                              data-height="<?php echo round($package->get("height") / $size,2) ?>"
                                              data-sizeunits="<?php echo $sizeUnits ?>"
                                              data-packageweight="<?php echo round($package->get("weight") / $unit,2) ?>"
                                              data-weightunits="<?php echo $weightUnits ?>"
                                              data-packageid="<?php echo $package->getObjectId() ?>"
                                              data-default="<?php if($default == $package->getObjectId()) { echo "1";} else { echo "0";} ?>"
                                              data-toggle="modal"
                                              data-target="#editPackageModal">Edit
                                            </a>
                                        </span>
                                        <br>
                                        <span style="color: #989898">
                                        <? echo round($package->get("length") / $size,2)." x ".round($package->get("width") / $size,2)." x ".round($package->get("height") / $size,2)." ".$sizeUnits ?>, <? echo round($package->get("weight") / $unit,2)." ".$weightUnits?>
                                        </span><? if ($default == $package->getObjectId()) { ?><span style="margin-left: 12px" class="badge badge-default"> Default </span> <? }?>
                                    <?} else {?>
                                        <span class="pull-right">
                                            <a class="normalLink" href="#"
                                               data-packagename2="<?php echo $package->get("name") ?>"
                                               data-packageid2="<?php echo $package->getObjectId() ?>"
                                               data-default2="<?php if($default == $package->getObjectId()) { echo "1";} else { echo "0";} ?>"
                                               data-toggle="modal"
                                               data-target="#editCarrierPackageModal">Edit
                                            </a>
                                        </span>
                                        <br>
                                        <span style="color: #989898">
                                            Carrier Packaging
                                        </span><? if ($default == $package->getObjectId()) { ?><span style="margin-left: 12px" class="badge badge-default"> Default </span> <? }?>
                                    <?}?>
                                <?}?>
                            </td>
                        </tr>
                    <? } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-lg-4 col-md-4">
            <h3 class="titles">Shipping labels</h3>
            <p class="explainText">Buy and print shipping labels for your orders. Costs are automatically deducted from your shipping balance.</p>
        </div>
        <form id="shippingLabelForm" name="shippingLabelForm" method="post">
            <input type="hidden" id="companyId" name="companyId" value="<? echo $shippingInfo[0]->getObjectId();?>">
            <div class=" col-lg-8 col-md-8">
                <div class="ibox-content img-rounded">
<!--                    <h3 style="padding-bottom: 8px">Label format <span class="pull-right"><a class="normalLink" href="#" data-toggle="modal" data-target="#shippingFromModal">Edit Address</a></span></h3>-->
                    <div class="form-group">
                        <div class="i-checks"><label style="font-weight: normal"> <input <? if($shippingInfo[0]->get("labelFormat") == "desktop") {?> checked <?}?>  <? if(in_array("changeShippingLabels", $this->session->permissions) || in_array("fullAccess", $this->session->permissions)) {?> <?} else {?> disabled <?}?> class="labelFormat" type="radio" value="desktop" name="labelFormat"> 8.5 × 11 inch paper for desktop printers </label></div>
                        <div class="i-checks"><label style="font-weight: normal"> <input <? if($shippingInfo[0]->get("labelFormat") == "label") {?> checked <?}?>  <? if(in_array("changeShippingLabels", $this->session->permissions) || in_array("fullAccess",$this->session->permissions)) {?> <?} else {?> disabled <?}?> class="labelFormat" type="radio" value="label" name="labelFormat"> 4 × 6 inch label for label printers </label></div>
                    </div>
                    <hr>
                    <h4>Shipping label providers</h4>
                    <img src="<? echo base_url()?>/assets/img/usps-logo.svg" width="30px" height="30px"/>
<!--                    <img src="--><?// echo base_url()?><!--/assets/img/ups-logo.svg" width="30px" height="30px"/>-->
                </div>
            </div>
        </form>
    </div>
</div>



