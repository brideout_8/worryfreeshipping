<?php
/**
 * Created by PhpStorm.
 * User: rapidware
 * Date: 8/29/18
 * Time: 3:38 PM
 */?>

<!-- Address Change Modal -->
<!--<div class="modal fade" id="shippingFromModal" name="shippingFromModal" tabindex="-1" role="dialog">-->
<!--    <div class="modal-dialog modal-lg">-->
<!--        <div class="modal-content">-->
<!--            <div class="modal-header">-->
<!--                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
<!--                <h4 class="modal-title" id="myModalLabel">Update Shipping From Address</h4>-->
<!--            </div>-->
<!--            <div class="modal-body">-->
<!--                <form id="shippingFromForm" name="shippingFromForm" method="post" class="form-horizontal">-->
<!--                    <input type="hidden" id="companyId" name="companyId" value="--><?php //echo $companyId?><!--" />-->
<!--                    <div class="form-group">-->
<!--                        <label autocomplete="false" class="col-lg-2 control-label">Company Name</label>-->
<!--                        <div class="col-lg-10">-->
<!--                            <input type="text" class="form-control" id="company" name="company" value="--><?// if(isset($shippingFrom->company)) { echo $shippingFrom->company; }?><!--">-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="form-group">-->
<!--                        <label autocomplete="false" class="col-lg-2 control-label">Address</label>-->
<!--                        <div class="col-lg-7">-->
<!--                            <input type="text" class="form-control" id="address1" name="address1" value="--><?// echo $shippingFrom->address1?><!--">-->
<!--                        </div>-->
<!--                        <div class="col-lg-3">-->
<!--                            <input type="text" class="form-control" id="address2" name="address2" value="--><?// echo $shippingFrom->address2?><!--">-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="row form-group">-->
<!--                        <label style="font-size:14px; font-weight: bolder;" for="inputLast" class="col-md-2 control-label">City</label>-->
<!--                        <div class="col-md-10">-->
<!--                            <input type="text" class="form-control" id="city" name="city" value="--><?// echo $shippingFrom->city?><!--">-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="row form-group">-->
<!--                        <label style="font-size:14px; font-weight: bolder;" for="inputLast" class="col-md-2 control-label">State/Province</label>-->
<!--                        <div class="col-md-3">-->
<!--                            <input type="text" class="form-control" id="state" name="state" value="--><?// echo $shippingFrom->state?><!--">-->
<!--                        </div>-->
<!--                        <label style="font-size:14px; font-weight: bolder;" for="inputLast" class="col-md-1 control-label">Zip</label>-->
<!--                        <div class="col-md-2">-->
<!--                            <input type="text" class="form-control" id="zip" name="zip" value="--><?// echo $shippingFrom->zip?><!--">-->
<!--                        </div>-->
<!--                        <label style="font-size:14px; font-weight: bolder;" for="inputLast" class="col-md-1 control-label">Country</label>-->
<!--                        <div class="col-md-3">-->
<!--                            <select id="country" name="country" class="form-control minimal ui-select">-->
<!--                                <option--><?// if($shippingFrom->country == "United States") {?><!-- selected --><?//}?><!-- value="United States">United Sates</option>-->
<!--                                <option --><?// if($shippingFrom->country == "Canada") {?><!-- selected --><?//}?><!-- value="Canada">Canada</option>-->
<!--                                <option --><?// if($shippingFrom->country == "United Kingdom") {?><!-- selected --><?//}?><!-- value="United Kingdom">United Kingdom</option>-->
<!--                                <option  --><?// if($shippingFrom->country == "Australia") {?><!-- selected --><?//}?><!--value="Australia">Australia</option>-->
<!--                            </select>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="modal-footer">-->
<!--                        <div class="col-sm-12 col-sm-12 pull-right">-->
<!--                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
<!--                            <input type="button" class="ladda-button btn btn-primary" data-style="zoom-out" id="save" value="Save" name="save" onclick="updateShippingFromAddress()"/>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </form>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!-- Add Package Modal -->
<div class="modal fade" id="addPackageModal" name="addPackageModal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Add Package</h4>
            </div>
            <div class="modal-body">
                <form id="addPackageForm" name="addPackageForm" method="post" class="form-horizontal">
                    <input type="hidden" id="companyId" name="companyId" value="<?php echo $companyId?>" />
                    <input type="hidden" id="default" name="default" value="0" />
                    <input type="hidden" id="packageDetail" name="packageDetail" value="custom" />
                    <div class="form-group">
                        <div style="padding-left: 3px; padding-right: 3px"  class="col-lg-12">
                            <label style="font-weight: normal">Package Name</label>
                            <input type="text" class="form-control" id="packageName" name="packageName" value="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div style="padding-left: 3px; padding-right: 3px" class="col-md-2 col-lg-2">
                            <label style="font-weight: normal;">Length</label>
                            <input type="number" class="form-control" id="length" name="length" value="">
                        </div>
                        <div style="padding-left: 3px; padding-right: 3px" class="col-md-2 col-lg-2">
                            <label style="font-weight: normal;">Width</label>
                            <input type="number" class="form-control" id="width" name="width" value="">
                        </div>
                        <div style="padding-left: 3px; padding-right: 3px" class="col-md-2 col-lg-2">
                            <label style="font-weight: normal;">Height</label>
                            <input type="number" class="form-control" id="height" name="height" value="">
                        </div>
                        <div style="padding-left: 3px; padding-right: 3px" class="col-md-2 col-lg-2">
                            <label style="font-weight: normal;">Units</label>
                            <select id="sizeUnits" name="sizeUnits" class="form-control minimal ui-select">
                                <option selected value="in">in</option>
                                <option value="cm">cm</option>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <div class=" row form-group">
                        <div style="padding-left: 3px; padding-right: 3px"  class="col-md-3 col-lg-3">
                            <label style="font-weight: normal;">Weight when empty (optional)</label>
                            <input type="number" class="form-control" id="packageWeight" name="packageWeight" value="">
                        </div>
                        <div style="padding-left: 3px; padding-right: 3px"  class="col-md-2 col-lg-2">
                            <label style="font-weight: normal; padding-top: 18px;">Units</label>
                            <select id="weightUnits" name="weightUnits" class="form-control minimal ui-select">
                                <option value="kg">kg</option>
                                <option value="g">g</option>
                                <option selected value="oz">oz</option>
                                <option value="lb">lb</option>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <div class="row form-group">
                        <div class="i-checks">
                            <label style="font-weight: normal;"> <input type="checkbox" id="default" name="default" value="1"> Set as default package </label>
                        </div>
                        <p class="explainText" style="padding-left: 25px">Package size and weight is used to calculate shipping rates during fulfillment.</p>
                    </div>
                    <div class="modal-footer">
                        <div class="col-sm-12 col-sm-12 pull-right">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="button" class="ladda-button btn btn-primary" data-style="zoom-out" id="save" value="Save" name="save" onclick="addPackage()"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Edit Package Modal -->
<div class="modal fade" id="editPackageModal" name="editPackageModal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Edit Package</h4>
            </div>
            <div class="modal-body">
                <form id="editPackageForm" name="editPackageForm" method="post" class="form-horizontal">
                    <input type="hidden" id="companyId" name="companyId" value="<?php echo $companyId?>" />
                    <input type="hidden" id="packageId" name="packageId" value="" />
                    <input type="hidden" id="packageDetail" name="packageDetail" value="custom" />
                    <div class="form-group">
                        <div style="padding-left: 3px; padding-right: 3px"  class="col-lg-12">
                            <label style="font-weight: normal">Package Name</label>
                            <input type="text" class="form-control" id="packageName" name="packageName" value="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div style="padding-left: 3px; padding-right: 3px" class="col-md-2 col-lg-2">
                            <label style="font-weight: normal;">Length</label>
                            <input type="number" class="form-control" id="length" name="length" value="">
                        </div>
                        <div style="padding-left: 3px; padding-right: 3px" class="col-md-2 col-lg-2">
                            <label style="font-weight: normal;">Width</label>
                            <input type="number" class="form-control" id="width" name="width" value="">
                        </div>
                        <div style="padding-left: 3px; padding-right: 3px" class="col-md-2 col-lg-2">
                            <label style="font-weight: normal;">Height</label>
                            <input type="number" class="form-control" id="height" name="height" value="">
                        </div>
                        <div style="padding-left: 3px; padding-right: 3px" class="col-md-2 col-lg-2">
                            <label style="font-weight: normal;">Units</label>
                            <select id="sizeUnitsEdit" name="sizeUnitsEdit" class="form-control minimal ui-select">
                                <option value="in">in</option>
                                <option value="cm">cm</option>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <div class=" row form-group">
                        <div style="padding-left: 3px; padding-right: 3px"  class="col-md-3 col-lg-3">
                            <label style="font-weight: normal;">Weight when empty (optional)</label>
                            <input type="number" class="form-control" id="packageWeight" name="packageWeight" value="">
                        </div>
                        <div style="padding-left: 3px; padding-right: 3px"  class="col-md-2 col-lg-2">
                            <label style="font-weight: normal; padding-top: 18px;">Units</label>
                            <select id="weightUnitsEdit" name="weightUnitsEdit" class="form-control minimal ui-select">
                                <option value="kg">kg</option>
                                <option value="g">g</option>
                                <option value="oz">oz</option>
                                <option value="lb">lb</option>
                            </select>
                        </div>
                    </div>
                    <div id="defaultPackage" >
                        <hr>
                        <div class="row form-group">
                            <div class="i-checks">
                                <label style="font-weight: normal;"> <input type="checkbox" id="default" name="default" value="1"> Set as default package </label>
                            </div>
                            <p class="explainText" style="padding-left: 25px">Package size and weight is used to calculate shipping rates during fulfillment.</p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="col-sm-12 col-sm-12 pull-right">
                            <input type="button" class="ladda-button btn btn-danger" data-style="zoom-out" id="delete" value="Delete Package" name="delete" onclick="deletePackage()"/>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="button" class="ladda-button btn btn-primary" data-style="zoom-out" id="save" value="Save" name="save" onclick="updatePackage()"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Add Carrier Package Modal -->
<div class="modal fade" id="addCarrierPackageModal" name="addCarrierPackageModal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Add Carrier Package</h4>
            </div>
            <div class="modal-body">
                <form id="addCarrierPackageForm" name="addCarrierPackageForm" method="post" class="form-horizontal">
                    <input type="hidden" id="companyId" name="companyId" value="<?php echo $companyId?>" />
                    <input type="hidden" id="default" name="default" value="0" />
                    <input type="hidden" id="packageDetail" name="packageDetail" value="carrier" />
                    <div class="form-group">
                        <div class="form-group">
                            <div class="col-md-12 col-lg-12">
                                <? foreach ($carrierPackages as $package) { ?>
                                    <div class="i-checks"><label style="font-weight: normal"> <input class="carrierPackageType" type="radio" value="<? echo $package->getObjectId()?>" name="carrierPackageType"> <? echo $package->get("name")?></label></div>
                                <?}?>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row form-group">
                        <div class="i-checks">
                            <label style="font-weight: normal;"> <input type="checkbox" id="default" name="default" value="1"> Set as default package </label>
                        </div>
                        <p class="explainText" style="padding-left: 25px">Package size and weight is used to calculate shipping rates during fulfillment.</p>
                    </div>
                    <div class="modal-footer">
                        <div class="col-sm-12 col-sm-12 pull-right">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="button" class="ladda-button btn btn-primary" data-style="zoom-out" id="save" value="Save" name="save" onclick="addCarrierPackage()"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Edit Carrier Package Modal -->
<div class="modal fade" id="editCarrierPackageModal" name="editCarrierPackageModal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Edit Package</h4>
            </div>
            <div class="modal-body">
                <form id="editCarrierPackageForm" name="editCarrierPackageForm" method="post" class="form-horizontal">
                    <input type="hidden" id="companyId" name="companyId" value="<?php echo $companyId?>" />
                    <input type="hidden" id="carrierPackageId" name="carrierPackageId" value="" />
                    <input type="hidden" id="carrierPackageDetail" name="packageDetail" value="carrier" />
                    <div class="form-group">
                        <div style="padding-left: 3px; padding-right: 3px"  class="col-lg-12">
                            <label style="font-weight: normal">Package Name</label>
                            <input type="text" class="form-control" id="carrierPackageName" name="carrierPackageName" disabled="" value="">
                        </div>
                    </div>
                    <div id="defaultCarrierPackage" >
                        <hr>
                        <div class="row form-group">
                            <div class="i-checks">
                                <label style="font-weight: normal;"> <input type="checkbox" id="default" name="default" value="1"> Set as default package </label>
                            </div>
                            <p class="explainText" style="padding-left: 25px">Package size and weight is used to calculate shipping rates during fulfillment.</p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="col-sm-12 col-sm-12 pull-right">
                            <input type="button" class="ladda-button btn btn-danger" data-style="zoom-out" id="deleteCarrier" value="Delete Package" name="deleteCarrier" onclick="deletePackage()"/>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="button" class="ladda-button btn btn-primary" data-style="zoom-out" id="save" value="Save" name="save" onclick="updateCarrierPackage()"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Set Up Stamps.com Modal -->
<div class="modal fade" id="stampsModal" name="stampsModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Set Up Stamps.com Account</h4>
            </div>
            <div class="modal-body">
                <form id="stampsForm" name="stampsForm" method="post" class="form-horizontal">
                    <input type="hidden" id="companyId" name="companyId" value="<?php echo $companyId?>" />
                    <div class="form-group">
                        <label autocomplete="false" class="col-lg-2 control-label">Account Nickname</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" id="nickname" name="nickname" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label autocomplete="false" class="col-lg-2 control-label">Email</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" id="email" name="email" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label autocomplete="false" class="col-lg-2 control-label">Name</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" id="name" name="name" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label autocomplete="false" class="col-lg-2 control-label">Phone</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" id="phone" name="phone" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label autocomplete="false" class="col-lg-2 control-label">Company Name</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" id="company" name="company" value="<? echo $shippingInfo[0]->get("name") ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label autocomplete="false" class="col-lg-2 control-label">Address</label>
                        <div class="col-lg-7">
                            <input type="text" class="form-control" id="address1" name="address1" value="<? echo $shippingInfo[0]->get("address1")?>">
                        </div>
                        <div class="col-lg-3">
                            <input type="text" class="form-control" id="address2" name="address2" value="<? echo $shippingInfo[0]->get("address2")?>">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label style="font-size:14px; font-weight: bolder;" for="inputLast" class="col-md-2 control-label">City</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="city" name="city" value="<? echo $shippingInfo[0]->get("city")?>">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label style="font-size:14px; font-weight: bolder;" for="inputLast" class="col-md-2 control-label">State/Province</label>
                        <div class="col-md-3">
                            <input type="text" class="form-control" id="state" name="state" value="<? echo $shippingInfo[0]->get("state")?>">
                        </div>
                        <label style="font-size:14px; font-weight: bolder;" for="inputLast" class="col-md-1 control-label">Zip</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control" id="zip" name="zip" value="<? echo $shippingInfo[0]->get("zip")?>">
                        </div>
                        <label style="font-size:14px; font-weight: bolder;" for="inputLast" class="col-md-1 control-label">Country</label>
                        <div class="col-md-3">
                            <select id="country" name="country" class="form-control minimal ui-select">
                                <option<? if($shippingInfo[0]->get("country") == "United States") {?> selected <?}?> value="United States">United Sates</option>
                                <option <? if($shippingInfo[0]->get("country") == "Canada") {?> selected <?}?> value="Canada">Canada</option>
                                <option <? if($shippingInfo[0]->get("country") == "United Kingdom") {?> selected <?}?> value="United Kingdom">United Kingdom</option>
                                <option  <? if($shippingInfo[0]->get("country") == "Australia") {?> selected <?}?>value="Australia">Australia</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="col-sm-12 col-sm-12 pull-right">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="button" class="ladda-button btn btn-primary" data-style="zoom-out" id="save" value="Save" name="save" onclick="updateShippingFromAddress()"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Add Funds Modal -->
<div class="modal fade" id="addFundsModal" name="addFundsModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Add Funds</h4>
            </div>
            <div class="modal-body">
                <form id="addFundsForm" name="addFundsForm" method="post" class="form-horizontal">
                    <input type="hidden" id="carrierId" name="carrierId" value="" />
                    <div class="form-group">
                        <label autocomplete="false" class="col-lg-2 control-label">Amount To Add</label>
                        <div class="col-lg-10">
                            <input type="number" class="form-control" id="amount" name="amount" value="">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="col-sm-12 col-sm-12 pull-right">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="button" class="ladda-button btn btn-primary" data-style="zoom-out" id="save" value="Add Funds" name="save" onclick="addFundsToCarrier()"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Add Carrier Modal -->
<div class="modal fade" id="addCarrierModal" name="addCarrierModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Select Carrier</h4>
            </div>
            <div class="modal-body">
                <form id="addCarrierForm" name="addCarrierForm" method="post" class="form-horizontal">
                    <input type="hidden" id="carrierId" name="carrierId" value="" />
                    <a href="<? echo base_url()?>/settings/addStamps">
                        <img class="carrierImage" src="<? echo base_url()?>/assets/img/stamps_com.png" />
                    </a>
                    <a href="<? echo base_url()?>/settings/addEndicia">
                        <img class="carrierImage" src="<? echo base_url()?>/assets/img/endicia-vector-logo.png"/>
                    </a>
                    <a href="<? echo base_url()?>/settings/addUPS">
                        <img class="carrierImage" src="<? echo base_url()?>/assets/img/ups.png"/>
                    </a>
                    <a href="<? echo base_url()?>/settings/addFedex">
                        <img class="carrierImage" src="<? echo base_url()?>/assets/img/fedex.png"/>
                    </a>
                    <div class="modal-footer">
                        <div class="col-sm-12 col-sm-12 pull-right">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
<!--                            <input type="button" class="ladda-button btn btn-primary" data-style="zoom-out" id="save" value="Add Funds" name="save" onclick="addFundsToCarrier()"/>-->
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>