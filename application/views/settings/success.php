<?php
/**
 * Created by PhpStorm.
 * User: Marketing101
 * Date: 7/23/18
 * Time: 9:40 AM
 */?>

<style>
    img {
        max-width: 60%;
        max-height: 60%;
        display: block;
        margin-left: auto;
        margin-right: auto
    }

    .signUpColumns {
        max-width: 1100px;
        margin: 0 auto;
        padding: 250px 20px 20px 20px;
    }
    .custom-bg {
        background-color: #f6f8fa;
    }
    div.ibox-content{
        /*box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);*/
        /*box-shadow: #3D3D3D;*/
        border: solid 1px #d9d9d9;
        border-radius: 5px;
    }
    label {
        font-weight: normal;
        color: black;
    }
    ::placeholder {
        font-weight: lighter;
    }
    .createAccount {
        margin-top: 15px;
    }
    h1 {
        font-size: 40px;
    }
</style>
</head>

<body class="custom-bg">
<div class="signUpColumns">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1">
                        <div class="row">
                            <div class="col-md-12">
                                <h1 style="color: black; font-weight: normal; text-align: center; padding-bottom: 25px">Success</h1>
                            </div>
                        </div>
                        <form method="post" id="login_form" name="login_form">
                            <div class="row">
                                <p style="text-align: center">Your new store is now connected.<br>Please enter your first order number you would like to import.<br>
                                    Any imports after the first import will only bring in orders created after this order.<br>
                                    This has to match how the order numbers are displayed in Shopify under the orders tab.</p>
                            </div>
<!--                            <a class="ladda-button btn btn-primary block full-width m-b createAccount" href="--><?php //echo base_url()?><!--settings/stores">See Store Details</a>-->
                        </form>
                        <form method="post" id="addStoreNumberForm" name="addStoreNumberForm">
                            <input type="hidden" id="shopifyToken" name="shopifyToken" value="<? echo $shopifyToken?>">
                            <input type="hidden" id="storeName" name="storeName" value="<? echo $storeName?>">
                            <div class="row signUpForm">
                                <div style="margin: 10px">
                                    <label>Starting Order Number</label>
                                    <input class="form-control" type="text" id="startingOrderNumber" placeholder="#1001" name="startingOrderNumber" />
                                </div>
                            </div>
                            <button type="button" id="connect" name="connect" class="ladda-button btn btn-primary block full-width m-b" data-style="zoom-out" onclick="addStartingOrder()">Add order</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-2.1.1.js"></script>
<!-- Sweet alert -->
<script src="<?php echo base_url() ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo base_url()?>assets/js/functions.js" type="text/javascript" charset="utf-8"></script>

<!-- Ladda -->
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.jquery.min.js"></script>
</body>

</html>




