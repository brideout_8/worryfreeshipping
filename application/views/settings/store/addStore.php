<?php
/**
 * Created by PhpStorm.
 * User: rapidware
 * Date: 7/21/18
 * Time: 10:29 AM
 */?>

<style>
    div.ibox-content{
        border: solid 1px #d9d9d9;
        border-radius: 5px;
    }
    #actionButtons {
        margin-top: 15px;
    }
    .form-control {
        border-radius: 5px;
    }
    hr {
        border-color: #dcdcdc;
    }
</style>
</head>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="col-lg-8 col-md-8 col-lg-offset-2 col-mg-offset-2">
                <h1 style="font-weight: bold">Add Store</h1>
                <hr>
                <div class="row">
                    <div class="col-lg-3 col-md-3">
                        <h3 class="titles">Connect store</h3>
                        <p class="explainText">The shop name must match the shopify name when logging into Shopify. EX: randomstore.myshopify.com <br> You will not be charged per store.</p>
                    </div>
                    <div class=" col-lg-9 col-md-9">
                        <div class="ibox-content img-rounded">
                            <form method="post" id="addStoreForm" name="addStoreForm">
                                <div class="row signUpForm">
                                    <div style="margin: 10px">
                                        <label>Shop Name</label>
                                        <input class="form-control" type="text" id="shopName" placeholder="randomstore.myshopify.com" name="shopName" />
<!--                                        <p>The shop name must match the shopify name when logging into Shopify. EX: randomstore.myshopify.com <br> You will not be charged per store.</p>-->
                                    </div>
                                </div>
                                <button type="button" id="connect" name="connect" class="ladda-button btn btn-primary" data-style="zoom-out" onclick="addStore()">Connect Store</button>
                                <a type="button" style="margin-left: 10px" id="connect2" name="connect2" class="ladda-button btn btn-default" href="<? echo base_url()?>settings/stores"  >Cancel</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<? $this->load->view("navigation/footer");?>
<!-- Mainly scripts -->
<script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"></script>
<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/jeditable/jquery.jeditable.js"></script>

<script src="<?php echo base_url() ?>assets/js/plugins/dataTables/datatables.min.js"></script>

<!-- Sweet alert -->
<script src="<?php echo base_url() ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo base_url() ?>assets/js/inspinia.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/pace/pace.min.js"></script>

<!-- Ladda -->
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.jquery.min.js"></script>

</body>

</html>




