<?php
/**
 * Created by PhpStorm.
 * User: rapidware
 * Date: 7/20/18
 * Time: 9:56 AM
 */?>

<style>
    .bottomBtn {
        margin-top: 15px;
    }
    #actionButtons {
        margin-top: 15px;
    }
    fieldset{
        margin-top: 15px;
    }
    .input-group-addon {
        background-color: #f5f5f5;
        color: black;
        /*font-weight: bold;*/
    }
    .normalLink {
        font-size: 15px;
        font-weight: normal;
    }
    .titles {
        padding-top: 17px;
    }
    hr {
        border-color: #dcdcdc;
    }
</style>
</head>

<div class="wrapper wrapper-content">
    <div id="listings"></div>
</div>
<? $this->load->view("navigation/footer");?>
<script type="text/javascript">
    $(document).ready(function(){
        $('#listings').html('<img height="50" width="50" style=" position: absolute; top: 50%; left: 50%;" src="'+loaderImage+'">');
        $.ajax({
            url  	: base_url+'settings/getStores',
            type 	: 'POST',
            success : function(data){
                $("#listings").html(data);
            }
        });
    });
</script>
<!-- Mainly scripts -->
<script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"></script>
<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/jeditable/jquery.jeditable.js"></script>

<script src="<?php echo base_url() ?>assets/js/plugins/dataTables/datatables.min.js"></script>

<!-- Sweet alert -->
<script src="<?php echo base_url() ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo base_url() ?>assets/js/inspinia.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/pace/pace.min.js"></script>

<!-- Ladda -->
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.jquery.min.js"></script>
</body>
</html>
<!-- Import orders info Modal -->
<div class="modal fade" id="addStoreModal" name="addStoreModal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Add store</h4>
            </div>
            <div class="modal-body">
                <h2 style="font-size: 15px">To add another store, please log into your Shopify admin for that store, find us in the app store, and click get app. Stay logged into Worry Free Shipping to make sure your new store is connected to this account.  We will handle the rest!</h2>
            </div>
            <div class="modal-footer">
                <div class="col-sm-12 col-sm-12 pull-right">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>




