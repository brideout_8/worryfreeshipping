<?php
/**
 * Created by PhpStorm.
 * User: rapidware
 * Date: 8/29/18
 * Time: 1:35 PM
 */?>
<form id="editStoreForm" name="editStoreForm" method="post">
    <input type="hidden" name="storeId" id="storeId" value="<? echo $storeInfo[0]->getObjectId();?>">
    <input type="hidden" name="storeName" id="storeName" value="<? echo $storeInfo[0]->get("shopifyName");?>">
    <div class="col-lg-8 col-md-8 col-lg-offset-2 col-mg-offset-2">
        <h1 style="font-weight: bold">Edit Store</h1>
        <hr>
        <div class="row">
            <div class="col-lg-3 col-md-3">
                <h3 class="titles">Connected store</h3>
                <p class="explainText">You can change the display name or disconnect the store</p>
            </div>
            <div class=" col-lg-9 col-md-9">
                <div class="ibox-content img-rounded">
                    <table class="table" >
                        <thead>
                        <tr style="text-align: center">
                            <th>Name</th>
                            <th>Display Name</th>
                            <th>Date Added</th>
<!--                            <th>Action</th>-->
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><?php echo $storeInfo[0]->get("shopifyName")?></td>
                            <td><input id="displayName" name="displayName" oninput="enableSave()" value="<?php echo $storeInfo[0]->get("displayName")?>"></td>
                            <td><?php $date = $storeInfo[0]->getUpdatedAt(); echo $date->format('m/d/Y');?></td>
                        <!--    <td><button class="btn btn-danger btn-xs" type="button" <?// if( $storeInfo[0]->get("payingStore") == "true") {?> disabled="" <?//}?> onclick="disconnectStore('<?// echo $storeInfo[0]->getObjectId()?>')">Disconnect Store</button></td> -->
                        </tr>
                        </tbody>
                    </table>
                </div>
                <button style="margin-top: 15px" id="saveBtn" name="saveBtn" data-style="zoom-out" class="ladda-button btn btn-primary btn-sm pull-right" disabled onclick="updateStore()">Save</button>
                <a style="margin-top: 15px; margin-right: 10px" href="<? echo base_url()?>/settings/stores" class="btn btn-default btn-sm pull-right"  >Cancel</a>
            </div>
        </div>
    </div>
</form>
