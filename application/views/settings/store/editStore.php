<?php
/**
 * Created by PhpStorm.
 * User: rapidware
 * Date: 8/29/18
 * Time: 12:09 PM
 */?>

<style>
    div.ibox-content{
        border: solid 1px #d9d9d9;
        border-radius: 5px;
    }
    .bottomBtn {
        margin-top: 15px;
    }
    #actionButtons {
        margin-top: 15px;
    }
    fieldset{
        margin-top: 15px;
    }
    .input-group-addon {
        background-color: #f5f5f5;
        color: black;
        /*font-weight: bold;*/
    }
    .normalLink {
        font-size: 15px;
        font-weight: normal;
    }
    .titles {
        padding-top: 17px;
    }
</style>
</head>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div id="listings"></div>
        </div>
    </div>
</div>
<? $this->load->view("navigation/footer");?>
<script type="text/javascript">
    $(document).ready(function(){
        $('#listings').html('<img height="50" width="50" style=" position: absolute; top: 50%; left: 50%;" src="'+loaderImage+'">');
        var orderData = {storeId: "<?php echo $storeId; ?>"};
        $.ajax({
            url  	: base_url+'settings/editStoreTable',
            type 	: 'POST',
            data    : orderData,
            success : function(data){
                $("#listings").html(data);
            }
        });
    });
</script>
<!-- Mainly scripts -->
<script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"></script>
<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/jeditable/jquery.jeditable.js"></script>

<script src="<?php echo base_url() ?>assets/js/plugins/dataTables/datatables.min.js"></script>

<!-- Sweet alert -->
<script src="<?php echo base_url() ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo base_url() ?>assets/js/inspinia.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/pace/pace.min.js"></script>

<!-- Ladda -->
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.jquery.min.js"></script>

</body>

</html>





