<?php
/**
 * Created by PhpStorm.
 * User: Marketing101
 * Date: 9/18/18
 * Time: 10:07 AM
 */?>
<style>
    div.ibox-content {
        border: solid 1px #d9d9d9;
        /*box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);*/
        /*margin-top: 20px;*/
        border-radius: 5px;
        /*box-shadow: #3D3D3D;*/
    }
    .table th {
        /*text-align: center;*/
        font-weight: normal;
        border: 0;
    }
    .table td {
        /*font-weight: bold;*/
        /*font-size: 15px;*/
    }
    table tr:first-child td {
        border-top: 0;
    }
    /*a {*/
    /*color: #717171;*/
    /*}*/
    .normalLink {
        font-size: 15px;
        font-weight: normal;
    }
    p {
        margin:-2px 0 -2px 0;
    }
    .buttonLink {
        background:none!important;
        color: steelblue;
        border:none;
        padding:0!important;
        font: inherit;
        /*border is optional*/
        cursor: pointer;
    }
    .explainText {
        padding-top: 10px;
        color: #9a9a9a;
    }
    .titles {
        padding-top: 17px;
    }
    .dataText p {
        margin-bottom: 3px;
    }
    hr {
        border-color: #dcdcdc;
    }
    .modal-backdrop {
        background-color: #c7c7c7
    }
    .explainText {
        /*padding-top: 10px;*/
        color: #9a9a9a;
    }
    #toast-container>.toast {
        background-image: none !important;
    }
    .form-control {
        border-radius: 5px;
    }
</style>
</head>

<div class="wrapper wrapper-content">
    <div class="row">
        <form id="updateUserForm" name="updateUserForm" method="post" class="form-horizontal">
            <input type="hidden" id="userId" name="userId" value="<? echo $userInfo[0]->getObjectId()?>">
            <div class="col-lg-12">
                <div class="col-lg-8 col-md-8 col-lg-offset-2 col-mg-offset-2">
                    <h1 style="font-weight: bold"><? echo $userInfo[0]->get("name")?></h1>
                    <hr>
                    <div class="row">
                        <div class="col-lg-4 col-md-4">
                            <h3 class="titles">Account information</h3>
                        </div>
                        <div class=" col-lg-8 col-md-8">
                            <div class="ibox-content img-rounded">
                                <h3 style="padding-bottom: 8px">Account Information</h3>
                                <div class="form-group">
                                    <label autocomplete="false" class="col-lg-2 control-label">First Name</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" id="firstName" name="firstName" value="<? echo $userInfo[0]->get("firstName")?>">
                                    </div>
                                    <label autocomplete="false" class="col-lg-2 control-label">Last Name</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" id="lastName" name="lastName" value="<? echo $userInfo[0]->get("lastName")?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label autocomplete="false" class="col-lg-2 control-label">Email</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" id="email" name="email" value="<? echo $userInfo[0]->get("email")?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label autocomplete="false" class="col-lg-2 control-label">Username</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" id="username" name="username" value="<? echo $userInfo[0]->get("username")?>">
                                    </div>
                                    <label autocomplete="false" class="col-lg-2 control-label">Phone (optional)</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" id="phone" name="phone" value="<? echo $userInfo[0]->get("phone")?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label autocomplete="false" class="col-lg-2 control-label">Password</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" id="password" name="password" value="">
                                        <p>Leave blank if you do not want to change the password</p>
                                    </div>
                                    <label autocomplete="false" class="col-lg-2 control-label">Confirm Password</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" id="passwordConfirm" name="passwordConfirm" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-lg-4 col-md-4">
                            <h3 class="titles">Account permissions</h3>
                            <!--                        <p class="explainText">The address used to calculate shipping rates and the return address on your labels</p>-->
                        </div>
                        <div class=" col-lg-8 col-md-8">
                            <div class="ibox-content img-rounded">
                                <h3 style="padding-bottom: 8px">Customize permissions</h3>
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <div class="i-checks">
                                            <label style="font-weight: normal"> <input class="fullAccess form-control" type="checkbox" <? if(in_array("fullAccess", $userInfo[0]->get("permissions"))) {?> checked <?}?> value="fullAccess" name="fullAccess" id="fullAccess"> This staff account will have full permissions</label>
                                        </div>
                                    </div>
                                </div>
                                <div id="permissions">
                                    <div class="row">
                                        <div class="form-group col-md-6 col-lg-6">
                                            <div class="col-lg-12 col-lg-offset-1">
                                                <h4 style="font-weight: bold">Fulfillment</h4>
                                                <div class="i-checks">
                                                    <label style="font-weight: normal"> <input class="fullAccess form-control" type="checkbox" <? if(in_array("pickOrders", $userInfo[0]->get("permissions"))) {?> checked <?}?> value="pickOrders" name="pickOrders" id="pickOrders"> Pick Orders</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-lg-offset-1">
                                                <div class="i-checks">
                                                    <label style="font-weight: normal"> <input class="fullAccess form-control" type="checkbox" <? if(in_array("shipOrders", $userInfo[0]->get("permissions"))) {?> checked <?}?> value="shipOrders" name="shipOrders" id="shipOrders"> Ship Orders</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-lg-offset-1">
                                                <div class="i-checks">
                                                    <label style="font-weight: normal"> <input class="fullAccess form-control" type="checkbox" <? if(in_array("voidLabels", $userInfo[0]->get("permissions"))) {?> checked <?}?> value="voidLabels" name="voidLabels" id="voidLabels"> Void Labels</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-lg-offset-1">
                                                <div class="i-checks">
                                                    <label style="font-weight: normal"> <input class="fullAccess form-control" type="checkbox" <? if(in_array("nonOrders", $userInfo[0]->get("permissions"))) {?> checked <?}?> value="nonOrders" name="nonOrders" id="nonOrders"> Ship Non Orders</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-lg-offset-1">
                                                <div class="i-checks">
                                                    <label style="font-weight: normal"> <input class="fullAccess form-control" type="checkbox" <? if(in_array("importOrders", $userInfo[0]->get("permissions"))) {?> checked <?}?> value="importOrders" name="importOrders" id="importOrders"> Import New Orders</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-lg-offset-1">
                                                <div class="i-checks">
                                                    <label style="font-weight: normal"> <input class="fullAccess form-control" type="checkbox" <? if(in_array("archiveOrders", $userInfo[0]->get("permissions"))) {?> checked <?}?> value="archiveOrders" name="archiveOrders" id="archiveOrders"> Archive Orders</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6 col-lg-6">
                                            <div class="col-lg-12 col-lg-offset-1">
                                                <h4 style="font-weight: bold">Settings</h4>
                                                <div class="i-checks">
                                                    <label style="font-weight: normal"> <input class="fullAccess form-control" type="checkbox" <? if(in_array("addUsers", $userInfo[0]->get("permissions"))) {?> checked <?}?> value="addUsers" name="addUsers" id="addUsers"> Add Staff Members</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-lg-offset-1">
                                                <div class="i-checks">
                                                    <label style="font-weight: normal"> <input class="fullAccess form-control" type="checkbox" <? if(in_array("addStores", $userInfo[0]->get("permissions"))) {?> checked <?}?> value="addStores" name="addStores" id="addStores"> Add Connected Stores</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-lg-offset-1">
                                                <div class="i-checks">
                                                    <label style="font-weight: normal"> <input class="fullAccess form-control" type="checkbox" <? if(in_array("editStores", $userInfo[0]->get("permissions"))) {?> checked <?}?> value="editStores" name="editStores" id="editStores"> Edit Connected Stores</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-lg-offset-1">
                                                <div class="i-checks">
                                                    <label style="font-weight: normal"> <input class="fullAccess form-control" type="checkbox" <? if(in_array("addPackages", $userInfo[0]->get("permissions"))) {?> checked <?}?> value="addPackages" name="addPackages" id="addPackages"> Add Packages</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-lg-offset-1">
                                                <div class="i-checks">
                                                    <label style="font-weight: normal"> <input class="fullAccess form-control" type="checkbox" <? if(in_array("editPackages", $userInfo[0]->get("permissions"))) {?> checked <?}?> value="editPackages" name="editPackages" id="editPackages"> Edit Packages</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-lg-offset-1">
                                                <div class="i-checks">
                                                    <label style="font-weight: normal"> <input class="fullAccess form-control" type="checkbox" <? if(in_array("changeShippingLabels", $userInfo[0]->get("permissions"))) {?> checked <?}?> value="changeShippingLabels" name="changeShippingLabels" id="changeShippingLabels"> Change Shipping Labels</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6 col-lg-6">
                                            <div class="col-lg-12 col-lg-offset-1">
                                                <h4 style="font-weight: bold">Returns</h4>
                                                <div class="i-checks">
                                                    <label style="font-weight: normal"> <input class="fullAccess form-control" type="checkbox" <? if(in_array("createOrderReturns", $userInfo[0]->get("permissions"))) {?> checked <?}?> value="createOrderReturns" name="createOrderReturns" id="createOrderReturns"> Create Order Returns</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-lg-offset-1">
                                                <div class="i-checks">
                                                    <label style="font-weight: normal"> <input class="fullAccess form-control" type="checkbox" <? if(in_array("createGeneralReturns", $userInfo[0]->get("permissions"))) {?> checked <?}?> value="createGeneralReturns" name="createGeneralReturns" id="createGeneralReturns"> Create General Returns</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-lg-offset-1">
                                                <div class="i-checks">
                                                    <label style="font-weight: normal"> <input class="fullAccess form-control" type="checkbox" <? if(in_array("receiveReturns", $userInfo[0]->get("permissions"))) {?> checked <?}?> value="receiveReturns" name="receiveReturns" id="receiveReturns"> Receive Returns</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-lg-offset-1">
                                                <div class="i-checks">
                                                    <label style="font-weight: normal"> <input class="fullAccess form-control" type="checkbox" <? if(in_array("archiveReturns", $userInfo[0]->get("permissions"))) {?> checked <?}?> value="archiveReturns" name="archiveReturns" id="archiveReturns"> Archive Returns</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-lg-4 col-md-4">
                            <h3 class="titles">Remove staff account</h3>
                            <!--                        <p class="explainText">The address used to calculate shipping rates and the return address on your labels</p>-->
                        </div>
                        <div class=" col-lg-8 col-md-8">
                            <div class="ibox-content img-rounded">
                                <h3 style="padding-bottom: 8px">Deactivate Staff Account</h3>
                                <? if($userInfo[0]->get("status") == "active") {?>
                                    <span>Deactivated staff accounts will no longer have access to your store. You can re-activate staff accounts at any time.</span>
                                    <div class="row">
                                        <button type="button" style="margin-left: 10px; margin-top: 25px" id="deactivate" name="deactivate" onclick="deactivateClient('deactivated')" class="btn btn-default ladda-button" data-style="zoom-out">Deactivate staff account</button>
                                    </div>
                                <?} else {?>
                                    <span>This account is currently deactivated. <? if($this->session->subscriptionLevel == 1) {?>Your current plan only allows for a single user. To activate this user, please upgrade your plan <a href="<? echo base_url()?>register//changePlan">here.</a> <?}?></span>
                                    <div class="row">
                                        <button <? if($this->session->subscriptionLevel == 1) {?> disabled="" <?}?> style="margin-left: 10px; margin-top: 25px" id="activate" type="button" name="activate" onclick="deactivateClient('active')" class="btn btn-default ladda-button" data-style="zoom-out">Activate staff account</button>
                                    </div>
                                <?}?>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
<!--                            <h3 class="titles">Remove staff account</h3>-->
                            <!--                        <p class="explainText">The address used to calculate shipping rates and the return address on your labels</p>-->
                        </div>
                        <div style="margin-top: 20px" class="col-lg-8 col-md-8">
                            <div class="ibox-content img-rounded">
                                <h3 style="padding-bottom: 8px">Delete Staff Account</h3>
                                <span>Deleted staff accounts will be permanently removed from Worry Free Shipping. This action cannot be reversed.</span>
                                <div class="row">
                                    <button style="margin-left: 10px; margin-top: 25px" id="staffBtn" type="button" name="staffBtn" onclick="deleteClient()" class="btn btn-danger ladda-button" data-style="zoom-out">Delete staff account</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <button <? if($userInfo[0]->getObjectId() == $this->session->userId) {?> disabled <?}?> style="margin-top: 10px" id="fullAccess" name="fullAccess" class="btn btn-primary pull-right ladda-button" data-style="zoom-out" onclick="updateUser()">Save</button>
                        <a style="margin-top: 10px; margin-right: 10px" href="<? echo base_url()?>/settings/account" class="btn btn-default pull-right ladda-button"  >Cancel</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<? $this->load->view("navigation/footer");?>
<script>
    $(document).ready(function(){
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green'
        });
        <? if(in_array("fullAccess", $userInfo[0]->get("permissions"))) { ?>
            $('#permissions').hide();
        <?}?>

        $('#fullAccess').on('ifChecked', function(event){
            $('#permissions').hide();
        });
        $('#fullAccess').on('ifUnchecked', function(event){
            $('#permissions').show();
        });
    });
</script>
<!-- Mainly scripts -->
<script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"></script>
<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/jeditable/jquery.jeditable.js"></script>

<script src="<?php echo base_url() ?>assets/js/plugins/dataTables/datatables.min.js"></script>

<!-- Sweet alert -->
<script src="<?php echo base_url() ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo base_url() ?>assets/js/inspinia.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/pace/pace.min.js"></script>

<!-- iCheck -->
<script src="<? echo base_url()?>assets/js/plugins/iCheck/icheck.min.js"></script>

<!-- Toastr script -->
<script src="<? echo base_url()?>assets/js/plugins/toastr/toastr.min.js"></script>

<!-- Ladda -->
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.jquery.min.js"></script>

</body>

</html>






