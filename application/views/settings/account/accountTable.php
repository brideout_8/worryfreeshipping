<?php
/**
 * Created by PhpStorm.
 * User: rapidware
 * Date: 8/19/18
 * Time: 11:48 AM
 */?>
<div class="col-lg-8 col-md-8 col-lg-offset-2 col-mg-offset-2">
    <h1>Account</h1>
    <hr>
    <? if(in_array("admin", $this->session->permissions)) {?>
        <div class="row">
            <div class="col-lg-4 col-md-4">
                <h3 class="titles">Account overview</h3>
<!--                <p class="explainText"><a href="--><?// echo base_url()?><!--settings/plan">Compare plans</a> with different features and rates.</p>-->
            </div>
            <div class=" col-lg-8 col-md-8">
                <div class="ibox-content img-rounded">
                    <div class="row">
                        <div class="col-lg-4 col-md-4">
                            <h3 style="padding-top: 8px">Member since</h3>
                            <span><? echo date_format($accountInfo[0]->getCreatedAt(),"M j, Y")?></span>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <h3 style="padding-top: 8px">Current plan</h3>
                            <span><? if($accountInfo[0]->get("trialStatus") == "trial") { echo "Trial";} else { echo $accountInfo[0]->get("subscriptionLevel");};?></span>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <h3 style="padding-top: 8px">Labels remaining</h3>
                            <span><? echo $accountInfo[0]->get("labelsRemaining")?></span>
                        </div>
                    </div>
                    <hr>
                    <? if($this->session->subscriptionLevel != "0") {?>
                        <span>Your labels will reset in <? echo $diff?> days</span>
                        <p class="explainText"><a href="<? echo base_url()?>settings/buyLabels">Purchase</a> more shipping labels.</p>
                    <?}?>
                    <p class="explainText"><a href="<? echo base_url()?>register/changePlan">Compare plans</a> with different features and rates.</p>
                </div>
            </div>
        </div>
        <hr>
        <form id="accountForm" name="accountForm" method="post">
            <input type="hidden" id="companyId" name="companyId" value="<? echo $accountInfo[0]->getObjectId()?>"/>
            <div class="row">
                <div class="col-lg-4 col-md-4">
                    <h3 class="titles">Standards and formats</h3>
                    <p class="explainText">Standards and formats are used to calculate shipping prices, shipping weights, and order times.</p>
                </div>
                <div class=" col-lg-8 col-md-8">
                    <div class="ibox-content img-rounded">
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label>Timezone</label>
                                <select onchange="enableSave()" id="timeZone" name="timeZone" class="form-control minimal ui-select">
                                    <?php foreach ($timeZones as $zone =>$name){ ?>
                                        <option <?php if($name == $accountInfo[0]->get("timeZone")) { echo "selected='selected'";} ?> value="<?php echo $name; ?>"><?php echo $zone; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Unit system</label>
                                <select onchange="changeWeight(this.value)" id="unitSystem" name="unitSystem" class="form-control minimal ui-select">
                                    <option <? if($accountInfo[0]->get("unitSystem") == "imperial") {?> selected <?}?> value="imperial">Imperial system</option>
                                    <option <? if($accountInfo[0]->get("unitSystem") == "metric") {?> selected <?}?> value="metric">Metric system</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Default weight</label>
                                <select onchange="enableSave()" id="weightMeasurement" name="weightMeasurement" class="form-control minimal ui-select">
                                    <? if($accountInfo[0]->get("unitSystem") == "imperial"){ ?>
                                        <option <? if($accountInfo[0]->get("weightMeasurement") == "oz") {?> selected <?}?> value="oz">Ounce (oz)</option>
                                        <option <? if($accountInfo[0]->get("weightMeasurement") == "lb") {?> selected <?}?>  value="lb">Pound (lb)</option>
                                    <? } else { ?>
                                        <option <? if($accountInfo[0]->get("weightMeasurement") == "g") {?> selected <?}?>  value="g">Gram (g)</option>
                                        <option <? if($accountInfo[0]->get("weightMeasurement") == "kg") {?> selected <?}?>  value="kg">Kilogram (kg)</option>
                                    <?} ?>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <button id="saveBtn" name="saveBtn" disabled class="btn btn-primary pull-right ladda-button" data-style="zoom-out" onclick="updateAccount()" >Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <hr>
    <?}?>
    <? if(in_array("fullAccess", $this->session->permissions) || in_array("addUsers", $this->session->permissions)) {?>
        <div class="row">
            <div class="col-lg-4 col-md-4">
                <h3 class="titles">Accounts and permissions</h3>
    <!--            <p class="explainText">Standards and formats are used to calculate shipping prices, shipping weights, and order times.</p>-->
            </div>
            <div class=" col-lg-8 col-md-8">
                <div class="ibox-content img-rounded">
                    <h3 style="padding-bottom: 8px">Account owner</h3>
                    <span><? echo $accountOwnerInfo[0]->get("firstName")." ". $accountOwnerInfo[0]->get("lastName");?></span>
                </div>
            </div>
            <div class="col-lg-8 col-md-8 col-md-offset-4 col-lg-offset-4" style="margin-top: 25px">
                <div class="ibox-content img-rounded">
                    <? if(count($staffInfo) > 0) {?>
                        <h3 style="padding-bottom: 8px">Staff accounts<span class="pull-right"><a class="normalLink" href="<? if($this->session->subscriptionLevel == "1" || $this->session->subscriptionLevel == "0") { echo base_url()?>register/changePlan <?} else { echo base_url()?>/settings/addUser<?}?>" >Add Staff Account</a></span></h3>
                        <? foreach ($staffInfo as $staff) { $x=0;?>
                            <span style="margin-left: 15px"><a href="<? base_url()?>/settings/editUser?id=<? echo $staff->getObjectId()?>"> <? echo $staff->get("firstName"). " ".$staff->get("lastName")?></a></span><span class="pull-right"><? if($staff->get("status") == "active") { if(in_array("fullAccess", $staff->get("permissions"))) { echo "Full access";} else { echo "Limited access";} } else { echo "No access";}?></span>
                            <? if($x != 0) {?>
                                <hr>
                            <?}?>
                        <? $x++;}?>
                    <?} else {?>
                        <h3 style="padding-bottom: 8px">Staff accounts</h3>
                        <span>Customize what your staff members can edit and access.</span>
                        <div class="row">
                            <a style="margin-left: 10px; margin-top: 25px" id="staffBtn" name="staffBtn" href="<? if($this->session->subscriptionLevel == "1" || $this->session->subscriptionLevel == "0") { echo base_url()?>register/changePlan <?} else { echo base_url()?>/settings/addUser<?}?>" class="btn btn-primary ladda-button" data-style="zoom-out">Add Staff Account</a>
                        </div>
                    <?}?>
                </div>
            </div>
        </div>
    <?}?>
    <? if(in_array("admin", $this->session->permissions)) {?>
        <hr>
        <div class="row">
            <div class="col-lg-4 col-md-4">
                <h3 class="titles">Company Logo</h3>
                <p class="explainText">This will be used on the invoice and packing slip.</p>
            </div>
            <div class="col-lg-8 col-md-8">
                <div class="ibox-content img-rounded">
                    <? if($logo != "") {?>
                        <a class="btn btn-primary pull-right" id="newLogo" name="newLogo" href="<? echo base_url()?>settings/uploadLogo">Change Logo</a>
                        <img class="productImage" src="<?php echo $logo->getURL();?>" alt="Company Logo" >
                    <? } else {?>
                        <h3>No logo has been uploaded</h3>
                        <a class="btn btn-primary" id="newLogo" name="newLogo" href="<? echo base_url()?>settings/uploadLogo">Upload Logo</a>
                    <? }?>
                </div>
            </div>
        </div>
    <?}?>
    <hr>
    <div class="row">
        <div class="col-lg-4 col-md-4">
            <h3 class="titles">Tour</h3>
            <p class="explainText">Reset the features tour</p>
        </div>
        <div class="col-lg-8 col-md-8">
            <div class="ibox-content img-rounded">
                <h3>Reset the tour</h3>
                <button type="button" class="btn btn-primary ladda-button" data-style="zoom-out" onclick="resetTour()">Reset tour</button>
            </div>
        </div>
    </div>
</div>