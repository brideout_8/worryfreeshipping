<?php
/**
 * Created by PhpStorm.
 * User: rapidware
 * Date: 9/15/18
 * Time: 7:00 PM
 */?>

<style>
    div.ibox-content {
        border: solid 1px #d9d9d9;
        /*box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);*/
        /*margin-top: 20px;*/
        border-radius: 5px;
        /*box-shadow: #3D3D3D;*/
    }
    .table th {
        /*text-align: center;*/
        font-weight: normal;
        border: 0;
    }
    .table td {
        /*font-weight: bold;*/
        /*font-size: 15px;*/
    }
    table tr:first-child td {
        border-top: 0;
    }
    /*a {*/
    /*color: #717171;*/
    /*}*/
    .normalLink {
        font-size: 15px;
        font-weight: normal;
    }
    p {
        margin:-2px 0 -2px 0;
    }
    .buttonLink {
        background:none!important;
        color: steelblue;
        border:none;
        padding:0!important;
        font: inherit;
        /*border is optional*/
        cursor: pointer;
    }
    .explainText {
        padding-top: 10px;
        color: #9a9a9a;
    }
    .titles {
        padding-top: 17px;
    }
    .dataText p {
        margin-bottom: 3px;
    }
    hr {
        border-color: #dcdcdc;
    }
    .modal-backdrop {
        background-color: #c7c7c7
    }
    .explainText {
        /*padding-top: 10px;*/
        color: #9a9a9a;
    }
    #toast-container>.toast {
        background-image: none !important;
    }
    .form-control {
        border-radius: 5px;
    }
</style>
</head>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="col-lg-8 col-md-8 col-lg-offset-2 col-mg-offset-2">
                <h1 style="font-weight: bold">Add User</h1>
                <hr>
                <div class="row">
                    <div class="col-lg-4 col-md-4">
                        <h3 class="titles">User information</h3>
                        <!--                        <p class="explainText">The address used to calculate shipping rates and the return address on your labels</p>-->
                    </div>
                    <div class=" col-lg-8 col-md-8">
                        <div class="ibox-content img-rounded">
                            <h3 style="padding-bottom: 8px">Account Information</h3>
                            <form id="addUserForm" name="addUserForm" method="post" class="form-horizontal">
                                <div class="form-group">
                                    <label autocomplete="false" class="col-lg-2 control-label">First Name</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" id="firstName" name="firstName" value="">
                                    </div>
                                    <label autocomplete="false" class="col-lg-2 control-label">Last Name</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" id="lastName" name="lastName" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label autocomplete="false" class="col-lg-2 control-label">Email</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" id="email" name="email" value="">
                                    </div>
                                    <label autocomplete="false" class="col-lg-2 control-label">Username</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" id="username" name="username" value="">
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <div class="i-checks">
                                            <label style="font-weight: normal"> <input class="fullAccess form-control" type="checkbox" value="fullAccess" name="fullAccess" id="fullAccess"> This staff account will have full permissions</label>
                                        </div>
                                    </div>
                                </div>
                                <div id="permissions">
                                    <div class="row">
                                        <div class="form-group col-md-6 col-lg-6">
                                            <div class="col-lg-12 col-lg-offset-1">
                                                <h4 style="font-weight: bold">Fulfillment</h4>
                                                <div class="i-checks">
                                                    <label style="font-weight: normal"> <input class="fullAccess form-control" type="checkbox" value="pickOrders" name="pickOrders" id="pickOrders"> Pick Orders</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-lg-offset-1">
                                                <div class="i-checks">
                                                    <label style="font-weight: normal"> <input class="fullAccess form-control" type="checkbox" value="shipOrders" name="shipOrders" id="shipOrders"> Ship Orders</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-lg-offset-1">
                                                <div class="i-checks">
                                                    <label style="font-weight: normal"> <input class="fullAccess form-control" type="checkbox" value="voidLabels" name="voidLabels" id="voidLabels"> Void Labels</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-lg-offset-1">
                                                <div class="i-checks">
                                                    <label style="font-weight: normal"> <input class="fullAccess form-control" type="checkbox" value="nonOrders" name="nonOrders" id="nonOrders"> Ship Non Orders</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-lg-offset-1">
                                                <div class="i-checks">
                                                    <label style="font-weight: normal"> <input class="fullAccess form-control" type="checkbox" value="importOrders" name="importOrders" id="importOrders"> Import New Orders</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-lg-offset-1">
                                                <div class="i-checks">
                                                    <label style="font-weight: normal"> <input class="fullAccess form-control" type="checkbox" value="archiveOrders" name="archiveOrders" id="archiveOrders"> Archive Orders</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6 col-lg-6">
                                            <div class="col-lg-12 col-lg-offset-1">
                                                <h4 style="font-weight: bold">Settings</h4>
                                                <div class="i-checks">
                                                    <label style="font-weight: normal"> <input class="fullAccess form-control" type="checkbox" value="addUsers" name="addUsers" id="addUsers"> Add Staff Members</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-lg-offset-1">
                                                <div class="i-checks">
                                                    <label style="font-weight: normal"> <input class="fullAccess form-control" type="checkbox" value="addStores" name="addStores" id="addStores"> Add Connected Stores</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-lg-offset-1">
                                                <div class="i-checks">
                                                    <label style="font-weight: normal"> <input class="fullAccess form-control" type="checkbox" value="editStores" name="editStores" id="editStores"> Edit Connected Stores</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-lg-offset-1">
                                                <div class="i-checks">
                                                    <label style="font-weight: normal"> <input class="fullAccess form-control" type="checkbox" value="addPackages" name="addPackages" id="addPackages"> Add Packages</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-lg-offset-1">
                                                <div class="i-checks">
                                                    <label style="font-weight: normal"> <input class="fullAccess form-control" type="checkbox" value="editPackages" name="editPackages" id="editPackages"> Edit Packages</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-lg-offset-1">
                                                <div class="i-checks">
                                                    <label style="font-weight: normal"> <input class="fullAccess form-control" type="checkbox" value="changeShippingLabels" name="changeShippingLabels" id="changeShippingLabels"> Change Shipping Labels</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6 col-lg-6">
                                            <div class="col-lg-12 col-lg-offset-1">
                                                <h4 style="font-weight: bold">Returns</h4>
                                                <div class="i-checks">
                                                    <label style="font-weight: normal"> <input class="fullAccess form-control" type="checkbox" value="createOrderReturns" name="createOrderReturns" id="createOrderReturns"> Create Order Returns</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-lg-offset-1">
                                                <div class="i-checks">
                                                    <label style="font-weight: normal"> <input class="fullAccess form-control" type="checkbox" value="createGeneralReturns" name="createGeneralReturns" id="createGeneralReturns"> Create General Returns</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-lg-offset-1">
                                                <div class="i-checks">
                                                    <label style="font-weight: normal"> <input class="fullAccess form-control" type="checkbox" value="receiveReturns" name="receiveReturns" id="receiveReturns"> Receive Returns</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-lg-offset-1">
                                                <div class="i-checks">
                                                    <label style="font-weight: normal"> <input class="fullAccess form-control" type="checkbox" value="archiveReturns" name="archiveReturns" id="archiveReturns"> Archive Returns</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <button style="margin-top: 10px" id="fullAccess" name="fullAccess" class="btn btn-primary pull-right ladda-button" data-style="zoom-out" onclick="createUser()">Send Invite</button>
                        <a style="margin-top: 10px; margin-right: 10px" href="<? echo base_url()?>/settings/account" class="btn btn-default pull-right ladda-button"  >Cancel</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<? $this->load->view("navigation/footer");?>
<script>
    $(document).ready(function(){
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green'
        });

        $('#fullAccess').on('ifChecked', function(event){
            $('#permissions').hide();
        });
        $('#fullAccess').on('ifUnchecked', function(event){
            $('#permissions').show();
        });
    });
</script>
<!-- Mainly scripts -->
<script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"></script>
<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/jeditable/jquery.jeditable.js"></script>

<script src="<?php echo base_url() ?>assets/js/plugins/dataTables/datatables.min.js"></script>

<!-- Sweet alert -->
<script src="<?php echo base_url() ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo base_url() ?>assets/js/inspinia.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/pace/pace.min.js"></script>

<!-- iCheck -->
<script src="<? echo base_url()?>assets/js/plugins/iCheck/icheck.min.js"></script>

<!-- Toastr script -->
<script src="<? echo base_url()?>assets/js/plugins/toastr/toastr.min.js"></script>

<!-- Ladda -->
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.jquery.min.js"></script>

</body>

</html>





