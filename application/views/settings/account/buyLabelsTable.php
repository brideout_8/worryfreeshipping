<?php
/**
 * Created by PhpStorm.
 * User: rapidware
 * Date: 9/22/18
 * Time: 5:13 PM
 */?>

<div class="col-lg-8 col-md-8 col-lg-offset-2 col-mg-offset-2">
    <h1 style="font-weight: bold">Purchase Labels</h1>
    <hr>
    <div class="row">
        <div class="col-lg-4 col-md-4">
            <h3 class="titles">User information</h3>
            <!--                        <p class="explainText">The address used to calculate shipping rates and the return address on your labels</p>-->
        </div>
        <div class=" col-lg-8 col-md-8">
            <div class="ibox-content img-rounded">
<!--                            <h3 style="padding-bottom: 8px">Account Information</h3>-->
                <form id="buyLabelsForm" name="buyLabelsForm" method="post" class="form-horizontal">
                    <input type="hidden" id="costPerLabel" name="costPerLabel" value="<? echo $labelCost["perLabel"]?>">
                    <input type="hidden" id="description" name="description" value="<? echo $labelCost["terms"]?>">
                    <input type="hidden" id="billingDate" name="billingDate" value="<? echo $billingDate?>">
                    <table class="table" >
                        <thead>
                        <tr>
                            <th>Number of labels</th>
                            <th>Cost per label</th>
                            <th>Total cost</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><input type="number" id="numberOfLabels" name="numberOfLabels" value="" onchange="increaseLabelCost()"></td>
                            <td id="costPer">$<? echo $labelCost["perLabel"]?></td>
                            <td id="totalCost">0.00</td>
                        </tr>
                        </tbody>
                    </table>
                    <hr>
                    <h4>Days until labels reset<span class="pull-right"><? echo $daysDiff?>*</span> </h4>
                    <h4>Current labels remaining<span id="currentLabelsRemaining" class="pull-right"><? echo $this->session->labelsRemaining?></span> </h4>
                    <h4>New labels remaining after purchase<span id="labelsRemaining" class="pull-right"><? echo $this->session->labelsRemaining?></span> </h4>
                </form>
            </div>
            <div class="ibox-footer">
                <h4>* You will not lose any unused labels you purchase when labels reset. After the reset your unused purchased labels will be added to the reset amount of <? echo $labelCost["labelsRemaining"]?></h4>
            </div>
            <button style="margin-top: 10px" id="fullAccess" name="fullAccess" class="btn btn-primary pull-right ladda-button" data-style="zoom-out" onclick="purchaseLabels()">Purchase labels</button>
            <a style="margin-top: 10px; margin-right: 10px" href="<? echo base_url()?>/settings/account" class="btn btn-default pull-right ladda-button"  >Cancel</a>
        </div>
    </div>
</div>






