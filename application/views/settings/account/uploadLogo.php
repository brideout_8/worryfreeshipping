<?php
/**
 * Created by PhpStorm.
 * User: Marketing101
 * Date: 9/25/18
 * Time: 8:59 AM
 */?>

<?php
/**
 * Created by PhpStorm.
 * User: rapidware
 * Date: 8/19/18
 * Time: 11:47 AM
 */?>
<style>
    div.ibox-content {
        border: solid 1px #d9d9d9;
        /*box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);*/
        /*margin-top: 20px;*/
        border-radius: 5px;
        /*box-shadow: #3D3D3D;*/
    }
    .table th {
        text-align: center;
        font-weight: normal;
        border: 0;
    }
    .table td {
        /*font-weight: bold;*/
        /*font-size: 15px;*/
    }
    table tr:first-child td {
        border-top: 0;
    }
    /*a {*/
    /*color: #717171;*/
    /*}*/
    .normalLink {
        font-size: 15px;
        font-weight: normal;
    }
    p {
        margin:-2px 0 -2px 0;
    }
    .buttonLink {
        background:none!important;
        color: steelblue;
        border:none;
        padding:0!important;
        font: inherit;
        /*border is optional*/
        cursor: pointer;
    }
    .explainText {
        padding-top: 10px;
        color: #9a9a9a;
    }
    .titles {
        padding-top: 17px;
    }
    .dataText p {
        margin-bottom: 3px;
    }
    hr {
        border-color: #dcdcdc;
    }
    label {
        font-weight: normal;
    }
    .productImage {
        /*display: block;*/
        max-width:250px;
        max-height:250px;
        width: auto;
        height: auto;
    }
</style>
</head>

<div class="wrapper wrapper-content">
    <div class="col-lg-8 col-md-8 col-lg-offset-2 col-mg-offset-2">
        <h1>Logo upload</h1>
        <hr>
        <div class="row">
            <div class="col-lg-4 col-md-4">
                <h3 class="titles">Company Logo</h3>
                <p class="explainText">This will be used on the invoice and packing slip.</p>
            </div>
            <div class="col-lg-8 col-md-8">
                <div class="ibox-content img-rounded">
<!--                    <center><img src="--><?php //echo $logo->getURL(); ?><!--" alt="Company Logo" style="height:100px;"></center> <br>-->
                    <center><h3> Recommended height 100px </h3> </center>
                    <center><h4> Accepted Types: jpg, pdf, png  </h4> </center>
                    <hr>
                    <form action="<?php echo base_url();?>settings/saveLogo" class="dropzone" id="dropzoneForm">
                        <div class="fallback">
                            <input type="file" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<? $this->load->view("navigation/footer");?>
<script type="text/javascript">
    $(document).ready(function(){
        Dropzone.options.dropzoneForm = {
            acceptedFiles: "image/jpeg,image/png, image/pdf",
            uploadMultiple: false,
            addRemoveLinks: true,
            dictRemoveFile: "Clear Thumbnail",
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 2, // MB
            accept: function(file, done) {
                if (file.name == "justi.jpg") {
                    done("Naha, you don't.");
                } else {
                    done();
                }
            },
            init: function() {
                this.on("complete", function(file) {
                    swal({
                            title: "Success",
                            text: "Your logo has been updated.",
                            type: "success",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Ok!",
                            //                                    cancelButtonText: "No, cancel.",
                            closeOnConfirm: true
                            //                                    closeOnCancel: true
                        },
                        function (isConfirm) {
                            if (isConfirm) {
                                top.location = base_url+"settings/account";
                            } else {
                                //                            swal("Not Deleted!", "Your file is safe");
                            }
                        });

                });
            }
        };
    });
</script>
<!-- Mainly scripts -->
<script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"></script>
<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/jeditable/jquery.jeditable.js"></script>

<script src="<?php echo base_url() ?>assets/js/plugins/dataTables/datatables.min.js"></script>

<!-- DROPZONE -->
<script src="<? echo base_url() ?>assets/js/plugins/dropzone/dropzone.js"></script>

<!-- Sweet alert -->
<script src="<?php echo base_url() ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo base_url() ?>assets/js/inspinia.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/pace/pace.min.js"></script>

<!-- iCheck -->
<script src="<? echo base_url()?>assets/js/plugins/iCheck/icheck.min.js"></script>

<!-- Ladda -->
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.jquery.min.js"></script>

</body>

</html>
