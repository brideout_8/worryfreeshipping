<?php
/**
 * Created by PhpStorm.
 * User: Marketing101
 * Date: 9/19/18
 * Time: 8:45 AM
 */?>

<style>
    img {
        max-width: 60%;
        max-height: 60%;
        display: block;
        margin-left: auto;
        margin-right: auto
    }

    .signUpColumns {
        max-width: 1100px;
        margin: 0 auto;
        padding: 250px 20px 20px 20px;
    }
    .custom-bg {
        background-color: #f6f8fa;
    }
    div.ibox-content{
        /*box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);*/
        /*box-shadow: #3D3D3D;*/
        border: solid 1px #d9d9d9;
        border-radius: 5px;
    }
    label {
        font-weight: normal;
        color: black;
    }
    ::placeholder {
        font-weight: lighter;
    }
    .createAccount {
        margin-top: 15px;
    }
    h1 {
        font-size: 40px;
    }
</style>
</head>

<body class="custom-bg">
<div class="signUpColumns animated fadeInDown">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1">
                        <div class="row">
                            <div class="col-md-12">
                                <h1 style="color: black; font-weight: normal; text-align: center; padding-bottom: 25px">Error</h1>
                            </div>
                        </div>
                        <form method="post" id="login_form" name="login_form">
                            <div class="row">
                                <p style="text-align: center">We encountered an error. Please contact support at support@worryfreeshipping.com with error: <? echo $saveShop?></p>
                            </div>
                            <a class="ladda-button btn btn-primary block full-width m-b createAccount" href="<?php echo base_url()?>settings/stores">See Store Details</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-2.1.1.js"></script>
<!-- Sweet alert -->
<script src="<?php echo base_url() ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo base_url()?>assets/js/functions.js" type="text/javascript" charset="utf-8"></script>
</body>

</html>





