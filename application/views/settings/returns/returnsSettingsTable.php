<?php
/**
 * Created by PhpStorm.
 * User: rapidware
 * Date: 9/1/18
 * Time: 1:56 PM
 */?>

<div class="col-lg-8 col-md-8 col-lg-offset-2 col-mg-offset-2">
    <h1>Returns</h1>
    <hr>
    <div class="row">
        <div class="col-lg-4 col-md-4">
            <h3 class="titles">Return locations</h3>
            <p class="explainText">The default address your returns will be sent to</p>
        </div>
        <div class=" col-lg-8 col-md-8">
            <div class="ibox-content img-rounded">
                <h3 style="padding-bottom: 8px">Locations <span class="pull-right"><a class="normalLink" href="#" onclick="locationNotice()">Refresh Locations</a></span></h3>
                <table class="table" >
                    <thead>
                    <tr style="text-align: center">
                        <th>Name</th>
                        <th>Address</th>
                        <th>City</th>
                        <th>State</th>
                        <th>Zip</th>
                        <th>Country</th>
                        <th size="5%">Default</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($locations as $location) { ?>
                        <tr>
                            <td><? echo $location->get("name"); ?></td>
                            <td><? echo $location->get("address1")." ".$location->get("address2")?></td>
                            <td><? echo $location->get("city") ?></td>
                            <td><? echo $location->get("state") ?></td>
                            <td><? echo $location->get("zip") ?></td>
                            <td><? echo $location->get("country") ?></td>
                            <? if($location->get("returnDefault") == "true") {?>
                                <td><span>Yes</span></td>
                            <?} else {?>
                                <td><button type="button" onclick="shippingAccountStatus('<? echo $location->getObjectId()?>')" class="btn btn-primary btn-xs ladda-button">Make Default</button></td>
                            <?}?>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-lg-4 col-md-4">
            <h3 class="titles">Returns overview</h3>
            <p class="explainText">General return data</p>
        </div>
        <div class=" col-lg-8 col-md-8">
            <div class="ibox-content img-rounded">
                <div class="row">
                    <div class="col-lg-4 col-md-4">
                        <h3 style="padding-top: 8px; text-align: center">Total Returns</h3>
                        <div style="text-align: center" class="dataText">
                            <? echo count($returns)?>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <h3 style="padding-top: 8px; text-align: center">Total Cost</h3>
                        <div style="text-align: center" class="dataText">
                            <? $returnTotals = 0.00;
                            foreach ($returns as $return) {
                                $returnTotals = $returnTotals + floatval($return->get("cost"))?>
                            <?}
                            echo $returnTotals;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




