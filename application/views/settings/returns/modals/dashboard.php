<?php
/**
 * Created by PhpStorm.
 * User: rapidware
 * Date: 9/1/18
 * Time: 2:32 PM
 */?>

<!-- Address Change Modal -->
<div class="modal fade" id="returnsToModal" name="returnsToModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Update Return To Address</h4>
            </div>
            <div class="modal-body">
                <form id="returnsToForm" name="returnsToForm" method="post" class="form-horizontal">
                    <input type="hidden" id="companyId" name="companyId" value="<?php echo $companyId?>" />
                    <div class="form-group">
                        <label autocomplete="false" class="col-lg-2 control-label">Company Name</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" id="company" name="company" value="<? if(isset($returnsTo->company)) { echo $returnsTo->company; }?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label autocomplete="false" class="col-lg-2 control-label">Address</label>
                        <div class="col-lg-7">
                            <input type="text" class="form-control" id="address1" name="address1" value="<? echo $returnsTo->address1?>">
                        </div>
                        <div class="col-lg-3">
                            <input type="text" class="form-control" id="address2" name="address2" value="<? echo $returnsTo->address2?>">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label style="font-size:14px; font-weight: bolder;" for="inputLast" class="col-md-2 control-label">City</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="city" name="city" value="<? echo $returnsTo->city?>">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label style="font-size:14px; font-weight: bolder;" for="inputLast" class="col-md-2 control-label">State/Province</label>
                        <div class="col-md-3">
                            <input type="text" class="form-control" id="state" name="state" value="<? echo $returnsTo->state?>">
                        </div>
                        <label style="font-size:14px; font-weight: bolder;" for="inputLast" class="col-md-1 control-label">Zip</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control" id="zip" name="zip" value="<? echo $returnsTo->zip?>">
                        </div>
                        <label style="font-size:14px; font-weight: bolder;" for="inputLast" class="col-md-1 control-label">Country</label>
                        <div class="col-md-3">
                            <select id="country" name="country" class="form-control minimal ui-select">
                                <option<? if($returnsTo->country == "United States") {?> selected <?}?> value="United States">United Sates</option>
                                <option <? if($returnsTo->country == "Canada") {?> selected <?}?> value="Canada">Canada</option>
                                <option <? if($returnsTo->country == "United Kingdom") {?> selected <?}?> value="United Kingdom">United Kingdom</option>
                                <option  <? if($returnsTo->country == "Australia") {?> selected <?}?>value="Australia">Australia</option>
                            </select>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label style="font-size:14px; font-weight: bolder;" for="inputLast" class="col-md-2 control-label">Phone Number</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="phone" name="phone" value="<? echo $returnsTo->phone?>">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="col-sm-12 col-sm-12 pull-right">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="button" class="ladda-button btn btn-primary" data-style="zoom-out" id="save" value="Save" name="save" onclick="updateReturnsToAddress()"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
