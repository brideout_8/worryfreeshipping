<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"></script>
    <script src="<?php echo base_url() ?>assets/js/functions.js" type="text/javascript" charset="utf-8"></script>

    <!-- 	<link href="<?php echo base_url() ?>assets/css/plugins/chosen/chosen.css" rel="stylesheet"> -->
    <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
    <!-- Sweet Alert -->
    <link href="<?php echo base_url() ?>assets/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo base_url()?>assets/js/validationEngine.jquery.css"/>
    <script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.validationEngine.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>

</head>

<script type="text/javascript">
    function checkEnter(e) {
        var keyCode = (e.keyCode ? e.keyCode : e.which);
        if(keyCode == 13 )
        {
            e.preventDefault();

            login();
        }
    }

    function login() {

        var formArray = $('#login_form').serialize();
        var base_url  = "<?php echo base_url(); ?>";
        document.login_form.loginButton.value="Logging In...";
        document.login_form.loginButton.disabled = true;
//        $('#loader').show();
//        $('#logInBox').hide();
        jQuery.post('<?php echo base_url(); ?>login/login_validation' , formArray, function(data)
        {
            //alert('here');

            if (data.success)
            {
                var url = data.url;
                top.location = base_url+url;
            }
            else
            {
//                $('#loader').hide();
//                $('#logInBox').show();
                document.login_form.loginButton.disabled = false;
                document.login_form.loginButton.value="Log Into Your Account";
                if(data.error == 1){
                    swal({
                        title: "Please enter an username and password"
                        // text: "You clicked the button!",
//                 type: "success"
                    });
                } else if (data.error == 2){
                    swal({
                        title: "Error",
                        text: "Your subscription is past due. Please contact your administrator to update your account."
//                 type: "success"
                    });
                } else if (data.error == 3) {
                    swal({
                        title: "Error",
                        text: "Your subscription has been cancelled. Please contact your administrator to activate your account."
//                 type: "success"
                    });
                } else {
                    swal({
                        title: "Error",
                        text: "Please contact support at 636-717-0001",
//                 type: "success"
                    });
                }
            }
        }, 'json');

        return false;
    }

    function upload() {
        jQuery.post('<?php echo base_url(); ?>login/getProduct' , function(data)
        {
            //alert('here');

            if (data.success)
            {
                swal({
                    title: "Success",
                    type: "success"
                })
            }
            else
            {
//                $('#loader').hide();
//                $('#logInBox').show();
                if(data.error == 1){
                    swal({
                        title: "Please enter an username and password"
                        // text: "You clicked the button!",
//                 type: "success"
                    });
                } else if (data.error == 2){
                    swal({
                        title: "Error",
                        text: "Your subscription is past due. Please contact your administrator to update your account."
//                 type: "success"
                    });
                } else if (data.error == 3) {
                    swal({
                        title: "Error",
                        text: "Your subscription has been cancelled. Please contact your administrator to activate your account."
//                 type: "success"
                    });
                } else {
                    swal({
                        title: "Error",
                        text: "Please contact support at 636-717-0001",
//                 type: "success"
                    });
                }
            }
        }, 'json');

        return false;
    }
</script>

<style>
    #photo{
        display:block;
        margin:auto;
        position: relative;
        top: 50px;
    }
    .middle-box {
        margin-top: 50px;
    }
</style>

<body class="gray-bg">

<div class="loginscreen animated fadeInDown row">
        <img id="photo" src="<?php echo base_url() ?>assets/img/rrlogo.png" alt="Logo" width="382" height="225" />
</div>

<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <div>

<!--            <h1 class="logo-name">IN+</h1>-->
<!--            <img id="photo" src="--><?php //echo base_url() ?><!--assets/img/genethixlogo.png" alt="Logo" width="375" height="175" />-->

        </div>
<!--        <h3>Welcome to GenEthix</h3>-->
<!--        <p>-->
            <!--Continually expanded and constantly improved Inspinia Admin Them (IN+)-->
<!--        </p>-->
<!--        <p>Strength and Conditioning Integration</p>-->
        <form class="m-t" name="login_form" id="login_form" method="post" action="">
            <div class="form-group">
                <input class="form-control" type="text" placeholder="Email" <?php if(@$username){ echo @$username;}else { set_value('username'); }?> id="username" name="username" />
            </div>
            <div class="form-group">
                <input class="form-control" onkeypress="checkEnter(event);" type="password" placeholder="Password" name="password" id="password" <?php if(@$password){ echo @$password;}else { set_value('password'); }?> />
            </div>
            <input type="button" id="loginButton" name="loginButton" value = "Log Into Your Account" class="btn btn-primary block full-width m-b" onClick="this.disabled=true; login();">

            <a href="<?php echo base_url() ?>forgotpassword/">
                <small>Forgot username/password?</small>
            </a>
<!--            <p class="text-muted text-center"><small>Do not have an account?</small></p>-->
            <hr>
            <a class="btn btn-sm btn-white btn-block" href="<?php echo base_url() ?>register">Create an account</a>
        </form>
        <p class="m-t"> <small>R&R Enterprises &copy; 2017</small> </p>
        <button type="submit" onclick="upload()" class="btn btn-primary">Get Product</button>
    </div>
</div>

<!-- Sweet alert -->
<script src="<?php echo base_url() ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>

</body>

</html>
