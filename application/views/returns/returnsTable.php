<?php
/**
 * Created by PhpStorm.
 * User: rapidware
 * Date: 9/2/18
 * Time: 2:23 PM
 */?>

<div class="table-responsive">
    <table class="table dataTables-example" >
        <thead>
        <tr>
            <th style="width: 10%">Order</th>
            <th style="width: 15%">Store</th>
            <th style="width: 10%">Date</th>
            <th style="width: 10%">Tracking Number</th>
            <th style="width: 10%">Service</th>
            <th style="width: 10%">Reason</th>
            <th style="width: 10%">Action</th>
            <th style="width: 15%">Download</th>
        </tr>
        </thead>
        <tbody>
        <?php $x = 0;
        foreach ( $results as $result ) { ?>
            <tr>
                <td style="vertical-align: middle"><?php echo $result->get('orderNumber');?></td>
                <td style="vertical-align: middle"><?php if($this->session->shopifyStoresInfo[$result->get("storeName")]["shopifyDisplayName"] != null) { echo $this->session->shopifyStoresInfo[$result->get("storeName")]["shopifyDisplayName"];} else { echo $result->get("storeName");}; ?></td>
                <!--                <td>--><?php //echo $result->get('orderId');?><!--</td>-->
                <td style="vertical-align: middle"><?php
                    if($result->get('dateShipped') != "") {
                        if(strtotime($result->get('dateShipped')) > strtotime('1 day ago')) {
                            $dateNow = new DateTime($result->get('dateShipped'), new DateTimeZone("America/Chicago"));
                            echo "Today";
                        } else if(strtotime($result->get('dateShipped')) > strtotime('2 days ago')) {
                            $dateNow = new DateTime($result->get('dateShipped'), new DateTimeZone("America/Chicago"));
                            echo "Yesterday";
                        } else if(strtotime($result->get('dateShipped')) > strtotime('7 days ago')) {
                            $dateNow = new DateTime($result->get('dateShipped'), new DateTimeZone("America/Chicago"));
                            echo $dateNow->format('l');
                        } else {
                            $dateNow = new DateTime($result->get('dateShipped'), new DateTimeZone("America/Chicago"));
                            echo $dateNow->format('M d');
                        }
                    }?></td>
                <td style="vertical-align: middle"><?php echo $result->get("trackingNumbers");?></td>
                <td style="vertical-align: middle"><?php echo $result->get("service");?></td>
                <td style="vertical-align: middle"><?php echo $result->get("reason");?></td>
                <td style="vertical-align: middle"><?php
                    if($result->get("status") == "received") {
                        if(in_array("archiveReturns", $this->session->userdata("permissions")) || in_array("fullAccess", $this->session->userdata("permissions"))) {?>
                            <button onclick="updateReturn('archive','<? echo $result->getObjectId()?>')" <? if($this->session->subscriptionLevel == "1") {?> disabled="" <?}?> class="btn btn-primary btn-xs ladda-button">Archive</button>
                        <?}?>
                    <?php } else if($result->get("status") == "archived") {?>
                        <button class="btn btn-primary btn-xs" disabled="">Archived</button>
                    <?php } else if(in_array("receiveReturns", $this->session->userdata("permissions")) || in_array("fullAccess", $this->session->userdata("permissions"))) { ?>
                        <button onclick="updateReturn('receive','<? echo $result->getObjectId()?>')" <? if($this->session->subscriptionLevel == "1") {?> disabled="" <?}?> class="btn btn-primary btn-xs ladda-button">Receive</button>
                    <?php } else { ?>
                        <!--No permissions -->
                        <button onclick="updateReturn('receive','<? echo $result->getObjectId()?>')" disabled class="btn btn-primary btn-xs ladda-button">Receive</button>
                    <?} ?>
                </td>
                <td style="vertical-align: middle">
                    <button class="btn btn-primary btn-xs labelDownload" <? if($this->session->subscriptionLevel == "1") {?> disabled="" <?}?> onclick="download('<?php echo $result->get("labelLink");?>?download=0')" ><?php echo "Download Label"?></button>
                </td>
            </tr>
            <?php $x++; } ?>
        </tbody>
    </table>
</div>


