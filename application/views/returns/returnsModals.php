<?php
/**
 * Created by PhpStorm.
 * User: rapidware
 * Date: 9/3/18
 * Time: 9:55 AM
 */?>


<!-- New Order Return Modal -->
<div class="modal inmodal" id="newOrderReturnModal" name="newOrderReturnModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Find Order</h4>
            </div>
            <div class="modal-body">
                <form id="newOrderReturnForm" name="newOrderReturnForm" method="post" class="form-horizontal">
                    <div class="row form-group">
                        <label class="col-md-2 control-label">Order Number</label>
                        <div class="col-md-3 col-sm-3">
                            <input type="text" id="orderNumber" name="orderNumber" class="form-control" value=""/>
                        </div>
                        <label class="col-md-2 control-label">Store Name</label>
                        <div class="col-md-5 col-sm-5">
                            <select id="storeName" name="storeName" class="form-control minimal ui-select">
                                <?php foreach ($this->session->shopifyStoresInfo as $store) {?>
                                <option value="<? echo $store["shopifyName"]?>"><? if($store["shopifyDisplayName"] != null) { echo $store["shopifyDisplayName"];} else { echo $store["shopifyName"];} ?></option>
                                <? } ?>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="col-sm-12 col-sm-12 pull-right">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="button" class="ladda-button btn btn-primary" data-style="zoom-out" id="findOrder" value="Find Order" name="findOrder" onclick="findReturnOrder()"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>