<?php
/**
 * Created by PhpStorm.
 * User: rapidware
 * Date: 9/1/18
 * Time: 3:11 PM
 */?>

<style>

    td {
        height: 35px;
    }

    .rounded {
        border-radius: 5px;
    }
    #title {
        text-align: center;
        color: green;
    }
    p {
        text-align: center;
    }
    #inputOrder {
        text-align: center;
        margin-top: 20px;
    }
    .bottomBtn {
        margin-top: 15px;
    }
    #actionButtons {
        margin-top: 15px;
    }
    h2 {
        text-align: center;
    }
    .dateTime {
        font-size: 15px;
        margin-left: 10px;
    }
    .badge {
        margin-left: 10px;
    }
    .title{
        padding-bottom: 20px;
        padding-left: 12px;
    }
    label {
        font-weight: normal;
        font-size: 14px;
    }
    .selection {
        background-color: transparent;
    }
    .bootstrap-select .dropdown-menu {
        margin: 15px 0 0;
    }

</style>
</head>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="tourTitle">Returns Manager</h1>
            <div class="ibox float-e-margins tourReturns">
                <div class="ibox-content img-rounded">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="tabs-container">
                                <? if(in_array("createGeneralReturns", $this->session->userdata("permissions")) || in_array("fullAccess", $this->session->userdata("permissions"))) {?>
                                    <a class="btn btn-primary pull-right tourGeneral" <? if($this->session->subscriptionLevel == "1") {?> disabled="" <?}?> id="newGeneralButton" name="newGeneralReturn" style="" href="<? base_url()?>/returns/newGeneralReturn">New General Return</a>
                                <?}?>
                                <? if(in_array("createOrderReturns", $this->session->userdata("permissions")) || in_array("fullAccess", $this->session->userdata("permissions"))) {?>
                                    <button class="btn btn-primary pull-right tourNewOrder" <? if($this->session->subscriptionLevel == "1") {?> disabled="" <?}?> href="#" data-toggle="modal" data-target="#newOrderReturnModal" style="margin-right: 10px" >New Order Return</button>
                                <?}?>
                                <ul id="myTabs" class="nav nav-tabs nav-fill">
                                    <li><a style="color: black" data-toggle="tab" href="#tab-20">Pending</a></li>
                                    <li><a style="color: black" data-toggle="tab" href="#tab-21">Received</a></li>
                                    <li><a style="color: black" data-toggle="tab" href="#tab-22">Archived</a></li>
                                </ul>
                                <div class="tab-content boxShadow">
                                    <div id="tab-20" class="tab-pane">
                                        <div class="panel-body">
                                            <div id="pending"></div>
                                        </div>
                                    </div>
                                    <div id="tab-21" class="tab-pane">
                                        <div class="panel-body">
                                            <div id="received"></div>
                                        </div>
                                    </div>
                                    <div id="tab-22" class="tab-pane">
                                        <div class="panel-body">
                                            <div id="archived"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<? $this->load->view("navigation/footer");?>
<script>
    function loader() {
        $('#listings2').html('<img height="50" width="50" style=" position: absolute; top: 50%; left: 50%;" src="'+base_url+'assets/img/loader.gif">');
    }
    $(document).ready(function() {
        var newOrders = false;
        var ordersToShip = false;
        var ordersOnHold = false;
        var allOrders = false;
        $('.nav-tabs a[href="#tab-20"]').on('show.bs.tab', function(event) {
            if(newOrders == false) {
                localStorage.setItem('activeTabReturns', $(event.target).attr('href'));
                tableInfoReturns('pending');
            }
            // newOrders = true;
        });
        $('.nav-tabs a[href="#tab-21"]').on('show.bs.tab', function(event) {
            if(newOrders == false) {
                localStorage.setItem('activeTabReturns', $(event.target).attr('href'));
                tableInfoReturns('received');
            }
            // newOrders = true;
        });
        $('.nav-tabs a[href="#tab-22"]').on('show.bs.tab', function(event) {
            if(newOrders == false) {
                localStorage.setItem('activeTabReturns', $(event.target).attr('href'));
                tableInfoReturns('archived');
                // newOrders = true;
            }
        });
        var activeTab = localStorage.getItem('activeTabReturns');
        if(activeTab){
            $('.nav-tabs a[href="' + activeTab + '"]').tab('show');
            console.log(activeTab);
        } else {
            $('.nav-tabs a[href="#tab-20"]').tab('show');
        }
        <? if(!in_array("returnOverview", $this->session->tour)) { ?>
        var enjoyhint_instance = new EnjoyHint({
            onEnd: function () {
                markTourDone("returnOverview");
            },
            onSkip: function () {
                markTourDone("returnOverview");
            }
        });
        var enjoyhint_script_steps = [
            {
                'next .tourTitle': "Welcome to the returns manager.",
                'skipButton': {className: "mySkip", text: "End tour"}
            },
            {
                'next .tourReturns': "All of your returns can be found here.",
                'skipButton': {className: "mySkip", text: "End tour"}
            },
            {
                'next .tourNewOrder': "Use this to create a return label for an order that has been imported.",
                'skipButton': {className: "mySkip", text: "End tour"}
            },
            {
                'next .tourGeneral': 'Use this to crate a return label for an order that has not been imported.',
                'nextButton': {className: "myNext", text: "Got it!"},
                'showSkip': false
            }
        ];
        enjoyhint_instance.set(enjoyhint_script_steps);
        enjoyhint_instance.run();
        <?}?>
    });
</script>
<!-- Mainly scripts -->
<script src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"></script>
<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/jeditable/jquery.jeditable.js"></script>

<script src="<?php echo base_url() ?>assets/js/plugins/dataTables/datatables.min.js"></script>

<!-- Sweet alert -->
<script src="<?php echo base_url() ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>

<!-- EnjoyHint -->
<script src="<?php echo base_url() ?>assets/js/plugins/enjoyhint/enjoyhint.min.js"></script>

<!-- Ladda -->
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.jquery.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo base_url() ?>assets/js/inspinia.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/pace/pace.min.js"></script>

</body>

</html>







