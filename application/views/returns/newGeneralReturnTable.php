<?php
/**
 * Created by PhpStorm.
 * User: rapidware
 * Date: 9/3/18
 * Time: 11:07 AM
 */?>

<h1>Returns Manager</h1>
<div class="row">
    <form method="post" id="generalLabelForm" name="generalLabelForm" action="viewLabelConfirmation">
        <input id="labelCost" name="labelCost" type="hidden" value=""/>
        <input id="trackingNumber" name="trackingNumber" type="hidden" value=""/>
        <input id="labelLink" name="labelLink" type="hidden" value=""/>
    </form>
    <form class="form-horizontal" id="shipReturnForm" name="shipReturnForm" method="post">
        <div class="col-lg-9">
            <div class="ibox float-e-margins">
                <div class="ibox-content img-rounded">
                    <input type="hidden" id="location" name="location" value="domestic"/>
                    <input type="hidden" id="returnName" name="returnName" value="<? echo $companyInfo->name?>"/>
                    <input type="hidden" id="returnAddress1" name="returnAddress1" value="<?php echo $returnAddress->address1;?>"/>
                    <input type="hidden" id="returnAddress2" name="returnAddress2" value="<?php echo $returnAddress->address2; ?>"/>
                    <input type="hidden" id="returnCity" name="returnCity" value="<?php echo $returnAddress->city; ?>"/>
                    <input type="hidden" id="returnState" name="returnState" value="<?php echo $returnAddress->stateCode; ?>"/>
                    <input type="hidden" id="returnZip" name="returnZip" value="<?php echo $returnAddress->zip; ?>"/>
                    <input type="hidden" id="returnCountry" name="returnCountry" value="<?php echo $returnAddress->countryCode; ?>"/>
                    <input type="hidden" id="returnNumber" name="returnNumber" value="<?php echo $returnAddress->phone; ?>"/>
                    <input type="hidden" id="confirmation" name="confirmation" value="none"/>
                    <input type="hidden" id="packaging" name="packaging" value="package"/>
                    <input type="hidden" name="carrierId" id="carrierId" value="">
                    <input type="hidden" name="serviceCode" id="serviceCode" value="">
                    <input type="hidden" name="carrierName" id="carrierName" value="">
                    <h1 id="title">New Return</h1>
                    <div class="row">
                        <hr>
                        <h3 class="title">Customer Info</h3>
                        <div id="packageInfo" class="col-lg-8 col-lg-offset-2">
                            <div class="form-group">
                                <label autocomplete="false" class="col-lg-2 control-label">First Name</label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control" id="firstName" name="firstName" value="">
                                </div>
                                <label autocomplete="false" class="col-lg-2 control-label">Last Name</label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control" id="lastName" name="lastName" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label autocomplete="false" class="col-lg-2 control-label">Company Name</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" id="companyName" name="companyName" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label autocomplete="false" class="col-lg-2 control-label">Address</label>
                                <div class="col-lg-7">
                                    <input type="text" class="form-control" id="address1" name="address1" value="">
                                </div>
                                <div class="col-lg-3">
                                    <input type="text" class="form-control" id="address2" name="address2" value="">
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-md-2 control-label">City</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" id="city" name="city" value="">
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-md-2 control-label">State</label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" id="state" name="state" value="">
                                </div>
                                <label class="col-md-1 control-label">Zip</label>
                                <div class="col-md-2">
                                    <input type="text" class="form-control" id="zip" name="zip" value="">
                                </div>
                                <label class="col-md-1 control-label">Country</label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" id="country" name="country" value="">
                                </div>
                            </div>
                            <div class="row form-group">
                                <label  class="col-md-2 control-label">Phone Number</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" id="phone" name="phone" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <div id="actionButtons" class="col-lg-2 col-lg-offset-5">
                                    <button class="bottomBtn btn btn-primary" name="validateBtn" id="validateBtn" style="" onclick="validateAddress('#shipReturnForm'); return false;">Validate Address</button>
                                </div>
                            </div>
                            <div class="row col-lg-2 col-lg-offset-5">
                                <div id="addressVerify" style="text-align: center; font-weight: bold; font-size: 15px"></div>
                                <!--                                <span style="text-align: center" id="addressVerify">Verifing</span>-->
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <hr>
                        <h3 class="title">Order Info</h3>
                        <div id="packageInfo" class="col-lg-8 col-lg-offset-2">
                            <div class="form-group">
                                <label autocomplete="false" class="col-lg-2 control-label">Order Number</label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control" id="orderNumber" name="orderNumber" placeholder="optional" value="">
                                </div>
                                <label autocomplete="false" class="col-lg-2 control-label">Store Name</label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control" id="storeName" name="storeName" placeholder="optional" value="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <hr>
                        <h3 class="title">Package and Weight</h3>
                        <div id="packageInfo" class="col-lg-12">
                            <!--                            <div class="row form-group">-->
                            <!--                                <label class="col-sm-2 col-md-2 col-lg-2 control-label">Package Type</label>-->
                            <!--                                <div class="col-sm-9 col-md-9 col-lg-9">-->
                            <!--                                    <select class="form-control selectpicker" name="packageType" id="packageType">-->
                            <!--                                        <option selected disabled="" value="">Choose Box</option>-->
                            <!--                                        --><?php //foreach (json_decode($companyInfo[0]->get("boxes")) as $box => $dims) { ?>
                            <!--                                            <option --><?// if($box == "Default Box") {?><!-- selected --><?// }?><!-- value="--><?php //echo $box?><!--">--><?php //echo $box?><!--</option>-->
                            <!--                                        --><?php //} ?>
                            <!--                                        <option value="other">Other</option>-->
                            <!--                                    </select>-->
                            <!--                                </div>-->
                            <!--                            </div>-->
                            <div class="row form-group">
                                <label class="col-md-2 control-label">Dimensions<? if($this->session->unitSystem == "imperial") { echo " (in)"; } else { echo " (cm)";}?></label>
                                <div class="col-lg-3">
                                    <div class="input-group">
                                        <span class="input-group-addon">Length</span><input type="number" class="form-control" id="length" name="length" value="">
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="input-group">
                                        <span class="input-group-addon">Width</span> <input type="number" class="form-control" id="width" name="width" value="">
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="input-group">
                                        <span class="input-group-addon">Height</span> <input type="number" class="form-control" id="height" name="height" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-md-2 control-label">Weight</label>
                                <div class="col-md-6 col-sm-6">
                                    <input type="number" onchange="hideShippingMethod()" id="weight" name="weight" class="form-control" value=""/>
                                </div>
                                <div class="col-md-3 col-sm-3">
                                    <select id="weightMeasurement" name="weightMeasurement" class="form-control minimal ui-select" onchange="hideShippingMethod()">
                                        <? if($this->session->unitSystem == "imperial") {?>
                                            <option <?php if($this->session->weightMeasurement == "oz") { ?> selected <?php } ?> value="oz">oz</option>
                                            <option <?php if($this->session->weightMeasurement == "lb") { ?> selected <?php } ?> value="lb">lb</option>
                                        <?} else {?>
                                            <option <?php if($this->session->weightMeasurement == "g") { ?> selected <?php } ?> value="g">g</option>
                                            <option <?php if($this->session->weightMeasurement == "kg") { ?> selected <?php } ?> value="kg">kg</option>
                                        <?}?>
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-md-2 col-lg-2 control-label">Reason for return</label>
                                <div class="col-lg-9 col-md-9">
                                    <textarea class="form-control" name="reason" rows="3" id="reason"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <hr>
                        <h3 class="title">Shipping Service</h3>
                        <div class="col-lg-12">
                            <div id="shippingMethodsButton" style="display: block" class="col-lg-2 col-md-2 col-lg-offset-5 col-md-offset-5">
                                <button class="bottomBtn btn btn-primary pull-right ladda-button" data-style="zoom-out" style="" onclick="getReturnRate('#shipReturnForm'); return false;">Get Shipping Methods</button>
                            </div>
                            <div id="shippingPackage">

                            </div>
                            <div id="shippingSelect" style="display: none" class="row form-group"><!-- Add boxed to class -->
                                <label style="font-size:14px" class="col-sm-2 col-md-2 col-lg-2 control-label">Ship Method</label>
                                <div class="col-sm-9 col-md-9 col-lg-9">
                                    <select class="form-control m-b" name="shippingType" id="shippingType" onchange="updateShippingCost()">
                                        <option selected disabled>Select shipping type</option>
                                        <!--                                        <option --><?// if($shippingLines["code"] == "FirstPackage") { ?><!-- selected --><?php //}?><!-- value="usps_first_class_mail">USPS First Class Mail</option>-->
                                        <!--                                        <option --><?// if($shippingLines["code"] == "Priority") { ?><!-- selected --><?php //}?><!-- value="usps_priority_mail">USPS Priority Mail</option>-->
                                        <!--                                        <option --><?// if($shippingLines["code"] == "PriorityExpress") { ?><!-- selected --><?php //}?><!--  value="usps_priority_mail_express">USPS Priority Mail Express</option>-->
                                        <!--                                        <option --><?// if($shippingLines["code"] == "FirstClassPackageInternationalService") { ?><!-- selected --><?php //}?><!--  value="usps_first_class_package_international">USPS First Class Mail Intl</option>-->
                                        <!--                                        <option --><?// if($shippingLines["code"] == "PriorityMailInternational") { ?><!-- selected --><?php //}?><!--  value="usps_priority_mail_international">USPS Priority Mail Intl</option>-->
                                        <!--                                        <option --><?// if($shippingLines["code"] == "PriorityMailExpressInternational") { ?><!-- selected --><?php //}?><!--  value="usps_priority_mail_express_international">USPS Priority Mail Express Intl</option>-->
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 pull-right">
            <div class="ibox float-e-margins">
                <div class="ibox-content img-rounded">
                    <h3>Shipping Date</h3>
                    <div class="input-group date col-lg-10" id="datepicker">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" id="dateShipped" name="dateShipped" value="<? echo date("m/d/Y")?>">
                    </div>
                    <span>The date you are going to send the<br>return label to customer.</span>
                </div>
            </div>
        </div>
        <div class="col-lg-3 pull-right">
            <div class="ibox float-e-margins">
                <div class="ibox-content img-rounded">
                    <h3>Return to</h3>
                    <span class="customerName secondaryText"><?php echo $companyInfo->name?><br></span>
                    <span class="secondaryText"><?php echo $returnAddress->address1;?><br></span>
                    <?php if ($returnAddress->address2 != "") { ?> <span class="secondaryText"> <?php echo $returnAddress->address2; ?> </span><br> <?php }?>
                    <span class="secondaryText"><?php echo $returnAddress->city." ".$returnAddress->state." ".$returnAddress->zip;?><br></span>
                    <span class="secondaryText"><?php echo $returnAddress->country;?></span><br>
                    <span class="secondaryText"><?php echo $returnAddress->phone;?></span>
                </div>
            </div>
        </div>
        <div class="col-lg-3 pull-right">
            <div class="ibox float-e-margins">
                <div class="ibox-content img-rounded">
                    <h3 style="padding-bottom: 10px">Summary</h3>
                    <!--                <span class="customerName secondaryText">Label is ready<br></span>-->
                    <!--                    <h4  class="customerName secondaryText">Customer Paid <span id="customerPaid" name="customerPaid" class="pull-right">$--><?php //echo $shippingLines[0]->price;?><!--</span><br></h4>-->
                    <h4  class="customerName secondaryText">Your Cost <span id="rate" name="rate" class="pull-right"></span> <br></h4>
                    <hr>
                    <button class="btn btn-primary block full-width ladda-button" name="downloadBtn" disabled="" id="downloadBtn" data-style="zoom-out" style="" onclick="returnLabel('#shipReturnForm','generalLabelForm'); return false;">Select shipping method</button>
                    <p class="secondaryText" style="padding-top: 10px">Buying return label will download<br>the return label.</p>
                </div>
            </div>
        </div>
    </form>
</div>
<script>
    $( document ).ready(function() {
        $('#datepicker').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true

        });
        <? if($this->session->labelsRemaining < 2) {?>
        swal({
                title: "Your labels remaining are low",
                text: "Your remaining label count is "+<? echo $this->session->labelsRemaining?>,
                showCancelButton: true,
                confirmButtonColor: "#0d8ddb",
                confirmButtonText: "Purchase Labels",
                cancelButtonText: "Ok",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function (isConfirm) {
                if(isConfirm) {
                    window.location = base_url+"settings/buyLabels"
                }
            });
        <?}?>
    });
</script>



