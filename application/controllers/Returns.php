<?php
/**
 * Created by PhpStorm.
 * User: rapidware
 * Date: 9/1/18
 * Time: 3:08 PM
 */

include(APPPATH.'libraries/allLibraries.php');

defined('BASEPATH') OR exit('No direct script access allowed');

// Add the "use" declarations where you'll be using the classes
use Parse\ParseClient;
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use ZfrShopify\OAuth\TokenExchanger;
use ZfrShopify\OAuth\AuthorizationRedirectResponse;
use ZfrShopify\Exception\InvalidApplicationProxyRequestException;
use ZfrShopify\Validator\ApplicationProxyRequestValidator;
use ZfrShopify\Exception\InvalidWebhookException;
use ZfrShopify\Validator\WebhookValidator;
use ZfrShopify\ShopifyClient;
use Mailgun\Mailgun;

if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Returns extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        include(APPPATH . 'libraries/keys.php');
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model("parse_model", "parse");
        $this->load->model("functions");
        $this->load->model("mail_gun_model", "mailGun");
        $this->load->model("ship_engine_model", 'shipEngine');
        $this->load->helper('download');
        $this->load->model("dashboard_model", "dashboard");
        $this->load->model("shopify_model");
        $this->load->helper("shopify");

        if ($this->session->userId == '') {
            redirect($this->config->item('base_url'), 'Location');
        }
    }

    function index() {
        if(getTrialStatus() == "expired") {
            $this->data["shopifyStore"] = $this->session->shopifyStore;
            $this->data["accessToken"] = $this->session->shopifyToken;
            $this->data["menu"] = "settings";
            $this->load->view("navigation/header", $this->data);
            $this->load->view("register/shopify/changePlan", $this->data);
        } else {
            $this->data["menu"] = "returnsManager";
            $this->load->view("navigation/header", $this->data);
            $this->load->view("returns/dashboard", $this->data);
            $this->load->view("returns/returnsModals", $this->data);
        }
    }

    function returnsMetrics() {
        $tab = $this->uri->segment(3);
        if($tab == "pending") {
            $objectArray = [
                "status" => null,
                "companyId" => $this->session->companyId
            ];
            $returns = $this->parse->getParseClassArray("Returns", $objectArray);
        } else if($tab == "received") {
            $objectArray = [
                "status" => "received",
                "companyId" => $this->session->companyId
            ];
            $returns = $this->parse->getParseClassArray("Returns", $objectArray);
        } else if($tab == "archived") {
            $objectArray = [
                "status" => "archived",
                "companyId" => $this->session->companyId
            ];
            $returns = $this->parse->getParseClassArray("Returns", $objectArray);
        }
        $this->data['results'] = $returns;
        return $this->load->view('returns/returnsTable', $this->data);
    }

    function newReturn() {
        if(getTrialStatus() == "expired") {
            $this->data["shopifyStore"] = $this->session->shopifyStore;
            $this->data["accessToken"] = $this->session->shopifyToken;
            $this->data["menu"] = "settings";
            $this->load->view("navigation/header", $this->data);
            $this->load->view("register/shopify/changePlan", $this->data);
        } else {
            $this->data["orderId"] = $this->input->get("orderId");
            $this->data["menu"] = "returnsManager";
            $this->load->view("navigation/header", $this->data);
            $this->load->view("returns/newReturn", $this->data);
        }
    }

    function getNewReturnTable() {
        $orderId = $this->input->post("orderId");
        $orderInfo = $this->parse->getParseClass("Orders", "objectId", $orderId);
        $companyInfo = $this->parse->getParseClass("Companies", "objectId", $this->session->companyId);
        $locationArray = ["companyId" => $this->session->companyId, "returnDefault" => "true"];
        $locationInfo = $this->parse->getParseClassArray("Locations", $locationArray);
        $this->data["companyInfo"] = $companyInfo[0];
        $this->data["customerAddress"] = json_decode($orderInfo[0]->get("shippingAddress"));
        $this->data["totalWeight"] = $orderInfo[0]->get("totalWeight");
        $this->data["orderInfo"] = $orderInfo[0];
        $this->data["returnAddress"] = $locationInfo[0];
        return $this->load->view("returns/newReturnTable", $this->data);
    }

    function newGeneralReturn() {
        $this->data["menu"] = "returnsManager";
        $this->load->view("navigation/header", $this->data);
        $this->load->view("returns/newGeneralReturn", $this->data);
    }

    function getNewGeneralReturnTable() {
        $companyInfo = $this->parse->getParseClass("Companies", "objectId", $this->session->companyId);
        $locationArray = ["companyId" => $this->session->companyId, "returnDefault" => "true"];
        $locationInfo = $this->parse->getParseClassArray("Locations", $locationArray);
        $this->data["returnAddress"] = $locationInfo[0];
        $this->data["companyInfo"] = $companyInfo[0];
        return $this->load->view("returns/newGeneralReturnTable", $this->data);
    }

    function getReturnLabel() {
        $this->shopify_model->resetRemainingLabels();
        if($this->session->labelsRemaining < 1) {
            $result["error"] = 2;
            $result["message"] = "You are out of labels. Please purchase more under settings account.";
        } else if($this->input->post("lbs") && $this->input->post("ounce") == "") {
            $result["error"] = 1;
            $result["message"] = "Please enter a weight";
        } else if ($this->input->post("length") == "" || $this->input->post("height") == "" || $this->input->post("width") == "") {
            $result["error"] = 1;
            $result["message"] = "Please enter package dimensions.";
        } else {
            $customsCountDesc = count($this->input->post("customsDescription"));
            $customsCountValue = count($this->input->post("customsValue"));
            $customsCountQty = count($this->input->post("customsQty"));
            $customsCountTariff = count($this->input->post("tariffCode"));
            $customsCountCOO = count($this->input->post("coo"));
            if($customsCountDesc != $customsCountCOO || $customsCountDesc != $customsCountTariff || $customsCountDesc != $customsCountQty || $customsCountDesc != $customsCountValue) {
                $result["error"] = 1;
                $result["message"] = "Please fill in all of the customs fields";
            } else if(getTrialStatus() == "expired") {
                $result["error"] = 1;
                $result["message"] = "Your trial has expired, please purchase a plan";
            } else {
                $labelData = $this->shipEngine->getReturnLabel($_POST);
                if (isset($labelData->errors)) {
                    $result['error'] = 1;
                    $result["message"] = $labelData->errors[0]->message;
                } else {
                    $labelsRemaining = $this->session->labelsRemaining;
                    $this->session->labelsRemaining = $labelsRemaining - 1;
                    $array = ["labelsRemaining" => $labelsRemaining - 1];
                    $this->parse->updateObject("Companies",$array, $this->session->companyId);
                    date_default_timezone_set($this->session->timeZone);
                    $date = new DateTime();
                    $dateFormat = $date->format('Y-m-d H:i:s');
                    $labelLink = $labelData->label_download->href;
                    $shipmentCost = $labelData->shipment_cost->amount;
                    $statusArray = [
                        "trackingNumbers" => $labelData->tracking_number,
                        "labelLink" => $labelLink,
                        "dateShipped" => $dateFormat,
                        "service" => $labelData->service_code,
                        "labelId" => $labelData->label_id,
                        "shipmentCost" => $shipmentCost,
                        "companyId" => $this->session->companyId,
                        "orderId" => $this->input->post("orderId"),
                        "reason" => $this->input->post("reason"),
                        "storeName" => $this->input->post("storeName"),
                        "orderNumber" => $this->input->post("orderNumber")
                    ];
                    $this->parse->newObject("Returns", $statusArray);
//                    if($this->input->post("emailAddress") != "") {
//                        $this->mailGun->sendLabel($this->input->post("emailAddress"), $labelLink);
//                    }
                    $result["success"] = 1;
                    $result["label"] = $labelLink;
                    $result["cost"] = $labelData->shipment_cost->amount;
                    $result["trackingNumber"] = $labelData->tracking_number;
                }
            }
        }
        echo json_encode($result);
    }

    function updateStatus() {
        $objectArray = [];
        if($this->input->post("status") == "receive") {
            $objectArray["status"] = "received";
        } else {
            $objectArray["status"] = "archived";
        }
        $updateStatus = $this->parse->updateObject("Returns", $objectArray, $this->input->post("orderId"));
        if($updateStatus == true) {
            $result["success"] = 1;
        } else {
            $result["error"] = 1;
            $result["message"] = $updateStatus;
        }

        echo json_encode($result);
    }

    function findReturnOrder() {
        $objectArray = [
            "storeName" => $this->input->post("storeName"),
            "orderNumber" => intval($this->input->post("orderNumber"))
        ];
        $order = $this->parse->getParseClassArray("Orders", $objectArray);
        if(count($order) > 0) {
            $result["success"] = 1;
            $result["orderId"] = $order[0]->getObjectId();
        } else {
            $result["error"] = 1;
            $result["message"] = "No order found";
        }
        echo json_encode($result);
    }

    function viewLabelConfirmation() {
        $this->data["link"] = $this->input->post("labelLink");
        $this->data["cost"] = $this->input->post("cost");
        $this->data["trackingNumber"] = $this->input->post("trackingNumber");
        $this->data["menu"] = "fulfillment";
        $this->load->view("navigation/header", $this->data);
        $this->load->view("returns/viewLabel", $this->data);
    }

    function getReturnRates() {
        $shippingRates = [];
        if(count($this->session->carriers) < 1) {
            $result["error"] = 3;
            $result["message"] = "Before you can create a label, you will need to set up a shipping service under settings.";
        } else if($this->input->post("weight") == "") {
            $result["error"] = 1;
            $result["message"] = "Please enter a weight";
        } else if ($this->input->post("length") == "" || $this->input->post("height") == "" || $this->input->post("width") == "") {
            $result["error"] = 1;
            $result["message"] = "Please enter package dimensions.";
        } else {
            $customsCountDesc = count($this->input->post("customsDescription"));
            $customsCountValue = count($this->input->post("customsValue"));
            $customsCountQty = count($this->input->post("customsQty"));
            //        $customsCountTariff = count($this->input->post("tariffCode"));
            $customsCountCOO = count($this->input->post("coo"));
            if ($customsCountDesc != $customsCountCOO || $customsCountDesc != $customsCountQty || $customsCountDesc != $customsCountValue) {
                $result["error"] = 1;
                $result["message"] = "Please fill in all of the customs fields";
            } else {
                $rateData = $this->shipEngine->getReturnRates($_POST);
                if (isset($rateData->errors)) {
                    $result["error"] = 1;
                    $result["message"] = $rateData->errors[0]->message;
                } else {
                    $result["success"] = 1;
                    $result["shippingRates"] = getShippingButtons($rateData, $this->input->post("packaging"), $this->input->post("packageCarrier"));
//                    $result["shippingRates"] = $this->getShippingButtons($rateData, $this->input->post("packaging"), $this->input->post("packageCarrier"));
                }
            }
        }
        echo json_encode($result);
    }
}
