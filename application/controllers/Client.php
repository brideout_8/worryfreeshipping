<?php

include(APPPATH.'libraries/allLibraries.php');

defined('BASEPATH') OR exit('No direct script access allowed');

// Add the "use" declarations where you'll be using the classes
use Parse\ParseClient;
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;

if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Client extends CI_Controller {

    function __construct() {
        parent::__construct();

        include(APPPATH.'libraries/keys.php');
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model("parse_model", "parse");
        $this->load->model("dashboard_model", "dashboard");
        $this->load->model("functions");

        if ($this->session->userId == '') {
            redirect($this->config->item('base_url'),'Location');
        }
    }

    function index() {
        $this->data["menu"] = "Dashboard";
        $this->load->view("navigation/header", $this->data);
        $this->load->view('client/dashboard');
    }

    function getMetrics() {
        $clientId = $this->session->userdata("login_id");
        $this->data["clientId"] = $clientId;
        $clientMetrics = $this->dashboard->getClientMetrics($clientId);
        if(count($clientMetrics) > 0) {
            $this->data['results'] = $clientMetrics[0];
        } else {
            $this->data["results"] = [];
        }
        $objectArray = [
            "clientId" => $clientId
        ];
        $royalties = $this->parse->getParseClassDsc("Royalties", $objectArray, "quarter");
        $royalitesPerQuarter = $this->dashboard->royaltiesPerQuarter($royalties);
        $dayArray = $this->dashboard->ordersPerDay($clientMetrics);
        $daysLeftInQuarter = $this->dashboard->daysLeftInQuarter();
        $this->data["dayArray"] = $dayArray;
        $this->data["royaltyQuarter"] = $royalitesPerQuarter[0];
        $this->data["royalty"] = $royalties;
        $this->data["yearNow"] = $royalitesPerQuarter[1];
        $this->data["yearLast"] = date('Y', $royalitesPerQuarter[2]);
        $this->data["totalRoyalty"] = round($clientMetrics[2],2);
        $this->data["daysLeftInQuarter"] = $daysLeftInQuarter;
        $this->data["totalRoyaltiesThisQuarter"] = $clientMetrics[3];
        return $this->load->view('client/metrics', $this->data);
    }

    function profile() {
        $this->data["menu"] = "profile";
        $this->load->view("navigation/header", $this->data);
        $this->load->view("client/profile", $this->data);
    }

    function getProfile() {
        $clientId = $this->session->userdata("userId");
        $this->data["results"] = $this->parse->getUserData($clientId);
        $this->data["clientId"] = $clientId;
        $this->data["documents"] = $this->parse->getParseClass("Documents", "clientId", $clientId);
        return $this->load->view("client/profileInfo", $this->data);
    }

    function updateProfile() {
        $error = false;
        foreach ($_POST as $key => $value) {
            if($key == "name" || $key == "email" || $key == "username") {
                if($value == "" || $value == " ") {
                    $error = true;
                }
            }
        }
        if($error) {
            $result["error"] = 1;
            $result["message"] = "Name, email address, and username are required.";
            echo json_encode($result);
        } else {
            $currentUser = ParseUser::getCurrentUser();
            $token = $currentUser->getSessionToken();
            $objectArray = [
                "name" => $this->input->post("firstName")." ".$this->input->post("lastName"),
                "firstName" => $this->input->post("firstName"),
                "lastName" => $this->input->post("lastName"),
                "email" => $this->input->post("email"),
                "username" => $this->input->post("username"),
                "phoneNumber" => $this->input->post("phoneNumber"),

            ];
//            if($this->input->post("password") != "") {
//                if($this->input->post("password") != " ") {
//                    $objectArray["password"] = $this->input->post("password");
//                }
//            }
            $update = $this->parse->updateUser($this->input->post("clientId"), $objectArray);
            if($update["result"] == "true") {
                try {
                    $user = ParseUser::become($token);
                    // The current user is now set to user.
                    $result["success"] = 1;
                    $result["message"] = "Profile Updated";
                } catch (ParseException $ex) {
                    // The token could not be validated.
                    $result["error"] = 1;
                    $result["message"] = $ex->getMessage();
                }
            } else {
                $result["error"] = 1;
                $result["message"] = $update["message"];
            }
            echo json_encode($result);
        }
    }
}