<?php
/**
 * Created by PhpStorm.
 * User: rapidware
 * Date: 10/8/18
 * Time: 11:24 AM
 */

include(APPPATH.'libraries/allLibraries.php');

defined('BASEPATH') OR exit('No direct script access allowed');

// Add the "use" declarations where you'll be using the classes
use Parse\ParseClient;
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use ZfrShopify\OAuth\TokenExchanger;
use ZfrShopify\OAuth\AuthorizationRedirectResponse;
use ZfrShopify\Exception\InvalidApplicationProxyRequestException;
use ZfrShopify\Validator\ApplicationProxyRequestValidator;
use ZfrShopify\Exception\InvalidWebhookException;
use ZfrShopify\Validator\WebhookValidator;
use ZfrShopify\ShopifyClient;
use Mailgun\Mailgun;

if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Support extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        include(APPPATH . 'libraries/keys.php');
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model("parse_model", "parse");
        $this->load->model("functions");
        $this->load->model("mail_gun_model", "mailGun");
        $this->load->model("ship_engine_model", 'shipEngine');
        $this->load->helper('download');
        $this->load->model("dashboard_model", "dashboard");
        $this->load->model("shopify_model");
        $this->load->helper("shopify");

        if ($this->session->userId == '') {
            redirect($this->config->item('base_url'), 'Location');
        }
    }

    function index() {
        $this->data["menu"] = "support";
        $this->load->view("navigation/header", $this->data);
        $this->load->view("support/supportCenter", $this->data);
    }

    function FAQ() {
        $this->data["menu"] = "faq";
        $this->load->view("navigation/header", $this->data);
        $this->load->view("support/faq", $this->data);
    }

    function supportTable() {
        $this->data["tickets"] = $this->parse->getParseClassArray("SupportTickets", ["companyId" => $this->session->companyId]);
        $this->load->view("support/supportTable", $this->data);
    }

    function newTicket() {
        $this->data["menu"] = "support";
        $this->load->view("navigation/header", $this->data);
        $this->load->view("support/newTicket", $this->data);
    }

    function createTicket() {
        $companyId = $this->session->companyId;
        $ticketArray = [
            "companyId" => $companyId,
            "name" => $this->input->post("name"),
            "userId" => $this->session->userId,
            "email" => $this->input->post("email"),
            "issue" => $this->input->post("issue"),
            "typeOfIssue" => $this->input->post("typeOfIssue"),
            "status" => "open"
        ];

        $createTicket = $this->parse->newObject("SupportTickets", $ticketArray);
        if(count($createTicket) > 1) {
            $result["success"] = 1;
            $this->mailGun->sendMeIssueEmail($this->input->post("email"), $createTicket[1], $this->input->post("issue"));
            $this->mailGun->sendUserIssueEmail($this->input->post("email"), $createTicket[1]);
        } else {
            $result["error"] = 1;
            $result["message"] = $createTicket[0];
        }
        echo json_encode($result);
    }
}
