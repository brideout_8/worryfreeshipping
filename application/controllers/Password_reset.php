<?php

include(APPPATH.'libraries/allLibraries.php');

defined('BASEPATH') OR exit('No direct script access allowed');

// Add the "use" declarations where you'll be using the classes
use Parse\ParseClient;
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
class Password_reset extends CI_Controller {

  function __construct()
	 {
			parent::__construct();
			
			include(APPPATH.'libraries/keys.php');
         $this->load->library('form_validation');
         $this->load->library('session');
	 }
   
	public function index() {
		$this->data['token']	= @$_GET['token'];
		$this->load->view('passwordReset/changepassword', $this->data);
	}

	function changepassword_validation() {

		if ($this->input->post("cpassword") == "" || $this->input->post("cpassword") == " ") {
			$result['error'] = 1;
			$result["message"] = "Please enter a new password";
		} else if($this->input->post("password") != $this->input->post("cpassword")) {
		    $result["error"] = 1;
		    $result["message"] = "Passwords do not match";
        } else {
            $reset_token = $this->input->post('reset_token');
            $query = new ParseQuery('_User');
            $query->equalTo("reset_token", $reset_token);
            try{
                $queryResults = $query->find();
                $object = $queryResults[0];
                $object->set("password", $this->input->post('password'));
                $object->save(true);
                $result['success'] = 1;
                $result['message'] = "Password successfully reset.";
            } catch (ParseException $ex) {
                $result['error'] = 1;
                $result["message"] = $ex->getMessage();
            }
		}
	  echo json_encode($result);
	}

    function success() {
        $this->load->view("passwordReset/successPassword");
    }
}
