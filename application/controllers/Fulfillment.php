<?php
/**
 * Created by PhpStorm.
 * User: rapidware
 * Date: 11/23/17
 * Time: 11:59 AM
 */

include(APPPATH.'libraries/allLibraries.php');

defined('BASEPATH') OR exit('No direct script access allowed');

// Add the "use" declarations where you'll be using the classes
use Parse\ParseClient;
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use ZfrShopify\OAuth\TokenExchanger;
use ZfrShopify\OAuth\AuthorizationRedirectResponse;
use ZfrShopify\Exception\InvalidApplicationProxyRequestException;
use ZfrShopify\Validator\ApplicationProxyRequestValidator;
use ZfrShopify\Exception\InvalidWebhookException;
use ZfrShopify\Validator\WebhookValidator;
use ZfrShopify\ShopifyClient;
use Mailgun\Mailgun;

if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Fulfillment extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        include(APPPATH . 'libraries/keys.php');
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model("parse_model", "parse");
        $this->load->model("functions");
        $this->load->model("mail_gun_model", "mailGun");
        $this->load->model("ship_engine_model", 'shipEngine');
        $this->load->helper('download');
        $this->load->model("dashboard_model", "dashboard");
        $this->load->helper('shopify');
        $this->load->model('shopify_model');

        if ($this->session->userId == '') {
            redirect($this->config->item('base_url'), 'Location');
        }
    }

    function index() {
        $this->data["menu"] = "fulfillment";
        $this->load->view("navigation/header", $this->data);
        $this->load->view("fulfillment/fulfillmentOverview", $this->data);
    }
    //Used when user clicks get orders on fulfillment overview
    function getOrders() {
        //Check count of orders
        if(getTrialStatus() == "expired") {
            $result["error"] = 2;
        } else {
            if ($this->session->subscriptionLevel == "0") {
                $ordersToFindLimit = $this->session->labelsRemaining;
            } else {
                $ordersToFindLimit = 249;
            }
            $saveOrders = $this->functions->updateServerWithOrders($ordersToFindLimit);
            if($this->session->subscriptionLevel == "0") {
                $this->session->labelsRemaining = $this->session->labelsRemaining - count($saveOrders);
                $this->parse->updateObject("Companies", $objectArray = ["labelsRemaining" => $this->session->labelsRemaining, "importedOrder" => "yes"], $this->session->companyId);
            } else {
                $this->parse->updateObject("Companies", $objectArray = ["importedOrder" => "yes"], $this->session->companyId);
            }
            $this->session->importedOrder = "yes";
            $result['success'] = 1;
            $result["orders"] = $saveOrders;
        }
        echo json_encode($result);
    }
    //Used when user adds a store
    function getStartingOrder() {
        $storeName = $this->input->post("storeName");
        $shopifyToken= $this->input->post("shopifyToken");
        $startingOrderNumber = $this->input->post("startingOrderNumber");
        //Check count of orders
        if (getTrialStatus() == "expired") {
            $result["error"] = 2;
        } else {
            $saveOrders = $this->functions->updateServerWithStartingOrder($storeName, $shopifyToken, $startingOrderNumber);
            if(count($saveOrders) > 0) {
                if ($this->session->subscriptionLevel == "0") {
                    $this->session->labelsRemaining = $this->session->labelsRemaining - count($saveOrders);
                    $this->parse->updateObject("Companies", $objectArray = ["labelsRemaining" => $this->session->labelsRemaining, "importedOrder" => "yes"], $this->session->companyId);
                } else {
                    $this->parse->updateObject("Companies", $objectArray = ["importedOrder" => "yes"], $this->session->companyId);
                }
                //Check to see if carrier is connected. If not it is a new user
                $carriers = $this->parse->getParseClass("Carriers", "companyId", $this->session->companyId);
                $newUser = "yes";
                foreach ($carriers as $carrier) {
                    if ($carrier->get("status") != "pending") {
                        $newUser = "no";
                    }
                }
                $result["newUser"] = $newUser;
                $this->session->importedOrder = "yes";
                $result['success'] = 1;
                $result["count"] = count($saveOrders);
                $result["orders"] = $saveOrders;
            } else {
                $result["error"] = 1;
                $result["message"] = "Order not found. Please try again";
            }
        }

        echo json_encode($result);
    }

    function importFirstOrder() {
        $this->data["shopifyToken"] = $this->session->shopifyToken;
        $this->data["storeName"] = $this->session->shopifyStore;
        $this->load->view("register/shopify/importFirstOrder", $this->data);
    }

    function fulfillmentMetrics() {
        $tab = $this->uri->segment(3);
        if($tab == "ordersToPull") {
            $objectArray = [
                "packed" => "2",
                "archived" => "2",
                "hold" => "2",
                "status" => "inactive"
            ];
            $orders = $this->parse->getParseClassArrayNotEqualOrders("Orders", $objectArray);
        } else if($tab == "ordersToShip") {
            $objectArray = [
                "shipped" => "2",
                "archived" => "2",
                "hold" => "2",
                "status" => "inactive"
            ];
            $orders = $this->parse->getParseClassArrayNotEqualOrders("Orders", $objectArray);
        } else if($tab == "ordersOnHold") {
            $objectArray = [
                "hold" => "2",
                "companyId" => $this->session->companyId,
                "status" => "active"
            ];
            $orders = $this->parse->getParseClassArray("Orders", $objectArray);
        } else if($tab == "allOrders") {
            $objectArray = [
                "companyId" => $this->session->companyId,
                "status" => "active"
            ];
            $orders = $this->parse->getParseClassArray("Orders", $objectArray);
        } else if($tab == "newOrders") {
            $objectArray = [
                "shipped" => null,
                "packed" => null,
                "archived" => null,
                "hold" => null,
                "companyId" => $this->session->companyId,
                "status" => "active"
            ];
            $orders = $this->parse->getParseClassArray("Orders", $objectArray);
        } else if($tab == "pendingOrders") {
            $objectArray = [
                "shipped" => "2",
                "archived" => "2",
                "status" => "inactive"
//                "companyId" => $this->session->companyId
            ];
//            $orders = $this->parse->getParseClassArray("Orders", $objectArray);
            $orders = $this->parse->getParseClassArrayNotEqualOrders("Orders", $objectArray);
        }
        $this->data['results'] = $orders;
        return $this->load->view('fulfillment/ordersTable', $this->data);
    }

    function quickOrderFind() {
        $objectArray = [
            "storeName" => $this->input->post("storeName"),
            "orderNumber" => intval($this->input->post("orderNumber"))
        ];
        $order = $this->parse->getParseClassArray("Orders", $objectArray);
        if(count($order) > 0) {
            $result["success"] = 1;
            $result["orderNumber"] = $order[0]->get("orderNumber");
            $result["storeName"] = $order[0]->get("storeName");
        } else {
            $result["error"] = 1;
            $result["message"] = "No order found";
        }
        echo json_encode($result);
    }

    //Used when clicking order from table
    function orderOverview() {
        $orderNumber = $this->uri->segment(4);
        $storeName = $this->uri->segment(3);
        $this->data["orderNumber"] = $orderNumber;
        $this->data["storeName"] = $storeName;
        $orderInfo = $this->parse->getOrderProducts($orderNumber, $storeName);
        $this->data["orderInfo"] = $orderInfo[2];
        $this->data["shippingInfo"] = json_decode($orderInfo[2]->get("shippingAddress"));
        $this->data["countryCodes"] = countryCodes();
        $this->data["menu"] = "Fulfillment";
        $this->load->view("navigation/header", $this->data);
        $this->load->view("fulfillment/orderOverview/orderOverview", $this->data);
    }

    function orderOverviewTable() {
        $orderNumber = $this->input->post("orderNumber");
        $storeName = $this->input->post("storeName");
        $orderInfo = $this->parse->getOrderProducts($orderNumber, $storeName);
        $productInfo = $this->functions->getImages($orderInfo[0], $storeName);
        $this->data["returns"] = $this->parse->getParseClassKey("Returns", "orderId", $orderInfo[1], "companyId", $this->session->companyId);
        $this->data["lineItems"] = $orderInfo[0];
        $this->data["orderInfo"] = $orderInfo[2];
        $this->data["shippingAddress"] = json_decode($orderInfo[2]->get("shippingAddress"));
        $this->data["orderNumber"] = $orderInfo[2]->get("orderNumber");
        $this->data["storeName"] = $storeName;
        $this->data["packedStatus"] = $orderInfo[2]->get("packed");
        $this->data["shippedStatus"] = $orderInfo[2]->get("shipped");
        $this->data["productIdsPacked"] = json_decode($orderInfo[2]->get("productIdsPacked"));
        $this->data["productIdsShipped"] = json_decode($orderInfo[2]->get("productIdsShipped"));
        $this->data["orderObjectId"] = $orderInfo[1];
        $this->data["note"] = $orderInfo[2]->get("note");
        $this->data["images"] = $productInfo[0];
        $this->data["holdReason"] = json_decode($orderInfo[2]->get("holdReason"));
        $this->data["companyInfo"] = $this->parse->getParseClass("Companies", "objectId", $this->session->companyId);
        $this->data["badge"] = "overview";
        $this->load->view("navigation/badges", $this->data);
        $this->load->view("fulfillment/orderOverview/orderOverviewTable", $this->data);
    }

    //Used when order has been marked pulled
    function orderOverviewFromPacked($orderNumber, $storeName) {
        $orderInfo = $this->parse->getOrderProducts($orderNumber, $storeName);
//        $productInfo = $this->functions->getImages($orderInfo[0], $storeName);

//        $this->data["lineItems"] = $orderInfo[0];
        $this->data["orderInfo"] = $orderInfo[2];
        $this->data["orderNumber"] = $orderInfo[2]->get("orderNumber");
//        $this->data["orderNameNumber"] = strtolower($orderNumber);
        $this->data["countryCodes"] = countryCodes();
        $this->data["storeName"] = $storeName;
//        $this->data["packedStatus"] = $orderInfo[2]->get("packed");
//        $this->data["shippedStatus"] = $orderInfo[2]->get("shipped");
//        $this->data["productIdsPacked"] = json_decode($orderInfo[2]->get("productIdsPacked"));
//        $this->data["productIdsShipped"] = json_decode($orderInfo[2]->get("productIdsShipped"));
        $this->data["shippingInfo"] = json_decode($orderInfo[2]->get("shippingAddress"));
//        $this->data["orderObjectId"] = $orderInfo[1];
//        $this->data["note"] = $orderInfo[2]->get("note");
//        $this->data["images"] = $productInfo[0];
//        $this->data["inventory"] = $productInfo[1];

        $this->data["menu"] = "Fulfillment";
        $this->load->view("navigation/header", $this->data);
        $this->load->view("fulfillment/orderOverview/orderOverview", $this->data);
    }

    function orderOverviewFromPartial() {
        $orderNumber = $this->input->get("orderStoreNumber");
        $storeName = $this->input->get("storeName");
        $this->orderOverviewFromPacked($orderNumber, $storeName);
    }

    function onHold() {
        $orderNumber = $this->input->post("orderNumber");
        $storeName = $this->input->post("storeName");
        $holdReason = $this->input->post("reason");
        $onHold = $this->input->post("hold");
        if($onHold == "2" && $holdReason == "") {
            $result["error"] = 1;
            $result["message"] = "Please enter a hold reason";
        } else {
            date_default_timezone_set($this->session->timeZone);
            $date = new DateTime();
            $dateFormat = $date->format('Y-m-d H:i:s');
            $holdArray = ["user" => $this->session->name, "note" => $holdReason, "time" => $dateFormat];
            $objectArray = [
                "hold" => $onHold,
                "holdReason" => json_encode($holdArray)
            ];
            $save = $this->parse->updateOrder($orderNumber, $storeName, $objectArray);
            if($save == true) {
                $result["success"] = 1;
            } else {
                $result["error"] = 1;
                $result["message"] = $save;
            }
        }
        echo json_encode($result);
    }

    function addInternalNote() {
        $orderNumber = $this->input->post("orderNumber");
        $storeName = $this->input->post("storeName");
        $note = $this->input->post("note");
        $objectArray = [
            "internalNote" => $note
        ];
        $save = $this->parse->updateOrder($orderNumber, $storeName, $objectArray);
        if($save == true) {
            $result["success"] = 1;
        } else {
            $result["error"] = 1;
            $result["message"] = $save;
        }
        echo json_encode($result);
    }

    function shipOrder() {
        $this->data["orderNumber"] = $this->input->get("orderNumber");
        $this->data["storeName"] = $this->input->get("storeName");
        $this->data["shipPack"] = "ship";
        $orderInfo = $this->parse->getOrderProducts($this->input->get("orderNumber"), $this->input->get("storeName"));
        $this->data["orderInfo"] = $orderInfo[2];
        $this->data["countryCodes"] = countryCodes();
        $this->data["shippingInfo"] = json_decode($orderInfo[2]->get("shippingAddress"));
        $this->data["menu"] = "Fulfillment";
        $this->load->view("navigation/header", $this->data);
        $this->load->view("fulfillment/ship/shipOrder", $this->data);
    }

    function packOrder() {
        $this->data["orderNumber"] = $this->input->get("orderNumber");
        $this->data["storeName"] = $this->input->get("storeName");
        $this->data["shipPack"] = "pack";
        $this->data["menu"] = "Fulfillment";
        $this->load->view("navigation/header", $this->data);
        $this->load->view("fulfillment/pack/scanProducts", $this->data);
    }

    //Used when selecting pull or ship on order overview page
    function orderActions() {
        $shipPack = $this->input->post("shipPack");
        $orderNumber = $this->input->post("orderNumber");
        $storeName = $this->input->post("storeName");
        $orderInfo = $this->parse->getOrderProducts($orderNumber, $storeName);
        $productInfo = $this->functions->getImages($orderInfo[0], $storeName);
        $productBarcodes = $productInfo[2];
        if($shipPack == "pack") {
            $this->data["lineItems"] = $orderInfo[0];
            $this->data["orderInfo"] = $orderInfo[2];
            $this->data["storeName"] = $storeName;
            $this->data["menu"] = "OrdersToPack";
            $this->data["orderNumber"] = $orderNumber;
            $this->data["packedStatus"] = $orderInfo[2]->get("packed");
            $this->data["productIdsPacked"] = json_decode($orderInfo[2]->get("productIdsPacked"));
            $this->data["orderObjectId"] = $orderInfo[1];
            $this->data["note"] = $orderInfo[2]->get("note");
            $this->data["images"] = $productInfo[0];
            $this->data["inventory"] = $productInfo[1];
            $this->data["barcodes"] = $productBarcodes;
            $this->data["badge"] = "shipPack";
            $this->load->view("navigation/badges", $this->data);
            $this->load->view("fulfillment/pack/scanProductsTable", $this->data);
        } else {
            $this->data["companyInfo"] = $this->parse->getParseClass("Companies", "objectId", $this->session->companyId);
            $this->data["packages"] = $this->parse->getParseClass("Packages", "companyId", $this->session->companyId);
            $this->data["lineItems"] = $orderInfo[0];
            $this->data["orderInfo"] = $orderInfo[2];
            $this->data["storeName"] = $storeName;
            $this->data["orderNumber"] = $orderNumber;
            $this->data["shippingInfo"] = json_decode($orderInfo[2]->get("shippingAddress"));
            $this->data["shippingLines"] = json_decode($orderInfo[2]->get("shippingLines"));
            $this->data["packedStatus"] = $orderInfo[2]->get("packed");
            $this->data["shippedStatus"] = $orderInfo[2]->get("shipped");
            $this->data["productIdsPacked"] = json_decode($orderInfo[2]->get("productIdsPacked"));
            $this->data["productIdsShipped"] = json_decode($orderInfo[2]->get("productIdsShipped"));
            $this->data["orderId"] = $orderInfo[2]->get("orderId");
            $this->data["note"] = $orderInfo[2]->get("note");
            $this->data["noteAttributes"] = $orderInfo[2]->get("noteAttributes");
            $this->data["menu"] = "OrdersToShip";
            $this->data["labelURL"] = $orderInfo[2]->get("labelLink");
            $this->data["totalWeight"] = $orderInfo[2]->get("totalWeight");
            $this->data["orderObjectId"] = $orderInfo[1];
            $this->data["images"] = $productInfo[0];
            $this->data["coo"] = $this->functions->coo();
            $this->data["badge"] = "shipPack";
            $this->data["locations"] = $this->parse->getParseClassArray("Locations", ["companyId" => $this->session->companyId, "shopifyName" => $storeName]);
            $this->load->view("navigation/badges", $this->data);
            $this->load->view("fulfillment/ship/shipOrderTable", $this->data);
        }
    }

    function invoicePrint() {
        $orderNumber = $this->input->get("orderNumber");
        $storeName = $this->input->get("storeName");
        $orderInfo = $this->parse->getOrderProducts($orderNumber, $storeName);
        $productInfo = $this->functions->getImages($orderInfo[0], $storeName);
        $companyInfo = $this->parse->getParseClass("Companies", "objectId", $this->session->companyId);
        $this->data["lineItems"] = $orderInfo[0];
        $this->data["orderInfo"] = $orderInfo[2];
        $this->data["orderNumber"] = $orderInfo[2]->get("orderNumber");
        $this->data["storeName"] = $storeName;
        $this->data["images"] = $productInfo[0];
        $this->data["logo"] = $companyInfo[0]->get("logo");
        $this->data["companyInfo"] = $companyInfo;
        $this->load->view("fulfillment/printPages/printOrder", $this->data);
    }

    function archiveOrder() {
        $storeName = $this->input->post("storeName");
        $orderObjectId = $this->input->post("orderObjectId");
        $orderId = $this->input->post("orderId");
        $index = array_search($storeName, $this->session->shopifyStores);
        $accessToken = $this->session->shopifyTokens[$index];

        $archive = $this->functions->archiveOrder($accessToken, $storeName, $orderId, $orderObjectId);
        //Archive order in shopify
        if(isset($archive->errors)) {
            $result["error"] = 1;
            $result["message"] = "Please try again. Tech Support Error: ".$archive->errors;
        } else {
            $result["success"] = 1;
        }
        echo json_encode($result);
    }

    function markedPacked() {
        $orderObjectId = $this->input->get("orderId");
        $orderNumber = $this->input->get("orderStoreNumber");
        $storeName = $this->input->get("storeName");
        $itemsPacked = $this->parse->getProductIdsPacked($orderObjectId);
        $statusArray = [
            "packed" => "2",
            "productIdsPacked" => $itemsPacked[1],
            "skusPacked" => $itemsPacked[0]
        ];
        $this->parse->updateStatus($orderObjectId, $statusArray);
        $this->orderOverviewFromPacked($orderNumber, $storeName);
    }

    function markedPartial() {
        $productIdsPacked = array();
        $skusPacked = array();
        foreach ($_POST as $key => $value) {
//            array_push($productIdsPacked, $value);
            $productIdsPacked[$key] = $value;
            array_push($skusPacked, strval($key));
        }
        $objectArray = [
            "packed" => "1",
            "productIdsPacked" => $productIdsPacked,
            "skusPacked" => $skusPacked
        ];
        $this->parse->updateStatus($this->uri->segment(3), $objectArray);
        $results["success"] = $productIdsPacked;
        echo json_encode($results);
    }

    function getPackage() {
        $packageId = $this->input->post("packageId");
        $nonOrder = $this->input->post("nonOrder");
        $shipmentWeightMeasurement = $this->input->post("shipmentWeightMeasurement");
        $boxes = $this->parse->getParseClass("Packages", "objectId", $packageId);
        if (count($boxes) > 0) {
            foreach ($boxes as $box) {
//                $weightUnits = $box->get("weightUnits");
                $sizeUnits = $box->get("sizeUnits");
                if ($shipmentWeightMeasurement == "oz") {
                    $unit = 28.34952;
                } else if ($shipmentWeightMeasurement == "lb") {
                    $unit = 453.59237;
                } else if ($shipmentWeightMeasurement == "kg") {
                    $unit = 1000;
                } else {
                    $unit = 1;
                }
                if ($sizeUnits == "in") {
                    $size = 2.54;
                } else {
                    $size = 1;
                }
                //Convert shipment weight into grams to add box weight
                if($nonOrder == "false") {
                    $shipmentWeightInGrams = $this->input->post("shipmentWeight") * $unit;
                    $totalWeightInGrams = $box->get("weight") + $shipmentWeightInGrams;
                } else {
                    //Not an order from database
                    $totalWeightInGrams = $box->get("weight");
                }
                $result["weight"] = round($totalWeightInGrams / $unit, 2);
                $result["test"] = $this->input->post("shipmentWeight");
                $result["success"] = 1;
                $result["length"] = round($box->get("length") / $size, 2);
                $result["width"] = round($box->get("width") / $size, 2);
                $result["height"] = round($box->get("height") / $size, 2);
                $result["packageType"] = $box->get("packageType");
                $result["packageCarrier"] = $box->get("carrierType");
            }
        } else {
            $result['error'] = 1;
        }
//        $noBox = ["0/0/0"];
        echo json_encode($result);
    }

    function getProvince() {
        $country = $this->input->post("country");
        if($country == "United States") {
            $result["provinceCodes"] = usStateCodes();
        } else if($country == "Canada") {
            $result["provinceCodes"] = canadaProvinceCodes();
        } else if($country == "Australia") {
            $result["provinceCodes"] = australiaProvinceCodes();
        } else {
            $result["provinceCodes"] = unitedKingdomProvinceCodes();
        }

        echo json_encode($result);
    }

    function getLabel() {
        //Check billing period
        $this->shopify_model->resetRemainingLabels();
        $anyShipping = false;
        $negativeShipping = false;
        foreach ($this->input->post("shipping") as $qty) {
            if($qty > 0) {
                $anyShipping = true;
            }
            if($qty < 0) {
                $negativeShipping = true;
            }
        }
        if($this->session->labelsRemaining < 1) {
            $result["error"] = 2;
            $result["message"] = "You are out of labels. Please purchase more under settings - account.";
        } else if($this->input->post("weight") == "") {
            $result["error"] = 1;
            $result["message"] = "Please enter a weight";
        } else if ($this->input->post("length") == "" && $this->input->post("packageCarrier") == "none") {
            $result["error"] = 1;
            $result["message"] = "Please enter package dimensions.";
        } else if($this->input->post("height") == "" && $this->input->post("packageCarrier") == "none") {
            $result["error"] = 1;
            $result["message"] = "Please enter package dimensions.";
        } else if($this->input->post("width") == "" && $this->input->post("packageCarrier") == "none") {
            $result["error"] = 1;
            $result["message"] = "Please enter package dimensions.";
        } else if ($this->input->post("serviceCode") == null) {
            $result["error"] = 1;
            $result["message"] = "Please select a shipping method";
        } else if($anyShipping == false) {
            $result["error"] = 1;
            $result["message"] = "All shipping quantities are 0. Please enter a quantity.";
        } else if($negativeShipping) {
            $result["error"] = 1;
            $result["message"] = "No shipping quantities can be negative.";
        } else {
            $customsCountDesc = count($this->input->post("customsDescription"));
            $customsCountValue = count($this->input->post("customsValue"));
            $customsCountQty = count($this->input->post("customsQty"));
//            $customsCountTariff = count($this->input->post("tariffCode"));
            $customsCountCOO = count($this->input->post("coo"));
            if($customsCountDesc != $customsCountCOO || $customsCountDesc != $customsCountQty || $customsCountDesc != $customsCountValue) {
                $result["error"] = 1;
                $result["message"] = "Please fill in all of the customs fields";
            } else {
                $labelData = $this->shipEngine->getLabel($_POST);
                if (isset($labelData->errors)) {
                    $result['error'] = 1;
                    $result["message"] = $labelData->errors[0]->message;
                } else {
                    //Increment order count in database and session data if not in trial.
                    if($this->session->subscriptionLevel != "0") {
                        $this->session->labelsRemaining = $this->session->labelsRemaining - 1;
                        $companyArray = ["labelsRemaining" => $this->session->labelsRemaining];
                        $result["updateComp"] = $this->parse->updateObject("Companies",$companyArray, $this->session->companyId);
                    }
//                    $previousIdsShippedQuery = $this->parse->getParseClass("Orders", "objectId", $this->input->post("orderObjectId"));
//                    $previousIdsShipped = json_decode($previousIdsShippedQuery[0]->get("productIdsShipped"));
                    //Set the object for products shipped
                    $productIdsShippedToday = [];
                    $allProductIdsShipped = [];
                    $productIds = $this->input->post("productId");
                    $productSkus = $this->input->post("productSku");
                    $qtyNeeded = $this->input->post("qtyNeeded");
                    $productShippingQty = $this->input->post("shipping");
                    $qtyShipped = $this->input->post("qtyShipped");
                    $partialShipping = false; //If this one shipment is not shipping in full. Used for shopify fulfillment
                    $partiallyFulfilled = false; //If the whole order has been shipped
                    for($x=0;$x<count($productSkus);$x++) {
                        $productIdsArrayShippedToday = [];
                        $allProductIdsAlreadyShipped = [];
                        for($z=0;$z<$productShippingQty[$x];$z++) {
                            array_push($productIdsArrayShippedToday, $productIds[$x]);
                            array_push($allProductIdsAlreadyShipped, $productIds[$x]);
                        }
                        //This is used to save previous shipped items
                        for($y=0;$y<$qtyShipped[$x];$y++) {
                            array_push($allProductIdsAlreadyShipped, $productIds[$x]);
                        }
                        //Check if shipping qty is less than qty needed
                        if($productShippingQty[$x] < $qtyNeeded[$x]) {
                            $partialShipping = true;
                            //Check if shipped qty is less than qty needed
                            if($qtyShipped[$x] < $qtyNeeded[$x]) {
                                $partiallyFulfilled = true;
                            }
                        }
                        if(count($productIdsArrayShippedToday) > 0) {
                            $productIdsShippedToday[$productSkus[$x]] = $productIdsArrayShippedToday;
                        }
                        if(count($allProductIdsAlreadyShipped) > 0) {
                            $allProductIdsShipped[$productSkus[$x]] = $allProductIdsAlreadyShipped;
                        }
                    }
                    date_default_timezone_set($this->session->timeZone);
                    $date = new DateTime();
                    $dateFormat = $date->format('Y-m-d H:i:s');
                    $labelLink = $labelData->label_download->href;
                    $labelDataArray = [
                        "id" => $labelData->label_id,
                        "trackingNumber" => $labelData->tracking_number,
                        "service" => $labelData->service_code,
                        "shippingCost" => $labelData->shipment_cost->amount,
                        "labelLink" => $labelLink,
                        "dateShipped" => $dateFormat
                    ];
                    $statusArray = [
                        "labelData" => $labelDataArray,
                        "skusShipped" => [],
                        "productIdsShipped" => $allProductIdsShipped,
                        "shippingCost" => $labelData->shipment_cost->amount
                    ];
                    if($partiallyFulfilled) {
                        $statusArray["shipped"] = "1";
                    } else {
                        $statusArray["shipped"] = "2";
                    }
                    //Carrier Name
                    switch ($this->input->post("carrierName")) {
                        case "ups":
                            $carrierName = "UPS";
                            break;
                        case "fedex":
                            $carrierName = "FedEx";
                            break;
                        case "usps":
                            $carrierName = "USPS";
                            break;
                        case "stamps_com":
                            $carrierName = "USPS";
                            break;
                        case "endicia":
                            $carrierName = "USPS";
                            break;
                        default :
                            $carrierName = "USPS";
                    }
                    $this->parse->updateStatus($this->input->post("orderObjectId"), $statusArray);
                    $storeName = $this->input->post("storeName");
                    $accessToken = $this->session->shopifyStoresInfo[$storeName]["shopifyToken"];
                    $updateShopify = $this->functions->updateFulfillment($this->input->post("orderId"), $labelData->tracking_number, $partialShipping, $productIdsShippedToday, $this->input->post("warehouse"), $accessToken, $storeName, $carrierName);
                    $result["success"] = 1;
                    $result["shopify"] = $updateShopify;
                    $result["label"] = $labelLink;
                    $result["cost"] = $labelData->shipment_cost->amount;
                    $result["trackingNumber"] = $labelData->tracking_number;
                }
            }
        }
        echo json_encode($result);
    }

    function viewLabelConfirmation() {
        $this->data["link"] = $this->input->post("labelLink");
        $this->data["cost"] = $this->input->post("cost");
        $this->data["trackingNumber"] = $this->input->post("trackingNumber");
        $this->data["menu"] = "fulfillment";
        $this->load->view("navigation/header", $this->data);
        $this->load->view("fulfillment/ship/viewLabel", $this->data);
    }

    function shipNonOrder() {
        if($this->session->subscriptionLevel == "1") {
            //Not in subscription
            $this->data["shopifyStore"] = $this->session->shopifyStore;
            $this->data["accessToken"] = $this->session->shopifyToken;
            $this->data["menu"] = "settings";
            $this->load->view("navigation/header", $this->data);
            $this->load->view("register/shopify/changePlan", $this->data);
        } else {
            $this->data["menu"] = "shipNonOrder";
            $this->load->view("navigation/header", $this->data);
            $this->load->view("shipNonOrder/shipNonOrder", $this->data);
        }
    }

    function getNonOrderTable() {
        $this->data["packages"] = $this->parse->getParseClass("Packages", "companyId", $this->session->companyId);
        $this->data["companyInfo"] = $this->parse->getParseClass("Companies", "objectId", $this->session->companyId);
        $this->data["locations"] = $this->parse->getParseClass("Locations", "companyId", $this->session->companyId);
        return $this->load->view("shipNonOrder/shipNonOrderTable", $this->data);
    }

    function getNonOrderLabel() {
        $this->shopify_model->resetRemainingLabels();
        if($this->session->labelsRemaining < 1) {
            $result["error"] = 2;
            $result["message"] = "You are out of labels. Please purchase more under settings account.";
        } else if($this->input->post("weight") == "") {
            $result["error"] = 1;
            $result["message"] = "Please enter a weight";
        } else if ($this->input->post("length") == "" && $this->input->post("package") == "none" || $this->input->post("height") == "" && $this->input->post("package") == "none" || $this->input->post("width") == "" && $this->input->post("package") == "none") {
            $result["error"] = 1;
            $result["message"] = "Please enter package dimensions.";
        } else if ($this->input->post("serviceCode") == null) {
            $result["error"] = 1;
            $result["message"] = "Please select a shipping method";
        } else {
            $customsCountDesc = count($this->input->post("customsDescription"));
            $customsCountValue = count($this->input->post("customsValue"));
            $customsCountQty = count($this->input->post("customsQty"));
//            $customsCountTariff = count($this->input->post("tariffCode"));
            $customsCountCOO = count($this->input->post("coo"));
            if($customsCountDesc != $customsCountCOO || $customsCountDesc != $customsCountQty || $customsCountDesc != $customsCountValue) {
                $result["error"] = 1;
                $result["message"] = "Please fill in all of the customs fields";
            } else if(getTrialStatus() == "expired") {
                $result["error"] = 1;
                $result["message"] = "Your trial has expired, please purchase a plan";
            } else {
                $labelData = $this->shipEngine->getLabel($_POST);
                if (isset($labelData->errors)) {
                    $result['error'] = 1;
                    $result["message"] = $labelData->errors[0]->message;
                } else {
                    $labelsRemaining = $this->session->labelsRemaining;
                    $this->session->labelsRemaining = $labelsRemaining - 1;
                    $array = ["labelsRemaining" => $labelsRemaining - 1];
                    $this->parse->updateObject("Companies",$array, $this->session->companyId);
                    date_default_timezone_set($this->session->timeZone);
                    $date = new DateTime();
                    $dateFormat = $date->format('Y-m-d H:i:s');
                    $labelLink = $labelData->label_download->href;
                    $statusArray = [
                        "trackingNumber" => $labelData->tracking_number,
                        "labelLinks" => $labelLink,
                        "dateShipped" => $dateFormat,
                        "service" => $labelData->service_code,
                        "shippingCost" => $labelData->shipment_cost->amount,
                        "labelId" => $labelData->label_id,
                        "companyId" => $this->session->companyId
                    ];
                    $this->parse->newObject("NonLabels", $statusArray);
                    $result["success"] = 1;
                    $result["label"] = $labelLink;
                }
            }
        }
        echo json_encode($result);
    }

    function returnLabels() {
        $objectArray = [
            "role" => "employee"
        ];
        $this->data["menu"] = "returnLabel";
        $this->data["labels"] = $this->parse->getAllParseClass("Labels");
        $this->load->view("navigation/header", $this->data);
        $this->load->view("employee/returnLabels", $this->data);
    }

    function returnLabel() {
        $this->data["menu"] = "returnLabel";
        $this->load->view("navigation/header", $this->data);
        $this->load->view("employee/returnLabel", $this->data);
    }

    function voidLabel() {
        $void = $this->shipEngine->voidLabel($this->input->post("labelId"), $this->session->shipEngineAPIKey);
        if(isset($void->errors)) {
            $result['error'] = 1;
            $result["message"] = $void->errors[0]->message;
        } else {
            if($void->approved == true) {
                //Updated server
                $this->parse->updateLabelData($this->input->post("labelId"),$this->input->post("orderObjectId"));
                $result["success"] = 1;
                $result["status"] = $void[0]->message;
            } else {
                $result["denied"] = 1;
                $result["message"] = $void[0]->message;
            }
        }
        echo json_encode($result);
    }

    function labelHistory() {
        $objectArray = [
            "role" => "employee"
        ];
        $this->data["menu"] = "labelHistory";
        $this->data["labels"] = $this->shipEngine->getReturnLabels();
        $this->load->view("navigation/header", $this->data);
        $this->load->view("employee/labelHistory", $this->data);
    }

    function validateAddress() {
        $addressInfo = $this->shipEngine->validateAddress($_POST);
        if(isset($addressInfo->errors)) {
            $result['error'] = 1;
            $result["message"] = $addressInfo->errors[0]->message;
        } else {
            $result["success"] = 1;
            $result["status"] = $addressInfo[0]->status;
        }
        echo json_encode($result);
    }

    function createScanForm() {
        $scanFormData = $this->shipEngine->createScanForm();
        if(isset($scanFormData->errors)) {
            $result["error"] = 1;
            $result["message"] = $scanFormData->errors[0]->message;
        } else {
            $objectArray = [
                "link" => $scanFormData->manifest_download->href,
                "formId" => $scanFormData->form_id,
                "date" => date("m/d/Y")
            ];
            $this->parse->newObject("ScanForms", $objectArray);
            $result["success"] = 1;
            $result["scanForm"] = $scanFormData->manifest_download->href;
        }
        echo json_encode($result);
    }

    function createScanForm2() {
//        $scanFormData = $this->shipEngine->createScanForm();
//        if(isset($scanFormData->errors)) {
//            $result["error"] = 1;
//            $result["message"] = $scanFormData->errors[0]->message;
//        } else {
//            $objectArray = [
//                "link" => $scanFormData->manifest_download->href,
//                "formId" => $scanFormData->form_id,
//                "date" => date("m/d/Y")
//            ];
//            $this->parse->newObject("ScanForms", $objectArray);
//            $result["success"] = 1;
//            $result["scanForm"] = $scanFormData->manifest_download->href;
//        }
        $headers = array(
            'Content-Type' => 'application/json',
            'X-Shopify-Access-Token' => $this->session->shopifyToken
        );
        $shop = $this->session->shopifyStore;
        $response = Requests::get('https://'.$shop.'/admin/oauth/access_scopes.json', $headers);
        $responseDecode = json_decode($response->body);

        echo json_encode($responseDecode);
    }

    function getRates() {
        $shippingRates = [];
        if(count($this->session->carriers) < 1) {
            $result["error"] = 3;
            $result["message"] = "Before you can create a label, you will need to set up a shipping service under settings.";
        } else if($this->input->post("weight") == "") {
            $result["error"] = 1;
            $result["message"] = "Please enter a weight";
        } else if ($this->input->post("length") == "" && $this->input->post("packageCarrier") == "none") {
            $result["error"] = 1;
            $result["message"] = "Please enter package dimensions.";
        } else if($this->input->post("height") == "" && $this->input->post("packageCarrier") == "none") {
            $result["error"] = 1;
            $result["message"] = "Please enter package dimensions.";
        } else if($this->input->post("width") == "" && $this->input->post("packageCarrier") == "none") {
            $result["error"] = 1;
            $result["message"] = "Please enter package dimensions.";
        } else {
            $customsCountDesc = count($this->input->post("customsDescription"));
            $customsCountValue = count($this->input->post("customsValue"));
            $customsCountQty = count($this->input->post("customsQty"));
            //        $customsCountTariff = count($this->input->post("tariffCode"));
            $customsCountCOO = count($this->input->post("coo"));
            if ($customsCountDesc != $customsCountCOO || $customsCountDesc != $customsCountQty || $customsCountDesc != $customsCountValue) {
                $result["error"] = 1;
                $result["message"] = "Please fill in all of the customs fields";
            } else {
                $rateData = $this->shipEngine->getRates($_POST);
                if (isset($rateData->errors)) {
                    $result["error"] = 1;
                    $result["message"] = $rateData->errors[0]->message;
                } else {
                    $result["success"] = 1;
                    $result["rates"] = $rateData;
                    $result["shippingRates"] = getShippingButtons($rateData, $this->input->post("packaging"), $this->input->post("packageCarrier"));
//                    $result["shippingRates"] = $this->getShippingButtons($rateData, $this->input->post("packaging"), $this->input->post("packageCarrier"));
                }
            }
        }
        echo json_encode($result);
    }

    function saveWarehouse() {
        $result = $this->shipEngine->createWarehouse();
        $results["success"] = $result;
        echo json_encode($results);
    }

    function getRRInventoryTable() {
        $lowInventory = $this->functions->getInventoryLevels("RR");
        $this->data["results"] = $lowInventory;
        return $this->load->view("employee/inventoryTable", $this->data);
    }

    function getDDAInventoryTable() {
        $lowInventory = $this->functions->getInventoryLevels("DDA");
        $this->data["results"] = $lowInventory;
        return $this->load->view("employee/inventoryTableDDA", $this->data);
    }

    function addClient() {
        $this->data["menu"] = "addClient";
//        $this->data["clients"] = $this->parse->getAllUsers();
        $this->load->view("navigation/header", $this->data);
        $this->load->view("employee/addClient", $this->data);
    }

    function updateServerOrders() {
        $results = $this->parse->getOrdersToFix();
        $result["success"] = $results;
        echo json_encode($result);
    }

    function updateSettings() {
        $error = false;
        foreach ($_POST as $key => $value) {
            if($key == "name" || $key == "email" || $key == "username") {
                if($value == "" || $value == " ") {
                    $error = true;
                }
            }
        }
        if($error) {
            $result["error"] = 1;
            $result["message"] = "Name, email address, and username are required.";
            echo json_encode($result);
        } else {
            $update = $this->parse->updateUser($this->input->post("clientId"), $_POST);
            if($update === true) {
                $result["success"] = 1;
                $result["message"] = "Profile Updated";
            } else {
                $result["error"] = 1;
                $result["message"] = $update;
            }
            echo json_encode($result);
        }
    }

    function saveCustomerAddress() {
        $orderNumber = $this->input->post("orderNumber");
        $storeName = $this->input->post("storeName");
        $missingValue = false;
        foreach ($_POST as $key => $value) {
            if($value == "" || $value == null || $value == " ") {
                if($key != "company") {
                    if($key != "address2") {
                        $missingValue = true;
                        break;
                    }
                }
            }
        }
        if($missingValue == false) {
            $objectArray = [
                "name" => $this->input->post("first_name") . " " . $this->input->post("last_name"),
                "country" => $this->input->post("countryAddress"),
                "province" => $this->input->post("provinceAddress"),
                "first_name" => $this->input->post("first_name"),
                "last_name" => $this->input->post("last_name"),
                "company" => $this->input->post("company"),
                "address1" => $this->input->post("address1"),
                "address2" => $this->input->post("address2"),
                "city" => $this->input->post("city"),
                "province_code" => $this->input->post("province_code"),
                "zip" => $this->input->post("zip"),
                "country_code" => $this->input->post("country_code"),
                "phone" => $this->input->post("phone")
            ];
            $save = $this->parse->updateCustomerAddress($orderNumber, $storeName, $objectArray);
            if ($save["result"] == "success") {
                //Update shopify
                $updateShopify = $this->shopify_model->updateShippingAddress($this->input->post("orderId"), $objectArray);
                if($updateShopify["result"] == "true") {
                    $result["success"] = 1;
                } else {
                    $result["error"] = 1;
                    $result["message"] = $updateShopify["error"];
                }
            } else {
                $result["error"] = 1;
                $result["message"] = $save["message"];
            }
        } else {
            $result["error"] = 1;
            $result["message"] = "All fields are required";
        }
        echo json_encode($result);
    }

    //Not needed, used as a test
    function listWebhook() {
//        $results = $this->functions->addWebhooks("rapidwaretwo.myshopify.com", "e97105605bffeea8e7464aa719ce13c9", "CXkE6gawDk");
        $headers = array(
            'Content-Type' => 'application/json',
            'X-Shopify-Access-Token' => "e97105605bffeea8e7464aa719ce13c9"
        );

        $data = '
        {
          "webhook": {
            "topic": "orders/updated",
            "address": "https://worryfreeshipping.com/webhook/orderUpdated",
            "format": "json"
          }
        }';
        $response = Requests::get('https://rapidwaretwo.myshopify.com/admin/webhooks.json', $headers);
        $responseDecoded = json_decode($response->body);
//        $objectArray = [
//            "webhookId" => $responseDecoded->webhook->id,
//            "storeId" => $storeId,
//            "topic" => "orders/updated",
//            "address" => "https://worryfreeshipping.com/webhook/orderUpdated"
//        ];
//        $this->parse->newObject("Webhooks", $objectArray);
//        return $responseDecoded;
        $result["message"] = $responseDecoded;
        echo json_encode($result);
    }
}