<?php
/**
 * Created by PhpStorm.
 * User: Marketing101
 * Date: 10/10/18
 * Time: 8:44 AM
 */

include(APPPATH.'libraries/allLibraries.php');

defined('BASEPATH') OR exit('No direct script access allowed');

// Add the "use" declarations where you'll be using the classes
use Parse\ParseClient;
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use ZfrShopify\OAuth\TokenExchanger;
use ZfrShopify\OAuth\AuthorizationRedirectResponse;
use ZfrShopify\Exception\InvalidApplicationProxyRequestException;
use ZfrShopify\Validator\ApplicationProxyRequestValidator;
use ZfrShopify\Exception\InvalidWebhookException;
use ZfrShopify\Validator\WebhookValidator;
use ZfrShopify\ShopifyClient;
use Mailgun\Mailgun;

if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Inventory extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        include(APPPATH . 'libraries/keys.php');
        $this->load->library('session');
        $this->load->model("parse_model", "parse");
        $this->load->model("functions");
        $this->load->model("mail_gun_model", "mailGun");
        $this->load->model("ship_engine_model", 'shipEngine');
        $this->load->model("dashboard_model", "dashboard");
        $this->load->helper('shopify');
        $this->load->model('shopify_model');

        if ($this->session->userId == '') {
            redirect($this->config->item('base_url'), 'Location');
        }
    }

    function index() {
        $this->data["menu"] = "Inventory";
        $this->load->view("navigation/header", $this->data);
        $this->load->view("employee/inventory", $this->data);
    }
}
