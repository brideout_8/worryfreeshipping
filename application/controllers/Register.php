<?php

include(APPPATH.'libraries/allLibraries.php');

defined('BASEPATH') OR exit('No direct script access allowed');



// Add the "use" declarations where you'll be using the classes
use Parse\ParseClient;
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseSessionStorage;
use ZfrShopify\OAuth\TokenExchanger;
use ZfrShopify\OAuth\AuthorizationRedirectResponse;
use ZfrShopify\Exception\InvalidApplicationProxyRequestException;
use ZfrShopify\Validator\ApplicationProxyRequestValidator;
use ZfrShopify\Exception\InvalidWebhookException;
use ZfrShopify\Validator\WebhookValidator;
use ZfrShopify\ShopifyClient;


class Register extends CI_Controller {

    function __construct() {

        parent::__construct();

        include(APPPATH . 'libraries/keys.php');
        $this->load->library('session');
        $this->load->model("parse_model", "parse");
        $this->load->model("mail_gun_model", "mailGun");
        $this->load->helper("shopify");
        $this->load->model("shopify_model");
    }

    public function index() {
        $this->data['token'] = @$_GET['token'];
        $this->load->view('register/createPassword', $this->data);
    }


    function createPassword(){

        if ($this->input->post("password") == "" || $this->input->post("passwordMatch") == "") {
            $result["error"] = 1;
            $result["message"] = "Please enter a password.";
        } else if($this->input->post("password") != $this->input->post("passwordMatch")) {
            $result["error"] = 1;
            $result["message"] = "Passwords do not match";
        } else {
            $setPasswordToken = $this->input->post('setPasswordToken');
            $query = new ParseQuery('_User');
            $query->equalTo("setPasswordToken", $setPasswordToken);

            try{
                $queryResults = $query->find();
                $object = $queryResults[0];
                $object->set("password", $this->input->post('password'));
                $object->save(true);
                $result['success'] = 1;
                $result['message'] = "Your password has been created";
            } catch (ParseException $ex) {
                $result['error'] = 1;
                $result["message"] = $ex->getMessage();
            }
        }
        echo json_encode($result);
    }

    //Shopify user clicks get on app store
    //#1
    function install() {
        $shop = $this->input->get("shop");
        //If the shop name exists log in the client
        $storeArray = ["shopifyName" => $shop];
        $storeInfo = $this->parse->getParseClassArray("Stores", $storeArray);
        if(count($storeInfo) > 0) {
            if($storeInfo[0]->get("status") != "inactive") {
                //Log client in
                $currentUser = ParseUser::getCurrentUser();
                if ($currentUser != null) {
                    $companyId = $currentUser->get("companyId");
                    if ($companyId == $storeInfo[0]->get("companyId")) {
                        $companyData = $this->parse->getParseClass("Companies", "objectId", $companyId);
                        if ($companyData[0]->get("status") == "active") {
                            $userData = $this->shopify_model->setUserData($companyData);
                            $this->data["menu"] = "fulfillment";
                            $this->load->view("navigation/header", $this->data);
                            $this->load->view("fulfillment/fulfillmentOverview", $this->data);
                        } else {
                            //Company is inactive. The paying store was deleted. Making the store they clicked on the paying store.
                            $this->parse->makeStorePaying($storeInfo[0]->getObjectId(), $companyId);
                            $this->data["shopifyStore"] = $shop;
                            $this->data["accessToken"] = $storeInfo[0]->get("token");
                            $this->data["newCompany"] = "no";
                            $this->data["newStore"] = "no";
                            $this->load->view("register/shopify/selectPlan", $this->data);
                        }
                    } else {
                        //User was logged into a different account
                        $this->load->view("login");
                    }
                } else {
                    $this->load->view("login");
                }
            } else {
                //User deleted the app in shopify admin but added back within 48 hours.
                $keys = $this->parse->shopifyKeysStore($shop);
                $scopes = "read_orders,write_orders,read_products,write_products,read_inventory,write_inventory,read_customers,read_locations";
                $install_url = "https://worryfreeshipping.com/register/auth";
                PHPShopify\AuthHelper::createAuthRequest($scopes, $install_url);
            }
        } else {
            //New shop
            $keys = $this->parse->shopifyKeysStore($shop);
            $scopes = "read_orders,write_orders,read_products,write_products,read_inventory,write_inventory,read_customers,read_locations";
            $install_url = "https://worryfreeshipping.com/register/auth";
//        $install_url = "http://".$shop."/admin/oauth/authorize?client_id=3307af73aa03082d5601d1e19794e204&scope=".$scopes."&redirect_uri=https://r-renterprises.net/register/auth";
            PHPShopify\AuthHelper::createAuthRequest($scopes, $install_url);
        }
    }

    //Shopify Auth on get from app store
    //#2
    function auth() {

        //Check for access token. There will be a token if the user declined the charge and started over. Keeps the user from adding another account.
        $shop = $this->input->get("shop");
        $this->parse->shopifyKeysStore($shop);
        $accessToken = \PHPShopify\AuthHelper::getAccessToken();
        $shopInfo = $this->shopify_model->getShopInfo($accessToken, $shop);
        $customerInfo = $this->parse->getParseClassArray("_User", ["email" => $shopInfo->email]);
        if(count($customerInfo) < 1) {
            //No user account for the shop's email
            $currentUser = ParseUser::getCurrentUser();
            if($currentUser != null) {
                //User signed in but adding a store with a different email address
                $array = ["objectId" => $currentUser->companyId];
                $companyInfo = $this->parse->getParseClassArray("Companies", $array);
                if($companyInfo[0]->get("status") == "active") {
                    //Adding another store
                    $storeArray = ["shopifyName" => $shop];
                    $storeInfo = $this->parse->getParseClassArray("Stores", $storeArray);
                    if(count($storeInfo) > 0) {
                        //If the store exists then it is inactive
                        //Make store active and make orders active
                        $this->shopify_model->makeStoreAndOrdersActive($storeInfo[0]->getObjectId(), $shop);
                        $this->parse->updateObject("Stores", ["token" => $accessToken], $storeInfo[0]->getObjectId());
                        $this->functions->addWebhooks($shop,$accessToken, $storeInfo[0]->getObjectId());
                        $this->shopify_model->setUserData($companyInfo);
                        $this->data["menu"] = "fulfillment";
                        $this->load->view("navigation/header", $this->data);
                        $this->load->view("fulfillment/fulfillmentOverview", $this->data);
                    } else {
                        $this->shopify_model->setUpNewStore($accessToken, $shop, $shopInfo->id, $companyInfo[0]->getObjectId(), "active");
                        $this->shopify_model->setUserData($companyInfo);
                        $this->data["shopifyToken"] = $accessToken;
                        $this->data["storeName"] = $shop;
                        $this->load->view("register/shopify/importFirstOrder", $this->data);
                    }
                } else {
                    //No paying store
                    $storeArray = ["shopifyName" => $shop];
                    $storeInfo = $this->parse->getParseClassArray("Stores", $storeArray);
                    if(count($storeInfo) > 0) {
                        //Store is inactive but it has not been 48 hours since deletion
                        $this->shopify_model->makeStoreAndOrdersActive($storeInfo[0]->getObjectId(), $shop);
                        $this->parse->updateObject("Stores", ["token" => $accessToken], $storeInfo[0]->getObjectId());
                        $this->functions->addWebhooks($shop,$accessToken, $storeInfo[0]->getObjectId());
                        $this->parse->makeStorePaying($storeInfo[0]->getObjectId(), $companyInfo[0]->getObjectId());
                    } else {
                        $shopId = $this->shopify_model->setUpNewStore($accessToken, $shop, $shopInfo->id, $companyInfo[0]->getObjectId(), "inactive");
                        $this->parse->makeStorePaying($shopId, $companyInfo[0]->getObjectId());
                    }
                    $this->data["shopifyStore"] = $shop;
                    $this->data["accessToken"] = $accessToken;
                    $this->data["newCompany"] = "no";
                    $this->data["newStore"] = "yes";
                    $this->load->view("register/shopify/selectPlan", $this->data);
                }
            } else {
                //New User
                $nameArray = explode(" ", $shopInfo->shop_owner, 2);
                $this->data["firstName"] = $nameArray[0];
                if (count($nameArray) > 1) {
                    $this->data["lastName"] = $nameArray[1];
                } else {
                    $this->data["lastName"] = "";
                }
                $this->data["shopInfo"] = $shopInfo;
                $this->data["shopifyStore"] = $shop;
                $this->data["accessToken"] = $accessToken;
                $this->load->view("register/shopify/createAccount", $this->data);
            }
        } else {
            //Returning user. Find out if they are adding another store or the current store is not a paying one.
            $array = ["objectId" => $customerInfo[0]->get("companyId")];
            $companyInfo = $this->parse->getParseClassArray("Companies", $array);
            if($companyInfo[0]->get("status") == "active") {
                //Adding another store
                $currentUser = ParseUser::getCurrentUser();
                if($currentUser != null) {
                    //User has been logged in
                    $storeArray = ["shopifyName" => $shop];
                    $storeInfo = $this->parse->getParseClassArray("Stores", $storeArray);
                    if(count($storeInfo) > 0) {
                        //If the store exists then it is inactive
                        //Make store active and make orders active
                        $this->shopify_model->makeStoreAndOrdersActive($storeInfo[0]->getObjectId(), $shop);
                        $this->parse->updateObject("Stores", ["token" => $accessToken], $storeInfo[0]->getObjectId());
                        $this->functions->addWebhooks($shop,$accessToken, $storeInfo[0]->getObjectId());
                        $this->shopify_model->setUserData($companyInfo);
                        $this->data["menu"] = "fulfillment";
                        $this->load->view("navigation/header", $this->data);
                        $this->load->view("fulfillment/fulfillmentOverview", $this->data);
                    } else {
                        $this->shopify_model->setUpNewStore($accessToken, $shop, $shopInfo->id, $companyInfo[0]->getObjectId(), "active");
                        $this->shopify_model->setUserData($companyInfo);
                        $this->data["shopifyToken"] = $accessToken;
                        $this->data["storeName"] = $shop;
                        $this->load->view("register/shopify/importFirstOrder", $this->data);
                    }
                } else {
                    //User not logged in. Show a login screen then proceed to connect the store.
                    $this->data["shopifyToken"] = $accessToken;
                    $this->data["storeName"] = $shop;
                    $this->data["payingStore"] = "no";
                    $this->data["storeId"] = $shopInfo->id;
                    $this->load->view("login", $this->data);
                }
            } else {
                //No paying store
                $currentUser = ParseUser::getCurrentUser();
                if($currentUser != null) {
                    //User has been logged in. Need to send to choose plan and create webhooks and store data
                    $storeArray = ["shopifyName" => $shop];
                    $storeInfo = $this->parse->getParseClassArray("Stores", $storeArray);
                    if(count($storeInfo) > 0) {
                        //Store is inactive but it has not been 48 hours since deletion
                        $this->shopify_model->makeStoreAndOrdersActive($storeInfo[0]->getObjectId(), $shop);
                        $this->parse->updateObject("Stores", ["token" => $accessToken], $storeInfo[0]->getObjectId());
                        $this->functions->addWebhooks($shop,$accessToken, $storeInfo[0]->getObjectId());
                        $this->parse->makeStorePaying($storeInfo[0]->getObjectId(), $companyInfo[0]->getObjectId());
                    } else {
                        $shopId = $this->shopify_model->setUpNewStore($accessToken, $shop, $shopInfo->id, $companyInfo[0]->getObjectId(), "inactive");
                        $this->parse->makeStorePaying($shopId, $companyInfo[0]->getObjectId());
                    }
                    $this->data["shopifyStore"] = $shop;
                    $this->data["accessToken"] = $accessToken;
                    $this->data["newCompany"] = "no";
                    $this->data["newStore"] = "yes";
                    $this->load->view("register/shopify/selectPlan", $this->data);
                } else {
                    //User has not logged in. Show a login screen then proceed to connect the store. Send to choose plan once logged in.
                    $this->data["shopifyToken"] = $accessToken;
                    $this->data["storeName"] = $shop;
                    $this->data["payingStore"] = "yes";
                    $this->data["storeId"] = $shopInfo->id;
                    $this->load->view("login", $this->data);
                }
            }
//            $storeArray = ["objectId" => $companyInfo[0]->get("payingStore")];
//            $storeInfo = $this->parse->getParseClassArray("Stores", $storeArray);
        }
    }

    //Used when shopify is handling billing
    //#3
    function createAccount() {
        if($this->input->post("firstName") == "" || $this->input->post("lastName") == "") {
            $result["error"] = 1;
            $result["message"] = "First and last name are required";
        } else if($this->input->post("email") == "") {
            $result["error"] = 1;
            $result["message"] = "Email is required";
        } else if($this->input->post("username") == "") {
            $result["error"] = 1;
            $result["message"] = "Username is required";
        } else if($this->input->post("password") == "") {
            $result["error"] = 1;
            $result["message"] = "A password is required";
        } else if($this->input->post("phoneNumber") == "") {
            $result["error"] = 1;
            $result["message"] = "A phone number is required to create labels";
        } else if($this->input->post("address1") == "" || $this->input->post("city") == "" || $this->input->post("zip") == "" || $this->input->post("state") == "") {
            $result["error"] = 1;
            $result["message"] = "An address, city, state, and zip are required";
        } else if($this->input->post("companyName") == "") {
            $result["error"] = 1;
            $result["message"] = "A company name is required";
        } else {
            if($this->input->post("country") == "United States") {
                $_POST["countryCode"] = "US";
            } else if($this->input->post("country") == "Canada") {
                $_POST["countryCode"] = "CA";
            } else if($this->input->post("country") == "Australia") {
                $_POST["countryCode"] = "AU";
            } else {
                $_POST["countryCode"] = "GB";
            }
            $createUser = $this->parse->newUserShopify($_POST);
            if ($createUser == "success") {
                $this->mailGun->sendMeNewUserEmail($this->input->post("firstName")." ".$this->input->post("lastName"), $this->input->post("city"), $this->input->post("state"), $this->input->post("country"), $this->input->post("shopifyStore"));
                $result["success"] = 1;
            } else if ($createUser == "209") {
                $createUser = $this->parse->newUserShopify($_POST);
                if ($createUser == "success") {
                    $this->mailGun->sendMeNewUserEmail($this->input->post("firstName")." ".$this->input->post("lastName"), $this->input->post("city"), $this->input->post("state"), $this->input->post("country"), $this->input->post("shopifyStore"));
                    $result["success"] = 1;
                } else {
                    $result["error"] = 1;
                    $result["message"] = $createUser;
                }
            } else {
                $result["error"] = 1;
                $result["message"] = $createUser;
            }
        }
        echo json_encode($result);
    }

    //After creating an account user must select plan
    //#4
    function plan() {
        $this->data["shopifyStore"] = $this->input->post("shopifyStore");
        $this->data["accessToken"] = $this->input->post("shopifyToken");
        $this->data["newCompany"] = $this->input->post("newCompany");
        $this->data["newStore"] = $this->input->post("newStore");
        $this->load->view("register/shopify/selectPlan", $this->data);
    }

    //Used to log customer in once account is created if trial is selected
    //#5Trial
    function logCustomerIn() {
        $currentUser = ParseUser::getCurrentUser();
        if($currentUser != null) {
            $companyId = $currentUser->get("companyId");
            $objectArray = ["subscriptionLevel" => "Trial", "status" => "active", "labelsRemaining" => 20];
            $this->parse->updateObject("Companies", $objectArray, $companyId);
            $companyData = $this->parse->getParseClass("Companies", "objectId", $companyId);
            $saveChargeArray = ["status" => "active"];
            $this->parse->updateObject("Stores", $saveChargeArray, $companyData[0]->get("payingStore"));
            $this->shopify_model->setUserData($companyData);
            $this->mailGun->sendMeNewPlanEmail("Trial");
            $sent = $this->mailGun->welcomeClient($currentUser->get("email"), $currentUser->get("name"));
            $this->load->view("register/shopify/success");
//            header("Location: $confirmationURL");
        }

    }

    //Used to create plan on shopify
    //#5Charge
    function createShopifyCharge() {
        $shop = $this->input->post("shopifyStore");
        $accessToken = $this->input->post("accessToken");
        $planCost = $this->input->post("planCost");
        $planName = $this->input->post("planName");
        $cappedAmount = $this->input->post("cappedAmount");
        $terms = $this->input->post("terms");
        $returnUrl = "https://worryfreeshipping.com/register/confirmShopifyCharge?newCompany=".$this->input->post("newCompany")."&newStore=".$this->input->post("newStore");
        $confirmationURL = $this->shopify_model->createShopifyCharge($accessToken, $planName, $planCost, $cappedAmount, $terms, $shop, $returnUrl);
        if(isset($confirmationURL["url"])) {
            $confirmationURL = $confirmationURL["url"];
            header("Location: $confirmationURL");
        } else {
            $this->data["reason"] = $confirmationURL["error"];
            $this->load->view("register/shopify/error", $this->data);
        }
    }

    //Used to confirm shopify purchase
    //#6Charge
    function confirmShopifyCharge() {
        //Find the charge to see if approved or declined
        $currentUser = ParseUser::getCurrentUser();
        if($currentUser != null) {
            $companyId = $currentUser->get("companyId");
            $companyData = $this->parse->getParseClass("Companies", "objectId", $companyId);
            $payingStore = $companyData[0]->get("payingStore");
            $shopQuery = $this->parse->getParseClass("Stores", "objectId", $payingStore);
            $chargeId = $this->input->get("charge_id");
            $chargeState = $this->shopify_model->activateShopifyCharge($shopQuery[0]->get("token"), $shopQuery[0]->get("shopifyName"), $chargeId);
            if(isset($chargeState["error"])) {
                $this->data["reason"] = $chargeState["error"];
                $this->load->view("register/shopify/error", $this->data);
            } else {
                if ($chargeState["status"] == "active") {
                    //Save chargeId
                    $saveChargeArray = ["shopifyChargeId" => $chargeId, "status" => "active"];
                    $this->parse->updateObject("Stores", $saveChargeArray, $payingStore);
                    //Save sub level and starting date
                    $subDate = getDatetimeNow();
                    $planName = $chargeState["planName"];
                    $subLevel = getSubLevel($planName);
                    $objectArray = ["subscriptionLevel" => $planName, "status" => "active", "billingPeriodStartDate" => $subDate, "labelsRemaining" => $subLevel["labelsRemaining"]];
                    $updateCompany = $this->parse->updateObject("Companies", $objectArray, $companyId);
                    $companyDataUpdated = $this->parse->getParseClass("Companies", "objectId", $companyId);
                    if ($updateCompany == true) {
                        $setUserData = $this->shopify_model->setUserData($companyDataUpdated);
                        $this->mailGun->sendMeNewPlanEmail($planName);
                        if ($this->input->get("newCompany") == "yes") {
                            $sent = $this->mailGun->welcomeClient($currentUser->get("email"), $currentUser->get("name"));
                            $this->load->view("register/shopify/success");
                        } else {
                            if ($this->input->get("newStore") == "yes") {
                                $this->data["shopifyToken"] = $shopQuery[0]->get("token");
                                $this->data["storeName"] = $shopQuery[0]->get("shopifyName");
                                $this->load->view("register/shopify/importFirstOrder", $this->data);
                            } else {
                                $this->data["menu"] = "fulfillment";
                                $this->load->view("navigation/header", $this->data);
                                $this->load->view("fulfillment/fulfillmentOverview", $this->data);
                            }
                        }
                    }
                } else if ($chargeState["status"] == "cardDeclined") {
                    //Card declined
                    $this->load->view("register/shopify/cardDeclined");
                } else {
                    //User declined the charge
                    $this->load->view("register/shopify/decline");
                }
            }
        }
    }

    function signUp() {
        $this->load->view("register/createAccount");
    }

    //Used when we are handling the billing
    function checkout() {
        $name = $this->input->post("name");
        $token = $this->input->post("token");
        $email = $this->input->post("email");
        $phone = $this->input->post("phone");
        $company = $this->input->post("company");
        $password = $this->input->post("password");

        try {
            $charge = \Stripe\Charge::create([
                'amount' => 999,
                'currency' => 'usd',
                'description' => 'Example charge',
                'source' => 'tok_chargeDeclinedInsufficientFunds',
                'metadata' => ['Company' => $company, 'Name' => $name, "Phone" => $phone],
            ]);

            $result["success"] = 1;
            $result["token"] = $token;

        } catch(\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught
            $body = $e->getJsonBody();
            $err  = $body['error'];

//            print('Type is:' . $err['type'] . "\n");
//            print('Code is:' . $err['code'] . "\n");
//            print('Message is:' . $err['message'] . "\n");
            $result["error"] = 1;
            $result["message"] = $err['message'];
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly
            $result["error"] = 1;
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API
            $result["error"] = 1;
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $result["error"] = 1;
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
            $result["error"] = 1;
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $result["error"] = 1;
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $result["error"] = 1;
        }

        echo json_encode($result);

    }

    //Customer already has plan and now is changing
    function changePlan() {
        $this->data["shopifyStore"] = $this->session->shopifyStore;
        $this->data["accessToken"] = $this->session->shopifyToken;
        $this->data["menu"] = "Account";
        $this->load->view("navigation/header", $this->data);
        $this->load->view("register/shopify/changePlan", $this->data);
    }
    //Used to upgrade or downgrade a plan
    //#1 Upgrade
    function newShopifyCharge() {
        $shop = $this->input->post("shopifyStore");
        $accessToken = $this->input->post("accessToken");
        $planCost = $this->input->post("planCost");
        $planName = $this->input->post("planName");
        $cappedAmount = $this->input->post("cappedAmount");
        $terms = $this->input->post("terms");
        $returnUrl = "https://worryfreeshipping.com/register/confirmNewShopifyCharge";
        $confirmationURL = $this->shopify_model->createShopifyCharge($accessToken, $planName, $planCost, $cappedAmount, $terms, $shop, $returnUrl);
        if(isset($confirmationURL["url"])) {
            $confirmationURL = $confirmationURL["url"];
            header("Location: $confirmationURL");
        } else {
            $this->data["reason"] = $confirmationURL["error"];
            $this->load->view("register/shopify/error", $this->data);
        }
    }
    //Used to upgrade or downgrade a plan
    //#2 Upgrade
    function confirmNewShopifyCharge() {
        //Find the charge to see if approved or declined
        $currentUser = ParseUser::getCurrentUser();
        if($currentUser != null) {
            $companyId = $currentUser->get("companyId");
            $companyData = $this->parse->getParseClass("Companies", "objectId", $companyId);
            $payingStore = $companyData[0]->get("payingStore");
            $shopQuery = $this->parse->getParseClass("Stores", "objectId", $payingStore);
            $chargeId = $this->input->get("charge_id");
            $chargeState = $this->shopify_model->activateShopifyCharge($shopQuery[0]->get("token"), $shopQuery[0]->get("shopifyName"), $chargeId);
            if($chargeState["status"] == "active") {
                //Save chargeId
                $saveChargeArray = ["shopifyChargeId" => $chargeId];
                $this->parse->updateObject("Stores", $saveChargeArray, $payingStore);
                //Save sub level and starting date
                $subDate = getDatetimeNow();
                $planName = $chargeState["planName"];
                $subLevel = getSubLevel($planName)["subLevel"];
                $this->session->subscriptionLevel = $subLevel;
                $this->shopify_model->resetRemainingLabelsOnPlanChange();
                $objectArray = ["subscriptionLevel" => $planName, "status" => "active", "billingPeriodStartDate" => $subDate, "labelsRemaining" => $this->session->labelsRemaining];
                $this->parse->updateObject("Companies", $objectArray, $companyId);
                //If downgrading to Lite plan, make users inactive
                if($planName == "Lite") {
                    $userQuery = $this->parse->getParseClass("_User", "companyId", $companyId);
                    if(count($userQuery) > 1) {
                        //Multiple Users
                        if($this->session->masterUserId != "") {
                            $masterUserId = $this->session->masterUserId;
                        } else {
                            $masterUserId = $currentUser->getObjectId();
                        }
                        foreach ($userQuery as $user) {
                            if($user->getObjectId() != $masterUserId) {
                                $this->parse->updateObject("_User", ["status" => "inactive"], $user->getObjectId());
                            }
                        }
                    }
                }
                $this->session->currentBillingPeriodStartDate = $subDate;
                $this->session->shopifyChargeId = $chargeId;
                $this->mailGun->sendMeNewPlanEmail($planName);
                $this->load->view("register/shopify/planChangeSuccess");
            } else if($chargeState["status"] == "cardDeclined") {
                //Card declined
                $this->load->view("register/shopify/cardDeclined");
            } else {
                //User declined the charge
                $this->load->view("register/decline");
            }
        }
    }
    //Used for shopify app store listing
    function plans() {
        $this->load->view("register/plans");
    }

    //Used when user does not have a paying store connected any longer
    #1
    function enterShopifyStore() {
        $currentUser = ParseUser::getCurrentUser();
        $companyId = $currentUser->get("companyId");
        $this->data["stores"] = $this->parse->getParseClass("Stores", "companyId", $companyId);
        $this->load->view("register/connectStore", $this->data);
    }
    #2 //Used when customer selects a store to make billing
    function newPayingStore(){
        $store = $this->parse->getParseClass("Stores", "shopifyName", $this->input->get("shop"));
        $this->parse->makeStorePaying($store[0]->getObjectId(), $store[0]->get("companyId"));
        $this->data["shopifyStore"] = $this->input->get("shop");
        $this->data["accessToken"] = $store[0]->get("token");
        $this->data["newCompany"] = "no";
        $this->data["newStore"] = "no";
        $this->load->view("register/shopify/selectPlan", $this->data);
    }

    //Used when customer is logging in from click the app inside of the admin page of Shopify
    function newStore() {
        if($this->input->post("payingStore") == "no") {
            $companyInfo = $this->parse->getParseClass("Companies", "objectId", $this->session->companyId);
            $this->shopify_model->setUserData($companyInfo);
            $orders = $this->parse->getParseClassArray("Orders", ["storeName" => $this->input->post("storeName")]);
            if(count($orders) > 0) {
                //Returning store already has orders imported
                $this->data["menu"] = "fulfillment";
                $this->load->view("navigation/header", $this->data);
                $this->load->view("fulfillment/fulfillmentOverview", $this->data);
            } else {
                //New store
                $this->data["shopifyToken"] = $this->input->post("shopifyToken");
                $this->data["storeName"] = $this->input->post("storeName");
                $this->load->view("register/shopify/importFirstOrder", $this->data);
            }
        } else {
            $this->data["shopifyStore"] = $this->input->post("storeName");
            $this->data["accessToken"] = $this->input->post("shopifyToken");
            $this->data["newCompany"] = "no";
            $this->data["newStore"] = "yes";
            $this->load->view("register/shopify/selectPlan", $this->data);
        }
    }

    function connectCarrier() {
        $this->load->view("register/connectCarrier");
    }

    function stampsSignUp() {
        $carriers = $this->parse->getParseClass("Carriers", "companyId", $this->session->companyId);
        foreach ($carriers as $carrier) {
            if ($carrier->get("status") == "pending") {
                $this->data["carrierObjectId"] = $carrier->getObjectId();
                break;
            }
        }
        $shippingInfo = $this->parse->getParseClass("Companies", "objectId", $this->session->companyId);
        $this->data["shippingFrom"] = $shippingInfo[0];
        $this->load->view("register/shopify/stampsSetUp", $this->data);
    }

    function addEndicia() {
        $this->load->view("register/shopify/endicia");
    }

    function addUps() {
        $this->data["countryCodes"] = countryCodes();
        $this->data["companyInfo"] = $this->parse->getParseClass("Companies", "objectId", $this->session->companyId);
        $this->load->view("register/shopify/ups", $this->data);
    }

    function addFedex() {
        $this->data["countryCodes"] = countryCodes();
        $this->data["companyInfo"] = $this->parse->getParseClass("Companies", "objectId", $this->session->companyId);
        $this->load->view("register/shopify/fedex", $this->data);
    }

    function addStamps() {
        $this->load->view("register/shopify/stamps");
    }
}