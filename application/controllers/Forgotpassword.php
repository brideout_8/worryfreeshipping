<?php

include(APPPATH.'libraries/allLibraries.php');

defined('BASEPATH') OR exit('No direct script access allowed');

// Add the "use" declarations where you'll be using the classes
use Parse\ParseClient;
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
class Forgotpassword extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        include(APPPATH.'libraries/keys.php');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->model("mail_gun_model", "mailGun");
    }

    public function index() {
        $this->load->view('passwordReset/forgotpassword');
    }

    public function forgotpassword_validation() {

        if ($this->input->post("email") == "" || $this->input->post("email") == " ") {
            $result['error'] = 1;
            $result["message"] = "Please fill in an email address";

        } else {
            $email = $this->input->post('email');
            $query = new ParseQuery("_User");
            $query->equalTo("email", $email);
            try {
                $results = $query->find();
                if(count($results) > 0) {
                    $username = $results[0]->get("username");
                    $reset_token = uniqid() . time();
                    $url = base_url() . "password_reset?token=" . @$reset_token;
                    $this->mailGun->forgotPassword($email, $url, $username);
                    $query = new ParseQuery('_User');
                    $query->equalTo("objectId", $results[0]->getObjectId());
                    try {
                        $object = $query->get($results[0]->getObjectId());
                        $object->set("reset_token", $reset_token);
                        $object->save(true);
                        $result['success'] = 1;
                        $result["message"] = "Your username has been sent to " . $this->input->post("email");
                    } catch (ParseException $ex) {
                        $result['error'] = 1;
                        $result["message"] = $ex->getMessage();
                        $this->session->set_flashdata('error_message', $ex);
                    }
                } else {
                    $result["error"] = 1;
                    $result["message"] = "Email address not found";
                }
            } catch (ParseException $ex) {
                $result['error'] = 1;
                $result["message"] = $ex->getMessage();
            }
        }
        echo json_encode($result);
    }

    function success() {
        $this->load->view("passwordReset/success");
    }

    function changepassword(){

        $this->data['body'] 	= 'changepassword';

        $this->data['token']	= @$_GET['token'];

        $this->load->view('structure', $this->data);

    }

    function autheticate()
    {

        $query = new ParseQuery("_User");
        $query->equalTo("email", $this->input->post('email'));
        $results = $query->find();

        if(count($results) == 0){

            $this->form_validation->set_message('autheticate', 'Sorry! Email does not exist');

            return FALSE;

        } else {

            return true;

        }
    }

}
