<?php
include(APPPATH.'libraries/allLibraries.php');

defined('BASEPATH') OR exit('No direct script access allowed');

// Add the "use" declarations where you'll be using the classes
use Parse\ParseClient;
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseSessionStorage;
use GuzzleHttp\Command\Exception\CommandException;
use GuzzleHttp\Client;
use ZfrShopify\OAuth\TokenExchanger;
use ZfrShopify\OAuth\AuthorizationRedirectResponse;
use ZfrShopify\Exception\InvalidApplicationProxyRequestException;
use ZfrShopify\Validator\ApplicationProxyRequestValidator;
use ZfrShopify\Exception\InvalidWebhookException;
use ZfrShopify\Validator\WebhookValidator;
use ZfrShopify\ShopifyClient;

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
//        session_start();
        $this -> load -> library('form_validation');
        $this->load->library('javascript');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->model("parse_model");
        $this->load->model("functions");
        $this->load->helper("shopify");
        $this->load->model("shopify_model");
        ParseClient::setStorage( new ParseSessionStorage() );

        include(APPPATH.'libraries/keys.php');
    }

	public function index() {
        $currentUser = ParseUser::getCurrentUser();
        if ($currentUser != null) {
            $companyId = $currentUser->get("companyId");
            $companyData = $this->parse->getParseClass("Companies", "objectId", $companyId);
            if ($companyData[0]->get("status") == "active") {
                $userData = $this->shopify_model->setUserData($companyData);
                $this->data["menu"] = "fulfillment";
                $this->load->view("navigation/header", $this->data);
                $this->load->view("fulfillment/fulfillmentOverview", $this->data);
            } else {
                //Company is inactive. The paying store was deleted. Making the store they clicked on the paying store.
                $this->load->view("login");
            }
        } else {
            $this->load->view('login');
        }
	}

	public function login_validation() {

        if ($this->input->post("email" == "") || $this->input->post("password" == "")) {
            $result["error"] = 1;
        } else {
            $username = $this->input->post("username");
            $password = $this->input->post("password");

            try {
                $user = ParseUser::logIn($username, $password);
                if ($this->input->post("payingStore")) {
                    //User is trying to connect a new store but was not logged in.
                    $companyId = $companyId = $user->companyId;
                    $companyData = $this->parse->getParseClass("Companies", "objectId", $companyId);
                    $this->shopify_model->resetRemainingLabels();
                    $result["newStore"] = 1;
                    if ($this->input->post("payingStore") == "yes") {
                        $this->shopify_model->setUserData($companyData);
                        $storeArray = ["shopifyName" => $this->input->post("storeName")];
                        $storeInfo = $this->parse->getParseClassArray("Stores", $storeArray);
                        if(count($storeInfo) > 0) {
                            //Store is inactive but it has not been 48 hours since deletion
                            $this->shopify_model->makeStoreAndOrdersActive($storeInfo[0]->getObjectId(), $this->input->post("storeName"));
                            $this->parse->updateObject("Stores", ["token" => $this->input->post("shopifyToken")], $storeInfo[0]->getObjectId());
                            $this->functions->addWebhooks($this->input->post("storeName"),$this->input->post("shopifyToken"), $storeInfo[0]->getObjectId());
                            $this->parse->makeStorePaying($storeInfo[0]->getObjectId(), $companyId);
                        } else {
                            $storeObjectId = $this->shopify_model->setUpNewStore($this->input->post("shopifyToken"), $this->input->post("storeName"), $this->input->post("storeId"), $companyId, "inactive");
                            $this->parse->makeStorePaying($storeObjectId, $companyId);
                        }
                    } else {
                        $storeArray = ["shopifyName" => $this->input->post("storeName")];
                        $storeInfo = $this->parse->getParseClassArray("Stores", $storeArray);
                        if(count($storeInfo) > 0) {
                            //Store is inactive but it has not been 48 hours since deletion
                            $this->shopify_model->makeStoreAndOrdersActive($storeInfo[0]->getObjectId(), $this->input->post("storeName"));
                            $this->parse->updateObject("Stores", ["token" => $this->input->post("shopifyToken")], $storeInfo[0]->getObjectId());
                            $this->functions->addWebhooks($this->input->post("storeName"),$this->input->post("shopifyToken"), $storeInfo[0]->getObjectId());
//                            $this->parse->makeStorePaying($storeInfo[0]->getObjectId(), $companyId);
                        } else {
                            $this->shopify_model->setUpNewStore($this->input->post("shopifyToken"), $this->input->post("storeName"), $this->input->post("storeId"), $companyId, "active");
                        }
                    }
                } else {
                    //User is logging in like normal
                    $user_id = $user->getObjectId();
                    $userStatus = $user->status;
                    $companyId = $user->companyId;
                    $companyData = $this->parse_model->getParseClass("Companies", "objectId", $companyId);
                    $subLevel = getSubLevel($companyData[0]->get("subscriptionLevel"))["subLevel"];
                    if ($companyData[0]->get("status") == "active") {
                        if ($userStatus == "active") {
                            $payingStoreId = $companyData[0]->get("payingStore");
                            $array = ["companyId" => $companyId, "objectId" => $payingStoreId];
                            $store = $this->parse->getParseClassArray("Stores", $array);
                            if (count($store) > 0) {
                                if ($subLevel != "0") {
                                    $chargeStatus = $this->shopify_model->checkApplicationChargeStatus($store[0]->get("token"), $store[0]->get("shopifyName"), $store[0]->get("shopifyChargeId"));
                                    if ($chargeStatus["result"] == "active") {
                                        $this->shopify_model->resetRemainingLabels();
                                        $this->shopify_model->setUserData($companyData);
                                        $result["url"] = 'fulfillment';
                                        $result["success"] = 1;
                                    } else if ($chargeStatus["result"] == "notActive" || $chargeStatus["result"] == "no charges") {
                                        //Charge is not active.
                                        $this->data["shopifyStore"] = $store[0]->get("shopifyName");
                                        $this->data["accessToken"] = $store[0]->get("token");
                                        $this->data["newCompany"] = "no";
                                        $this->data["newStore"] = "no";
                                        $this->load->view("register/shopify/selectPlan", $this->data);
                                    } else {
                                        //Error occurred gettings charge status
                                        $result["error"] = 1;
                                        $result["message"] = "Please contact support with error: " . var_dump($chargeStatus["error"]);
                                    }
                                } else {
                                    //On Trial
                                    $this->shopify_model->setUserData($companyData);
                                    $result["url"] = 'fulfillment';
                                    $result["success"] = 1;
                                }
                            } else {
                                //No paying store exists. Only way to get here if user deleted the app but the webhook did not make the company inactive. This is a fail safe
                                $stores = $this->parse->getParseClass("Stores", "companyId", $companyId);
                                if (count($stores) > 0) {
                                    //Has other stores connected, allow user to select one as a paying store
                                    $result["success"] = 1;
                                    $result["url"] = 'register/enterShopifyStore';
                                } else {
                                    //No other stores. Let customer know how to connect a store
                                    $result["error"] = 1;
                                    $result["message"] = "You do not have any stores connected. Please go to the shopify store click get app for Worry Free Shipping";
                                }
                            }
                        } else {
                            //An additional user account was deactivated by admin
                            $result["error"] = 1;
                            $result["message"] = "Your account is not active. Please contact your company administrator";
                        }
                    } else {
                        //No paying store
                        $stores = $this->parse->getParseClassArray("Stores", ["companyId" =>$companyId, "status" => "active"]);
                        if (count($stores) > 0) {
                            //Has other stores connected, allow user to select one as a paying store
                            $result["success"] = 1;
                            $result["url"] = 'register/enterShopifyStore';
                        } else {
                            //No other stores. Let customer know how to connect a store
                            $result["error"] = 1;
                            $result["message"] = "You do not have any stores connected. Please go to the shopify store and click get app for Worry Free Shipping";
                        }
                    }
                }
            } catch (ParseException $error) {
                $result["error"] = 1;
                $result["message"] = $error->getMessage();
            }
        }
        echo json_encode($result);
    }

    function getProduct() {
        $sku = "11170001-2-BLK";
        $productTitle = "test";
        $shopify = $this->parse->shopify("RR");
        $params = array(
            'title' => $productTitle,
        );
        $product = $shopify->Product->get($params);
        $variants = $product[0]["variants"];
        $id = $product[0]["id"];
        $variantArrayToSave = array();
        $x=0;
        foreach ($variants as $variant) {
            $variantArrayToSave[$x]["id"] = $variant["id"];
            if($variant["sku"] == $sku) {
                $variantArrayToSave[$x]["inventory_quantity"] = $variant["inventory_quantity"] - 1;
            }
            $x++;
        }
        $productInfo = array(
            "id"=> $id,
            "variants" => $variantArrayToSave
        );
        $productUpdate = $shopify->Product($id)->put($productInfo);
        $result["success"] = 1;
        echo json_encode($result);
    }

    function logout() {
        ParseUser::logOut();
        $this->session->set_userdata(
            array(
                'login_id'=>'',
                'username'=>'',
                'email'=>'',
                'name' => '',
                'role'=>''
            ));
        $this->session->sess_destroy();
        redirect($this->config->item('base_url'),'Location');
    }
}
