<?php 

include(APPPATH.'libraries/allLibraries.php');


defined('BASEPATH') OR exit('No direct script access allowed');

// Add the "use" declarations where you'll be using the classes
use Parse\ParseClient;
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseSessionStorage;

/*
ParseClient::setStorage( new ParseSessionStorage() );
$currentUser = ParseUser::getCurrentUser();
$currentUserObjectId = $currentUser->getObjectId();
*/
//$currentUserObjectId2 = "dDY9Pft5gB";
//$email = $_POST["email"];
//$username = $_POST["username"];


class Webhook extends CI_Controller {

  function __construct()

   {

		parent::__construct();
		
		include(APPPATH.'libraries/keys.php');
       $this->load->model("parse_model", "parse");
       $this->load->model("mail_gun_model", "mailGun");
		
   }
   
	function index() {

    }

    function verify_webhook($data, $hmac_header) {
        $calculated_hmac = base64_encode(hash_hmac('sha256', $data, SHOPIFY_APP_SECRET, true));
        return hash_equals($hmac_header, $calculated_hmac);
    }

	//Shopify Test Hooks
    function newOrder() {
        echo http_response_code(200);


        $storeNameServer = $_SERVER["HTTP_X_SHOPIFY_SHOP_DOMAIN"];
        if ($storeNameServer == "rr-racewear.myshopify.com") {
            $storeName = "RR";
            define('SHOPIFY_APP_SECRET', 'd3ad667f4936f2c94116bb62a8809551299eeb55459006617b093c9aee596da2');
        } else if ($storeNameServer == "directdriverapparel.myshopify.com") {
            $storeName = "DDA";
            define('SHOPIFY_APP_SECRET', 'e8bae231352f4eed23e772035db0b39856d39eb4c70a8ef807e449b3fc9baf8c');
        } else if ($storeNameServer == "rapidware.myshopify.com") {
            $storeName = "RapidWare";
            define('SHOPIFY_APP_SECRET', 'dcc0216b24fa2862952324689361c4020c6620ef620bb5d2c24bfefb680c867a');
        } else if ($storeNameServer == "justsenditapparel.myshopify.com") {
            $storeName = "SIO";
            define('SHOPIFY_APP_SECRET', 'fc52dad37b1ffc4c60e9407632988a0e7c18ce8f6e7b6bea4fb84a347640f348');
        } else {
            $storeName = "";
        }

        $hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
        $data = file_get_contents('php://input');
        $verified = $this->verify_webhook($data, $hmac_header);
        if($verified == true) {
            $event_json = json_decode($data, true);
            if ($storeName != "") {
//                $this->parse->saveOrder($event_json, $storeName);
                $date = $event_json["created_at"];
                $newDate = date("Y/m/d", strtotime($date));
                //Used to show date and time ordered
//                $timeZone = "America/Chicago";
//                $dateNow = new DateTime($date, new DateTimeZone($timeZone));
//                $dateOrderedTime = $dateNow->format('m/d/Y H:i:s');
                $event_json["store"] = $storeName;
                $event_json["created_at_date"] = $newDate;
                $event_json["dateOrderedTime"] = $date;
                $event_json["shipping_address"]["address1"] = htmlentities($event_json["shipping_address"]["address1"]);
                foreach ($event_json["shipping_address"] as $key => $value) {
                    $event_json["shipping_address"][$key] = htmlentities($value);
                }
                foreach ($event_json["customer"] as $key => $value) {
                    if($key == "default_address") {
                        foreach ($event_json["customer"]["default_address"] as $key => $value) {
                            $event_json["customer"]["default_address"][$key] = htmlentities($value);
                        }
                    } else {
                        $event_json["customer"][$key] = htmlentities($value);
                    }
                }

//                ParseCloud::startJob("saveOrder", $event_json);
//                self::startJob("saveOrder", $event_json);
                ParseCloud::run("saveOrder", $event_json);
//                if($storeName == "RapidWare") {
////                    $this->parse->updateProduct($event_json, $storeName);
//                    $test = [];
//                    ParseCloud::startJob("getProducts", $test);
//                }
            }
        }
    }

    function orderUpdated() {
        echo http_response_code(200);
        $storeNameServer = $_SERVER["HTTP_X_SHOPIFY_SHOP_DOMAIN"];
//        if ($storeNameServer == "rr-racewear.myshopify.com") {
//            $storeName = "RR";
//            define('SHOPIFY_APP_SECRET', 'd3ad667f4936f2c94116bb62a8809551299eeb55459006617b093c9aee596da2');
//        } else if ($storeNameServer == "directdriverapparel.myshopify.com") {
//            $storeName = "DDA";
//            define('SHOPIFY_APP_SECRET', 'e8bae231352f4eed23e772035db0b39856d39eb4c70a8ef807e449b3fc9baf8c');
//        } else if ($storeNameServer == "rapidware.myshopify.com") {
//            $storeName = "RapidWare";
//            define('SHOPIFY_APP_SECRET', 'dcc0216b24fa2862952324689361c4020c6620ef620bb5d2c24bfefb680c867a');
//        }  else if ($storeNameServer == "justsenditapparel.myshopify.com") {
//            $storeName = "SIO";
//            define('SHOPIFY_APP_SECRET', 'fc52dad37b1ffc4c60e9407632988a0e7c18ce8f6e7b6bea4fb84a347640f348');
//        } else {
//            $storeName = "";
//        }
        define('SHOPIFY_APP_SECRET', '02d79d42d2d08bc81968434bfd95f57a');
        $hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
        $data = file_get_contents('php://input');
        $verified = $this->verify_webhook($data, $hmac_header);
        if($verified == true) {
            $event_json = json_decode($data, true);
            $event_json["store"] = $storeNameServer;
            ParseCloud::run("orderUpdated", $event_json);
//            if ($storeName != "") {
//                $event_json["store"] = $storeName;
//                $event_json["shipping_address"]["address1"] = htmlentities($event_json["shipping_address"]["address1"]);
//                foreach ($event_json["shipping_address"] as $key => $value) {
//                    $event_json["shipping_address"][$key] = htmlentities($value);
//                }
//                foreach ($event_json["customer"] as $key => $value) {
//                    if($key == "default_address") {
//                        foreach ($event_json["customer"]["default_address"] as $key => $value) {
//                            $event_json["customer"]["default_address"][$key] = htmlentities($value);
//                        }
//                    } else {
//                        $event_json["customer"][$key] = htmlentities($value);
//                    }
//                }
////                self::startJob("orderUpdated", $event_json);
//                ParseCloud::run("orderUpdated", $event_json);
//                if($storeName == "RapidWare") {
////                    $this->parse->updateProduct($event_json, $storeName);
//                    $test = [];
////                    ParseCloud::startJob("getProducts", $test);
//                }
//            }
        } else {
//            $event_json = json_decode($data, true);
//            $event_json["store"] = $storeNameServer;
//            ParseCloud::run("orderUpdated", $event_json);
        }
    }

    function trackingUpdate() {
        echo http_response_code(200);
        $data = @file_get_contents("php://input");
        $event_json = json_decode($data);
        $this->email_model->shipEngine($event_json);
    }

	function paymentFailed(){
		
		
// 		$this->data['id'] = 'pricing-alt';
		
// 		$this->data['body'] = 'register/choose_plan';
				
// 		$this->data['plans']= \Stripe\Plan::all();
        http_response_code(200);
		$input = @file_get_contents("php://input");
		$event_json = json_decode($input);

		if($event_json != ""){
			$event = \Stripe\Event::retrieve($event_json->id);
		}
		// Check against Stripe to confirm that the ID is valid
		
		
		if (isset($event) && $event->type == "invoice.payment_failed") {
		  $customer = \Stripe\Customer::retrieve($event->data->object->customer);
		  $email = $customer->email;
// 		  $customerName = $customer->sources->data->name;
// 		  $customerPlan = $event->data->object->lines->data->plan;
// 		  $statement = $event->data->object->lines->data[plan]->statement_descriptor;
// 		  $invoiceNumber = $event->data->object->receipt_number;
		  // Sending your customers the amount in pennies is weird, so convert to dollars
		  $amount = sprintf('$%0.2f', $event->data->object->amount_due / 100.0);
// 		  $today = date("F j, Y"); 
		  //Get Company Name
/*
		  $query = ParseUser::query();
		  $query->equalTo("customer_id", $customer);
		  $businessName = $query->get($customer);
		  $businessName = $object->get("taxRate");
*/
// 		  $this->data["customerName"] = $customerName;
// 		  $this->data["customerPlan"] = $customerPlan;
		  $this->data["email"] = $email;
		  $this->data["amount"] = $amount;
// 		  $this->data["invoiceNumber"] = $invoiceNumber;
// 		  $this->data["statement"] = $statement;
// 		  $this->data["date"] = $today;
// 		  $this->data["businessName"] = $businessName;
		} else {
			$this->data["amount"] = "";
		}

		$this->load->view('paymentFailed', $this->data);
	}

	function customerDataRequest() {
        echo http_response_code(200);
        $storeNameServer = $_SERVER["HTTP_X_SHOPIFY_SHOP_DOMAIN"];
        define('SHOPIFY_APP_SECRET', '02d79d42d2d08bc81968434bfd95f57a');
        $hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
        $data = file_get_contents('php://input');
        $verified = $this->verify_webhook($data, $hmac_header);
        if($verified == true) {
            $event_json = json_decode($data, true);
            $this->mailGun->customerDataRequestEmail($event_json);
        }
    }

    function customerDataErase() {
        echo http_response_code(200);
        $storeNameServer = $_SERVER["HTTP_X_SHOPIFY_SHOP_DOMAIN"];
        define('SHOPIFY_APP_SECRET', '02d79d42d2d08bc81968434bfd95f57a');
        $hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
        $data = file_get_contents('php://input');
        $verified = $this->verify_webhook($data, $hmac_header);
        if($verified == true) {
            $event_json = json_decode($data, true);
            $this->mailGun->customerDataEraseEmail($event_json);
        }
    }

    function shopDataErase() {
        echo http_response_code(200);
        $storeNameServer = $_SERVER["HTTP_X_SHOPIFY_SHOP_DOMAIN"];
        define('SHOPIFY_APP_SECRET', '02d79d42d2d08bc81968434bfd95f57a');
        $hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
        $data = file_get_contents('php://input');
        $verified = $this->verify_webhook($data, $hmac_header);
        if($verified == true) {
            $event_json = json_decode($data, true);
            $this->mailGun->shopDataEraseEmail($event_json);
        }
    }

    function shopRemoved() {
        echo http_response_code(200);
        $storeNameServer = $_SERVER["HTTP_X_SHOPIFY_SHOP_DOMAIN"];
        define('SHOPIFY_APP_SECRET', '02d79d42d2d08bc81968434bfd95f57a');
        $hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
        $data = file_get_contents('php://input');
        $verified = $this->verify_webhook($data, $hmac_header);
        if($verified == true) {
            $event_json = json_decode($data, true);
            $storeId = $event_json["id"];
            $this->mailGun->shopRemovedEmail($event_json);
            //Delete Store
            $storeQuery = $this->parse->getParseClass("Stores", "shopifyId", $storeId);
            $companyId = $storeQuery[0]->get("companyId");
            if($storeQuery[0]->get("payingStore") == "true") {
                //Make company status inactive
                $array = ["status" => "inactive"];
                $this->parse->updateObject("Companies", $array, $companyId);
            }
            //Mark orders inactive
            $orders = $this->parse->getParseClassArray("Orders", ["storeName" => $storeQuery[0]->get("shopifyName")]);
            foreach ($orders as $order) {
                $this->parse->updateObject("Orders", ["status" => "inactive"], $order->getObjectId());
            }
            //Mark store inactive
            $this->parse->updateObject("Stores", ["status" => "inactive"], $storeQuery[0]->getObjectId());
        }
    }
}