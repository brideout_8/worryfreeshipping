<?php
/**
 * Created by PhpStorm.
 * User: rapidware
 * Date: 7/21/18
 * Time: 10:32 AM
 */


include(APPPATH.'libraries/allLibraries.php');

defined('BASEPATH') OR exit('No direct script access allowed');

// Add the "use" declarations where you'll be using the classes
use Parse\ParseClient;
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use ZfrShopify\OAuth\TokenExchanger;
use ZfrShopify\OAuth\AuthorizationRedirectResponse;
use ZfrShopify\Exception\InvalidApplicationProxyRequestException;
use ZfrShopify\Validator\ApplicationProxyRequestValidator;
use ZfrShopify\Exception\InvalidWebhookException;
use ZfrShopify\Validator\WebhookValidator;
use ZfrShopify\ShopifyClient;
use Mailgun\Mailgun;

if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Settings extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        include(APPPATH . 'libraries/keys.php');
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model("parse_model", "parse");
        $this->load->model("functions");
        $this->load->model("mail_gun_model", "mailGun");
        $this->load->model("ship_engine_model", 'shipEngine');
        $this->load->helper('download');
        $this->load->model("dashboard_model", "dashboard");
        $this->load->model("shopify_model");
        $this->load->helper("shopify");

        if ($this->session->userId == '') {
            redirect($this->config->item('base_url'), 'Location');
        }
    }

    function index() {
        $this->data["menu"] = $this->uri->segment(3);
        $this->load->view("navigation/header", $this->data);
        $this->load->view('employee/dashboard', $this->data);
    }

    function stores() {
        if(getTrialStatus() == "expired") {
            //Not in subscription
            $this->data["menu"] = "settings";
            $this->load->view("navigation/header", $this->data);
            $this->load->view("settings/account/selectPlan", $this->data);
        } else {
            $this->data["menu"] = "stores";
            $this->load->view("navigation/header", $this->data);
            $this->load->view("settings/store/stores", $this->data);
        }
    }

    function shipping() {
        if(getTrialStatus() == "expired") {
            //Not in subscription
            $this->data["menu"] = "settings";
            $this->load->view("navigation/header", $this->data);
            $this->load->view("settings/account/selectPlan", $this->data);
        } else {
            $this->data["menu"] = "shipping";
            $shippingInfo = $this->parse->getParseClass("Companies", "objectId", $this->session->companyId);
            $this->data["shippingInfo"] = $shippingInfo;
//        $this->data["boxes"] = json_decode($shippingInfo[0]->get("shippingFrom"));
            $this->data["companyId"] = $shippingInfo[0]->getObjectId();
            $this->data["carrierPackages"] = $this->parse->getAllParseClass("CarrierPackages");
            $this->load->view("navigation/header", $this->data);
            $this->load->view("settings/shipping/dashboard", $this->data);
            $this->load->view("settings/shipping/modals/dashboard", $this->data);
        }
    }

    function getShippingInfo() {
        $shippingInfo = $this->parse->getParseClass("Companies", "objectId", $this->session->companyId);
        //Update Funds Amount
        $carriers = $this->parse->getParseClass("Carriers", "companyId", $this->session->companyId);
        foreach ($carriers as $carrier) {
            if($carrier->get("name") == "Stamps.com" || $carrier->get("name") == "Endicia") {
                if($carrier->get("status") == "endabled") {
                    $funds = $this->shipEngine->getFunds($this->session->shipEngineAPIKey, $carrier->getObjectId());
                    $objectArray = ["funds" => $funds];
                    $this->parse->updateObject("Carriers", $objectArray, $carrier->get("carrierId"));
                }
            }
        }
        $this->data["shippingInfo"] = $shippingInfo;
        $this->data["packages"] = $this->parse->getParseClass("Packages", "companyId", $this->session->companyId);
        $this->data["locations"] = $this->parse->getParseClass("Locations", "companyId", $this->session->companyId);
        $this->data["carriers"] = $carriers;
        return $this->load->view("settings/shipping/shippingTable", $this->data);
    }

    function refreshLocations() {
        $companyInfo = $this->parse->getParseClassArray("Companies", ["objectId" => $this->session->companyId]);
        foreach ($this->session->shopifyStoresInfo as $shopifyStore) {
            $newLocationsFind = $this->shopify_model->getLocations($shopifyStore["shopifyToken"], $shopifyStore["shopifyName"]);
            $shopInfo = $this->shopify_model->getShopInfo($shopifyStore["shopifyToken"], $shopifyStore["shopifyName"]);
            if(!isset($newLocationsFind["error"])) {
                $newLocations = $newLocationsFind["locations"];
                foreach ($newLocations as $newLocation) {
                    $currentLocationFind = $this->parse->getParseClassArray("Locations", ["locationId" => $newLocation->id]);
                    $locationArray = [
                        "locationId" => $newLocation->id,
                        "companyId" => $this->session->companyId,
                        "name" => $newLocation->name,
                        "phone" => $companyInfo[0]->get("phone"),
                        "address1" => $newLocation->address1,
                        "address2" => $newLocation->address2,
                        "city" => $newLocation->city,
                        "state" => $newLocation->province,
                        "stateCode" => $newLocation->province_code,
                        "zip" => $newLocation->zip,
                        "country" => $newLocation->country_name,
                        "countryCode" => $newLocation->country_code,
                        "legacy" => $newLocation->legacy,
                        "shopifyName" => $shopifyStore["shopifyName"]
                    ];
                    if ($shopInfo->primary_location_id == $newLocation->id) {
                        $locationArray["default"] = "true";
                        $locationArray["returnDefault"] = "true";
                    } else {
                        $locationArray["default"] = "false";
                        $locationArray["returnDefault"] = "false";
                    }
                    if(count($currentLocationFind) > 0) {
                        //Location exists. Just update it
                        $updateLocation = $this->parse->updateObject("Locations", $locationArray, $currentLocationFind[0]->getObjectId());
                        $updateWarehouse =  $this->shipEngine->updateWarehouse($newLocation, $this->session->shipEngineAPIKey, $companyInfo[0]->get("phone"), $companyInfo[0]->get("name"), $currentLocationFind[0]->get("warehouseId"));
                        $result["success"] = 1;
                        $result["updateLocation"] = $updateLocation;
                        $result["updateWarehouse"] = $updateWarehouse;
                        $result["shopInfo"] = $shopInfo;

                    } else {
                        //New Location
                        $warehouse = $this->shipEngine->createWarehouse($newLocation, $this->session->shipEngineAPIKey, $companyInfo[0]->get("phone"), $companyInfo[0]->get("name"));
                        $locationArray["warehouseId"] = $warehouse->warehouse_id;
                        $saveLocation = $this->parse->newObject("Locations", $locationArray);
                        $result["success"] = 1;
                        $result["saveLocation"] = $saveLocation;
                        $result["warehouse"] = $warehouse;
                    }

                }
            } else {
                //Error
                $result["error"] = 1;
                $result["message"] = $newLocationsFind["error"];
            }
        }

        echo json_encode($result);
    }

    function stampsSignUp() {
        $shippingInfo = $this->parse->getParseClass("Companies", "objectId", $this->session->companyId);
        $this->data["carrierObjectId"] = $this->input->get("id");
        $this->data["shippingFrom"] = $shippingInfo[0];
        $this->data["menu"] = "shipping";
        $this->load->view("navigation/header", $this->data);
        $this->load->view("settings/shipping/stampsSetUp", $this->data);
    }

    function createStampsAccount() {
        $stampsForm = [];
        foreach ($_POST as $key => $value) {
            if($key == "country") {
                if($value == "United States") {
                    $stampsForm["countryCode"] = "US";
//                    $shippingFrom[$key] = $value;
                } else if($value == "Canada") {
                    $stampsForm["countryCode"] = "CA";
//                    $shippingFrom[$key] = $value;
                } else if($value == "United Kingdom") {
                    $stampsForm["countryCode"] = "GB";
//                    $shippingFrom[$key] = $value;
                } else if($value == "Australia") {
                    $stampsForm["countryCode"] = "AU";
//                    $shippingFrom[$key] = $value;
                }
            } else if($key != "companyId") {
                $stampsForm[$key] = $value;
            }
        }
        $accountInfo = $this->shipEngine->createStampsAccount($stampsForm, $this->session->shipEngineAPIKey);
        if(isset($accountInfo->errors)) {
            $result["error"] = 1;
            $result["message"] = $accountInfo->errors[0]->message;
        } else {
            //Save carrierId
            $carriersArray = [];
            foreach ($this->session->carriers as $carrierService => $carrierId) {
                $carriersArray[$carrierService] = $carrierId;
            }
            $carriersArray["usps"] = $accountInfo->carrier_id;
            $updateArray = [
                "carrierId" => $accountInfo->carrier_id,
                "status" => "enabled"
            ];
            $this->parse->updateObject("Carriers", $updateArray, $this->input->post("carrierObjectId"));
            $result["success"] = 1;
        }
        echo json_encode($result);
    }

    function changeShippingAccountStatus() {
        $carrierId = $this->input->get("id");
        $statusChangeTo = $this->input->get("status");
        $carrierInfo = $this->parse->getParseClass("Carriers", "objectId", $carrierId);
        $objectArray = [
            "status" => $statusChangeTo
        ];
        $update = $this->parse->updateObject("Carriers", $objectArray, $carrierId);
        $service = $carrierInfo[0]->get("service");
        if($update == true) {
            //Update user data array
            $carrierArray = [];
            if(count($this->session->carriers) > 0) {
                foreach ($this->session->carriers as $carrierType => $shipEngineId) {
                    if ($carrierType != $service) {
                        $carrierArray[$carrierType] = $shipEngineId;
                    }
                    if ($statusChangeTo == "enabled") {
                        //Add to user data array
                        $carrierArray[$service] = $carrierInfo[0]->get("carrierId");
                    }
                }
            } else {
                $carrierArray[$service] = $carrierInfo[0]->get("carrierId");
            }
            $this->session->carriers = $carrierArray;
            //Disable other similar shipping accounts
            $findArray = [
                "status" => "enabled",
                "service" => $service,
                "companyId" => $this->session->companyId
            ];
            $otherCarriers = $this->parse->getParseClassArray("Carriers", $findArray);
            foreach ($otherCarriers as $carrier) {
                if($carrier->getObjectId() != $carrierId) {
                    $changeArray = [
                        "status" => "disabled"
                    ];
                    $this->parse->updateObject("Carriers", $changeArray, $carrier->getObjectId());
                }
            }
            $result["success"] = 1;
            $result["test"] = $carrierArray;
            $result["hi"] = count($otherCarriers);
            $result["he"] = $carrierId;
        } else {
            $result["error"] = 1;
            $result["message"] = $update;
        }

        echo json_encode($result);
    }

    function addFundsToCarrier() {
        $addFunds = $this->shipEngine->addFundsToCarrier($this->input->post("amount"), $this->session->shipEngineAPIKey, $this->input->post("carrierId"));
        if(isset($addFunds->errors)) {
            $result["error"] = 1;
            $result["message"] = $addFunds->errors[0]->message;
        } else {
            $result["success"] = 1;
        }

        echo json_encode($result);
    }

    function saveShippingFrom() {
        $shippingFrom = [];
        foreach ($_POST as $key => $value) {
            if($key == "country") {
                if($value == "United States") {
                    $shippingFrom["countryCode"] = "US";
                    $shippingFrom[$key] = $value;
                } else if($value == "Canada") {
                    $shippingFrom["countryCode"] = "CA";
                    $shippingFrom[$key] = $value;
                } else if($value == "United Kingdom") {
                    $shippingFrom["countryCode"] = "GB";
                    $shippingFrom[$key] = $value;
                } else if($value == "Australia") {
                    $shippingFrom["countryCode"] = "AU";
                    $shippingFrom[$key] = $value;
                }
            } else if($key != "companyId") {
                $shippingFrom[$key] = $value;
            }
        }
        $objectArray = [
            "shippingFrom" => json_encode($shippingFrom)
        ];
        $save = $this->parse->updateObject("Companies", $objectArray, $this->input->post("companyId"));
        if($save == true) {
            $result["success"] = 1;
        } else {
            $result["error"] = 1;
            $result["message"] = $save;
        }

        echo json_encode($result);
    }

    function addPackage() {
        $currentBoxes = $this->parse->getParseClass("Packages", "companyId", $this->input->post("companyId"));
        $objectArray = [];
        if($this->input->post("packageDetail") != "carrier") {
            $sizeUnits = $this->input->post("sizeUnits");
            if ($sizeUnits == "in") {
                //converting to cm
                $length = $this->input->post("length") * 2.54;
                $width = $this->input->post("width") * 2.54;
                $height = $this->input->post("height") * 2.54;
            } else {
                $length = $this->input->post("length");
                $width = $this->input->post("width");
                $height = $this->input->post("height");
            }
            $weightUnits = $this->input->post("weightUnits");
            $packageWeight = $this->input->post("packageWeight");
            if ($weightUnits == "oz") {
                $packageWeight = $packageWeight * 28.34952;
            } else if ($weightUnits == "lb") {
                $packageWeight = $packageWeight * 453.59237;
            } else if ($weightUnits == "kg") {
                $packageWeight = $packageWeight / 1000;
            }
            if (count($currentBoxes) < 1 || $this->input->post("default") == "1") {
                $defaultBox = true;
            } else {
                $defaultBox = false;
            }
            $objectArray["length"] = $length;
            $objectArray["width"] = $width;
            $objectArray["height"] = $height;
            $objectArray["sizeUnits"] = $sizeUnits;
            $objectArray["weightUnits"] = $weightUnits;
            $objectArray["weight"] = $packageWeight;
            $objectArray["packageType"] = "package";
            $objectArray["carrierType"] = "none";
            $objectArray["name"] = $this->input->post("packageName");
        } else {
            $carrierPackages = $this->parse->getParseClass("CarrierPackages", "objectId", $this->input->post("carrierPackageType"));
            if (count($currentBoxes) < 1 || $this->input->post("default") == "1") {
                $defaultBox = true;
            } else {
                $defaultBox = false;
            }

            $objectArray["weightUnits"] = "g";
            $objectArray["weight"] = $carrierPackages[0]->get("weight");
            $objectArray["packageType"] = $carrierPackages[0]->get("packageCode");
            $objectArray["carrierType"] = $carrierPackages[0]->get("carrierType");
            $objectArray["name"] = $carrierPackages[0]->get("name");
        }

        $objectArray["companyId"] = $this->input->post("companyId");


        $save = $this->parse->newObject("Packages", $objectArray);
        if($save[0] == true) {
            $result["success"] = 1;
            $result["save"] = $save;
            //Save default box to company
            if($defaultBox) {
                $companyInfo = $this->parse->getParseClass("Companies", "objectId", $this->input->post("companyId"));
                $companyInfo[0]->set("defaultBox", $save[1]);
                try {
                    $companyInfo[0]->save(true);
                } catch (ParseException $ex) {
                    $error = $ex->getMessage();
                }
            }
        } else {
            $result["error"] = 1;
            $result["message"] = $save;
        }

        echo json_encode($result);
    }

    function updatePackage() {
        $currentBoxes = $this->parse->getParseClass("Packages", "companyId", $this->input->post("companyId"));
        if($this->input->post("packageDetail") != "carrier") {
            $sizeUnits = $this->input->post("sizeUnitsEdit");
            if ($sizeUnits == "in") {
                //converting to cm
                $length = $this->input->post("length") * 2.54;
                $width = $this->input->post("width") * 2.54;
                $height = $this->input->post("height") * 2.54;
            } else {
                $length = $this->input->post("length");
                $width = $this->input->post("width");
                $height = $this->input->post("height");
            }
            $weightUnits = $this->input->post("weightUnitsEdit");
            $packageWeight = $this->input->post("packageWeight");
            if ($weightUnits == "oz") {
                $packageWeight = $packageWeight * 28.34952;
            } else if ($weightUnits == "lb") {
                $packageWeight = $packageWeight * 453.59237;
            } else if ($weightUnits == "kg") {
                $packageWeight = $packageWeight / 1000;
            }
            if (count($currentBoxes) < 1 || $this->input->post("default") == "1") {
                $defaultBox = true;
            } else {
                $defaultBox = false;
            }
            $objectArray = [
                "length" => floatval($length),
                "width" => floatval($width),
                "height" => floatval($height),
                "sizeUnits" => $sizeUnits,
                "weight" => floatval($packageWeight),
                "weightUnits" => $weightUnits,
                "name" => $this->input->post("packageName"),
                "companyId" => $this->input->post("companyId")
            ];
            $packageId = $this->input->post("packageId");
        } else {
            if (count($currentBoxes) < 1 || $this->input->post("default") == "1") {
                $defaultBox = true;
            } else {
                $defaultBox = false;
            }
            $objectArray = [
                "companyId" => $this->input->post("companyId")
            ];
            $packageId = $this->input->post("carrierPackageId");
        }
        $save = $this->parse->updateObject("Packages", $objectArray, $packageId);
        if($save == true) {
            $result["success"] = $save;
            if($defaultBox == true) {
                $companyInfo = $this->parse->getParseClass("Companies", "objectId", $this->input->post("companyId"));
                $companyInfo[0]->set("defaultBox", $packageId);
                try {
                    $companyInfo[0]->save(true);
                } catch (ParseException $ex) {
                    $error = $ex->getMessage();
                }
            }
        } else {
            $result["error"] = 1;
            $result["message"] = $save;
        }

        echo json_encode($result);
    }

    function deletePackage() {
        if($this->input->post("packageDetail") != "carrier") {
            $packageId = $this->input->post("packageId");
        } else {
            $packageId = $this->input->post("carrierPackageId");
        }
        $delete = $this->parse->deleteObject("Packages", "objectId", $packageId);
        if($delete == true) {
            $result["success"] = 1;
        } else {
            $result["error"] = 1;
            $result["message"] = $delete;
        }
        echo json_encode($result);
    }

    function updateLabel() {
        $objectArray = [
            "labelFormat" => $this->input->post("labelFormat")
        ];
        $save = $this->parse->updateObject("Companies", $objectArray, $this->input->post("companyId"));
        if($save == true) {
            $result["success"] = 1;
        } else {
            $result["error"] = 1;
            $result["message"] = $save;
        }

        echo json_encode($result);
    }

    function billing() {
        $this->data["menu"] = "billing";
        $this->load->view("navigation/header", $this->data);
        $this->load->view("settings/billing/dashboard", $this->data);
    }

    function getBillingInfo() {
        $bilingInfo = $this->parse->getParseClass("Companies", "objectId", $this->session->companyId);
        $this->data["billingInfo"] = $bilingInfo;
        $this->data["labelsPerPlan"] = getSubLevel($bilingInfo[0]->get("subscriptionLevel"));
        $array = ["companyId" => $this->session->companyId, "chargeType" => "recurring"];
        $invoices = $this->parse->getParseClassDsc("Invoices", $array, "billingDate");
        if(count($invoices) > 0) {
            //Have a previous invoice
            $lastId = $invoices[0]->get("shopifyId");
            $chargeResults = $this->shopify_model->getRecurringCharges($lastId);
        } else {
            //No invoices found
            $chargeResults = $this->shopify_model->getRecurringCharges("none");
        }
        if(!isset($chargeResults["error"])) {
            //Save the new invoices
            foreach ($chargeResults["results"] as $result) {
                if($result->test != null) {
                    $price = $result->price;
                } else {
                    $price = 0;
                }
//                $currentStartDate = date_create($result->billing_on);
                $startDate = date_create($result->billing_on);
//                $billingDate = $result->billing_on;
                $billingPeriodEndDate = date_add(date_create($result->billing_on), date_interval_create_from_date_string("30 days"));
                $invoiceArray = ["shopifyId" => $result->id, "price" => $price, "billingDate" => $startDate->format("Y-m-d"), "chargeType" => "recurring", "companyId" => $this->session->companyId, "billingPeriodStartDate" =>  $startDate->format("Y-m-d"), "billingPeriodEndDate" => $billingPeriodEndDate->format("Y-m-d")];
                $test = $this->parse->newObject("Invoices", $invoiceArray);
            }
            $array = ["companyId" => $this->session->companyId];
            $newInvoices = $this->parse->getParseClassDsc("Invoices", $array, "billingDate");
            $this->data["invoices"] = $newInvoices;
        } else {
            //Show Error
            $this->data["error"] = $chargeResults["error"];
        }
        $startDate = date_create($this->session->currentBillingPeriodStartDate);
        $endDateCreate = date_create($this->session->currentBillingPeriodStartDate);
        $endDate = date_add($endDateCreate, date_interval_create_from_date_string("30 days"));
        $this->data["endDate"] = $endDate->format("M d, Y");
        $this->data["startDate"] = $startDate->format("M d, Y");
        return $this->load->view("settings/billing/billingTable", $this->data);
    }

    function updateBilling() {
        $objectArray = [];
        foreach ($_POST as $key => $value) {
            if($key == "country") {
                if($value == "United States") {
                    $objectArray["countryCode"] = "US";
                    $objectArray[$key] = $value;
                } else if($value == "Canada") {
                    $objectArray["countryCode"] = "CA";
                    $objectArray[$key] = $value;
                } else if($value == "United Kingdom") {
                    $objectArray["countryCode"] = "GB";
                    $objectArray[$key] = $value;
                } else if($value == "Australia") {
                    $objectArray["countryCode"] = "AU";
                    $objectArray[$key] = $value;
                }
            } else if($key != "companyId") {
                $objectArray[$key] = $value;
            }
        }
        $save = $this->parse->updateObject("Companies", $objectArray, $this->input->post("companyId"));
        if($save == true) {
            $result["success"] = 1;
        } else {
            $result["error"] = 1;
            $result["message"] = $save;
        }

        echo json_encode($result);
    }

    function invoice() {
        $invoiceId = $this->input->get("invoice");
        $this->data["invoiceId"] = $invoiceId;
        $this->data["menu"] = "billing";
        $this->load->view("navigation/header", $this->data);
        $this->load->view("settings/billing/viewInvoice", $this->data);
    }

    function getInvoice() {
        $invoiceId = $this->input->post("invoiceId");
        $this->data["invoice"] = $this->parse->getParseClass("Invoices", "objectId", $invoiceId);
        $this->load->view("settings/billing/viewInvoiceTable", $this->data);
    }

    function account() {
        $this->data["menu"] = "account";
        $this->load->view("navigation/header", $this->data);
        $this->load->view("settings/account/dashboard", $this->data);
    }

    function getAccountInfo() {
        $accountInfo = $this->parse->getParseClass("Companies", "objectId", $this->session->companyId);
        $this->data["accountInfo"] = $accountInfo;
        $this->data["logo"] = $accountInfo[0]->get("logo");
        if($this->session->masterUserId != "") {
            //This user is not the account owner
            $this->data["accountOwnerInfo"] = $this->parse->getUserData($this->session->masterUserId);
            $objectArray = [
                "masterUserId" => $this->session->masterUserId
            ];
            $this->data["staffInfo"] = $this->parse->getParseClassArray("_User", $objectArray);
        } else {
            //This user is the account owner
            $this->data["accountOwnerInfo"] = $this->parse->getUserData($this->session->userId);
            $objectArray = [
                "masterUserId" => $this->session->userId
            ];
            $this->data["staffInfo"] = $this->parse->getParseClassArray("_User", $objectArray);
            $currentStartDate = date_create($accountInfo[0]->get("billingPeriodStartDate"));
            $currentEndDate = date_add($currentStartDate,date_interval_create_from_date_string("30 days"));
            $todaysDate = date_create();
            $diff = date_diff($currentEndDate, $todaysDate);
            $this->data["diff"] = $diff->days;
        }
        $timezones = $this->functions->getTimeZones();
        $this->data["timeZones"] = $timezones;
        return $this->load->view("settings/account/accountTable", $this->data);
    }

    function uploadLogo() {
        $this->data["menu"] = "account";
        $this->load->view("navigation/header", $this->data);
        $this->load->view("settings/account/uploadLogo", $this->data);
    }

    function saveLogo() {
        $imageName = $_FILES['file']['name'];
        $fileName = str_replace(' ', '', $imageName); //Removes Spaces
        $correctedFileName = preg_replace('/[^A-Za-z0-9\-]/', '', $fileName); //Removes bad characters
        $file = ParseFile::createFromData( file_get_contents( $_FILES['file']['tmp_name'] ), $correctedFileName );
        $file->save();
        $array = ["logo" => $file];
        $saveLogo = $this->parse->updateObject("Companies", $array, $this->session->companyId);
    }

    function addUser() {
        if($this->session->subscriptionLevel == "1") {
            $this->data["shopifyStore"] = $this->session->shopifyStore;
            $this->data["accessToken"] = $this->session->shopifyToken;
            $this->data["menu"] = "Account";
            $this->load->view("navigation/header", $this->data);
            $this->load->view("register/shopify/changePlan", $this->data);
        } else {
            $this->data["menu"] = "account";
            $this->load->view("navigation/header", $this->data);
            $this->load->view("settings/account/addUser", $this->data);
        }
    }

    function createClient() {
        if($this->input->post("firstName") == "" || $this->input->post("lastName") == "" || $this->input->post("email") == "" || $this->input->post("username") == "") {
            $result["error"] = 1;
            $result["message"] = "First name, last name, username, and email are required";
        } else {
            $permissions = [];
            $hasPermissions = false;
            if($this->input->post("fullAccess")) {
                array_push($permissions, "fullAccess");
                $hasPermissions = true;
            } else {
                foreach ($this->input->post() as $key => $value) {
                    if($key == "firstName") {

                    } else if ($key == "lastName") {

                    } else if ($key == "email") {

                    } else if ($key == "username") {

                    } else {
                        array_push($permissions, $value);
                        $hasPermissions = true;
                    }
                }
            }
            if($hasPermissions) {
                $currentUser = ParseUser::getCurrentUser();
                if($this->session->masterUserId != "") {
                    $masterUserId = $this->session->masterUserId;
                } else {
                    $masterUserId = $currentUser->getObjectId();
                }
                $token = $currentUser->getSessionToken();
                $user = new ParseUser();
                $user->set("firstName", $this->input->post("firstName"));
                $user->set("lastName", $this->input->post("lastName"));
                $user->set("name", $this->input->post("firstName") . " " . $this->input->post("lastName"));
                $user->set("username", $this->input->post("username"));
                $user->set("email", $this->input->post("email"));
                $user->setArray("permissions", $permissions);
                $user->set("role", "user");
                $user->set("masterUserId", $masterUserId);
                $user->set("companyId", $this->session->companyId);
                $user->set("password", "400Art101");
                $setPasswordToken = uniqid() . time();
                $user->set("setPasswordToken", $setPasswordToken);
                $user->set("status", "active");
                try {
                    $user->signUp();
                    try {
                        $user = ParseUser::become($token);
                        // The current user is now set to user.
                    } catch (ParseException $ex) {
                        // The token could not be validated.
                    }
                    $url = base_url() . "register?token=" . @$setPasswordToken;
                    $sent = $this->mailGun->addUser($this->input->post("email"), $this->input->post("firstName"), $this->input->post("lastName"), $url, $this->session->name, $this->input->post("username"));
                    $result["success"] = $sent;
                } catch (ParseException $ex) {
                    $result["error"] = 1;
                    $result["message"] = $ex->getMessage();
                }
            } else {
                $result["error"] = 1;
                $result["message"] = "Please select permissions";
            }
        }
        echo json_encode($result);
    }

    function editUser() {
        $userId = $this->input->get("id");
        $this->data["userInfo"] = $this->parse->getUserData($userId);
        $this->data["menu"] = "account";
        $this->load->view("navigation/header", $this->data);
        $this->load->view("settings/account/editUser", $this->data);
    }

    function updateClient() {
        if($this->input->post("firstName") == "" || $this->input->post("lastName") == "" || $this->input->post("email") == "" || $this->input->post("username") == "") {
            $result["error"] = 1;
            $result["message"] = "First name, last name, username, and email are required";
        } if($this->input->post("password") != $this->input->post("passwordConfirm")) {
            $result["error"] = 1;
            $result["message"] = "Passwords do not match.";
        } else {
            $permissions = [];
            $objectArray = [];
            $hasPermissions = false;
            if($this->input->post("fullAccess")) {
                array_push($permissions, "fullAccess");
                $hasPermissions = true;
            } else {
                foreach ($this->input->post() as $key => $value) {
                    if($key == "firstName") {
                        $objectArray[$key] = $value;
                    } else if ($key == "lastName") {
                        $objectArray[$key] = $value;
                    } else if ($key == "email") {
                        $objectArray[$key] = $value;
                    } else if ($key == "password") {
                        $objectArray[$key] = $value;
                    } else if ($key == "passwordConfirm") {

                    } else if ($key == "phone") {
                        $objectArray[$key] = $value;
                    } else if ($key == "userId") {

                    } else if ($key == "username") {
                        $objectArray[$key] = $value;
                    } else {
                        array_push($permissions, $value);
                        $hasPermissions = true;
                    }
                }
            }
            if($hasPermissions) {
                $objectArray["permissions"] = $permissions;
                $saveUser = $this->parse->updateUser($this->input->post("userId"), $objectArray);
                if($saveUser["result"] == "true") {
                    $result["success"] = 1;
                } else {
                    $result["error"] = 1;
                    $result["message"] = $saveUser["message"];
                }
            } else {
                $result["error"] = 1;
                $result["message"] = "Please select permissions";
            }
        }
        echo json_encode($result);
    }

    function deactivateClient() {
        $status = $this->input->get("status");
        $userId = $this->input->post("userId");
        $objectArray = ["status" => $status];
        $saveUser = $this->parse->updateUser($userId, $objectArray);
        if($saveUser == true) {
            $result["success"] = 1;
        } else {
            $result["error"] = 1;
            $result["message"] = $saveUser;
        }
        echo json_encode($result);
    }

    function deleteClient() {
        $userId = $this->input->post("userId");
        $delete = $this->parse->deleteObject("_User", "objectId", $userId);
        if($delete == true) {
            $result["success"] = 1;
        } else {
            $result["error"] = 1;
            $result["message"] = $delete;
        }
        echo json_encode($result);
    }

    function updateAccount() {
        $objectArray = [];
        foreach ($_POST as $key => $value) {
            if($key != "companyId") {
                $objectArray[$key] = $value;
            }
        }
        $save = $this->parse->updateObject("Companies", $objectArray, $this->input->post("companyId"));
        if($save == true) {
            $result["success"] = 1;
            $this->session->unitSystem = $this->input->post("unitSystem");
            $this->session->weightMeasurement = $this->input->post("weightMeasurement");
            $this->session->timeZone = $this->input->post("timeZone");
        } else {
            $result["error"] = 1;
            $result["message"] = $save;
        }

        echo json_encode($result);
    }

    function getStores() {
        $this->data["stores"] = $this->parse->getParseClassArray("Stores", ["companyId" => $this->session->companyId, "status" => "active"]);
        return $this->load->view("employee/storesTable", $this->data);
    }

    function addStore() {
        if($this->session->subscriptionLevel == "1") {
            //Not in subscription
            $this->data["shopifyStore"] = $this->session->shopifyStore;
            $this->data["accessToken"] = $this->session->shopifyToken;
            $this->data["menu"] = "settings";
            $this->load->view("navigation/header", $this->data);
            $this->load->view("register/shopify/changePlan", $this->data);
        } else {
            $this->data["menu"] = "stores";
            $this->load->view("navigation/header", $this->data);
            $this->load->view("settings/store/addStore", $this->data);
        }
    }
    //Takes user to authorize screen in shopify
    function newStoreInstall() {
        $shop = $this->input->post("shopName");
        $keys = $this->parse->shopifyKeys();
        $scopes = "read_orders,write_orders,read_products,write_products,read_inventory,write_inventory,read_customers,read_locations";
        $install_url = "https://worryfreeshipping.com/settings/auth";
        $url =  'https://'.$shop. '/admin/oauth/authorize?client_id=' . $keys["ApiKey"]. '&redirect_uri=' . $install_url . "&scope=$scopes";
        $result["success"] = 1;
        $result["url"] = $url;
        echo json_encode($result);
    }
    //Shopify sends back here when user authorizes new store
    function auth() {
        $shop = $this->input->get("shop");
        $this->parse->shopifyKeysStore($shop);
        $accessToken = \PHPShopify\AuthHelper::getAccessToken();
        //Get store id
        $headers = array(
            'Content-Type' => 'application/json',
            'X-Shopify-Access-Token' => $accessToken
        );
        $response = Requests::get('https://'.$shop.'/admin/shop.json', $headers);
        $shopBody = json_decode($response->body);
        $shopInfo = $shopBody->shop;
        //Save accessToken, shop name, display name
        $objecyArray = [
            "shopifyName" => $shop,
            "token" => $accessToken,
            "companyId" => $this->session->companyId,
            "shopifyId" => $shopInfo->id
        ];
        //Update Userdata
        $saveShop = $this->parse->newObject("Stores", $objecyArray);
        if($saveShop[0] == true) {
            //Set up webhooks on client store
            $webhooks = $this->functions->addWebhooks($shop,$accessToken, $saveShop[1]);
            $stores = $this->parse->getParseClass("Stores", "userId", $this->session->userId);
//            $shopifyTokensArray = [];
            $shopifyStoresArray = [];
            $shopifyDisplayNames = [];
            foreach ($stores as $store) {
                array_push($shopifyStoresArray, $store->get("shopifyName"));
                array_push($shopifyTokensArray, $store->get("token"));
                array_push($shopifyDisplayNames, $store->get("displayName"));
            }
            $this->session->set_userdata(
                array(
                    "shopifyStores" => $shopifyStoresArray,
                    "shopifyTokens" => $shopifyTokensArray,
                   "shopifyDisplayNames" => $shopifyDisplayNames
                )
            );
            $this->data["storeName"] = $shop;
            $this->data["shopifyToken"] = $accessToken;
            $this->data["menu"] = "stores";
            $this->load->view("navigation/header", $this->data);
            $this->load->view("settings/success", $this->data);
        } else {
            $this->data["menu"] = "stores";
            $this->data["saveShop"] = $saveShop[0];
            $this->load->view("navigation/header", $this->data);
            $this->load->view("settings/error", $this->data);
        }
    }

    function editStore() {
        $this->data["storeId"] = $this->input->get("storeId");
        $this->data["menu"] = "stores";
        $this->load->view("navigation/header", $this->data);
        $this->load->view("settings/store/editStore", $this->data);
    }

    function editStoreTable() {
        $this->data["storeInfo"] = $this->parse->getParseClass("Stores", "objectId", $this->input->post("storeId"));
        return $this->load->view("settings/store/editStoreTable", $this->data);
    }

    function updateStore() {
        $objectArray = [
            "displayName" => $this->input->post("displayName")
        ];
        $save = $this->parse->updateObject("Stores", $objectArray, $this->input->post("storeId"));
        if($save == true) {
            $storesArray = [];
            foreach ($this->session->shopifyStoresInfo as $storeInfo) {
                if($storeInfo["shopifyName"] == $this->input->post("storeName")) {
                    $storeInfo["shopifyDisplayName"] = $this->input->post("displayName");
                    $storesArray[$storeInfo["shopifyName"]] = $storeInfo;
                } else {
                    $storesArray[$storeInfo["shopifyName"]] = $storeInfo;
                }
            }
            $this->session->shopifyStoresInfo = $storesArray;
            $result["success"] = 1;
        } else {
            $result["error"] = 1;
            $result["message"] = $save;
        }

        echo json_encode($result);
    }

    function returns() {
        if(getTrialStatus() == "expired") {
            //Not in subscription
            $this->data["menu"] = "settings";
            $this->load->view("navigation/header", $this->data);
            $this->load->view("settings/account/selectPlan", $this->data);
        } else {
            $this->data["menu"] = "returnsSettings";
            $this->load->view("navigation/header", $this->data);
            $this->load->view("settings/returns/dashboard", $this->data);
        }
    }

    function getReturnsInfo() {
        $companyInfo = $this->parse->getParseClass("Companies", "objectId", $this->session->companyId);
        $this->data["companyInfo"] = $companyInfo;
        $this->data["locations"] = $this->parse->getParseClass("Locations", "companyId", $this->session->companyId);
//        $this->data["returnsTo"] = json_decode($companyInfo[0]->get("returnsTo"));
        $this->data["returns"] = $this->parse->getParseClass("Returns", "companyId", $this->session->companyId);
        return $this->load->view("settings/returns/returnsSettingsTable", $this->data);
    }

    function saveReturnsToFrom() {
        $returnsFrom = [];
        foreach ($_POST as $key => $value) {
            if($key == "country") {
                if ($value == "United States") {
                    $returnsFrom["countryCode"] = "US";
                    $returnsFrom[$key] = $value;
                } else if ($value == "Canada") {
                    $returnsFrom["countryCode"] = "CA";
                    $returnsFrom[$key] = $value;
                } else if ($value == "United Kingdom") {
                    $returnsFrom["countryCode"] = "GB";
                    $returnsFrom[$key] = $value;
                } else if ($value == "Australia") {
                    $returnsFrom["countryCode"] = "AU";
                    $returnsFrom[$key] = $value;
                }
            } else if($key != "companyId") {
                $returnsFrom[$key] = $value;
            }
        }
        $objectArray = [
            "returnsTo" => json_encode($returnsFrom)
        ];
        $save = $this->parse->updateObject("Companies", $objectArray, $this->input->post("companyId"));
        if($save == true) {
            $result["success"] = 1;
        } else {
            $result["error"] = 1;
            $result["message"] = $save;
        }

        echo json_encode($result);
    }
    //Add user carrier accounts
    function addStamps() {
        $this->data["menu"] = "settings";
        $this->load->view("navigation/header", $this->data);
        $this->load->view("settings/shipping/userCarriers/stamps", $this->data);
    }

    function connectStamps() {
        $connect = $this->shipEngine->connectStamps($_POST, $this->session->shipEngineAPIKey);
        if(isset($connect->errors)) {
            $result["error"] = 1;
            $result["message"] = $connect->errors[0]->message;
        } else {
            $objectArray = [
                "carrierId" => $connect->carrier_id,
                "companyId" => $this->session->companyId,
                "name" => "Stamps.com",
                "service" => "usps",
                "status" => "disabled",
                "funds" => 0
            ];
            $create = $this->parse->newObject("Carriers", $objectArray);
            if($create[0] == true) {
                $result["success"] = 1;
            } else {
                $result["error"] = 1;
                $result["message"] = $create;
            }
        }

        echo json_encode($result);
    }

    function addEndicia() {
        $this->data["menu"] = "settings";
        $this->load->view("navigation/header", $this->data);
        $this->load->view("settings/shipping/userCarriers/endicia", $this->data);
    }

    function connectEndicia() {
        $connect = $this->shipEngine->connectEndicia($_POST, $this->session->shipEngineAPIKey);
        if(isset($connect->errors)) {
            $result["error"] = 1;
            $result["message"] = $connect->errors[0]->message;
        } else {
            $objectArray = [
                "carrierId" => $connect->carrier_id,
                "companyId" => $this->session->companyId,
                "name" => "Endicia",
                "service" => "usps",
                "status" => "disabled",
                "funds" => 0
            ];
            $create = $this->parse->newObject("Carriers", $objectArray);
            if($create[0] == true) {
                $result["success"] = 1;
            } else {
                $result["error"] = 1;
                $result["message"] = $create;
            }
        }
        echo json_encode($result);
    }

    function addUPS() {
        $this->data["menu"] = "settings";
        $this->data["countryCodes"] = countryCodes();
        $this->data["companyInfo"] = $this->parse->getParseClass("Companies", "objectId", $this->session->companyId);
        $this->load->view("navigation/header", $this->data);
        $this->load->view("settings/shipping/userCarriers/ups", $this->data);
    }

    function connectUPS() {
        $connect = $this->shipEngine->connectUPS($_POST, $this->session->shipEngineAPIKey);
        if(isset($connect->errors)) {
            $result["error"] = 1;
            $result["message"] = $connect->errors[0]->message;
        } else {
            $objectArray = [
                "carrierId" => $connect->carrier_id,
                "companyId" => $this->session->companyId,
                "name" => "UPS",
                "service" => "ups",
                "status" => "disabled",
                "funds" => -1
            ];
            $create = $this->parse->newObject("Carriers", $objectArray);
            if($create[0] == true) {
                $result["success"] = 1;
            } else {
                $result["error"] = 1;
                $result["message"] = $create;
            }
        }
        echo json_encode($result);
    }

    function addFedex() {
        $this->data["menu"] = "settings";
        $this->data["countryCodes"] = countryCodes();
        $this->data["companyInfo"] = $this->parse->getParseClass("Companies", "objectId", $this->session->companyId);
        $this->load->view("navigation/header", $this->data);
        $this->load->view("settings/shipping/userCarriers/fedex", $this->data);
    }

    function connectFedex() {
        $connect = $this->shipEngine->connectFedex($_POST, $this->session->shipEngineAPIKey);
        if(isset($connect->errors)) {
            $result["error"] = 1;
            $result["message"] = $connect->errors[0]->message;
        } else {
            $objectArray = [
                "carrierId" => $connect->carrier_id,
                "companyId" => $this->session->companyId,
                "name" => "FEDEX",
                "service" => "fedex",
                "status" => "disabled",
                "funds" => -1
            ];
            $create = $this->parse->newObject("Carriers", $objectArray);
            if($create[0] == true) {
                $result["success"] = 1;
            } else {
                $result["error"] = 1;
                $result["message"] = $create;
            }
        }
        echo json_encode($result);
    }

    function disconnectStore() {
        $storeId = $this->input->get("id");
        //Delete Store
        $storeQuery = $this->parse->getParseClass("Stores", "objectId", $storeId);
        $shopifyName = $storeQuery[0]->get("shopifyName");
        //Delete Orders
        $deleteOrders = $this->parse->deleteObjects("Orders", "storeName", $shopifyName);
        if($deleteOrders == "true") {
            $deleteStore = $this->parse->deleteObject("Stores", "objectId", $storeId);
            if($deleteStore == true) {
                $result["success"] = 1;
            } else {
                $result["error"] = 1;
                $result["message"] = $deleteStore;
            }
        } else {
            $result["error"] = 1;
            $result["message"] = $deleteOrders;
        }

        echo json_encode($result);
    }

    function buyLabels() {
        $this->data["menu"] = "settings";
        $this->load->view("navigation/header", $this->data);
        $this->load->view("settings/account/buyLabels", $this->data);
    }

    function buyLabelsTable() {
        $companyInfo = $this->parse->getParseClass("Companies", "objectId", $this->session->companyId);
        $currentStartDate = date_create($companyInfo[0]->get("billingPeriodStartDate"));
//        $this->data["billingPeriodStartDate"] = $currentStartDate->format("m/d/Y");
        $currentEndDate = date_add($currentStartDate,date_interval_create_from_date_string("30 days"));
        $todaysDate = date_create();
        $diff = date_diff($currentEndDate, $todaysDate);
        $this->data["billingDate"] = $todaysDate->format("m/d/Y");
        $this->data["daysDiff"] = $diff->days;
        $this->data["account"] = $companyInfo[0];
        $this->data["labelCost"] = getSubLevel($companyInfo[0]->get("subscriptionLevel"));
        $this->load->view("settings/account/buyLabelsTable", $this->data);
    }

    function purchaseLabels() {
        $costPerLabel = $this->input->post("costPerLabel");
        $numberOfLabels = $this->input->post("numberOfLabels");
        $price = $costPerLabel * $numberOfLabels;
        $purchaseResult = $this->shopify_model->usageCharge($this->input->post("description"), $price);
        if(!isset($purchaseResult["error"])) {
            //Good Purchase
            $result["success"] = 1;
            $array = ["labelsRemaining" => $this->session->labelsRemaining + $numberOfLabels];
            $this->parse->updateObject("Companies", $array, $this->session->companyId);
            $this->session->labelsRemaining = $this->session->labelsRemaining + $numberOfLabels;
            $createdDate = date_create($purchaseResult["createdDate"], timezone_open($this->session->timeZone));
            $createdDateString = date_format($createdDate,'Y-m-d');
            $this->functions->createInvoice($purchaseResult["billingDate"], $createdDateString, $numberOfLabels, $costPerLabel);
            $this->mailGun->sendMeNewLabelsPurchaseEmail($costPerLabel, $numberOfLabels, $price);
        } else {
            //Bad Purchase
            $result["error"] = 1;
            $result["message"] = "There was an error. Please try again or contact support at support@worryfreeshipping.com with error: ".json_encode($purchaseResult["error"]);
        }
        echo json_encode($result);
    }

    function tourComplete() {
        $tourDesc = $this->input->get("tourDesc");
        $this->parse->updateObject("_User", ["tour" => $tourDesc], $this->session->userId);
        $tourArray = [];
        foreach ($this->session->tour as $tour) {
            array_push($tourArray, $tour);
        }
        if(!in_array($tourDesc, $tourArray)) {
            array_push($tourArray, $tourDesc);
        }
        $this->session->tour = $tourArray;
    }

    function resetTour() {
        $this->parse->updateObject("_User", ["tour" => []], $this->session->userId);
        $this->session->tour = [];
        $result["success"] = 1;
        echo json_encode($result);
    }
}
