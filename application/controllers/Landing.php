<?php
/**
 * Created by PhpStorm.
 * User: rapidware
 * Date: 11/22/17
 * Time: 6:18 PM
 */

include(APPPATH.'libraries/allLibraries.php');

defined('BASEPATH') OR exit('No direct script access allowed');

// Add the "use" declarations where you'll be using the classes
use Parse\ParseClient;
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseSessionStorage;

class Landing extends CI_Controller {

    function __construct() {
        parent::__construct();
//        session_start();
        $this->load->library('form_validation');
        $this->load->library('javascript');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->model("parse_model", 'parse');
        $this->load->model("functions");
        $this->load->model("mail_gun_model", "mailGun");
        ParseClient::setStorage(new ParseSessionStorage());

        include(APPPATH . 'libraries/keys.php');
    }

    public function index() {
        $this->load->view('landing');
    }

    function customerProfile() {
        $this->load->view("client/customerProfile");
    }

    function contact() {
        $this->load->view("contact");
    }

    function sendEmail() {
        $subject = $this->input->post("subject");
        $name =$this->input->post("name");
        $email = $this->input->post("email");
        $message = $this->input->post("message");
        if($subject != "" && $name != "" && $email != "" && $message != "") {
            $this->mailGun->customerEmail($email, $subject, $message, $name);
            $result["success"] = 1;
        } else {
            $result["error"] = 1;
        }

        echo json_encode($result);
    }

    function sendForm() {
        $objectArray = $_POST;
        foreach($objectArray as $key => $value) {
            if($value == ""){
                $result['error'] = 1;
                $result['message'] = "Please fill in all fields";
                break;
            }
        }
        if(!in_array(1, $result)) {
            $resultPost = $this->parse->saveCustomerForm($_POST);
            if ($resultPost != false) {
                $result['success'] = 1;
            } else {
                $result['error'] = 2;
                $result['message'] = $result;
            }
        }
        echo json_encode($result);
    }
}