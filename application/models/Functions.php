<?php
/**
 * Created by PhpStorm.
 * User: rapidware
 * Date: 10/30/17
 * Time: 7:59 PM
 */

use Parse\ParseClient;
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseSessionStorage;

class Functions extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->model("parse_model", "parse");
        $this->load->helper('download');
    }

    function updateServerWithOrders($ordersToFindLimit) {
        $totalOrders = 0;
        foreach ($this->session->shopifyStoresInfo as $store) {
            $shopify = $this->parse->shopify($store["shopifyName"], $store["shopifyToken"]);
            $lastOrderId = $this->parse->getLastOrderId($store["shopifyName"]);
//        $lastOrderId = "0";
            if ($lastOrderId == "0") {
                //No current orders
                $params = array(
//                    'limit' => $ordersToFindLimit,
                    'status' => "open",
//                    "name" => "#1003"
                );
            } else {
                $params = array(
                    'since_id' => $lastOrderId,
                    'limit' => $ordersToFindLimit,
//                "name" => "#1051",
                    'status' => "open"
                );
            }
            $newOrders = array();
            $orders = $shopify->Order->get($params);
            //Used when having webhooks send orders into server
//        $orderIdsNotInShopworks = $this->parse->getOrderIdsNotInShopworks($storeName);
            foreach ($orders as $order) {
//            if(!in_array($order["id"],$orderIdsNotInShopworks)) {
                array_push($newOrders, $order);
                $totalOrders = $totalOrders + 1;
//                array_push($newOrdersId, $order["id"]);
            }
//        }
                $giftCardCount = $this->parse->saveOrders($newOrders, $store["shopifyName"]);
        }
//        return [$newOrders, $orderIdsNotInShopworks, $newOrdersId, $giftCardCount[0], $giftCardCount[1], $giftCardCount[2], $giftCardCount[3]];
        return $totalOrders;
    }

    function updateServerWithStartingOrder($storeName, $shopifyToken, $startingOrderNumber) {
        $shopify = $this->parse->shopify($storeName, $shopifyToken);
        $params = [
            "limit" => 1,
            "name" => $startingOrderNumber
        ];
        $newOrders = [];
        $orders = $shopify->Order->get($params);
        foreach ($orders as $order) {
            array_push($newOrders, $order);
        }
        $this->parse->saveOrders($newOrders, $storeName);
        return $orders;
    }

    function updateFulfillment($orderId, $trackingNumber, $shippedStatus, $productIdsShipped, $warehouseId, $accessToken, $storeName, $carrierName) {
        $shopify = $this->parse->shopifyKeys($storeName);
        $locations = $this->parse->getParseClass("Locations", "warehouseId", $warehouseId);
        $locationId = $locations[0]->get("locationId");
        if($shippedStatus) {
            //Get item Ids and quantity
//            $orderInfo = $this->parse->getParseClass("Orders","objectId", $orderObjectId);
            $data = '
            {  
              "fulfillment": {
                "location_id": '.$locationId.',
                "tracking_number": "' . $trackingNumber . '",
                "tracking_company": "'.$carrierName.'",
                "notify_customer": true,
                "line_items": [';
//            $productIdsPacked = $orderInfo[0]->get("productIdsPacked");
            foreach ($productIdsShipped as $key => $value) {
                end($productIdsShipped);
                if ($key === key($productIdsShipped)) {
                    $data .= '
                      {
                        "id": ' . $value[0] . ',
                        "quantity": ' . count($value) . '
                      }';
                } else {
                    $data .= '
                      {
                        "id": ' . $value[0] . ',
                        "quantity": ' . count($value) . '
                      },';
                }
            }
            $data .= '
                ]
              }
            }';
        } else {

            $data = '
            {  
              "fulfillment": {
                "location_id": '.$locationId.',
                "tracking_number": "' . $trackingNumber . '",
                "tracking_company": "'.$carrierName.'",
                "notify_customer": true
              }
            }';
        }
//        $response = Requests::post('https://'.$shopify["ApiKey"].':'.$shopify["Password"].'@'.$shopify["ShopUrl"].'/admin/orders/'.$orderId.'/fulfillments.json', $headers, $data);
        $headers = $this->parse->headers($accessToken);
        $response = Requests::post('https://'.$storeName.'/admin/orders/'.$orderId.'/fulfillments.json', $headers, $data);
        return $response;
    }

    function addNoteAttributes($storeName, $skus, $orderId) {
        $shopify = $this->parse->shopifyKeys($storeName);
        $headers = array(
            'Content-Type' => 'application/json'
        );
        $data = '
        {
          "order": {
            "id": '.$orderId.',
            "note_attributes": [
              {
                "name": "Mystery Skus",
                "value": "'.$skus.'"
              }
            ]
          }
        }';
        $response = Requests::put('https://'.$shopify["ApiKey"].':'.$shopify["Password"].'@'.$shopify["ShopUrl"].'/admin/orders/'.$orderId.'.json', $headers, $data);
        return $response;
    }

    function getImages($lineItems, $storeName) {
        $productImages = [];
        $productBarcodes = [];
        $productQty = [];
//        $index = array_search($storeName, $this->session->shopifyStores);
//        $token = $this->session->shopifyTokens[$index];
        $token = $this->session->shopifyStoresInfo[$storeName]["shopifyToken"];
        foreach ($lineItems as $item) {
            $headers = $this->parse->headers($token);
            $response = Requests::get('https://'.$storeName.'/admin/products/'.$item->product_id.'.json', $headers);
            $productDecode = json_decode($response->body);
            $product = $productDecode->product;
            //Find the variant Id inside of the images array
            $variantId = $item->variant_id;
            if($product->image != null) {
                for ($x = 0; $x < count($product->images); $x++) {
                    $imageFound = false;
                    if (in_array($variantId, $product->images[$x]->variant_ids)) {
                        array_push($productImages, $product->images[$x]->src);
                        $imageFound = true;
                        break;
                    }
                    if ($x + 1 == count($product->images) && $imageFound == false) {
                        //used if product variant does not have an image assigned
                        if ($product->image != null) {
                            array_push($productImages, $product->image->src);
                        }
                    }
                }
            } else {
                    //Used when no product image exists
                $missingImage = base_url()."/assets/img/missingImage.png";
                    array_push($productImages, $missingImage);
            }
            //Find the varient barcodes
            foreach ($product->variants as $variant) {
                if($variant->id == $variantId) {
                    array_push($productBarcodes, $variant->barcode);
                    break;
                }
            }
            //Get the inventory item id for inventory locations

//            foreach ($product->variants as $variant) {
//                if($variantId == $variant->id) {
//                    array_push($productQty, $variant->old_inventory_quantity);
//                    break;
//                }
//            }
        }

        return [$productImages,$productQty, $productBarcodes];
    }

    function createInvoice($billingDate, $createdDate, $numberOfLabels, $costPerLabel) {
        $objectArray = ["createdDate" => $createdDate, "billingDate" => $billingDate, "orderCountToCharge" => intval($numberOfLabels), "chargePerOrder" => floatval($costPerLabel), "companyId" => $this->session->companyId, "chargeType" => "usage"];
        $result = $this->parse->newObject("Invoices", $objectArray);
        return $result;
    }

    function addWebhooks($shop, $accessToken, $storeId) {
        $responses = [];
        $headers = array(
            'Content-Type' => 'application/json',
            'X-Shopify-Access-Token' => $accessToken
        );

        $data = '
        {
          "webhook": {
            "topic": "orders/updated",
            "address": "https://worryfreeshipping.com/webhook/orderUpdated",
            "format": "json"
          }
        }';
        $response = Requests::post('https://'.$shop.'/admin/webhooks.json', $headers, $data);
        $responseDecoded = json_decode($response->body);
        if(!isset($responseDecoded->errors)) {
            $objectArray = [
                "webhookId" => $responseDecoded->webhook->id,
                "storeId" => $storeId,
                "topic" => "orders/updated",
                "address" => "https://worryfreeshipping.com/webhook/orderUpdated"
            ];
            $this->parse->newObject("Webhooks", $objectArray);
            $responses["orders"] = $responseDecoded;
        } else {
            $objectArray = [
                "webhookError" => json_encode($responseDecoded->errors),
                "storeId" => $storeId,
                "topic" => "orders/updated",
                "address" => "https://worryfreeshipping.com/webhook/orderUpdated"
            ];
            $this->parse->newObject("Webhooks", $objectArray);
            $responses["orders"] = $responseDecoded;
        }
        //Second Webhook
        $headers2 = array(
            'Content-Type' => 'application/json',
            'X-Shopify-Access-Token' => $accessToken
        );

        $data2 = '
            {
              "webhook": {
                "topic": "app/uninstalled",
                "address": "https://worryfreeshipping.com/webhook/shopRemoved",
                "format": "json"
              }
            }';
        $response = Requests::post('https://'.$shop.'/admin/webhooks.json', $headers2, $data2);
        $responseDecoded = json_decode($response->body);
        if(!isset($responseDecoded->errors)) {
            $objectArray = [
                "webhookId" => $responseDecoded->webhook->id,
                "storeId" => $storeId,
                "topic" => "app/uninstalled",
                "address" => "https://worryfreeshipping.com/webhook/orderUpdated"
            ];
            $this->parse->newObject("Webhooks", $objectArray);
            $responses["app"] = $responseDecoded;
        } else {
            $objectArray = [
                "webhookError" => json_encode($responseDecoded->errors),
                "storeId" => $storeId,
                "topic" => "app/uninstalled",
                "address" => "https://worryfreeshipping.com/webhook/orderUpdated"
            ];
            $this->parse->newObject("Webhooks", $objectArray);
            $responses["app"] = $responseDecoded;
        }
        return $responses;
    }

    function archiveOrder($accessToken, $shop, $orderId, $orderObjectId) {
        $headers = array(
            'Content-Type' => 'application/json',
            'X-Shopify-Access-Token' => $accessToken
        );

        $response = Requests::post('https://'.$shop.'/admin/orders/'.$orderId.'/close.json', $headers);
        $responseDecoded = json_decode($response->body);
        if(!isset($responseDecoded->errors)) {
            $objectArray = [
                "archived" => "2"
            ];
            $this->parse->updateObject("Orders", $objectArray, $orderObjectId);
        }
        return $responseDecoded;
    }

    function testImage2() {
        $shopify = $this->parse->shopify("RR");
        $productID = 1303342055495;
        $product = $shopify->Product($productID)->get();
        return $product;
    }

    function getInventoryLevels($storeNameAb) {
        $shopify = $this->parse->shopify($storeNameAb);
        $params = array(
            'published_status' => "published",
            'limit' => 250
        );
        $inventory = $shopify->Product->get($params);
        return $inventory;
    }

    function coo() {
        $countries = array
        (
            'AF' => 'Afghanistan',
            'AX' => 'Aland Islands',
            'AL' => 'Albania',
            'DZ' => 'Algeria',
            'AS' => 'American Samoa',
            'AD' => 'Andorra',
            'AO' => 'Angola',
            'AI' => 'Anguilla',
            'AQ' => 'Antarctica',
            'AG' => 'Antigua And Barbuda',
            'AR' => 'Argentina',
            'AM' => 'Armenia',
            'AW' => 'Aruba',
            'AU' => 'Australia',
            'AT' => 'Austria',
            'AZ' => 'Azerbaijan',
            'BS' => 'Bahamas',
            'BH' => 'Bahrain',
            'BD' => 'Bangladesh',
            'BB' => 'Barbados',
            'BY' => 'Belarus',
            'BE' => 'Belgium',
            'BZ' => 'Belize',
            'BJ' => 'Benin',
            'BM' => 'Bermuda',
            'BT' => 'Bhutan',
            'BO' => 'Bolivia',
            'BA' => 'Bosnia And Herzegovina',
            'BW' => 'Botswana',
            'BV' => 'Bouvet Island',
            'BR' => 'Brazil',
            'IO' => 'British Indian Ocean Territory',
            'BN' => 'Brunei Darussalam',
            'BG' => 'Bulgaria',
            'BF' => 'Burkina Faso',
            'BI' => 'Burundi',
            'KH' => 'Cambodia',
            'CM' => 'Cameroon',
            'CA' => 'Canada',
            'CV' => 'Cape Verde',
            'KY' => 'Cayman Islands',
            'CF' => 'Central African Republic',
            'TD' => 'Chad',
            'CL' => 'Chile',
            'CN' => 'China',
            'CX' => 'Christmas Island',
            'CC' => 'Cocos (Keeling) Islands',
            'CO' => 'Colombia',
            'KM' => 'Comoros',
            'CG' => 'Congo',
            'CD' => 'Congo, Democratic Republic',
            'CK' => 'Cook Islands',
            'CR' => 'Costa Rica',
            'CI' => 'Cote D\'Ivoire',
            'HR' => 'Croatia',
            'CU' => 'Cuba',
            'CY' => 'Cyprus',
            'CZ' => 'Czech Republic',
            'DK' => 'Denmark',
            'DJ' => 'Djibouti',
            'DM' => 'Dominica',
            'DO' => 'Dominican Republic',
            'EC' => 'Ecuador',
            'EG' => 'Egypt',
            'SV' => 'El Salvador',
            'GQ' => 'Equatorial Guinea',
            'ER' => 'Eritrea',
            'EE' => 'Estonia',
            'ET' => 'Ethiopia',
            'FK' => 'Falkland Islands (Malvinas)',
            'FO' => 'Faroe Islands',
            'FJ' => 'Fiji',
            'FI' => 'Finland',
            'FR' => 'France',
            'GF' => 'French Guiana',
            'PF' => 'French Polynesia',
            'TF' => 'French Southern Territories',
            'GA' => 'Gabon',
            'GM' => 'Gambia',
            'GE' => 'Georgia',
            'DE' => 'Germany',
            'GH' => 'Ghana',
            'GI' => 'Gibraltar',
            'GR' => 'Greece',
            'GL' => 'Greenland',
            'GD' => 'Grenada',
            'GP' => 'Guadeloupe',
            'GU' => 'Guam',
            'GT' => 'Guatemala',
            'GG' => 'Guernsey',
            'GN' => 'Guinea',
            'GW' => 'Guinea-Bissau',
            'GY' => 'Guyana',
            'HT' => 'Haiti',
            'HM' => 'Heard Island & Mcdonald Islands',
            'VA' => 'Holy See (Vatican City State)',
            'HN' => 'Honduras',
            'HK' => 'Hong Kong',
            'HU' => 'Hungary',
            'IS' => 'Iceland',
            'IN' => 'India',
            'ID' => 'Indonesia',
            'IR' => 'Iran, Islamic Republic Of',
            'IQ' => 'Iraq',
            'IE' => 'Ireland',
            'IM' => 'Isle Of Man',
            'IL' => 'Israel',
            'IT' => 'Italy',
            'JM' => 'Jamaica',
            'JP' => 'Japan',
            'JE' => 'Jersey',
            'JO' => 'Jordan',
            'KZ' => 'Kazakhstan',
            'KE' => 'Kenya',
            'KI' => 'Kiribati',
            'KR' => 'Korea',
            'KW' => 'Kuwait',
            'KG' => 'Kyrgyzstan',
            'LA' => 'Lao People\'s Democratic Republic',
            'LV' => 'Latvia',
            'LB' => 'Lebanon',
            'LS' => 'Lesotho',
            'LR' => 'Liberia',
            'LY' => 'Libyan Arab Jamahiriya',
            'LI' => 'Liechtenstein',
            'LT' => 'Lithuania',
            'LU' => 'Luxembourg',
            'MO' => 'Macao',
            'MK' => 'Macedonia',
            'MG' => 'Madagascar',
            'MW' => 'Malawi',
            'MY' => 'Malaysia',
            'MV' => 'Maldives',
            'ML' => 'Mali',
            'MT' => 'Malta',
            'MH' => 'Marshall Islands',
            'MQ' => 'Martinique',
            'MR' => 'Mauritania',
            'MU' => 'Mauritius',
            'YT' => 'Mayotte',
            'MX' => 'Mexico',
            'FM' => 'Micronesia, Federated States Of',
            'MD' => 'Moldova',
            'MC' => 'Monaco',
            'MN' => 'Mongolia',
            'ME' => 'Montenegro',
            'MS' => 'Montserrat',
            'MA' => 'Morocco',
            'MZ' => 'Mozambique',
            'MM' => 'Myanmar',
            'NA' => 'Namibia',
            'NR' => 'Nauru',
            'NP' => 'Nepal',
            'NL' => 'Netherlands',
            'AN' => 'Netherlands Antilles',
            'NC' => 'New Caledonia',
            'NZ' => 'New Zealand',
            'NI' => 'Nicaragua',
            'NE' => 'Niger',
            'NG' => 'Nigeria',
            'NU' => 'Niue',
            'NF' => 'Norfolk Island',
            'MP' => 'Northern Mariana Islands',
            'NO' => 'Norway',
            'OM' => 'Oman',
            'PK' => 'Pakistan',
            'PW' => 'Palau',
            'PS' => 'Palestinian Territory, Occupied',
            'PA' => 'Panama',
            'PG' => 'Papua New Guinea',
            'PY' => 'Paraguay',
            'PE' => 'Peru',
            'PH' => 'Philippines',
            'PN' => 'Pitcairn',
            'PL' => 'Poland',
            'PT' => 'Portugal',
            'PR' => 'Puerto Rico',
            'QA' => 'Qatar',
            'RE' => 'Reunion',
            'RO' => 'Romania',
            'RU' => 'Russian Federation',
            'RW' => 'Rwanda',
            'BL' => 'Saint Barthelemy',
            'SH' => 'Saint Helena',
            'KN' => 'Saint Kitts And Nevis',
            'LC' => 'Saint Lucia',
            'MF' => 'Saint Martin',
            'PM' => 'Saint Pierre And Miquelon',
            'VC' => 'Saint Vincent And Grenadines',
            'WS' => 'Samoa',
            'SM' => 'San Marino',
            'ST' => 'Sao Tome And Principe',
            'SA' => 'Saudi Arabia',
            'SN' => 'Senegal',
            'RS' => 'Serbia',
            'SC' => 'Seychelles',
            'SL' => 'Sierra Leone',
            'SG' => 'Singapore',
            'SK' => 'Slovakia',
            'SI' => 'Slovenia',
            'SB' => 'Solomon Islands',
            'SO' => 'Somalia',
            'ZA' => 'South Africa',
            'GS' => 'South Georgia And Sandwich Isl.',
            'ES' => 'Spain',
            'LK' => 'Sri Lanka',
            'SD' => 'Sudan',
            'SR' => 'Suriname',
            'SJ' => 'Svalbard And Jan Mayen',
            'SZ' => 'Swaziland',
            'SE' => 'Sweden',
            'CH' => 'Switzerland',
            'SY' => 'Syrian Arab Republic',
            'TW' => 'Taiwan',
            'TJ' => 'Tajikistan',
            'TZ' => 'Tanzania',
            'TH' => 'Thailand',
            'TL' => 'Timor-Leste',
            'TG' => 'Togo',
            'TK' => 'Tokelau',
            'TO' => 'Tonga',
            'TT' => 'Trinidad And Tobago',
            'TN' => 'Tunisia',
            'TR' => 'Turkey',
            'TM' => 'Turkmenistan',
            'TC' => 'Turks And Caicos Islands',
            'TV' => 'Tuvalu',
            'UG' => 'Uganda',
            'UA' => 'Ukraine',
            'AE' => 'United Arab Emirates',
            'GB' => 'United Kingdom',
            'US' => 'United States',
            'UM' => 'United States Outlying Islands',
            'UY' => 'Uruguay',
            'UZ' => 'Uzbekistan',
            'VU' => 'Vanuatu',
            'VE' => 'Venezuela',
            'VN' => 'Viet Nam',
            'VG' => 'Virgin Islands, British',
            'VI' => 'Virgin Islands, U.S.',
            'WF' => 'Wallis And Futuna',
            'EH' => 'Western Sahara',
            'YE' => 'Yemen',
            'ZM' => 'Zambia',
            'ZW' => 'Zimbabwe',
        );

        return $countries;
    }

    function getTimeZones() {
        $timezones = array(
            '(GMT-12:00) International Date Line West' => 'Pacific/Kwajalein',
            '(GMT-11:00) Midway Island' => 'Pacific/Midway',
            '(GMT-11:00) Samoa' => 'Pacific/Apia',
            '(GMT-10:00) Hawaii' => 'Pacific/Honolulu',
            '(GMT-09:00) Alaska' => 'America/Anchorage',
            '(GMT-08:00) Pacific Time (US & Canada)' => 'America/Los_Angeles',
            '(GMT-08:00) Tijuana' => 'America/Tijuana',
            '(GMT-07:00) Arizona' => 'America/Phoenix',
            '(GMT-07:00) Mountain Time (US & Canada)' => 'America/Denver',
            '(GMT-07:00) Chihuahua' => 'America/Chihuahua',
            '(GMT-07:00) La Paz' => 'America/Chihuahua',
            '(GMT-07:00) Mazatlan' => 'America/Mazatlan',
            '(GMT-06:00) Central Time (US & Canada)' => 'America/Chicago',
            '(GMT-06:00) Central America' => 'America/Managua',
            '(GMT-06:00) Guadalajara' => 'America/Mexico_City',
            '(GMT-06:00) Mexico City' => 'America/Mexico_City',
            '(GMT-06:00) Monterrey' => 'America/Monterrey',
            '(GMT-06:00) Saskatchewan' => 'America/Regina',
            '(GMT-05:00) Eastern Time (US & Canada)' => 'America/New_York',
            '(GMT-05:00) Indiana (East)' => 'America/Indiana/Indianapolis',
            '(GMT-05:00) Bogota' => 'America/Bogota',
            '(GMT-05:00) Lima' => 'America/Lima',
            '(GMT-05:00) Quito' => 'America/Bogota',
            '(GMT-04:00) Atlantic Time (Canada)' => 'America/Halifax',
            '(GMT-04:00) Caracas' => 'America/Caracas',
            '(GMT-04:00) La Paz' => 'America/La_Paz',
            '(GMT-04:00) Santiago' => 'America/Santiago',
            '(GMT-03:30) Newfoundland' => 'America/St_Johns',
            '(GMT-03:00) Brasilia' => 'America/Sao_Paulo',
            '(GMT-03:00) Buenos Aires' => 'America/Argentina/Buenos_Aires',
            '(GMT-03:00) Georgetown' => 'America/Argentina/Buenos_Aires',
            '(GMT-03:00) Greenland' => 'America/Godthab',
            '(GMT-02:00) Mid-Atlantic' => 'America/Noronha',
            '(GMT-01:00) Azores' => 'Atlantic/Azores',
            '(GMT-01:00) Cape Verde Is.' => 'Atlantic/Cape_Verde',
            '(GMT) Casablanca' => 'Africa/Casablanca',
            '(GMT) Dublin' => 'Europe/London',
            '(GMT) Edinburgh' => 'Europe/London',
            '(GMT) Lisbon' => 'Europe/Lisbon',
            '(GMT) London' => 'Europe/London',
            '(GMT) Monrovia' => 'Africa/Monrovia',
            '(GMT+01:00) Amsterdam' => 'Europe/Amsterdam',
            '(GMT+01:00) Belgrade' => 'Europe/Belgrade',
            '(GMT+01:00) Berlin' => 'Europe/Berlin',
            '(GMT+01:00) Bern' => 'Europe/Berlin',
            '(GMT+01:00) Bratislava' => 'Europe/Bratislava',
            '(GMT+01:00) Brussels' => 'Europe/Brussels',
            '(GMT+01:00) Budapest' => 'Europe/Budapest',
            '(GMT+01:00) Copenhagen' => 'Europe/Copenhagen',
            '(GMT+01:00) Ljubljana' => 'Europe/Ljubljana',
            '(GMT+01:00) Madrid' => 'Europe/Madrid',
            '(GMT+01:00) Paris' => 'Europe/Paris',
            '(GMT+01:00) Prague' => 'Europe/Prague',
            '(GMT+01:00) Rome' => 'Europe/Rome',
            '(GMT+01:00) Sarajevo' => 'Europe/Sarajevo',
            '(GMT+01:00) Skopje' => 'Europe/Skopje',
            '(GMT+01:00) Stockholm' => 'Europe/Stockholm',
            '(GMT+01:00) Vienna' => 'Europe/Vienna',
            '(GMT+01:00) Warsaw' => 'Europe/Warsaw',
            '(GMT+01:00) West Central Africa' => 'Africa/Lagos',
            '(GMT+01:00) Zagreb' => 'Europe/Zagreb',
            '(GMT+02:00) Athens' => 'Europe/Athens',
            '(GMT+02:00) Bucharest' => 'Europe/Bucharest',
            '(GMT+02:00) Cairo' => 'Africa/Cairo',
            '(GMT+02:00) Harare' => 'Africa/Harare',
            '(GMT+02:00) Helsinki' => 'Europe/Helsinki',
            '(GMT+02:00) Istanbul' => 'Europe/Istanbul',
            '(GMT+02:00) Jerusalem' => 'Asia/Jerusalem',
            '(GMT+02:00) Kyev' => 'Europe/Kiev',
            '(GMT+02:00) Minsk' => 'Europe/Minsk',
            '(GMT+02:00) Pretoria' => 'Africa/Johannesburg',
            '(GMT+02:00) Riga' => 'Europe/Riga',
            '(GMT+02:00) Sofia' => 'Europe/Sofia',
            '(GMT+02:00) Tallinn' => 'Europe/Tallinn',
            '(GMT+02:00) Vilnius' => 'Europe/Vilnius',
            '(GMT+03:00) Baghdad' => 'Asia/Baghdad',
            '(GMT+03:00) Kuwait' => 'Asia/Kuwait',
            '(GMT+03:00) Moscow' => 'Europe/Moscow',
            '(GMT+03:00) Nairobi' => 'Africa/Nairobi',
            '(GMT+03:00) Riyadh' => 'Asia/Riyadh',
            '(GMT+03:00) St. Petersburg' => 'Europe/Moscow',
            '(GMT+03:00) Volgograd' => 'Europe/Volgograd',
            '(GMT+03:30) Tehran' => 'Asia/Tehran',
            '(GMT+04:00) Abu Dhabi' => 'Asia/Muscat',
            '(GMT+04:00) Baku' => 'Asia/Baku',
            '(GMT+04:00) Muscat' => 'Asia/Muscat',
            '(GMT+04:00) Tbilisi' => 'Asia/Tbilisi',
            '(GMT+04:00) Yerevan' => 'Asia/Yerevan',
            '(GMT+04:30) Kabul' => 'Asia/Kabul',
            '(GMT+05:00) Ekaterinburg' => 'Asia/Yekaterinburg',
            '(GMT+05:00) Islamabad' => 'Asia/Karachi',
            '(GMT+05:00) Karachi' => 'Asia/Karachi',
            '(GMT+05:00) Tashkent' => 'Asia/Tashkent',
            '(GMT+05:30) Chennai' => 'Asia/Kolkata',
            '(GMT+05:30) Kolkata' => 'Asia/Kolkata',
            '(GMT+05:30) Mumbai' => 'Asia/Kolkata',
            '(GMT+05:30) New Delhi' => 'Asia/Kolkata',
            '(GMT+05:45) Kathmandu' => 'Asia/Kathmandu',
            '(GMT+06:00) Almaty' => 'Asia/Almaty',
            '(GMT+06:00) Astana' => 'Asia/Dhaka',
            '(GMT+06:00) Dhaka' => 'Asia/Dhaka',
            '(GMT+06:00) Novosibirsk' => 'Asia/Novosibirsk',
            '(GMT+06:00) Sri Jayawardenepura' => 'Asia/Colombo',
            '(GMT+06:30) Rangoon' => 'Asia/Rangoon',
            '(GMT+07:00) Bangkok' => 'Asia/Bangkok',
            '(GMT+07:00) Hanoi' => 'Asia/Bangkok',
            '(GMT+07:00) Jakarta' => 'Asia/Jakarta',
            '(GMT+07:00) Krasnoyarsk' => 'Asia/Krasnoyarsk',
            '(GMT+08:00) Beijing' => 'Asia/Hong_Kong',
            '(GMT+08:00) Chongqing' => 'Asia/Chongqing',
            '(GMT+08:00) Hong Kong' => 'Asia/Hong_Kong',
            '(GMT+08:00) Irkutsk' => 'Asia/Irkutsk',
            '(GMT+08:00) Kuala Lumpur' => 'Asia/Kuala_Lumpur',
            '(GMT+08:00) Perth' => 'Australia/Perth',
            '(GMT+08:00) Singapore' => 'Asia/Singapore',
            '(GMT+08:00) Taipei' => 'Asia/Taipei',
            '(GMT+08:00) Ulaan Bataar' => 'Asia/Irkutsk',
            '(GMT+08:00) Urumqi' => 'Asia/Urumqi',
            '(GMT+09:00) Osaka' => 'Asia/Tokyo',
            '(GMT+09:00) Sapporo' => 'Asia/Tokyo',
            '(GMT+09:00) Seoul' => 'Asia/Seoul',
            '(GMT+09:00) Tokyo' => 'Asia/Tokyo',
            '(GMT+09:00) Yakutsk' => 'Asia/Yakutsk',
            '(GMT+09:30) Adelaide' => 'Australia/Adelaide',
            '(GMT+09:30) Darwin' => 'Australia/Darwin',
            '(GMT+10:00) Brisbane' => 'Australia/Brisbane',
            '(GMT+10:00) Canberra' => 'Australia/Sydney',
            '(GMT+10:00) Guam' => 'Pacific/Guam',
            '(GMT+10:00) Hobart' => 'Australia/Hobart',
            '(GMT+10:00) Melbourne' => 'Australia/Melbourne',
            '(GMT+10:00) Port Moresby' => 'Pacific/Port_Moresby',
            '(GMT+10:00) Sydney' => 'Australia/Sydney',
            '(GMT+10:00) Vladivostok' => 'Asia/Vladivostok',
            '(GMT+11:00) Magadan' => 'Asia/Magadan',
            '(GMT+11:00) New Caledonia' => 'Asia/Magadan',
            '(GMT+11:00) Solomon Is.' => 'Asia/Magadan',
            '(GMT+12:00) Auckland' => 'Pacific/Auckland',
            '(GMT+12:00) Fiji' => 'Pacific/Fiji',
            '(GMT+12:00) Kamchatka' => 'Asia/Kamchatka',
            '(GMT+12:00) Marshall Is.' => 'Pacific/Fiji',
            '(GMT+12:00) Wellington' => 'Pacific/Auckland',
            '(GMT+13:00) Nuku\'alofa' => 'Pacific/Tongatapu'
        );

        return $timezones;
    }
}