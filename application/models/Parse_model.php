<?php
	
use Parse\ParseClient;
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseSessionStorage;
use ZfrShopify\ShopifyClient;

class Parse_model extends CI_Model {

    public function __construct() {

        parent::__construct();
            // Your own constructor code
        $this->load->model("functions");
        $this->load->model("ship_engine_model", "shipEngine");
    }

    function newUserShopify($post) {
        $permissions = [
            "fullAccess",
            "admin"
        ];
        $user = new ParseUser();
        $user->set("username", $post["username"]);
        $user->set("email", $post["email"]);
        $user->set("password", $post["password"]);
        $user->set("firstName", $post["firstName"]);
        $user->set("name", $post["firstName"]." ".$post["lastName"]);
        $user->set("lastName", $post["lastName"]);
        $user->set("phoneNumber", $post["phoneNumber"]);
        $user->set("role", "user");
        $user->setArray("permissions", $permissions);
        $user->set("status", "active");

        try {
            //Create User
            $user->signUp();
            //Create default package
            $packageArray = [
                "length" => "30.48",
                "width" => "22.225",
                "height" => "15.24",
                "sizeUnits" => "in",
                "weight" => 0,
                "weightUnits" => "oz",
                "name" => "Default Box",
                "carrierType" => "none",
                "packageType" => "package"
            ];
            $savePackage = $this->newObject("Packages", $packageArray);
            if($savePackage[0] == true) {
                //Create company
                $companyArray = [
                    "name" => $post["companyName"],
                    "labelFormat" => "desktop",
                    "defaultBox" => $savePackage[1],
                    "phone" => $post["phoneNumber"],
                    "address1" => $post["address1"],
                    "address2" => $post["address2"],
                    "city" => $post["city"],
                    "state" => $post["state"],
                    "stateCode" => $post["stateCode"],
                    "zip" => $post["zip"],
                    "country" => $post["country"],
                    "countryCode" => $post["countryCode"],
                    "labelsRemaining" => 0,
                    "weightMeasurement" => $post["weightUnit"],
                    "timeZone" => $post["timeZone"],
                    "status" => "inactive"
                ];
                if($post["weightUnit"] == "oz" || $post["weightUnit"] == "lb") {
                    $companyArray["unitSystem"] = "imperial";
                } else {
                    $companyArray["unitSystem"] = "metric";
                }
                $saveCompany = $this->newObject("Companies", $companyArray);
                if ($saveCompany[0] == true) {
                    //Update user
                    $user->set("companyId", $saveCompany[1]);
                    $user->save(true);
                    //Create Store
                    $objectArray = [
                        "shopifyName" => $post["shopifyStore"],
                        "shopifyId" => intval($post["shopifyStoreId"]),
                        "token" => $post["shopifyToken"],
                        "companyId" => $saveCompany[1],
                        "payingStore" => "true",
                        "status" => "inactive"
                    ];
                    $saveStore = $this->newObject("Stores", $objectArray);
                    if(count($saveStore) > 1) {
                        //Update package company id
                        $updatePackageArray = [
                            "companyId" => $saveCompany[1]
                        ];
                        $this->updateObject("Packages", $updatePackageArray, $savePackage[1]);
                        //Create shipengine account
                        $shipEngineAccount = $this->shipEngine->createAccount($post["firstName"], $post["lastName"], $post["companyName"], $saveCompany[1], $post["countryCode"]);
                        if (isset($labelData->errors)) {

                        } else {
                            //Update company
                            $updateCompanyArray = [
                                "payingStore" => $saveStore[1],
                                "shipEngineAPIKey" => $shipEngineAccount->api_key->encrypted_api_key,
                                "apiKeyId" => $shipEngineAccount->api_key->api_key_id,
                                "shipEngineAccountId" => $shipEngineAccount->account_id,
                                "shipEngineExternalAccountId" => $saveCompany[1],
                            ];
                            $this->updateObject("Companies", $updateCompanyArray, $saveCompany[1]);
                        }
                        //Create Stamps.com Carrier
                        $carrierArray = [
                            "companyId" => $saveCompany[1],
                            "funds" => 0,
                            "service" => "usps",
                            "status" => "pending",
                            "name" => "Stamps.com"

                        ];
                        $this->newObject("Carriers", $carrierArray);
                        //Create Locations
                        $headers = $this->headers($post["shopifyToken"]);
                        $response = Requests::get('https://' . $post["shopifyStore"] . '/admin/locations.json', $headers);
                        $locationsBody = json_decode($response->body);
                        $locations = $locationsBody->locations;
                        foreach ($locations as $locationInfo) {
                            $warehouse = $this->shipEngine->createWarehouse($locationInfo, $shipEngineAccount->api_key->encrypted_api_key, $post["phoneNumber"], $post["companyName"]);
                            $locationArray = [
                                "locationId" => $locationInfo->id,
                                "companyId" => $saveCompany[1],
//                                "name" => $post["companyName"],
                                "name" => $locationInfo->name,
                                "phone" => $post["phoneNumber"],
                                "address1" => $locationInfo->address1,
                                "address2" => $locationInfo->address2,
                                "city" => $locationInfo->city,
                                "state" => $locationInfo->province,
                                "stateCode" => $locationInfo->province_code,
                                "zip" => $locationInfo->zip,
                                "country" => $locationInfo->country_name,
                                "countryCode" => $locationInfo->country_code,
                                "legacy" => $locationInfo->legacy,
                                "warehouseId" => $warehouse->warehouse_id,
                                "shopifyName" => $post["shopifyStore"]
                            ];
                            if ($post["primaryLocationId"] == $locationInfo->id) {
                                $locationArray["default"] = "true";
                                $locationArray["returnDefault"] = "true";
                            } else {
                                $locationArray["default"] = "false";
                                $locationArray["returnDefault"] = "false";
                            }
                            $saveLocations = $this->newObject("Locations", $locationArray);
                        }
                        //Set up webhooks on client store
                        $webhooks = $this->functions->addWebhooks($post["shopifyStore"], $post["shopifyToken"], $saveStore[1]);
                        return "success";
                    } else {
                        return $saveStore;
                    }
                } else {
                    return $saveCompany;
                }
            } else {
                return $savePackage;
            }
        } catch (ParseException $ex) {
            if( $ex->getCode() == 209) {
                ParseUser::logOut();
                return "209";
            }
            return $ex->getMessage();
        }
    }

    function updateUser($objectId, $objectArray) {

        $query = new ParseQuery('_User');
        $query->equalTo("objectId", $objectId);

        $object = $query->get($objectId);
        foreach($objectArray as $key => $value) {
            if($key == "password" && $value != "" && $value != " "){
                $object->set($key, $value);
            } else if($key == "permissions") {
                $object->setArray($key, $value);
            } else {
                $object->set($key, $value);
            }
        }
        try{
            $object->save(true);
            return ["result" => "true"];
        } catch (ParseException $ex) {
            return ["result" => "error", "message" => $ex->getMessage()];
        }
    }

    function getUserData($userId) {
        $query = new ParseQuery('_User');
        $query->equalTo("objectId", $userId);
        return $query->find(true);
    }

    function getAllUsers() {
        $query = new ParseQuery("_User");
        $query->limit(900000);
        return $query->find();
    }

    function getParseClassKey($class, $key, $value, $key2, $value2) {
        $query = new ParseQuery($class);
        $query->equalTo($key, $value);
        $query->equalTo($key2, $value2);
        $query->limit(10000);
        try{
            $results = $query->find();
            return $results;
        } catch (ParseException $ex) {
            if($ex->getCode() == 209) {
                $this->session->set_userdata(
                    array(
                        'login_id'=>'',
                        'username'=>'',
                        'email'=>'',
                        'name' => '',
                        'role'=>''
                    ));
            }
            return $ex->getCode();
        }
    }

    function getParseClassArray($class, $objectArray) {
        $query = new ParseQuery($class);
        foreach ($objectArray as $key => $value) {
            $query->equalTo($key, $value);
        }
        $query->limit(90000);
        return $query->find();
    }

    function getParseClassArrayNotEqual($class, $objectArray) {
        $query = new ParseQuery($class);
        foreach ($objectArray as $key => $value) {
            $query->notEqualTo($key, $value);
        }
        $query->limit(900000);
        return $query->find();
    }

    function getParseClassArrayNotEqualOrders($class, $objectArray) {
        $query = new ParseQuery($class);
        $query->equalTo("companyId", $this->session->companyId);
        foreach ($objectArray as $key => $value) {
            $query->notEqualTo($key, $value);
        }
        $query->limit(900000);
        try{
            $results = $query->find();
            return $results;
        } catch (ParseException $ex) {
            if($ex->getCode() == 209) {
                $this->session->set_userdata(
                    array(
                        'login_id'=>'',
                        'username'=>'',
                        'email'=>'',
                        'name' => '',
                        'role'=>''
                    ));
            }
            redirect($this->config->item('base_url'), 'Location');
        }
    }

    function getParseClass($class, $key, $value) {
        $query = new ParseQuery($class);
        $query->equalTo($key, $value);
        $query->limit(10000);
        return $query->find();
    }

    function getAllParseClass($class) {
        $query = new ParseQuery($class);
        $query->limit(900000);
        return $query->find();
    }

    function getParseClassDsc($class, $objectArray, $dsc) {
        $query = new ParseQuery($class);
        foreach ($objectArray as $key => $value) {
            $query->equalTo($key, $value);
        }
        $query->descending($dsc);
        return $query->find();
    }

    function addUniqueSku($sku, $clientId, $percentage) {
        $query = $this->getParseClass("Skus", "clientId", $clientId);
        if(count($query) > 0) {
            $object = $query[0];
            $skusArray = $query[0]->get("sku");
            foreach ($skusArray as $key => $value) {
                if($key == strval($percentage)) {
                    foreach ($sku as $item) {
                        if (!in_array($item, $value)) {
                            array_push($value, $item);
                        }
                    }
                    $skusArray[strval($percentage)] = $value;
                } else {
                    if(!array_key_exists(strval($percentage),$skusArray)) {
                        $skusArray[strval($percentage)] = $sku;
                    }
                }
            }
            $object->setAssociativeArray("sku", $skusArray);
            try {
                $object->save(true);
                return "success";
            } catch (ParseException $ex) {
                return $ex->getMessage();
            }
        } else {
            $object = new ParseObject("Skus");
            $object->set("clientId", $clientId);
            $json[strval($percentage)] = $sku;
            $object->setAssociativeArray("sku", $json);
            try {
                $object->save(true);
                return "success";
            } catch (ParseException $ex) {
                return $ex->getMessage();
            }
        }
    }

    function getParseNotEqual($class, $objectArray, $asc) {
        $query = new ParseQuery($class);
        foreach ($objectArray as $key => $value) {
            $query->notEqualTo($key, $value);
        }
        if($asc != false) {
            $query->ascending($asc);
        }
        try {
            $results = $query->find();
            return $results;
        } catch (ParseException $ex) {
            return $ex->getMessage();
        }
    }

    function getParseClassGreaterThan($class, $key, $value, $key2, $value2) {
        $query = new ParseQuery($class);
        $query->equalTo($key, $value);
        $query->greaterThan($key2, $value2);
        $query->limit(10000);
        try{
            $results = $query->find();
            return $results;
        } catch (ParseException $ex) {
            return $ex->getCode();
        }
    }

    function getParseGreaterThan($class, $equalArray, $greaterArray) {
        $query = new ParseQuery($class);
        foreach ($equalArray as $key => $value) {
            $query->equalTo($key, $value);
        }
        foreach ($greaterArray as $key => $value) {
            $query->greaterThan($key, $value);
        }
        $query->limit(10000);
        try{
            $results = $query->find();
            return ["results" => $results];
        } catch (ParseException $ex) {
            return ["error" => $ex->getCode()];
        }
    }

    function getScanFormDate() {
        $query = new ParseQuery("ScanForms");
        $query->descending("createdAt");
        $scanForm = $query->first();
        $dateFromServer = $scanForm->get("date");
        $serverDateConverted = date('l', strtotime($dateFromServer));
        $dateToday = date("m/d/Y");
        if($dateToday == $dateFromServer) {
            if ($serverDateConverted == 'Friday') {
                $newDate = date('m/d/Y', strtotime($dateFromServer . "+3 days"));
            } else {
                $newDate = date('m/d/Y', strtotime($dateFromServer . ' + 1 days'));
            }
        } else {
            $newDate = $dateToday;
        }
        return $newDate;
    }

    //Used for oAuth
    function shopifyKeysStore($store) {
        $storeKeys = array(
            'ShopUrl' => $store,
            'ApiKey' => '3f3a09569c2717310698427548c5cfee',
            'SharedSecret' => '02d79d42d2d08bc81968434bfd95f57a'
        );
        $newClient = new PHPShopify\ShopifySDK($storeKeys);
        return $newClient;
    }

    //Used to make API calls on user store
    function shopify($store, $sharedSecret) {
        $storeKeys = array(
            'ShopUrl' => $store,
            'AccessToken' => $sharedSecret
        );

        $newClient = new PHPShopify\ShopifySDK($storeKeys);
        return $newClient;
    }
    //WorryFreeShipping keys
    function shopifyKeys() {
//        $storeKeys = array(
//            'ApiKey' => 'dbc7466ae0be34412fd3c68e73e7fc06',
//            'SharedSecret' => '4f75be72f45a6e3de0b9e7ba334b9a46'
//        );
        $storeKeys = array(
            'ApiKey' => '3f3a09569c2717310698427548c5cfee',
            'SharedSecret' => '02d79d42d2d08bc81968434bfd95f57a'
        );
        return $storeKeys;
    }

    function headers($token) {
        $headers = array(
            'Content-Type' => 'application/json',
            'X-Shopify-Access-Token' => $token
        );
        return $headers;
    }

    function getOrderProducts($orderNumber, $storeName) {
//        $str = $orderNumber;
        //Get store name from barcode
//        $strlen = strlen($str);
//        $storeName = "";
//        $orderNumberSearch = "";
//        for( $i = 0; $i <= $strlen; $i++ ) {
//            $char = substr( $str, $i, 1 );
//            if(is_numeric( $char )) {
//                $orderNumberSearch .=$char;
//            } else {
////                $storeName .= $char;
//                $storeName .= strtoupper($char);
//            }
//        }
        $orderInfo = $this->getParseClassKey("Orders", "orderNumber", intval($orderNumber), "storeName", $storeName);
        if(count($orderInfo) < 1) {
            return false;
        } else {
            return [json_decode($orderInfo[0]->get("lineItems")), $orderInfo[0]->getObjectId(), $orderInfo[0]];
        }
    }

    function updateStatus($orderObjectId, $statusArray) {
        $orderInfo = $this->getParseClass("Orders", "objectId", $orderObjectId);
        foreach ($statusArray as $key => $value) {
            if($key == "skusShipped" || $key == "skusPacked") {
                $orderInfo[0]->set($key, json_encode($value));
            } else if($key == "trackingNumbers" || $key == "shippingCost" || $key == "labelData") {
                $orderInfo[0]->add($key, [$value]);
            } else if($key == "labelLinks") {
                $links = json_decode($orderInfo[0]->get($key));
                if(!in_array($value, $links)) {
                    array_push($links, $value);
                }
                $orderInfo[0]->set($key, json_encode($links));
//                $orderInfo[0]->add($key, [$value]);
            } else if($key == "productIdsPacked" || $key == "productIdsShipped") {
                $orderInfo[0]->set($key, json_encode($value));
            } else {
                $orderInfo[0]->set($key, $value);
            }
        }
        try{
            $orderInfo[0]->save(true);
            return true;
        } catch (ParseException $ex) {
            return $ex->getMessage();
        }
    }

    function updateLabelData($labelId, $orderObjectId) {
        $order = $this->getParseClass("Orders", "objectId", $orderObjectId);
        $orderLabelData = $order[0]->get("labelData");
        $labelDataArray = [[]];
        $x = 0;
        foreach ($orderLabelData as $data) {
            foreach ($data as $key => $value) {
                $labelDataArray[$x][$key] = $value;
            }
            if($data["id"] == $labelId) {
                $labelDataArray[$x]["voided"] = "true";
            }
            $x++;
        }
        $order[0]->setArray("labelData", $labelDataArray);
        try{
            $order[0]->save(true);
            return true;
        } catch (ParseException $ex) {
            return $ex->getMessage();
        }
    }

    function updateObject($class, $objectArray, $objectId) {
        $objectInfo = $this->getParseClass($class, "objectId", $objectId);
        foreach ($objectArray as $key => $value) {
            if($key == "storeName") {
                $objectInfo[0]->set($key, json_encode($value));
            } if($key == "tour") {
                $objectInfo[0]->addUnique($key, [$value]);
            } else {
                $objectInfo[0]->set($key, $value);
            }
        }
        try {
            $objectInfo[0]->save(true);
            return true;
        } catch (ParseException $ex) {
            return $ex->getMessage();
        }
    }

    function getProductIdsPacked($orderObjectId) {
        $orderInfo = $this->getParseClass("Orders", "objectId", $orderObjectId);
        $skusPacked = array();
        $productIdsPacked = array();

        $lineItems = json_decode($orderInfo[0]->get("lineItems"));
        for($x=0;$x<count($lineItems);$x++) {
            array_push($skusPacked, $lineItems[$x]->sku);
            $productIds = array();
            for($y=0;$y<$lineItems[$x]->quantity;$y++) {
                array_push($productIds, $lineItems[$x]->id);
            }
            $productIdsPacked[$lineItems[$x]->sku] = $productIds;
        }
        return [$skusPacked, $productIdsPacked, $orderInfo[0]->get("storeName"), $orderInfo[0]->get("orderId")];
    }

    function saveCustomerForm($postArray) {
        $customerProfile = new ParseObject("CustomerProfiles");
        foreach ($postArray as $key => $value) {
            $customerProfile->set($key, $value);
        }
        try {
            $customerProfile->save(true);
            return true;
        } catch (ParseException $ex) {
            return $ex->getMessage();
        }
    }

    function correctDateFormat($date) {
        return date("Y/m/d", strtotime($date));
    }

    function testForWebHook() {
        $newOrder = new ParseObject("Orders");
        $newOrder->set("note", "Hi");
        $newOrder->save(true);
    }

    function getParseRelativeTime($class, $daysAgo, $objectArray, $dateField) {
        $query = new ParseQuery($class);
        $query->lessThanRelativeTime($dateField, $daysAgo.' days ago');
        foreach ($objectArray as $key => $value) {
            $query->equalTo($key, $value);
        }
        $query->limit(10000);
        try{
            $results = $query->find();
            return ["results" => $results];
        } catch (ParseException $ex) {
            return ["error" => $ex->getCode()];
        }
    }

    //Used for downloading orders
    function saveOrder($order, $store) {
        $giftCardOnlyOrdersCount = 0;
        $giftCardOrdersCount = 0;
//        foreach ($newOrders as $order) {
            $date = $this->correctDateFormat($order->created_at);
            $newOrder = new ParseObject("Orders");
            $newOrder->set("subTotal", $order->subtotal_price);
            $newOrder->set("grandTotal", $order->total_price);
            $newOrder->set("orderId", $order->id);
            $newOrder->set("orderNumber", $order->order_number);
            $newOrder->set("note", $order->note);
            $newOrder->set("dateOrdered", $date);
            $lineItems = $order->line_items;
            $skus = array();
            $giftCard = array();
            foreach ($lineItems as $item) {
                if($item->gift_card == true ) {
                    //if only a gift card do not put in shopworks
                    array_push($giftCard, true);
                } else {
                    array_push($giftCard, false);
                }
                array_push($skus, $item->sku);
            }
            if(!in_array(false, $giftCard)) {
                $newOrder->set("inShopworks", 1);
                $newOrder->set("giftCard", 1);
                $giftCardOnlyOrdersCount = $giftCardOnlyOrdersCount + 1;
            } else if (in_array(true, $giftCard)){
                $newOrder->set("giftCard", 1);
                $giftCardOrdersCount = $giftCardOrdersCount + 1;
            }
            $newOrder->setArray("sku", $skus);
            if(isset($order->customer)) {
                $customerArray = array();
                foreach ($order->customer as $key => $value) {
                    array_push($customerArray, htmlentities($value));
                }
                $newOrder->setArray("customer", $customerArray);
            } else {
                $newOrder->setArray("customer", ["no"]);
            }
            $newOrder->setArray("discountCode", $order->discount_codes);
            $newOrder->set("totalDiscounts", $order->total_discounts);
        if(isset($order->shipping_address)) {
            $shippingAddressArray = array();
            foreach ($order->shipping_address as $key => $value) {
                array_push($shippingAddressArray[$key], htmlentities($value));
            }
//            $newOrder->setArray("shippingAddress", $order->shipping_address);

            $newOrder->setArray("shippingLines", $order->shipping_lines);
        } else {
            $newOrder->setArray("shippingAddress", ["no"]);
            $newOrder->setArray("shippingLines", ["no"]);
        }
            if(isset($order->tax_lines)) {
                $newOrder->setArray("taxes", $order->tax_lines);
            } else {
                $newOrder->setArray("taxes", ["no"]);
            }
//            $newOrder->set("inShopworks", 1);
            $newOrder->set("storeName", $store);
           $newOrder->set("userId", $this->session->userId);
            $newOrder->setArray("gateway", $order->payment_gateway_names);
            $newOrder->setArray("lineItems", $order->line_items);
            $newOrder->setAssociativeArray("productIdsPack", "");
            $newOrder->setAssociativeArray("productIdsShipped", "");
            $newOrder->set("dateShipped", "");
            $newOrder->setArray("labelLinks", []);
            $newOrder->setArray("skusPacked", []);
            $newOrder->setArray("skusShipped", []);
            $newOrder->setArray("trackingNumbers", []);
            $newOrder->setArray("service", []);
            $newOrder->save(true);
//        }
    }

    function updateProduct($order, $storeNameAb) {
        if($storeNameAb == "RapidWare") {
            $storeName = "RR";
        }
        $shopify = $this->parse->shopify($storeName);
        $lineItems = $order->line_items;
        $skus = array();
        $variantArrayToSave = array();
        foreach ($lineItems as $item) {
            array_push($skus, $item->sku);
            $productTitle = $item->title;
            $purchasedQty = $item->quantity;
            $sku = $item->sku;
            $params = array(
                'title' => $productTitle,
            );
            $product = $shopify->Product->get($params);
            $variants = $product[0]["variants"];
            $id = $product[0]["id"];
            $x=0;
            foreach ($variants as $variant) {
                $variantArrayToSave[$x]["id"] = $variant["id"];
                if($variant["sku"] == $sku) {
                    $variantArrayToSave[$x]["inventory_quantity"] = $variant["inventory_quantity"] - $purchasedQty;
                }
                $x++;
            }
            $productInfo = array(
                "id"=> $id,
                "variants" => $variantArrayToSave
            );
            try{
                $productUpdate = $shopify->Product($id)->put($productInfo);
            } catch (Exception $ex) {

            }
        }
//        $products = $shopify->Webhook->count();
//        $save = new ParseObject("Skus");
//        $save->set("hi2", $products);
//        $save->save(true);
    }

    function getLastOrderId($store) {
        $query = new ParseQuery("Orders");
        $query->descending("orderNumber");
        $query->equalTo("storeName", $store);
        $query->equalTo("companyId", $this->session->companyId);
        $results = $query->first();
        if(count($results) < 1) {
            return "0";
        } else {
            return $results->get("orderId");
        }
    }
    //Used to compare new orders from shopify to existing orders in database
    function getOrderIdsNotInShopworks($store) {
        $query = new ParseQuery("Orders");
        $query->notEqualTo("inShopworks", 1);
        $query->equalTo("storeName", $store);
        $query->limit(90000);
        $results = $query->find();
        $orderIds = array();
        foreach ($results as $order) {
            array_push($orderIds, $order->get("orderId"));
        }
        return $orderIds;
    }

    //Used on employee get orders button
    function saveOrders($newOrders, $store) {
        $giftCardOnlyOrdersCount = 0;
        $giftCardOrdersCount = 0;
        $giftCardPayments = 0;
        $giftCardOrderNumbers = "";
        foreach ($newOrders as $order) {
            $date = $this->correctDateFormat($order["created_at"]);
            $newOrder = new ParseObject("Orders");
            $newOrder->set("subTotal", $order["subtotal_price"]);
            $newOrder->set("totalPrice", $order["total_price"]);
            $newOrder->set("totalTax", $order["total_tax"]);
            $newOrder->set("orderId", $order["id"]);
            $newOrder->set("orderNumber", $order["order_number"]);
            $newOrder->set("note", json_encode($order["note"]));
            $newOrder->set("dateOrdered", $date);
            $newOrder->set("dateOrderedTime", $order["created_at"]);
            $newOrder->set("status", "active");
            $lineItems = $order["line_items"];
            $skus = array();
            $giftCard = array();
            foreach ($lineItems as $item) {
                if($item["gift_card"] == true ) {
                    //if only a gift card do not put in shopworks
                    array_push($giftCard, true);
                } else {
                    array_push($giftCard, false);
                }
                array_push($skus, $item["sku"]);
            }
            if(!in_array(false, $giftCard)) {
//                $newOrder->set("inShopworks", 1);
                $newOrder->set("giftCard", 1);
                $giftCardOnlyOrdersCount = $giftCardOnlyOrdersCount + 1;
            } else if (in_array(true, $giftCard)){
                $newOrder->set("giftCard", 1);
                $giftCardOrdersCount = $giftCardOrdersCount + 1;
            }
            $newOrder->set("sku", json_encode($skus));
            if(isset($order["customer"])) {
                $customerArray = [];
//                foreach ($order["customer"] as $key => $value) {
//                    if($key == "default_address") {
//                        foreach ($order["customer"]["default_address"] as $key => $value) {
//                            $customerArray["default_address"][$key] = htmlentities($value);
//                        }
//                    } else {
//                        $customerArray[$key] = htmlentities($value);
//                    }
//                }
                $newOrder->set("customer", json_encode($order["customer"]));
            } else {
                $newOrder->set("customer", json_encode([]));
            }
            $newOrder->set("discountCode", json_encode($order["discount_codes"]));
            $newOrder->set("totalDiscounts", $order["total_discounts"]);
            if(isset($order["shipping_address"])) {
//                $shippingArray = [];
//                foreach ($order["shipping_address"] as $key => $value) {
//                    $shippingArray[$key] = htmlentities($value);
//                }
//                $newOrder->setAssociativeArray("shippingAddress", $shippingArray);
                $newOrder->set("shippingAddress", json_encode($order["shipping_address"]));
                $newOrder->set("shippingLines", json_encode($order["shipping_lines"]));
            } else {
                $newOrder->set("shippingAddress", json_encode([]));
                $newOrder->set("shippingLines", json_encode([]));
            }
            if(isset($order["tax_lines"])) {
                $newOrder->set("taxes", json_encode($order["tax_lines"]));
            } else {
                $newOrder->set("taxes", json_encode([]));
            }
            $emptyArray = json_encode([]);
            $newOrder->set("financialStatus", $order["financial_status"]);
            $newOrder->set("storeName", $store);
//            $newOrder->set("userId", $this->session->userId);
            $newOrder->set("companyId", $this->session->companyId);
            $newOrder->set("lineItems", json_encode($order["line_items"]));
            $newOrder->set("gateway", json_encode($order["payment_gateway_names"]));
            $newOrder->set("productIdsPack", json_encode($emptyArray));
            $newOrder->set("productIdsShipped", json_encode($emptyArray));
            $newOrder->set("dateShipped", "");
            $newOrder->set("labelLinks", json_encode($emptyArray));
            $newOrder->set("skusPacked", json_encode($emptyArray));
            $newOrder->set("skusShipped", json_encode($emptyArray));
            $newOrder->setArray("trackingNumbers", []);
            $newOrder->set("service", json_encode($emptyArray));
            $newOrder->set("totalWeight", $order["total_weight"]);
            $newOrder->set("refunds", json_encode($order["refunds"]));
            $newOrder->save(true);
        }
        return [$giftCardOrdersCount, $giftCardOnlyOrdersCount, $giftCardPayments, $giftCardOrderNumbers];
    }

    function saveFile($contents, $storeNameAb) {
        date_default_timezone_set('America/Chicago');
        $date = date('m-d-YTh-i-s');
        $file = ParseFile::createFromData($contents, "shopworks".$date.".txt");
        $file->save();
        $shopworksFiles = new ParseObject("ShopworksDownloads");
        $shopworksFiles->set("user", $this->session->userdata("name"));
        $shopworksFiles->set("downloadFile", $file);
        $shopworksFiles->set("fileURL", $file->getUrl());
        $shopworksFiles->set("storeName", $storeNameAb);
        $shopworksFiles->save();
        return $file->getURL();
    }

    function newObject($class, $objectArray) {
        $newObject = new ParseObject($class);
        foreach ($objectArray as $key => $value) {
            if($key == "userIds" || $key == "storeIds") {
                $newObject->addUnique($key, $value);
            } else {
                $newObject->set($key, $value);
            }
        }
        try{
            $newObject->save(true);
            return [true, $newObject->getObjectId()];
        } catch (ParseException $ex) {
            return [$ex->getMessage()];
        }
    }

    function uploadRoyalty($files, $post) {
        // save file to Parse
        //Remove any spaces
        //$string = preg_replace('/\s+/', '', $string); $string = str_replace(' ', '', $string);
        $imageName = $files['file']['name'];
        $fileName = str_replace(' ', '', $imageName); //Removes Spaces
        $correctedFileName = preg_replace('/[^A-Za-z0-9\-]/', '', $fileName); //Removes bad characters
        $file = ParseFile::createFromData(file_get_contents($_FILES['file']['tmp_name']), $fileName);
        $file->save();
        $newObject = new ParseObject("Royalties");
        foreach ($post as $key => $value) {
            if($key == "datePayed" && $value == " ") {
                $newObject->set($key, "Processing");
            }
            $newObject->set($key, $value);
        }
        $newObject->set("file", $file);
        try{
            $newObject->save(true);
            return true;
        } catch (ParseException $ex) {
            return $ex->getMessage();
        }
    }

    function updateCustomerAddress($orderNumber, $storeName, $objectArray) {
        $orderInfo = $this->getParseClassKey("Orders", "orderNumber", intval($orderNumber), "storeName", $storeName);
        try {
            $order = $orderInfo[0];
            $shippingInfo = json_decode($order->get("shippingAddress"));
//                $newShippingInfo = [];
                if(count($shippingInfo) > 0) {
                    foreach ($objectArray as $key => $value) {
                        if (isset($shippingInfo->{$key})) {
                            $shippingInfo->{$key} = $value;
                        } else {
                            $shippingInfo->{$key} = $value;
                        }
                    }
                } else {
                    foreach ($objectArray as $key => $value) {
                        $shippingInfo[$key] = $value;
                    }
                }
//            foreach ($shippingInfo as $info => $data) {
//                foreach ($objectArray as $key => $value) {
//                    if($key == $info) {
//                        $newShippingInfo[$info] = $value;
//                        break;
//                    } else {
//                        $newShippingInfo[$info] = $data;
//                    }
//                }
//            }
            $order->set("shippingAddress", json_encode($shippingInfo));
//            $order->setAssociativeArray("shippingAddress", $newShippingInfo);
            $order->save();
            return ["result" => "success"];
        } catch (ParseException $ex) {
            return ["result" => "error", "message" => $ex->getMessage()];
        }
    }

    function updateOrder($orderNumber, $storeName, $objectArray) {
        $orderInfo = $this->getParseClassKey("Orders", "orderNumber", intval($orderNumber), "storeName", $storeName);
        try {
            $order = $orderInfo[0];
            foreach ($objectArray as $key => $value) {
                $order->set($key, $value);
            }
            $order->save();
            return true;
        } catch (ParseException $ex) {
            return $ex->getMessage();
        }
    }

    function deleteObject($class, $key, $value) {
        $object = $this->getParseClass($class, $key, $value);
        try {
            $object[0]->destroy(true);
            return true;
        } catch (ParseException $ex) {
            return $ex->getMessage();
        }
    }

    function deleteObjects($class, $key, $value) {
        $object = $this->getParseClass($class, $key, $value);
        $successArray = [];
        $errorMessage = "";
        foreach ($object as $item) {
            try {
                $item->destroy(true);
                array_push($successArray, true);
            } catch (ParseException $ex) {
                array_push($successArray, false);
                $errorMessage = $ex->getMessage();
            }
        }
        if(in_array(false, $successArray)) {
            return $errorMessage;
        } else {
            return "true";
        }
    }

    function connectStore($shopInfo, $accessToken, $shop, $companyId) {
        //Create Store
        $objectArray = [
            "shopifyName" => $shop,
            "shopifyId" => $shopInfo->id,
            "token" => $accessToken,
            "companyId" => $companyId,
            "payingStore" => "true"
        ];
        $saveStore = $this->newObject("Stores", $objectArray);
        if(count($saveStore) > 1) {
            //Update company
            $updateCompanyArray = [
                "payingStore" => $saveStore[1]
            ];
            $this->updateObject("Companies", $updateCompanyArray, $companyId);
        }
    }

    function makeStorePaying($storeId, $companyId) {
        $objectArray = [
            "payingStore" => "true"
        ];
        $saveStore = $this->updateObject("Stores", $objectArray, $storeId);
//        if(count($saveStore) > 1) {
            //Update company
            $updateCompanyArray = [
                "payingStore" => $storeId
            ];
            $this->updateObject("Companies", $updateCompanyArray, $companyId);
//        }
    }

    function markThem() {
        $query = new ParseQuery("Orders");
        $query->equalTo("storeName", "DDA");
        $query->lessThan("orderNumber", 1349);
        $query->limit(90000);
        $orders = $query->find();
        foreach ($orders as $order) {
            $order->set("packed",  "2");
            $order->set("shipped", "2");
            $order->save(true);
        }
        return "success";
    }
}