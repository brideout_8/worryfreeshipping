<?php
/**
 * Created by PhpStorm.
 * User: rapidware
 * Date: 9/21/18
 * Time: 4:58 PM
 */


use Parse\ParseClient;
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseSessionStorage;
use ZfrShopify\ShopifyClient;

class Shopify_model extends CI_Model{

    public function __construct(){

        parent::__construct();
        $this->load->model("functions");
        $this->load->model("parse_model", "parse");
    }

    function usageCharge($description, $price) {
        $headers = array(
            'Content-Type' => 'application/json',
            'X-Shopify-Access-Token' => $this->session->shopifyToken
        );
        $data = '
        {
            "usage_charge": 
            {
                "description": "'.$description.'",
                "price": '.$price.'
            } 
        }';
        $response = Requests::post('https://'.$this->session->shopifyStore.'/admin/recurring_application_charges/'.$this->session->shopifyChargeId.'/usage_charges.json', $headers, $data);
        $responseDecode = json_decode($response->body);
        if(!isset($responseDecode->errors)) {
            $array = ["id" => $responseDecode->usage_charge->id, "price" => $responseDecode->usage_charge->price, "createdDate" => $responseDecode->usage_charge->created_at, "billingOn" => $responseDecode->usage_charge->billing_on, "companyId" => $this->session->companyId];
            $this->parse->newObject("UsageCharges", $array);
            return ["result" => "billed", "createdDate" => $responseDecode->usage_charge->created_at, "billingDate" => $responseDecode->usage_charge->billing_on];
        } else {
            $array = ["error" => json_encode($responseDecode->errors), "companyId" => $this->session->companyId];
            $this->parse->newObject("UsageCharges", $array);
            return ["result" => "notBilled", "error" => $responseDecode->errors];
        }
//        $confirmationURL = $responseDecode->recurring_application_charge->confirmation_url;
    }

    function createShopifyCharge($accessToken, $planName, $planCost, $cappedAmount, $terms, $shop, $returnUrl) {
        $headers = array(
            'Content-Type' => 'application/json',
            'X-Shopify-Access-Token' => $accessToken
        );
        $data = '
        {
            "recurring_application_charge": 
            {
                "name": "'.$planName.'",
                "price": '.$planCost.',
                "capped_amount": '.$cappedAmount.',
                "terms": "'.$terms.'", 
                "return_url": "'.$returnUrl.'"
            } 
        }';
        $response = Requests::post('https://'.$shop.'/admin/recurring_application_charges.json', $headers, $data);
        $responseDecode = json_decode($response->body);
        if(!isset($responseDecode->error)) {
            $confirmationURL = $responseDecode->recurring_application_charge->confirmation_url;
            return ["url" => $confirmationURL];
        } else {
            return ["error" => $responseDecode->error];
        }
    }

    function activateShopifyCharge($token, $shopifyName, $chargeId) {
        $headers = array(
            'Content-Type' => 'application/json',
            'X-Shopify-Access-Token' => $token
        );
        $response = Requests::get('https://'.$shopifyName.'/admin/recurring_application_charges/'.$chargeId.'.json', $headers);
        $responseDecoded = json_decode($response->body);
        if(isset($responseDecoded->recurring_application_charge)) {
            if ($responseDecoded->recurring_application_charge->status == "accepted") {
                //Activate the charge
                $response = Requests::post('https://' . $shopifyName . '/admin/recurring_application_charges/' . $chargeId . '/activate.json', $headers);
                $responseDecoded = json_decode($response->body);
                if ($responseDecoded->recurring_application_charge->status == "active") {
                    return ["status" => "active", "planName" => $responseDecoded->recurring_application_charge->name];
                } else {
                    //Card Declined
                    return ["status" => "cardDeclined"];
                }
            } else {
                //User Declined
                return ["status" => "userDeclined"];
            }
        } else {
            return ["error" =>  $responseDecoded];
        }
    }

    function checkApplicationChargeStatus($payingToken, $payingStore, $shopifyChargeId) {
        $headers = array(
            'Content-Type' => 'application/json',
            'X-Shopify-Access-Token' => $payingToken
        );
        $response = Requests::get('https://' . $payingStore . '/admin/recurring_application_charges/' . $shopifyChargeId . '.json', $headers);
        $responseDecoded = json_decode($response->body);
        if (isset($responseDecoded->recurring_application_charge)) {
            if (count($responseDecoded->recurring_application_charge) > 0) {
                if ($responseDecoded->recurring_application_charge->status == "active") {
                    return ["result" => "active"];
                } else {
                    return ["result" => "notActive"];
                }
            } else {
                return ["result" => "no charges"];
            }
        } else {
            return ["result" => "errors", "error" => $responseDecoded];
        }
    }

    function resetRemainingLabels() {
        $boughtLabels = 0;
        $currentStartDate = date_create($this->session->currentBillingPeriodStartDate);
        $formattedCurrentStartDate = $currentStartDate->format("Y-m-d");
        $todaysDate = date_create();
        $diff = date_diff($currentStartDate, $todaysDate);
        $daysDiff = $diff->days;
        if ($daysDiff > 30) {
            //New billing period
            switch ($this->session->subscriptionLevel) {
                case "1":
                    $orders = 200;
                    break;
                case "2":
                    $orders = 1000;
                    break;
                case "3":
                    $orders = 2500;
                    break;
                case "4":
                    $orders = 5000;
                    break;
                default:
                    $orders = 200;
            }
            $billingPeriodDiff = floor($daysDiff / 30);
            $daysToAdd = 30 * $billingPeriodDiff;
            $newBillingPeriodStartDate = date_add($currentStartDate, date_interval_create_from_date_string($daysToAdd . " days"));
            $equalArray = ["companyId" => $this->session->companyId, "chargeType" => "usage"];
            $greaterArray = ["billingDate" => $formattedCurrentStartDate];
            $usageCharges = $this->parse->getParseGreaterThan("Invoices", $equalArray, $greaterArray);
            if(isset($usageCharges["results"])) {
                if(count($usageCharges["results"]) > 0) {
                    foreach ($usageCharges["results"] as $result) {
                        $boughtLabels = $boughtLabels + $result->get("orderCountToCharge");
                    }
                    if($boughtLabels > $this->session->labelsRemaining) {
                        //User bought more labels than are remaining. All remaining labels get rolled over. Ex: 200 + 50 (bought) remaining is 30. User gets all 30 rolled.
                        $this->session->labelsRemaining = $this->session->labelsRemaining + $orders;
                    } else {
                        //User bought less labels than are remaining. Only the bought labels are rolled over. Ex: 200 + 50 (bought) remaining is 60. User gets 50 rolled.
                        $this->session->labelsRemaining = $orders + $boughtLabels;
                    }
                } else {
                    //No charges
                    $this->session->labelsRemaining = $orders;
                }
            } else {
                //error
            }
            $this->session->currentBillingPeriodStartDate = $newBillingPeriodStartDate->format("m/d/Y");
            $companyObject = ["labelsRemaining" => $this->session->labelsRemaining, "billingPeriodStartDate" => $this->session->currentBillingPeriodStartDate];
            $this->parse->updateObject("Companies", $companyObject, $this->session->companyId);
        }
    }

    function getRecurringCharges($lastChargeId) {
        $headers = array(
            'Content-Type' => 'application/json',
            'X-Shopify-Access-Token' => $this->session->shopifyToken
        );
        if($lastChargeId != "none") {
            $response = Requests::get('https://'.$this->session->shopifyStore.'/admin/recurring_application_charges.json?since_id='.$lastChargeId.'', $headers);
        } else {
            $response = Requests::get('https://'.$this->session->shopifyStore.'/admin/recurring_application_charges.json', $headers);
        }
        $responseDecode = json_decode($response->body);
        if(!isset($responseDecode->error)) {
            $charges = $responseDecode->recurring_application_charges;
            return ["results" => $charges];
        } else {
            return ["error" => $responseDecode->error];
        }
    }

    function resetRemainingLabelsOnPlanChange() {
        $boughtLabels = 0;
        switch ($this->session->subscriptionLevel) {
            case "1":
                $orders = 200;
                break;
            case "2":
                $orders = 1000;
                break;
            case "3":
                $orders = 2500;
                break;
            case "4":
                $orders = 5000;
                break;
            default:
                $orders = 200;
        }
        $currentStartDate = date_create($this->session->currentBillingPeriodStartDate);
        $currentStartDateMinusOne = date_sub($currentStartDate,date_interval_create_from_date_string("1 day"));
        $formattedCurrentStartDate = date_format($currentStartDateMinusOne,"Y-m-d");
        $equalArray = ["companyId" => $this->session->companyId, "chargeType" => "usage"];
        $greaterArray = ["createdDate" => $formattedCurrentStartDate];
        $usageCharges = $this->parse->getParseGreaterThan("Invoices", $equalArray, $greaterArray);
        if(isset($usageCharges["results"])) {
            if(count($usageCharges["results"]) > 0) {
                foreach ($usageCharges["results"] as $result) {
                    $boughtLabels = $boughtLabels + $result->get("orderCountToCharge");
                }
                if($boughtLabels > $this->session->labelsRemaining) {
                    //User bought more labels than are remaining. All remaining labels get rolled over. Ex: 200 + 50 (bought) remaining is 30. User gets all 30 rolled.
                    $this->session->labelsRemaining = $this->session->labelsRemaining + $orders;
                } else {
                    //User bought less labels than are remaining. Only the bought labels are rolled over. Ex: 200 + 50 (bought) remaining is 60. User gets 50 rolled.
                    $this->session->labelsRemaining = $orders + $boughtLabels;
                }
            } else {
                //No charges
                $this->session->labelsRemaining = $orders;
            }
        } else {
            //error
            $this->session->labelsRemaining = $orders;
        }
    }

    function updateShippingAddress($orderId, $newAddressArray) {
        $headers = array(
            'Content-Type' => 'application/json',
            'X-Shopify-Access-Token' => $this->session->shopifyToken
        );
        $data = '
        {
            "order": {
                "id": '.$orderId.',
                "shipping_address": {
                  "address1": "'.$newAddressArray["address1"].'",
                  "address2": "'.$newAddressArray["address2"].'",
                  "name": "'.$newAddressArray["name"].'",
                  "country": "'.$newAddressArray["country"].'",
                  "province": "'.$newAddressArray["province"].'",
                  "first_name": "'.$newAddressArray["first_name"].'",
                  "last_name": "'.$newAddressArray["last_name"].'",
                  "city": "'.$newAddressArray["city"].'",              
                  "province_code": "'.$newAddressArray["province_code"].'",              
                  "zip": "'.$newAddressArray["zip"].'",              
                  "country_code": "'.$newAddressArray["country_code"].'",              
                  "phone": "'.$newAddressArray["phone"].'",              
                  "company": "'.$newAddressArray["company"].'"              
                }
            }
        }';
        $response = Requests::put('https://'.$this->session->shopifyStore.'/admin/orders/'.$orderId.'.json', $headers, $data);
        $responseDecode = json_decode($response->body);
        if(!isset($responseDecode->error)) {
            return ["result" => "true", "test" => $response];
        } else {
            return ["result" => "error", "error" => $responseDecode->error];
        }
    }

    function getShopInfo($accessToken, $shopName) {
        $headers = array(
            'Content-Type' => 'application/json',
            'X-Shopify-Access-Token' => $accessToken
        );
        $response = Requests::get('https://'.$shopName.'/admin/shop.json', $headers);
        $shopBody = json_decode($response->body);
        $shopInfo = $shopBody->shop;
        return $shopInfo;
    }

    function setUpNewStore($accessToken, $shopName, $shopId, $companyId, $storeStatus) {
        //Save accessToken, shop name, display name
        $objecyArray = [
            "shopifyName" => $shopName,
            "token" => $accessToken,
            "companyId" => $companyId,
            "shopifyId" => intval($shopId),
            "status" => $storeStatus
        ];
        $saveShop = $this->parse->newObject("Stores", $objecyArray);
        $webhooks = $this->functions->addWebhooks($shopName,$accessToken, $saveShop[1]);
        //Create location and warehouse
        $companyInfo = $this->parse->getParseClassArray("Companies", ["objectId" => $this->session->companyId]);
        $shopInfo = $this->shopify_model->getShopInfo($accessToken, $shopName);
        $newLocationsFind = $this->shopify_model->getLocations($accessToken, $shopName);
        $newLocations = $newLocationsFind["locations"];
        foreach ($newLocations as $newLocation) {
            $locationArray = [
                "locationId" => $newLocation->id,
                "companyId" => $this->session->companyId,
                "name" => $newLocation->name,
                "phone" => $companyInfo[0]->get("phone"),
                "address1" => $newLocation->address1,
                "address2" => $newLocation->address2,
                "city" => $newLocation->city,
                "state" => $newLocation->province,
                "stateCode" => $newLocation->province_code,
                "zip" => $newLocation->zip,
                "country" => $newLocation->country_name,
                "countryCode" => $newLocation->country_code,
                "legacy" => $newLocation->legacy,
                "shopifyName" => $shopName,
            ];
            if ($shopInfo->primary_location_id == $newLocation->id) {
                $locationArray["default"] = "true";
                $locationArray["returnDefault"] = "true";
            } else {
                $locationArray["default"] = "false";
                $locationArray["returnDefault"] = "false";
            }
            $warehouse = $this->shipEngine->createWarehouse($newLocation, $this->session->shipEngineAPIKey, $companyInfo[0]->get("phone"), $companyInfo[0]->get("name"));
            $locationArray["warehouseId"] = $warehouse->warehouse_id;
            $saveLocation = $this->parse->newObject("Locations", $locationArray);
        }
        return $saveShop[1];
    }

    function setUserData($companyData) {
        $currentUser = ParseUser::getCurrentUser();
        $payingStore = $companyData[0]->get("payingStore");
        $shopifyStoresInfo = [];
        $payingToken = "";
        $shopifyName = "";
        $shopifyChargeId = "";
        $carriersArray = [];
        $carriers = $this->parse->getParseClass("Carriers", "companyId", $companyData[0]->getObjectId());
        foreach ($carriers as $carrier) {
            if ($carrier->get("status") == "enabled") {
                $carriersArray[$carrier->get("service")] = $carrier->get("carrierId");
            }
        }
        $planName = $companyData[0]->get("subscriptionLevel");
        $subLevel = getSubLevel($planName)["subLevel"];
        $stores = $this->parse->getParseClass("Stores", "companyId", $companyData[0]->getObjectId());
        foreach ($stores as $store) {
            if($store->get("status") != "inactive") {
                $storeArray = [];
                $storeArray["shopifyName"] = $store->get("shopifyName");
                $storeArray["shopifyToken"] = $store->get("token");
                $storeArray["shopifyDisplayName"] = $store->get("displayName");
                $storeArray["importOrders"] = $store->get("importOrders");
                $shopifyStoresInfo[$store->get("shopifyName")] = $storeArray;
                if ($store->getObjectId() == $payingStore) {
                    $payingToken = $store->get("token");
                    $shopifyChargeId = $store->get("shopifyChargeId");
                    $shopifyName = $store->get("shopifyName");
                }
            }
        }
        if($currentUser->get("tour") == null) {
            $tour = [];
        } else {
            $tour = $currentUser->get("tour");
        }
        $this->session->set_userdata(
            array(
                "userId" => $currentUser->getObjectId(),
                "email" => $currentUser->get("email"),
                "role" => $currentUser->get("role"),
                "name" => $currentUser->get("name"),
                "username" => $currentUser->get("username"),
                "permissions" => $currentUser->get("permissions"),
                "shopifyToken" => $payingToken,
                "shopifyStore" => $shopifyName,
                "masterUserId" => $currentUser->get("masterUserId"),
                "companyId" => $companyData[0]->getObjectId(),
                "unitSystem" => $companyData[0]->get("unitSystem"),
                "weightMeasurement" => $companyData[0]->get("weightMeasurement"),
                "timeZone" => $companyData[0]->get("timeZone"),
                "trialStatus" => $companyData[0]->get("trialStatus"),
                "labelsRemaining" => $companyData[0]->get("labelsRemaining"),
                "shipEngineAPIKey" => $companyData[0]->get("shipEngineAPIKey"),
                "carriers" => $carriersArray,
                "subscriptionLevel" => $subLevel,
                "currentBillingPeriodStartDate" => $companyData[0]->get("billingPeriodStartDate"),
                "shopifyChargeId" => $shopifyChargeId,
                "importedOrder" => $companyData[0]->get("importedOrder"),
                "tour" => $tour,
                "shopifyStoresInfo" => $shopifyStoresInfo
            )
        );
    }

    function makeStoreAndOrdersActive($storeObjectId, $storeName) {
        $this->parse->updateObject("Stores", ["status" => "active"], $storeObjectId);
        $orders = $this->parse->getParseClassArray("Orders", ["storeName" => $storeName]);
        foreach ($orders as $order) {
            $this->parse->updateObject("Orders", ["status" => "active"], $order->getObjectId());
        }
    }

    function getLocations($accessToken, $shop) {
        $headers = array(
            'Content-Type' => 'application/json',
            'X-Shopify-Access-Token' => $accessToken
        );
        $response = Requests::get('https://'.$shop.'/admin/locations.json', $headers);
        $responseDecode = json_decode($response->body);
        if(!isset($responseDecode->error)) {
            $locations = $responseDecode->locations;
            return ["locations" => $locations];
        } else {
            return ["error" => $responseDecode->error];
        }
    }
}