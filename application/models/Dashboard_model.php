<?php
/**
 * Created by PhpStorm.
 * User: rapidware
 * Date: 10/30/17
 * Time: 7:59 PM
 */

use Parse\ParseClient;
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseSessionStorage;

class Dashboard_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->model("parse_model", "parse");
    }

    function getClientMetrics($clientId){
        $skusFind = $this->parse->getParseClass("Skus", "clientId", $clientId);
        $skus = [];
        if(count($skusFind) > 0) {
            $skusObject = $skusFind[0]->get("sku");
            foreach ($skusObject as $key => $value) {
                foreach ($value as $array)
                    array_push($skus, $array);
            }
        }
        if(count($skus) > 0) {
            $dateNow = new DateTime();
            $month = $dateNow->format('n') ;
            if($month == 4 || $month == 7 || $month == 10 || $month == 1) {
                $startDate = date('Y/m/d', strtotime('today - 28 days'));
            } else {
                if($month < 4) {
                    $startDate = date("Y/m/d", strtotime('01/01/'.$dateNow->format("Y")));
                } else if($month < 7) {
                    $startDate = date("Y/m/d", strtotime('04/01/'.$dateNow->format("Y")));
                } else if($month < 10) {
                    $startDate = date("Y/m/d", strtotime('07/01/'.$dateNow->format("Y")));
                } else {
                    $startDate = date("Y/m/d", strtotime('01/01/'. date('Y',strtotime('today + 80 days'))));
                }
            }
            $query = new ParseQuery("Orders");
            $query->containedIn("sku", $skus);
            $query->greaterThan("dateOrdered", $startDate);
            $query->limit(90000);
            //Get qty of shirts sold
            $orders = $query->find();
            $skuTitle = [];
            $ordersPerDay = [];
            $totalRoyalties = 0;
            $totalRoyaltiesPerQuarter = 0;
            foreach ($orders as $order) {
                //Used to find if order was placed in past 30 days
                if ($order->get("dateOrdered") > date('Y/m/d', strtotime('today - 28 days'))) {
                    foreach ($order->get("lineItems") as $lineItems) {
                        if (in_array($lineItems["sku"], $skus)) {
                            //Used for Top products
                            if (array_key_exists($lineItems["title"], $skuTitle)) {
                                $skuTitle[$lineItems["title"]] = $skuTitle[$lineItems["title"]] + $lineItems["quantity"];
                            } else {
                                $skuTitle[$lineItems["title"]] = $lineItems["quantity"];
                            }
                            //Used for orders per day
                            foreach ($skusObject as $key => $array) {
                                if (in_array($lineItems["sku"], $array)) {
                                    $royaltyAmount = intval($key) / 100;
                                    break;
                                }
                            }
                            //Check to see if orders already exists in array for that day
                            if (array_key_exists($order->get("dateOrdered"), $ordersPerDay)) {
                                $ordersPerDay[$order->get("dateOrdered")] = $ordersPerDay[$order->get("dateOrdered")] + round((floatval($lineItems["price"]) * $royaltyAmount) * $lineItems["quantity"], 2);
                                $totalRoyalties = $totalRoyalties + round((floatval($lineItems["price"]) * $royaltyAmount) * $lineItems["quantity"] , 2);
                            } else {
                                //If the day does not exist in the array add it.
                                $ordersPerDay[$order->get("dateOrdered")] = round(((floatval($lineItems["price"]) * $royaltyAmount))* $lineItems["quantity"], 2);
                                $totalRoyalties = $totalRoyalties + round((floatval($lineItems["price"]) * $royaltyAmount) * $lineItems["quantity"] , 2);
                            }
                            //Used for total royalties in quarter only
                            if($month == 4) {
                                if ($order->get("dateOrdered") > date("Y/m/d", strtotime('04/01/'.$dateNow->format("Y")))) {
                                    $totalRoyaltiesPerQuarter = $totalRoyaltiesPerQuarter + round(((floatval($lineItems["price"]) * $royaltyAmount))* $lineItems["quantity"], 2);
                                };
                            } else if($month == 7) {
                                if ($order->get("dateOrdered") > date("Y/m/d", strtotime('07/01/'.$dateNow->format("Y")))) {
                                    $totalRoyaltiesPerQuarter = $totalRoyaltiesPerQuarter + round(((floatval($lineItems["price"]) * $royaltyAmount))* $lineItems["quantity"], 2);
                                };
                            } else if($month == 10) {
                                if ($order->get("dateOrdered") > date("Y/m/d", strtotime('10/01/'.$dateNow->format("Y")))) {
                                    $totalRoyaltiesPerQuarter = $totalRoyaltiesPerQuarter + round(((floatval($lineItems["price"]) * $royaltyAmount))* $lineItems["quantity"], 2);
                                };
                            } else {
                                if ($order->get("dateOrdered") > date("Y/m/d", strtotime('01/01/'.$dateNow->format("Y")))) {
                                    $totalRoyaltiesPerQuarter = $totalRoyaltiesPerQuarter + round(((floatval($lineItems["price"]) * $royaltyAmount))* $lineItems["quantity"], 2);
                                };
                            }

                        }
                    }
                } else {
                    //Used for total royalties in quarter only
                    foreach ($order->get("lineItems") as $lineItems) {
                        if (in_array($lineItems["sku"], $skus)) {
                            //Used for orders per day
                            foreach ($skusObject as $key => $array) {
                                if (in_array($lineItems["sku"], $array)) {
                                    $royaltyAmount = intval($key) / 100;
                                    break;
                                }
                            }
                            $totalRoyaltiesPerQuarter = $totalRoyaltiesPerQuarter + round(((floatval($lineItems["price"]) * $royaltyAmount))* $lineItems["quantity"], 2);
                        }
                    }
                }
            }
            return [$skuTitle,$ordersPerDay, $totalRoyalties, $totalRoyaltiesPerQuarter];
        } else {
            return [];
        }
    }

    function royaltiesPerQuarter($royalties) {
        if(count($royalties)>0) {
            $royaltyArray = ["Quarter 1", "Quarter 2", "Quarter 3", "Quarter 4", "Quarter 1B", "Quarter 2B", "Quarter 3B", "Quarter 4B"];
            $royaltiesArray = array();
            //Used if client does not have a full two years of royalties
            if (count($royalties) < 9) {
                $count = count($royalties);
            } else {
                $count = 9;
            }
            //Excluding a year when a new year happens
            $dateYear = date("Y");
            $yearExcludeStamp = strtotime($dateYear . ' -2 year');
            $yearExclude = date('Y', $yearExcludeStamp);
            //Current year of royalty report
            $currentYearRoyalty = substr($royalties[0]->get("quarter"), 0, 4);
            $currentYearRoyaltyDate = $currentYearRoyalty . '/12/1';
            $lastYearRoyalty = strtotime($currentYearRoyaltyDate . ' -1 year');
            for ($x = 0; $x < $count; $x++) {
                $quarter = $royalties[$x]->get("quarter");
                $y = 0;
                foreach ($royaltyArray as $key) {
                    //Checking if the word in royaltyArray exist in the database "quarter". Also checking if the year to exclude exists
                    if (strpos($quarter, $key) !== false && !array_key_exists($key, $royaltiesArray) && $y < 4 && strpos($quarter, $yearExclude) === false && strpos($quarter, $currentYearRoyalty) !== false) {
                        $royaltiesArray[$key] = $royalties[$x]->get("paymentAmount");
                        break;
                    } else if(strpos($quarter, $key) !== false && $y < 4 && strpos($quarter, $yearExclude) === false && strpos($quarter, $currentYearRoyalty) !== false) {
                        $royaltiesArray[$key] = $royaltiesArray[$key] + $royalties[$x]->get("paymentAmount");
                        break;
                    } else if (strpos($quarter, substr($key, 0, -1)) !== false && !array_key_exists($key, $royaltiesArray) && $y > 3) {
                        $royaltiesArray[$key] = $royalties[$x]->get("paymentAmount");
                        break;
                    } else if (strpos($quarter, substr($key, 0, -1)) !== false && $y > 3) {
                        $royaltiesArray[$key] = $royaltiesArray[$key] + $royalties[$x]->get("paymentAmount");
                        break;
                    }
                    $y++;
                }
            };
        } else {
            $royaltiesArray = [];
            $currentYearRoyalty = "2018";
            $lastYearRoyalty = 1517441556;
        }

        return [$royaltiesArray, $currentYearRoyalty, $lastYearRoyalty];
    }

    function ordersPerDay($clientMetrics) {
        $dateDay = date("Y/m/d");
        $dayArray = [];
        for($x=0;$x<28;$x++) {
            $newDateLong = date('Y/m/d', strtotime('today - ' . $x . ' days'));
            $newDateDay = date('d', strtotime('today - ' . $x . ' days'));
            if(count($clientMetrics) > 1) {
                if (array_key_exists($newDateLong, $clientMetrics[1])) {
                    $dayArray[$newDateDay] = $clientMetrics[1][$newDateLong];
                } else {
                    $dayArray[$newDateDay] = 0;
                }
            } else {
                $dayArray[$newDateDay] = 0;
            }
        }
        return $dayArray;
    }

    function daysLeftInQuarter() {
        $newDate = new DateTime();
        $month = $newDate->format('n') ;

        if ($month < 4) {
            $newDate = new DateTime("04/01/".$newDate->format("Y"));
        } elseif ($month > 3 && $month < 7) {
            $newDate = new DateTime("07/01/".$newDate->format("Y"));
        } elseif ($month > 6 && $month < 10) {
            $newDate = new DateTime("10/01/".$newDate->format("Y"));
        } elseif ($month > 9) {
            $newDate = new DateTime("01/01/".$newDate->format("Y"));
        }
        $todaysDate = new DateTime();
        $diff = $newDate->diff($todaysDate)->format("%a");
        return $diff;
    }

}