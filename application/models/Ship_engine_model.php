<?php
/**
 * Created by PhpStorm.
 * User: Marketing101
 * Date: 12/17/17
 * Time: 2:24 PM
 */


use Parse\ParseClient;
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseSessionStorage;
use ZfrShopify\ShopifyClient;

class Ship_engine_model extends CI_Model {

    public function __construct() {

        parent::__construct();
        // Your own constructor code
        include(APPPATH. 'libraries/vendor/rmccue/requests/library/Requests.php');
        Requests::register_autoloader();
        $this->load->model("functions");
        $this->load->model("parse_model", "parse");
//        if($this->session->userdata('login_id')=='') {
//            redirect($this->config->item('base_url'),'Location');
//        }
    }

    function createAccount($firstName, $lastName, $companyName, $companyId, $originCountry) {
        $shop = $this->input->post("shopifyStore");
        $accessToken = $this->input->post("accessToken");
        $headers = array(
            'Content-type' => 'application/json',
            'api-key' => 'amd6k/C4oRbFsLO+TpdvyQBINiDvusOxVIFUAdP14r4'
        );
        $data = '
        {
            "first_name": "'.$firstName.'",
            "last_name": "'.$lastName.'",
            "company_name": "'.$companyName.'",
            "external_account_id": "'.$companyId.'",
            "origin_country_code": "'.$originCountry.'"
          }';
        $response = Requests::post('https://api.shipengine.com/v1/partners/accounts', $headers, $data);
        $responseDecode = json_decode($response->body);
        return $responseDecode;
    }

    function getLabel() {
        $post = $_POST;
        $dateShippedDate = DateTime::createFromFormat('m/d/Y', $post["dateShipped"]);
        $dateShipped = $dateShippedDate->format('Y-m-d');
        if($post["weightMeasurement"] == "lb") {
            $totalWeight = $post["weight"] * 453.59237;
        } else if($post["weightMeasurement"] == "oz") {
            $totalWeight = $post["weight"] * 28.34952;
        } else if($post["weightMeasurement"] == "kg") {
            $totalWeight = $post["weight"] * 1000;
        } else {
            $totalWeight = $post["weight"];
        }

        if($this->session->unitSystem == "imperial") {
            $dim = "inch";
        } else {
            $dim = "centimeter";
        }
        $serviceCode = $post["serviceCode"];
        //usps_first_class_mail usps_priority_mail usps_priority_mail_express usps_first_class_mail_international usps_priority_mail_international usps_priority_mail_express_international
        //Endecia se-186799  Stamps free se-186794

        $headers = array(
            'Content-type' => 'application/json',
            'api-key' => $this->session->shipEngineAPIKey
        );
//        $data = "'".$myJSON."'";
        $data = '
        {
          "shipment": {
            "carrier_id": "'.$post["carrierId"].'",
            "service_code": "'.$serviceCode.'",
            "ship_date": "'.$dateShipped.'",
            "ship_to": {
              "name": "'.$post["firstName"]." ".$post["lastName"].'",
              "phone": "'.$post["phone"].'",
              "company_name": "'.$post["companyName"].'",
              "address_line1": "'.$post["address1"].'",
              "address_line2": "'.$post["address2"].'",
              "city_locality": "'.$post["city"].'",
              "state_province": "'.strtoupper($post["state"]).'",
              "postal_code": "'.$post["zip"].'",
              "country_code": "'.strtoupper($post["country"]).'",
              "address_residential_indicator": "No"
            },
            "warehouse_id": "'.$post["warehouse"].'",';
        if($post["location"] == "international") {
            $data .= '
            "customs": {
              "contents": "merchandise",
              "customs_items": [';
            for($x=0; $x<count($post["customsDescription"]); $x++) {
                if($x + 1 == count($post["customsDescription"])) {
                    $data .= '
                    {
                      "description": "'.$post["customsDescription"][$x].'",
                      "quantity": '.$post["customsQty"][$x].',
                      "value": '.$post["customsValue"][$x].',
                      "country_of_origin": "'.$post["coo"][$x].'"
                    }';
                } else {
                    $data .= '
                    {
                      "description": "'.$post["customsDescription"][$x].'",
                      "quantity": '.$post["customsQty"][$x].',
                      "value": '.$post["customsValue"][$x].',
                      "harmonized_tariff_code": "'.$post["tariffCode"][$x].'",
                      "country_of_origin": "'.$post["coo"][$x].'"
                    },';
                }
            }
            $data .= '
            ],
            "non_delivery": "return_to_sender"
             },';
        }
            $data .= '
            "confirmation": "'.$post["confirmation"].'",
            "packages": [
              {
                "package_code": "'.$post["packaging"].'",
                "weight": {
                  "value": '.$totalWeight.',
                  "unit": "gram"
                },';
        if($post["packaging"] == "none") {
            $data .='"dimensions": {
                  "unit": "'.$dim.'",
                  "length": '.$post["length"].',
                  "width": '.$post["width"].',
                  "height": '.$post["height"].'
                }';
        }
        $data .='
              }
            ]
          },
        }';

        $response = Requests::post('https://api.shipengine.com/v1/labels', $headers, $data);
        $labelData = json_decode($response->body);
        return $labelData;
    }

    function getReturnLabel() {
        $post = $_POST;
        $dateShippedDate = DateTime::createFromFormat('m/d/Y', $post["dateShipped"]);
        $dateShipped = $dateShippedDate->format('Y-m-d');
        if($post["weightMeasurement"] == "lb") {
            $totalWeight = $post["weight"] * 453.59237;
        } else if($post["weightMeasurement"] == "oz") {
            $totalWeight = $post["weight"] * 28.34952;
        } else if($post["weightMeasurement"] == "kg") {
            $totalWeight = $post["weight"] * 1000;
        } else {
            $totalWeight = $post["weight"];
        }
        if($this->session->unitSystem == "imperial") {
            $dim = "inch";
        } else {
            $dim = "centimeter";
        }

        $serviceCode = $post["serviceCode"];
        //usps_first_class_mail usps_priority_mail usps_priority_mail_express usps_first_class_mail_international usps_priority_mail_international usps_priority_mail_express_international
        //Endecia se-186799  Stamps free se-186794

        $headers = array(
            'Content-type' => 'application/json',
            'api-key' => $this->session->shipEngineAPIKey
        );
        $data = '
        {
          "shipment": {
            "carrier_id": "'.$post["carrierId"].'",
            "service_code": "'.$serviceCode.'",
            "ship_date": "'.$dateShipped.'",
            "ship_to": {
              "name": "'.$post["returnName"].'",
              "phone": "'.$post["returnNumber"].'",
              "company_name": "",
              "address_line1": "'.$post["returnAddress1"].'",
              "address_line2": "'.$post["returnAddress2"].'",
              "city_locality": "'.$post["returnCity"].'",
              "state_province": "'.strtoupper($post["returnState"]).'",
              "postal_code": "'.$post["returnZip"].'",
              "country_code": "'.strtoupper($post["returnCountry"]).'",
              "address_residential_indicator": "No"
            },
            "ship_from": {
              "name": "'.$post["firstName"]." ".$post["lastName"].'",
              "phone": "'.$post["phone"].'",
              "company_name": "'.$post["companyName"].'",
              "address_line1": "'.$post["address1"].'",
              "address_line2": "'.$post["address2"].'",
              "city_locality": "'.$post["city"].'",
              "state_province": "'.strtoupper($post["state"]).'",
              "postal_code": "'.$post["zip"].'",
              "country_code": "'.strtoupper($post["country"]).'",
              "address_residential_indicator": "No"
            },';
        if($post["location"] == "international") {
            $data .= '
            "customs": {
              "contents": "merchandise",
              "customs_items": [';
            for($x=0; $x<count($post["customsDescription"]); $x++) {
                if($x + 1 == count($post["customsDescription"])) {
                    $data .= '
                    {
                      "description": "'.$post["customsDescription"][$x].'",
                      "quantity": '.$post["customsQty"][$x].',
                      "value": '.$post["customsValue"][$x].',
                      "country_of_origin": "'.$post["coo"][$x].'"
                    }';
                } else {
                    $data .= '
                    {
                      "description": "'.$post["customsDescription"][$x].'",
                      "quantity": '.$post["customsQty"][$x].',
                      "value": '.$post["customsValue"][$x].',
                      "harmonized_tariff_code": "'.$post["tariffCode"][$x].'",
                      "country_of_origin": "'.$post["coo"][$x].'"
                    },';
                }
            }
            $data .= '
            ],
            "non_delivery": "return_to_sender"
             },';
        }
            $data .= '
            "packages": [
              {
                "weight": {
                  "value": '.$totalWeight.',
                  "unit": "gram"
                },
                "dimensions": {
                  "unit": "'.$dim.'",
                  "length": '.$post["length"].',
                  "width": '.$post["width"].',
                  "height": '.$post["height"].'
                }
              }
            ]
          },
        }';

        $response = Requests::post('https://api.shipengine.com/v1/labels', $headers, $data);
        $labelData = json_decode($response->body);
        return $labelData;
    }

    function validateAddress($post) {
        $headers = array(
            'Content-Type' => 'application/json',
            'api-key' => $this->session->shipEngineAPIKey
        );
        $data = '
        [
          {
              "name": "'.$post["firstName"]." ".$post["lastName"].'",
              "phone": "+1 (714) 781-4565",
              "company_name": "'.$post["companyName"].'",
              "address_line1": "'.$post["address1"].'",
              "address_line2": "'.$post["address2"].'",
              "city_locality": "'.$post["city"].'",
              "state_province": "'.strtoupper($post["state"]).'",
              "postal_code": "'.$post["zip"].'",
              "country_code": "'.strtoupper($post["country"]).'"
          }
        ]';
        $response = Requests::post('https://api.shipengine.com/v1/addresses/validate', $headers, $data);
        $responseData = json_decode($response->body);
        return $responseData;
    }

    function createScanForm() {
        $headers = array(
            'Content-type' => 'application/json',
            'api-key' => '6LP/ETFCykKXYsDD2Wt416qQuwEer4Jj+G54kL0zI9E'
        );
        $data = '
        {
          "carrier_id": "se-186799",
          "excluded_label_ids": [],
          "warehouse_id": "se-251885",
          "ship_date": "'.date("c").'"
        }';
        $response = Requests::post('https://api.shipengine.com/v1/manifests', $headers, $data);
        $scanFormData = json_decode($response->body);
        return $scanFormData;
    }

    function getRates($post) {
        if($post["weightMeasurement"] == "lb") {
            $totalWeight = $post["weight"] * 453.59237;
        } else if($post["weightMeasurement"] == "oz") {
            $totalWeight = $post["weight"] * 28.34952;
        } else if($post["weightMeasurement"] == "kg") {
            $totalWeight = $post["weight"] * 1000;
        } else {
            $totalWeight = $post["weight"];
        }
        if($this->session->unitSystem == "imperial") {
            $dim = "inch";
        } else {
            $dim = "centimeter";
        }
        //If using a carrier specific packaging only get rates for them
        $carrierIds = [];
        $carriers = $this->session->carriers;
        if($post["packaging"] != "package") {
            array_push($carrierIds, $carriers[$post["packageCarrier"]]);
        } else {
            foreach ($carriers as $name => $id) {
                array_push($carrierIds, $id);
            }
        }
        $headers = array(
            'Content-type' => 'application/json',
            'api-key' => $this->session->shipEngineAPIKey
        );
        $data = '
        {
          "shipment": {        
            "ship_to": {
              "name": "'.$post["firstName"]." ".$post["lastName"].'",
              "phone": "'.$post["phone"].'",
              "company_name": "'.$post["companyName"].'",
              "address_line1": "'.$post["address1"].'",
              "address_line2": "'.$post["address2"].'",
              "city_locality": "'.$post["city"].'",
              "state_province": "'.strtoupper($post["state"]).'",
              "postal_code": "'.$post["zip"].'",
              "country_code": "'.strtoupper($post["country"]).'",
              "address_residential_indicator": "No"
            },
            "warehouse_id": "'.$post["warehouse"].'",';
        if($post["location"] == "international") {
            $data .= '
            "customs": {
              "contents": "merchandise",
              "customs_items": [';
            for($x=0; $x<count($post["customsDescription"]); $x++) {
                if($x + 1 == count($post["customsDescription"])) {
                    $data .= '
                    {
                      "description": "'.$post["customsDescription"][$x].'",
                      "quantity": '.$post["customsQty"][$x].',
                      "value": '.$post["customsValue"][$x].',
                      "country_of_origin": "'.$post["coo"][$x].'"
                    }';
                } else {
                    $data .= '
                    {
                      "description": "'.$post["customsDescription"][$x].'",
                      "quantity": '.$post["customsQty"][$x].',
                      "value": '.$post["customsValue"][$x].',
                      "harmonized_tariff_code": "'.$post["tariffCode"][$x].'",
                      "country_of_origin": "'.$post["coo"][$x].'"
                    },';
                }
            }
            $data .= '
            ],
            "non_delivery": "return_to_sender"
             },';
        }
        $data .= '
            "confirmation": "'.$post["confirmation"].'",
            "packages": [
              {
                "package_code": "'.$post["packaging"].'",
                "weight": {
                  "value": '.$totalWeight.',
                  "unit": "gram"
                },';
        if($post["packaging"] == "none") {
            $data .='"dimensions": {
                  "unit": "'.$dim.'",
                  "length": '.$post["length"].',
                  "width": '.$post["width"].',
                  "height": '.$post["height"].'
                }';
        }
        $data .='
              }
            ]
          },
          "rate_options": {
            "carrier_ids": [';
        foreach ($carrierIds as $carrierId) {
            $data .= '"'.$carrierId.'",';
        }
        $data .= ']
          }
        }';
        $response = Requests::post('https://api.shipengine.com/v1/rates', $headers, $data);
        $rateData = json_decode($response->body);
        return $rateData;
    }

    function getReturnRates($post) {
        if($post["weightMeasurement"] == "lb") {
            $totalWeight = $post["weight"] * 453.59237;
        } else if($post["weightMeasurement"] == "oz") {
            $totalWeight = $post["weight"] * 28.34952;
        } else if($post["weightMeasurement"] == "kg") {
            $totalWeight = $post["weight"] * 1000;
        } else {
            $totalWeight = $post["weight"];
        }
        if($this->session->unitSystem == "imperial") {
            $dim = "inch";
        } else {
            $dim = "centimeter";
        }
        //If using a carrier specific packaging only get rates for them
        $carrierIds = [];
        $carriers = $this->session->carriers;
        if($post["packaging"] != "package") {
            array_push($carrierIds, $carriers[$post["packageCarrier"]]);
        } else {
            foreach ($carriers as $name => $id) {
                array_push($carrierIds, $id);
            }
        }
        $headers = array(
            'Content-type' => 'application/json',
            'api-key' => $this->session->shipEngineAPIKey
        );
        $data = '
        {
          "shipment": {        
            "ship_to": {
              "name": "'.$post["returnName"].'",
              "phone": "'.$post["returnNumber"].'",
              "company_name": "",
              "address_line1": "'.$post["returnAddress1"].'",
              "address_line2": "'.$post["returnAddress2"].'",
              "city_locality": "'.$post["returnCity"].'",
              "state_province": "'.strtoupper($post["returnState"]).'",
              "postal_code": "'.$post["returnZip"].'",
              "country_code": "'.strtoupper($post["returnCountry"]).'",
              "address_residential_indicator": "No"
            },
            "ship_from": {
              "name": "'.$post["firstName"]." ".$post["lastName"].'",
              "phone": "'.$post["phone"].'",
              "company_name": "'.$post["companyName"].'",
              "address_line1": "'.$post["address1"].'",
              "address_line2": "'.$post["address2"].'",
              "city_locality": "'.$post["city"].'",
              "state_province": "'.$post["state"].'",
              "postal_code": "'.$post["zip"].'",
              "country_code": "'.$post["country"].'",
              "address_residential_indicator": "No"
            },';
        if($post["location"] == "international") {
            $data .= '
            "customs": {
              "contents": "merchandise",
              "customs_items": [';
            for($x=0; $x<count($post["customsDescription"]); $x++) {
                if($x + 1 == count($post["customsDescription"])) {
                    $data .= '
                    {
                      "description": "'.$post["customsDescription"][$x].'",
                      "quantity": '.$post["customsQty"][$x].',
                      "value": '.$post["customsValue"][$x].',
                      "country_of_origin": "'.$post["coo"][$x].'"
                    }';
                } else {
                    $data .= '
                    {
                      "description": "'.$post["customsDescription"][$x].'",
                      "quantity": '.$post["customsQty"][$x].',
                      "value": '.$post["customsValue"][$x].',
                      "harmonized_tariff_code": "'.$post["tariffCode"][$x].'",
                      "country_of_origin": "'.$post["coo"][$x].'"
                    },';
                }
            }
            $data .= '
            ],
            "non_delivery": "return_to_sender"
             },';
        }
        $data .= '
            "confirmation": "'.$post["confirmation"].'",
            "packages": [
              {
                "package_code": "'.$post["packaging"].'",
                "weight": {
                  "value": '.$totalWeight.',
                  "unit": "gram"
                },';
        if($post["packaging"] == "none") {
            $data .='"dimensions": {
                  "unit": "'.$dim.'",
                  "length": '.$post["length"].',
                  "width": '.$post["width"].',
                  "height": '.$post["height"].'
                }';
        }
        $data .='
              }
            ]
          },
          "rate_options": {
            "carrier_ids": ["';
        foreach ($carrierIds as $carrierId) {
            $data .= $carrierId;
        }
        $data .= '"]
          }
        }';
        $response = Requests::post('https://api.shipengine.com/v1/rates', $headers, $data);
        $rateData = json_decode($response->body);
        return $rateData;
    }

    function getReturnLabels() {
        $headers = array(
            'content-type' => 'application/json',
            'api-key' => $this->session->shipEngineAPIKey
        );
        $response = Requests::get('https://api.shipengine.com/v1/labels?sort_dir=desc&page_size=100', $headers);
        $labelData = json_decode($response->body);
        return $labelData->labels;
    }

    function getFunds($apiKey, $carrierId) {
        $headers = array(
            'Content-type' => 'application/json',
            'api-key' => $apiKey
        );
        $response = Requests::get('https://api.shipengine.com/v1/carriers/'.$carrierId.'', $headers);
        $body = json_decode($response->body);
        return $body->balance;
    }

    function createWarehouse($locationInfo, $shipEngineAPIKey, $phoneNumber,  $companyName) {
        $headers = array(
            'Content-type' => 'application/json',
            'api-key' => $shipEngineAPIKey
        );
        $data = '
        {
          "name": "'.$companyName.'",
          "origin_address": {
            "name": "'.$companyName.'",
            "phone": "'.$phoneNumber.'",
            "address_line1": "'.$locationInfo->address1.'",
            "address_line2": "'.$locationInfo->address2.'",
            "city_locality": "'.$locationInfo->city.'",
            "state_province": "'.$locationInfo->province_code.'",
            "postal_code": "'.$locationInfo->zip.'",
            "country_code": "'.$locationInfo->country_code.'",
            "address_residential_indicator": "No"
          },
          "return_address": {
            "name": "'.$companyName.'",
            "phone": "'.$phoneNumber.'",
            "address_line1": "'.$locationInfo->address1.'",
            "address_line2": "'.$locationInfo->address2.'",
            "city_locality": "'.$locationInfo->city.'",
            "state_province": "'.$locationInfo->province_code.'",
            "postal_code": "'.$locationInfo->zip.'",
            "country_code": "'.$locationInfo->country_code.'",
            "address_residential_indicator": "No"
          }
        }';
        $response = Requests::post('https://api.shipengine.com/v1/warehouses', $headers, $data);
        $warehouse = json_decode($response->body);
//        $objectArray = [
//            "warehouseId" => $warehouse->warehouse_id,
//            "name" => $warehouse->name,
//        ];
//        $saveWarehouse = $this->parse->newObject("Warehouses", $objectArray);
        return $warehouse;
    }

    function updateWarehouse($locationInfo, $shipEngineAPIKey, $phoneNumber,  $companyName, $warehouseId) {
        $headers = array(
            'Content-type' => 'application/json',
            'api-key' => $shipEngineAPIKey
        );
        $data = '
        {
          "warehouse_id": "'.$warehouseId.'",
          "name": "'.$companyName.'",
          "origin_address": {
            "name": "'.$companyName.'",
            "phone": "'.$phoneNumber.'",
            "address_line1": "'.$locationInfo->address1.'",
            "address_line2": "'.$locationInfo->address2.'",
            "city_locality": "'.$locationInfo->city.'",
            "state_province": "'.$locationInfo->province_code.'",
            "postal_code": "'.$locationInfo->zip.'",
            "country_code": "'.$locationInfo->country_code.'",
            "address_residential_indicator": "No"
          },
          "return_address": {
            "name": "'.$companyName.'",
            "phone": "'.$phoneNumber.'",
            "address_line1": "'.$locationInfo->address1.'",
            "address_line2": "'.$locationInfo->address2.'",
            "city_locality": "'.$locationInfo->city.'",
            "state_province": "'.$locationInfo->province_code.'",
            "postal_code": "'.$locationInfo->zip.'",
            "country_code": "'.$locationInfo->country_code.'",
            "address_residential_indicator": "No"
          }
        }';
        $response = Requests::put('https://api.shipengine.com/v1/warehouses/'.$warehouseId.'', $headers, $data);
        $warehouse = json_decode($response->body);
        //Returns 204 (no content)
        return $warehouse;
    }

    function createStampsAccount($post, $apiKey) {
        $options = [];
        $options['timeout'] = 180;
        $headers = array(
            "Content-type" => "application/json",
            "api-key" => $apiKey
        );
        $data = '
        {
          "nickname": "'.$post["nickname"].'",
          "email": "'.$post["email"].'",
          "agree_to_carrier_terms" : true,
          "address": {
            "name": "'.$post["name"].'",
            "phone": "'.$post["phone"].'",
            "company_name": "'.$post["company"].'",
            "address_line1": "'.$post["address1"].'",
            "address_line2": "'.$post["address2"].'",
            "city_locality": "'.$post["city"].'",
            "state_province": "'.$post["state"].'",
            "postal_code": "'.$post["zip"].'",
            "country_code": "'.$post["countryCode"].'",
          },
          "credit_card": {
            "name": "'.$post["cardName"].'",
            "number": "'.$post["cardNumber"].'",
            "type": "'.$post["type"].'",
            "expiration_year": "'.$post["expirationYear"].'",
            "expiration_month": "'.$post["expirationMonth"].'",
          },
        }';
        $response = Requests::post('https://api.shipengine.com/v1/registration/stamps_com', $headers, $data, $options);
//        $warehouse = json_decode($response->body);
        return json_decode($response->body);
    }

    function connectStamps($post, $apiKey) {
        $options = [];
        $options['timeout'] = 180;
        $headers = array(
            "Content-type" => "application/json",
            "api-key" => $apiKey
        );
        $data = '
        {
          "nickname": "'.$post["nickname"].'",
          "username": "'.$post["username"].'",
          "password": "'.$post["password"].'"
        }';
        $response = Requests::post('https://api.shipengine.com/v1/connections/carriers/stamps_com', $headers, $data, $options);
//        $warehouse = json_decode($response->body);
        return json_decode($response->body);
    }

    function connectEndicia($post, $apiKey) {
        $options = [];
        $options['timeout'] = 180;
        $headers = array(
            "Content-type" => "application/json",
            "api-key" => $apiKey
        );
        $data = '
        {
          "nickname": "'.$post["nickname"].'",
          "account": "'.$post["username"].'",
          "passphrase": "'.$post["passphrase"].'"
        }';
        $response = Requests::post('https://api.shipengine.com/v1/connections/carriers/endicia', $headers, $data, $options);
//        $warehouse = json_decode($response->body);
        return json_decode($response->body);
    }

    function connectUPS($post, $apiKey) {
        $options = [];
        $options['timeout'] = 180;
        $headers = array(
            "Content-type" => "application/json",
            "api-key" => $apiKey
        );
        $data = ' 
        {
            "nickname": "'.$post["nickname"].'",
            "account_number": "'.$post["accountNumber"].'",
            "account_country_code": "'.$post["accountCountryCode"].'",
            "account_postal_code": "'.$post["accountPostalCode"].'",
            "first_name": "'.$post["firstName"].'",
            "last_name": "'.$post["lastName"].'",
            "company": "'.$post["company"].'",
            "address1": "'.$post["address1"].'",
            "address2": "'.$post["address2"].'",
            "city": "'.$post["city"].'",
            "state": "'.$post["state"].'",
            "postal_code": "'.$post["postalCode"].'",
            "country_code": "'.$post["countryCode"].'",
            "phone": "'.$post["phone"].'",
            "email": "'.$post["email"].'",
            "agree_to_technology_agreement": "true",
          "invoice": {
            "control_id": "'.$post["controlId"].'",
            "invoice_number": "'.$post["invoiceNumber"].'",
            "invoice_amount": "'.$post["invoiceAmount"].'",
            "invoice_date": "'.$post["invoiceDate"].'"
          }
        }';
        $response = Requests::post('https://api.shipengine.com/v1/connections/carriers/ups', $headers, $data, $options);
//        $warehouse = json_decode($response->body);
        return json_decode($response->body);
    }

    function connectFedex($post, $apiKey) {
        $options = [];
        $options['timeout'] = 180;
        $headers = array(
            "Content-type" => "application/json",
            "api-key" => $apiKey
        );
        $data = ' 
            {
                "nickname": "'.$post["nickname"].'",
                "account_number": "'.$post["accountNumber"].'",
                "first_name": "'.$post["firstName"].'",
                "last_name": "'.$post["lastName"].'",
                "phone": "'.$post["phone"].'",
                "address1": "'.$post["address1"].'",
                "address2": "'.$post["address2"].'",
                "city": "'.$post["city"].'",
                "company": "'.$post["company"].'",
                "country_code": "'.$post["countryCode"].'",
                "postal_code": "'.$post["postalCode"].'",
                "state": "'.$post["state"].'",
                "email": "'.$post["email"].'",
                "agree_to_eula": "true"
            }';
        $response = Requests::post('https://api.shipengine.com/v1/connections/carriers/fedex', $headers, $data, $options);
//        $warehouse = json_decode($response->body);
        return json_decode($response->body);
    }

    function addFundsToCarrier($amount, $apiKey, $carrierId) {
        $options = [];
        $options['timeout'] = 180;
        $headers = array(
            "Content-type" => "application/json",
            "api-key" => $apiKey
        );
        $data = '
        {
          "currency": "usd",
          "amount": "'.$amount.'"
        }';
        $response = Requests::post('https://api.shipengine.com/v1/carriers/'.$carrierId.'/add_funds', $headers, $data, $options);
        return json_decode($response->body);
    }

    function voidLabel($labelId, $apiKey) {
        $headers = array(
            "Content-type" => "application/json",
            "api-key" => $apiKey
        );
        $response = Requests::put('https://api.shipengine.com/v1/labels/'.$labelId.'/void', $headers);
        return json_decode($response->body);
    }
}
