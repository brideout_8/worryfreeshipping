<?php
/**
 * Created by PhpStorm.
 * User: rapidware
 * Date: 10/30/17
 * Time: 10:49 PM
 */
require_once APPPATH.'libraries/vendor/autoload.php';

use Mailgun\Mailgun;

class Mail_gun_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    private function mailGunKey() {
        return Mailgun::create('d5ecd7d52087f2c2aebb2fc3d7290e4c-b0aac6d0-aaf575f2');
    }

    function welcomeClient($email, $firstName) {
        $image = base_url()."assets/img/WorryFreeShipping.png";

        $message  = '<html><body>';
        $message .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		  <head>
		   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		    <meta name="viewport" content="width=device-width, initial-scale=1" />
		    <title>Welcome</title>
		    <!-- Designed by https://github.com/kaytcat -->
		    <!-- Header image designed by Freepik.com -->
		
		
		    <style type="text/css">
		    /* Take care of image borders and formatting */
		
		    img { max-width: 600px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;}
		    a img { border: none; }
		    table { border-collapse: collapse !important; }
		    #outlook a { padding:0; }
		    .ReadMsgBody { width: 100%; }
		    .ExternalClass {width:100%;}
		    .backgroundTable {margin:0 auto; padding:0; width:100% !important;}
		    table td {border-collapse: collapse;}
		    .ExternalClass * {line-height: 115%;}
		
		
		    /* General styling */
		
		    td {
		      font-family: Arial, sans-serif;
		    }
		
		    body {
		      -webkit-font-smoothing:antialiased;
		      -webkit-text-size-adjust:none;
		      width: 100%;
		      height: 100%;
		      color: #6f6f6f;
		      font-weight: 400;
		      font-size: 18px;
		    }
		
		
		    h1 {
		      margin: 10px 0;
		    }
		
		    a {
		      color: #27aa90;
		      text-decoration: none;
		    }
		
		    .force-full-width {
		      width: 100% !important;
		    }
		    
		    #productImage {
                /*display: block;*/
                max-width:200px;
                max-height:400px;
                width: auto;
                height: auto;
            }
		
		
		    .body-padding {
		      padding: 0 75px;
		    }
		
		
		    .force-width-80 {
		      width: 80% !important;
		    }
		
		
		    </style>
		
		    <style type="text/css" media="screen">
		        @media screen {
		          @import url(http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,900);
		          /* Thanks Outlook 2013! */
		          * {
		            font-family: Source Sans Pro, Helvetica Neue, Arial, sans-serif !important;
		          }
		          .w280 {
		            width: 280px !important;
		          }
		
		        }
		    </style>
		
		    <style type="text/css" media="only screen and (max-width: 480px)">
		      /* Mobile styles */
		      @media only screen and (max-width: 480px) {
		
		        table[class*="w320"] {
		          width: 320px !important;
		        }
		
		        td[class*="w320"] {
		          width: 280px !important;
		          padding-left: 20px !important;
		          padding-right: 20px !important;
		        }
		
		        img[class*="w320"] {
		          width: 250px !important;
		          height: 67px !important;
		        }
		        #productImage {
                    /*display: block;*/
                    max-width:200px;
                    max-height:400px;
                    width: auto;
                    height: auto;
                }
		
		        td[class*="mobile-spacing"] {
		          padding-top: 10px !important;
		          padding-bottom: 10px !important;
		        }
		
		        *[class*="mobile-hide"] {
		          display: none !important;
		        }
		
		        *[class*="mobile-br"] {
		          font-size: 12px !important;
		        }
		
		        td[class*="mobile-w20"] {
		          width: 20px !important;
		        }
		
		        img[class*="mobile-w20"] {
		          width: 20px !important;
		        }
		
		        td[class*="mobile-center"] {
		          text-align: center !important;
		        }
		
		        table[class*="w100p"] {
		          width: 100% !important;
		        }
		
		        td[class*="activate-now"] {
		          padding-right: 0 !important;
		          padding-top: 20px !important;
		        }
		
		        td[class*="mobile-resize"] {
		          font-size: 22px !important;
		          padding-left: 15px !important;
		        }
		
		      }
		    </style>
		  </head>
		  <body  offset="0" class="body" style="padding:0; margin:0; display:block; background:#eeebeb; -webkit-text-size-adjust:none" bgcolor="#eeebeb">
		  <table align="center" cellpadding="0" cellspacing="0" width="100%" height="100%">
		    <tr>
		      <td align="center" valign="top" style="background-color:#eeebeb" width="100%">
		
		      <center>
		
		        <table cellspacing="0" cellpadding="0" width="600" class="w320">
		          <tr>
		            <td align="center" valign="top">
		
					<br>
		            <table style="margin:0 auto;" cellspacing="0" cellpadding="0" width="100%">
		              <tr>
		                <td style="text-align: center;">
		                  <a href="#"><img class="w320" id="productImage" src="'.$image.'" alt="Worry Free Shipping" ></a>
		                </td>
		              </tr>
		            </table>
					<br>
		
		            <table cellspacing="0" cellpadding="0" class="force-full-width" bgcolor="#ffffff" >
		              <tr>
		                <td style="background-color:#ffffff;">
		                <br>
		
		                <center>
		
		                <table style="margin: 0 auto;" cellspacing="0" cellpadding="0" class="force-width-80">
		                  <tr>
		                    <td style="text-align:left; color: #6f6f6f; font-size: 15px; font-weight: 125;">
		                    <br>
		                    <div style="font-weight: bold">
                                <br>
                                Hi '.$firstName.',<br><br>
                                We are glad to have you on board.<br><br>We offer a free one hour setup and consultation to help get you started with Worry Free Shipping. We strongly believe a good foundation
                                is the key to success with our platform. In the future you can go to www.worryfreeshipping.com to access your account or inside of Shopify under the apps section.<br><br>To schedule your
                                setup call, please email our onboarding team at  <a href="mailto:onboarding@worryfreeshipping.com">onboarding@worryfreeshipping.com</a><br><br>
                              
                                The Worry Free Shipping Team <br><br><br>
                            </div>
		                  </tr>
		                </table>
		                </center>
		
		                <table cellspacing="0" cellpadding="0" bgcolor="#363636"  class="force-full-width">
		                  <tr>
		                    <td style="background-color:#363636; text-align:center;">
		                    <br>
		                    <br>
		                      
		                    </td>
		                  </tr>
		                  <tr>
		                    <td style="color:#f0f0f0; font-size: 14px; text-align:center; padding-bottom:4px;">
		                      © 2018 All Rights Reserved
		                    </td>
		                  </tr>
		                  <tr>
		                    <td style="color:#27aa90; font-size: 14px; text-align:center;">
		                      <a href="mailto:bret@rapidwarellc.com">Contact</a> 
		                    </td>
		                  </tr>
		                  <tr>
		                    <td style="font-size:12px;">
		                      &nbsp;
		                    </td>
		                  </tr>
		                </table>
		
		                </td>
		              </tr>
		            </table>
		            </td>
		          </tr>
		        </table>
		
		      </center>
		      </td>
		    </tr>
		  </table>
		  </body>
		</html>';
        $message .= "</body></html>";
        $mg = $this->mailGunKey();
        $result = $mg->messages()->send('mg.worryfreeshipping.com', [
            'from'    => 'Worry Free Shipping <support@worryfreeshipping.com>',
            'to'      => $email,
            'subject' => 'Welcome to Worry Free Shipping',
            'html'    => $message,
            'o:tag'   => array('Welcome Email')
        ]);

        return $result;
    }

    function addUser($email, $firstName, $lastName, $url, $name, $username) {
        $image = base_url()."assets/img/WorryFreeShipping.png";

        $message  = '<html><body>';
        $message .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		  <head>
		   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		    <meta name="viewport" content="width=device-width, initial-scale=1" />
		    <title>Welcome</title>
		    <!-- Designed by https://github.com/kaytcat -->
		    <!-- Header image designed by Freepik.com -->
		
		
		    <style type="text/css">
		    /* Take care of image borders and formatting */
		
		    img { max-width: 600px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;}
		    a img { border: none; }
		    table { border-collapse: collapse !important; }
		    #outlook a { padding:0; }
		    .ReadMsgBody { width: 100%; }
		    .ExternalClass {width:100%;}
		    .backgroundTable {margin:0 auto; padding:0; width:100% !important;}
		    table td {border-collapse: collapse;}
		    .ExternalClass * {line-height: 115%;}
		
		
		    /* General styling */
		
		    td {
		      font-family: Arial, sans-serif;
		    }
		
		    body {
		      -webkit-font-smoothing:antialiased;
		      -webkit-text-size-adjust:none;
		      width: 100%;
		      height: 100%;
		      color: #6f6f6f;
		      font-weight: 400;
		      font-size: 18px;
		    }
		    
		    #productImage {
                /*display: block;*/
                max-width:200px;
                max-height:400px;
                width: auto;
                height: auto;
            }
		
		
		    h1 {
		      margin: 10px 0;
		    }
		
		    a {
		      color: #27aa90;
		      text-decoration: none;
		    }
		
		    .force-full-width {
		      width: 100% !important;
		    }
		
		
		    .body-padding {
		      padding: 0 75px;
		    }
		
		
		    .force-width-80 {
		      width: 80% !important;
		    }
		
		
		    </style>
		
		    <style type="text/css" media="screen">
		        @media screen {
		          @import url(http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,900);
		          /* Thanks Outlook 2013! */
		          * {
		            font-family: Source Sans Pro, Helvetica Neue, Arial, sans-serif !important;
		          }
		          .w280 {
		            width: 280px !important;
		          }
		
		        }
		    </style>
		
		    <style type="text/css" media="only screen and (max-width: 480px)">
		      /* Mobile styles */
		      @media only screen and (max-width: 480px) {
		
		        table[class*="w320"] {
		          width: 320px !important;
		        }
		
		        td[class*="w320"] {
		          width: 280px !important;
		          padding-left: 20px !important;
		          padding-right: 20px !important;
		        }
		
		        img[class*="w320"] {
		          width: 250px !important;
		          height: 67px !important;
		        }
		
		        td[class*="mobile-spacing"] {
		          padding-top: 10px !important;
		          padding-bottom: 10px !important;
		        }
		        
		        #productImage {
                    /*display: block;*/
                    max-width:200px;
                    max-height:400px;
                    width: auto;
                    height: auto;
                }
		
		        *[class*="mobile-hide"] {
		          display: none !important;
		        }
		
		        *[class*="mobile-br"] {
		          font-size: 12px !important;
		        }
		
		        td[class*="mobile-w20"] {
		          width: 20px !important;
		        }
		
		        img[class*="mobile-w20"] {
		          width: 20px !important;
		        }
		
		        td[class*="mobile-center"] {
		          text-align: center !important;
		        }
		
		        table[class*="w100p"] {
		          width: 100% !important;
		        }
		
		        td[class*="activate-now"] {
		          padding-right: 0 !important;
		          padding-top: 20px !important;
		        }
		
		        td[class*="mobile-resize"] {
		          font-size: 22px !important;
		          padding-left: 15px !important;
		        }
		
		      }
		    </style>
		  </head>
		  <body  offset="0" class="body" style="padding:0; margin:0; display:block; background:#eeebeb; -webkit-text-size-adjust:none" bgcolor="#eeebeb">
		  <table align="center" cellpadding="0" cellspacing="0" width="100%" height="100%">
		    <tr>
		      <td align="center" valign="top" style="background-color:#eeebeb" width="100%">
		
		      <center>
		
		        <table cellspacing="0" cellpadding="0" width="600" class="w320">
		          <tr>
		            <td align="center" valign="top">
		
					<br>
		            <table style="margin:0 auto;" cellspacing="0" cellpadding="0" width="100%">
		              <tr>
		                <td style="text-align: center;">
		                  <a href="#"><img id="productImage" class="w320" src="'.$image.'" alt="Worry Free Shipping" ></a>
		                </td>
		              </tr>
		            </table>
					<br>
		
		            <table cellspacing="0" cellpadding="0" class="force-full-width" bgcolor="#ffffff" >
		              <tr>
		                <td style="background-color:#ffffff;">
		                <br>
		
		                <center>
		
		                <table style="margin: 0 auto;" cellspacing="0" cellpadding="0" class="force-width-80">
		                  <tr>
		                    <td style="text-align:left; color: #6f6f6f; font-size: 15px; font-weight: 125;">
		                    <br>
		                    <div style="font-weight: bold">
                                <br>
                                Hi '.$firstName.' '.$lastName.',<br><br>
                                '.$name.' created you an account on Worry Free Shipping. Your username is '.$username.'. You can create a password at the link below.<br><br> 
                                If you have any questions please reach out to support at <a href="mailto:support@worryfreeshipping.com">support@worryfreeshipping.com</a><br><br>
                                We are glad to have you on board.<br>                           
                                The Worry Free Shipping Team <br><br><br>
                            </div>
		                  </tr>
		                </table>
		                </center>
		                
		                <table style="margin:0 auto;" cellspacing="0" cellpadding="10" width="100%">
		                  <tr>
		                    <td style="text-align:center; margin:0 auto;">
		                    <br>
		                      <div>
		                         <a href="'.$url.'"
		                          style="background-color:#f5774e;color:#ffffff;display:inline-block;font-family:Source Sans Pro, Helvetica, Arial, sans-serif;font-size:18px;font-weight:400;line-height:45px;text-align:center;text-decoration:none;width:220px;-webkit-text-size-adjust:none;">Set Password
		                         </a>
		                      </div>
		                      <div style="font-size: 15px"><br><br>
		                      If the button does not work, copy and paste the url below into a web browser.<br>
		                      '.$url.'
		                     </div>	
		                      <br>
		                    </td>
		                  </tr>
		                </table>
		
		                <table cellspacing="0" cellpadding="0" bgcolor="#363636"  class="force-full-width">
		                  <tr>
		                    <td style="background-color:#363636; text-align:center;">
		                    <br>
		                    <br>
		                      
		                    </td>
		                  </tr>
		                  <tr>
		                    <td style="color:#f0f0f0; font-size: 14px; text-align:center; padding-bottom:4px;">
		                      © 2018 All Rights Reserved
		                    </td>
		                  </tr>
		                  <tr>
		                    <td style="color:#27aa90; font-size: 14px; text-align:center;">
		                      <a href="mailto:support@worryfreeshipping.com">Contact</a> 
		                    </td>
		                  </tr>
		                  <tr>
		                    <td style="font-size:12px;">
		                      &nbsp;
		                    </td>
		                  </tr>
		                </table>
		
		                </td>
		              </tr>
		            </table>
		            </td>
		          </tr>
		        </table>
		
		      </center>
		      </td>
		    </tr>
		  </table>
		  </body>
		</html>';
        $message .= "</body></html>";
        $mg = $this->mailGunKey();
        $result = $mg->messages()->send('mg.worryfreeshipping.com', [
            'from'    => 'Worry Free Shipping <support@worryfreeshipping.com>',
            'to'      => $email,
            'subject' => 'Welcome to Worry Free Shipping',
            'html'    => $message,
            'o:tag'   => array('Welcome Email')
        ]);

        return $result;
    }

    function sendLabel($email, $labelLink) {
        $image = base_url()."assets/img/rrlogo.png";
//        $url = base_url()."register?token=".@$setPasswordToken;

        $message  = '<html><body>';
        $message .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		  <head>
		   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		    <meta name="viewport" content="width=device-width, initial-scale=1" />
		    <title>Return Label</title>
		    <!-- Designed by https://github.com/kaytcat -->
		    <!-- Header image designed by Freepik.com -->
		
		
		    <style type="text/css">
		    /* Take care of image borders and formatting */
		
		    img { max-width: 600px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;}
		    a img { border: none; }
		    table { border-collapse: collapse !important; }
		    #outlook a { padding:0; }
		    .ReadMsgBody { width: 100%; }
		    .ExternalClass {width:100%;}
		    .backgroundTable {margin:0 auto; padding:0; width:100% !important;}
		    table td {border-collapse: collapse;}
		    .ExternalClass * {line-height: 115%;}
		
		
		    /* General styling */
		
		    td {
		      font-family: Arial, sans-serif;
		    }
		
		    body {
		      -webkit-font-smoothing:antialiased;
		      -webkit-text-size-adjust:none;
		      width: 100%;
		      height: 100%;
		      color: #6f6f6f;
		      font-weight: 400;
		      font-size: 18px;
		    }
		
		
		    h1 {
		      margin: 10px 0;
		    }
		
		    a {
		      color: #27aa90;
		      text-decoration: none;
		    }
		
		    .force-full-width {
		      width: 100% !important;
		    }
		
		
		    .body-padding {
		      padding: 0 75px;
		    }
		
		
		    .force-width-80 {
		      width: 80% !important;
		    }
		
		
		    </style>
		
		    <style type="text/css" media="screen">
		        @media screen {
		          @import url(http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,900);
		          /* Thanks Outlook 2013! */
		          * {
		            font-family: Source Sans Pro, Helvetica Neue, Arial, sans-serif !important;
		          }
		          .w280 {
		            width: 280px !important;
		          }
		
		        }
		    </style>
		
		    <style type="text/css" media="only screen and (max-width: 480px)">
		      /* Mobile styles */
		      @media only screen and (max-width: 480px) {
		
		        table[class*="w320"] {
		          width: 320px !important;
		        }
		
		        td[class*="w320"] {
		          width: 280px !important;
		          padding-left: 20px !important;
		          padding-right: 20px !important;
		        }
		
		        img[class*="w320"] {
		          width: 250px !important;
		          height: 67px !important;
		        }
		
		        td[class*="mobile-spacing"] {
		          padding-top: 10px !important;
		          padding-bottom: 10px !important;
		        }
		
		        *[class*="mobile-hide"] {
		          display: none !important;
		        }
		
		        *[class*="mobile-br"] {
		          font-size: 12px !important;
		        }
		
		        td[class*="mobile-w20"] {
		          width: 20px !important;
		        }
		
		        img[class*="mobile-w20"] {
		          width: 20px !important;
		        }
		
		        td[class*="mobile-center"] {
		          text-align: center !important;
		        }
		
		        table[class*="w100p"] {
		          width: 100% !important;
		        }
		
		        td[class*="activate-now"] {
		          padding-right: 0 !important;
		          padding-top: 20px !important;
		        }
		
		        td[class*="mobile-resize"] {
		          font-size: 22px !important;
		          padding-left: 15px !important;
		        }
		
		      }
		    </style>
		  </head>
		  <body  offset="0" class="body" style="padding:0; margin:0; display:block; background:#eeebeb; -webkit-text-size-adjust:none" bgcolor="#eeebeb">
		  <table align="center" cellpadding="0" cellspacing="0" width="100%" height="100%">
		    <tr>
		      <td align="center" valign="top" style="background-color:#eeebeb" width="100%">
		
		      <center>
		
		        <table cellspacing="0" cellpadding="0" width="600" class="w320">
		          <tr>
		            <td align="center" valign="top">
		
					<br>
		            <table style="margin:0 auto;" cellspacing="0" cellpadding="0" width="100%">
		              <tr>
		                <td style="text-align: center;">
		                  <a href="#"><img class="w320" width="382" height="225" src="'.$image.'" alt="R&R Enterprises" ></a>
		                </td>
		              </tr>
		            </table>
					<br>
		
		            <table cellspacing="0" cellpadding="0" width="100%" style="background-color:#3bcdb0;">
		              <tr>
		                <td style="background-color:#3bcdb0;">
		
		                  <table cellspacing="0" cellpadding="0" width="100%">
		                    <tr>
		                      <td style="font-size:40px; font-weight: 600; color: #ffffff; text-align:center;" class="mobile-spacing">
		                      <div class="mobile-br">&nbsp;</div>
		                       Return Label
		                      <br>
		                      </td>
		                    </tr>
		                    <tr>
		                      <td style="font-size:24px; text-align:center; padding: 0 75px; color:#6f6f6f;" class="w320 mobile-spacing; ">
		                          Download your return label below.
		                      </td>
		                    </tr>
		                  </table>
		
		                  <table cellspacing="0" cellpadding="0" width="100%">
		                    <tr>
		                      <td>
		                        <img src="https://www.filepicker.io/api/file/4BgENLefRVCrgMGTAENj" style="max-width:100%; display:block;">
		                      </td>
		                    </tr>
		                  </table>
		
		                </td>
		              </tr>
		            </table>
		
		            <table cellspacing="0" cellpadding="0" class="force-full-width" bgcolor="#ffffff" >
		              <tr>
		                <td style="background-color:#ffffff;">
		                <br>
		
		                <center>
		
		                <table style="margin: 0 auto;" cellspacing="0" cellpadding="0" class="force-width-80">
		                  <tr>
		                    <td style="text-align:left; color: #6f6f6f; font-size: 15px; font-weight: 125;">
		                    <br>
		                    Hi,<br><br>
		                    Your username is return label is below. If you have any questions feel free to reach out to us at sales@r-renterprises.com
		                  
		                    The R&R Team <br>
		                  </tr>
		                </table>
		                </center>
		
		
		                <table style="margin:0 auto;" cellspacing="0" cellpadding="10" width="100%">
		                  <tr>
		                    <td style="text-align:center; margin:0 auto;">
		                    <br>
		                      <div><!--[if mso]>
		                        <v:rect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="http://" style="height:45px;v-text-anchor:middle;width:220px;" stroke="f" fillcolor="#f5774e">
		                          <w:anchorlock/>
		                          <center>
		                        <![endif]-->
		                            <a href="'.$labelLink.'"
		                          style="background-color:#f5774e;color:#ffffff;display:inline-block;font-family:Source Sans Pro, Helvetica, Arial, sans-serif;font-size:18px;font-weight:400;line-height:45px;text-align:center;text-decoration:none;width:220px;-webkit-text-size-adjust:none;">Download Label</a>
		                            <!--[if mso]>
		                          </center>
		                        </v:rect>
		                      <![endif]--></div>
		                      <div style="font-size: 15px"><br><br>
		                      If the button does not work, copy and paste the url below into a web browser.<br>
		                      '.$labelLink.'
		                     </div>	
		                      <br>
		                    </td>
		                  </tr>
		                </table>
		
		
		                <table cellspacing="0" cellpadding="0" bgcolor="#363636"  class="force-full-width">
		                  <tr>
		                    <td style="background-color:#363636; text-align:center;">
		                    <br>
		                    <br>
		                      
		                    </td>
		                  </tr>
		                  <tr>
		                    <td style="color:#f0f0f0; font-size: 14px; text-align:center; padding-bottom:4px;">
		                      © 2018 All Rights Reserved
		                    </td>
		                  </tr>
		                  <tr>
		                    <td style="color:#27aa90; font-size: 14px; text-align:center;">
		                      <a href="mailto:support@worryfreeshipping.com">Contact</a> 
		                    </td>
		                  </tr>
		                  <tr>
		                    <td style="font-size:12px;">
		                      &nbsp;
		                    </td>
		                  </tr>
		                </table>
		
		                </td>
		              </tr>
		            </table>
		            </td>
		          </tr>
		        </table>
		
		      </center>
		      </td>
		    </tr>
		  </table>
		  </body>
		</html>';
        $message .= "</body></html>";
        $mg = $this->mailGunKey();
        $result = $mg->messages()->send('mg.worryfreeshipping.com', [
            'from'    => 'Worry Free Shipping <support@worryfreeshipping.com>',
            'to'      => $email,
            'subject' => 'R&R Enterprises Return Label',
            'html'    => $message,
            'o:tag'   => array('Return Label')
        ]);

        return $result;
    }

    function forgotPassword($email, $url, $username) {
        $image = base_url()."assets/img/WorryFreeShipping.png";

        $message  = '<html><body>';
        $message .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		  <head>
		   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		    <meta name="viewport" content="width=device-width, initial-scale=1" />
		    <title>Worry Free Shipping</title>
		    <!-- Designed by https://github.com/kaytcat -->
		    <!-- Header image designed by Freepik.com -->
		
		
		    <style type="text/css">
		    /* Take care of image borders and formatting */
		
		    img { max-width: 600px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;}
		    a img { border: none; }
		    table { border-collapse: collapse !important; }
		    #outlook a { padding:0; }
		    .ReadMsgBody { width: 100%; }
		    .ExternalClass {width:100%;}
		    .backgroundTable {margin:0 auto; padding:0; width:100% !important;}
		    table td {border-collapse: collapse;}
		    .ExternalClass * {line-height: 115%;}
		
		
		    /* General styling */
		
		    td {
		      font-family: Arial, sans-serif;
		    }
		
		    body {
		      -webkit-font-smoothing:antialiased;
		      -webkit-text-size-adjust:none;
		      width: 100%;
		      height: 100%;
		      color: #6f6f6f;
		      font-weight: 400;
		      font-size: 18px;
		    }
		
		
		    h1 {
		      margin: 10px 0;
		    }
		
		    a {
		      color: #27aa90;
		      text-decoration: none;
		    }
		
		    .force-full-width {
		      width: 100% !important;
		    }
		
		
		    .body-padding {
		      padding: 0 75px;
		    }
		
		
		    .force-width-80 {
		      width: 80% !important;
		    }
		
		
		    </style>
		
		    <style type="text/css" media="screen">
		        @media screen {
		          @import url(http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,900);
		          /* Thanks Outlook 2013! */
		          * {
		            font-family: Source Sans Pro, Helvetica Neue, Arial, sans-serif !important;
		          }
		          .w280 {
		            width: 280px !important;
		          }
		
		        }
		    </style>
		
		    <style type="text/css" media="only screen and (max-width: 480px)">
		      /* Mobile styles */
		      @media only screen and (max-width: 480px) {
		
		        table[class*="w320"] {
		          width: 320px !important;
		        }
		
		        td[class*="w320"] {
		          width: 280px !important;
		          padding-left: 20px !important;
		          padding-right: 20px !important;
		        }
		
		        img[class*="w320"] {
		          width: 250px !important;
		          height: 67px !important;
		        }
		
		        td[class*="mobile-spacing"] {
		          padding-top: 10px !important;
		          padding-bottom: 10px !important;
		        }
		
		        *[class*="mobile-hide"] {
		          display: none !important;
		        }
		
		        *[class*="mobile-br"] {
		          font-size: 12px !important;
		        }
		
		        td[class*="mobile-w20"] {
		          width: 20px !important;
		        }
		
		        img[class*="mobile-w20"] {
		          width: 20px !important;
		        }
		
		        td[class*="mobile-center"] {
		          text-align: center !important;
		        }
		
		        table[class*="w100p"] {
		          width: 100% !important;
		        }
		
		        td[class*="activate-now"] {
		          padding-right: 0 !important;
		          padding-top: 20px !important;
		        }
		
		        td[class*="mobile-resize"] {
		          font-size: 22px !important;
		          padding-left: 15px !important;
		        }
		
		      }
		    </style>
		  </head>
		  <body  offset="0" class="body" style="padding:0; margin:0; display:block; background:#eeebeb; -webkit-text-size-adjust:none" bgcolor="#eeebeb">
		  <table align="center" cellpadding="0" cellspacing="0" width="100%" height="100%">
		    <tr>
		      <td align="center" valign="top" style="background-color:#eeebeb" width="100%">
		
		      <center>
		
		        <table cellspacing="0" cellpadding="0" width="600" class="w320">
		          <tr>
		            <td align="center" valign="top">
		
					<br>
		            <table style="margin:0 auto;" cellspacing="0" cellpadding="0" width="100%">
		              <tr>
		                <td style="text-align: center;">
		                  <a href="#"><img class="w320" width="382" height="225" src="'.$image. '" alt="Worry Free Shipping" ></a>
		                </td>
		              </tr>
		            </table>
					<br>
		
		            <table cellspacing="0" cellpadding="0" width="100%" style="background-color:#3bcdb0;">
		              <tr>
		                <td style="background-color:#3bcdb0;">
		
		                  <table cellspacing="0" cellpadding="0" width="100%">
		                    <tr>
		                      <td style="font-size:40px; font-weight: 600; color: #ffffff; text-align:center;" class="mobile-spacing">
		                      <div class="mobile-br">&nbsp;</div>
		                        Password Reset
		                      <br>
		                      </td>
		                    </tr>
		                    <tr>
		                      <td style="font-size:24px; text-align:center; padding: 0 75px; color:#6f6f6f;" class="w320 mobile-spacing; ">
		                          Password Reset Instructions Below.
		                      </td>
		                    </tr>
		                  </table>
		
		                  <table cellspacing="0" cellpadding="0" width="100%">
		                    <tr>
		                      <td>
		                        <img src="https://www.filepicker.io/api/file/4BgENLefRVCrgMGTAENj" style="max-width:100%; display:block;">
		                      </td>
		                    </tr>
		                  </table>
		
		                </td>
		              </tr>
		            </table>
		
		            <table cellspacing="0" cellpadding="0" class="force-full-width" bgcolor="#ffffff" >
		              <tr>
		                <td style="background-color:#ffffff;">
		                <br>
		
		                <center>
		
		                <table style="margin: 0 auto;" cellspacing="0" cellpadding="0" class="force-width-80">
		                  <tr>
		                    <td style="text-align:left; color: #000000; font-size: 15px;">
		                    <br>
		                    Hi,<br><br>
		                    Your username is <strong>' .$username.'</strong><br> Can&rsquo;t remember your password? No problem, just click the link below and set a new password.<br> If you need help, please contact support at <a href="mailto:support@worryfreeshipping.com">support@worryfreeshipping.com</a>. <br><br>
		                    Thanks, <br>
		                    The Worry Free Shipping Team <br>
		                  </tr>
		                </table>
		                </center>
		
		
		                <table style="margin:0 auto;" cellspacing="0" cellpadding="10" width="100%">
		                  <tr>
		                    <td style="text-align:center; margin:0 auto;">
		                    <br>
		                      <div><!--[if mso]>
		                        <v:rect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="http://" style="height:45px;v-text-anchor:middle;width:220px;" stroke="f" fillcolor="#f5774e">
		                          <w:anchorlock/>
		                          <center>
		                        <![endif]-->
		                            <a href="'.@$url.'"
		                          style="background-color:#f5774e;color:#ffffff;display:inline-block;font-family:Source Sans Pro, Helvetica, Arial, sans-serif;font-size:18px;font-weight:400;line-height:45px;text-align:center;text-decoration:none;width:220px;-webkit-text-size-adjust:none;">Reset Password</a>
		                            <!--[if mso]>
		                          </center>
		                        </v:rect>
		                      <![endif]--></div>
		                      <div style="font-size: 15px;"><br><br>
		                      If the button does not work, copy and paste the url below into a web browser.<br>
		                      '.$url.'
		                     </div>	                   
		                      <br>
		                    </td>
		                  </tr>
		                </table>
		
		
		                <table cellspacing="0" cellpadding="0" bgcolor="#363636"  class="force-full-width">
		                  <tr>
		                    <td style="background-color:#363636; text-align:center;">
		                    <br>
		                    <br>
		                      
		                    </td>
		                  </tr>
		                  <tr>
		                    <td style="color:#f0f0f0; font-size: 14px; text-align:center; padding-bottom:4px;">
		                      © 2018 All Rights Reserved
		                    </td>
		                  </tr>
		                  <tr>
		                    <td style="color:#27aa90; font-size: 14px; text-align:center;">
		                      <a href="mailto:support@worryfreeshipping.com">Contact</a> 
		                    </td>
		                  </tr>
		                  <tr>
		                    <td style="font-size:12px;">
		                      &nbsp;
		                    </td>
		                  </tr>
		                </table>
		
		                </td>
		              </tr>
		            </table>
		            </td>
		          </tr>
		        </table>
		
		      </center>
		      </td>
		    </tr>
		  </table>
		  </body>
		</html>';
        $message .= "</body></html>";

        $mg = $this->mailGunKey();
        $result = $mg->messages()->send('mg.worryfreeshipping.com', [
            'from'    => 'Worry Free Shipping <support@worryfreeshipping.com>',
            'to'      => $email,
            'subject' => 'Worry Free Shipping Password Reset',
            'html'    => $message,
            'o:tag'   => array('Password Reset')
        ]);

    }

    function sendUserIssueEmail($email, $ticketId) {
        $image = base_url()."assets/img/WorryFreeShipping.png";

        $message  = '<html><body>';
        $message .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		  <head>
		   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		    <meta name="viewport" content="width=device-width, initial-scale=1" />
		    <title>Worry Free Shipping</title>
		    <!-- Designed by https://github.com/kaytcat -->
		    <!-- Header image designed by Freepik.com -->
		
		
		    <style type="text/css">
		    /* Take care of image borders and formatting */
		
		    img { max-width: 600px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;}
		    a img { border: none; }
		    table { border-collapse: collapse !important; }
		    #outlook a { padding:0; }
		    .ReadMsgBody { width: 100%; }
		    .ExternalClass {width:100%;}
		    .backgroundTable {margin:0 auto; padding:0; width:100% !important;}
		    table td {border-collapse: collapse;}
		    .ExternalClass * {line-height: 115%;}
		
		
		    /* General styling */
		
		    td {
		      font-family: Arial, sans-serif;
		    }
		
		    body {
		      -webkit-font-smoothing:antialiased;
		      -webkit-text-size-adjust:none;
		      width: 100%;
		      height: 100%;
		      color: #6f6f6f;
		      font-weight: 400;
		      font-size: 18px;
		    }
		    #productImage {
                /*display: block;*/
                max-width:200px;
                max-height:400px;
                width: auto;
                height: auto;
            }
		
		
		    h1 {
		      margin: 10px 0;
		    }
		
		    a {
		      color: #27aa90;
		      text-decoration: none;
		    }
		
		    .force-full-width {
		      width: 100% !important;
		    }
		
		
		    .body-padding {
		      padding: 0 75px;
		    }
		
		
		    .force-width-80 {
		      width: 80% !important;
		    }
		
		
		    </style>
		
		    <style type="text/css" media="screen">
		        @media screen {
		          @import url(http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,900);
		          /* Thanks Outlook 2013! */
		          * {
		            font-family: Source Sans Pro, Helvetica Neue, Arial, sans-serif !important;
		          }
		          .w280 {
		            width: 280px !important;
		          }
		
		        }
		    </style>
		
		    <style type="text/css" media="only screen and (max-width: 480px)">
		      /* Mobile styles */
		      @media only screen and (max-width: 480px) {
		
		        table[class*="w320"] {
		          width: 320px !important;
		        }
		
		        td[class*="w320"] {
		          width: 280px !important;
		          padding-left: 20px !important;
		          padding-right: 20px !important;
		        }
		
		        img[class*="w320"] {
		          width: 250px !important;
		          height: 67px !important;
		        }
		        #productImage {
                    /*display: block;*/
                    max-width:200px;
                    max-height:400px;
                    width: auto;
                    height: auto;
                }
		
		        td[class*="mobile-spacing"] {
		          padding-top: 10px !important;
		          padding-bottom: 10px !important;
		        }
		
		        *[class*="mobile-hide"] {
		          display: none !important;
		        }
		
		        *[class*="mobile-br"] {
		          font-size: 12px !important;
		        }
		
		        td[class*="mobile-w20"] {
		          width: 20px !important;
		        }
		
		        img[class*="mobile-w20"] {
		          width: 20px !important;
		        }
		
		        td[class*="mobile-center"] {
		          text-align: center !important;
		        }
		
		        table[class*="w100p"] {
		          width: 100% !important;
		        }
		
		        td[class*="activate-now"] {
		          padding-right: 0 !important;
		          padding-top: 20px !important;
		        }
		
		        td[class*="mobile-resize"] {
		          font-size: 22px !important;
		          padding-left: 15px !important;
		        }
		
		      }
		    </style>
		  </head>
		  <body  offset="0" class="body" style="padding:0; margin:0; display:block; background:#eeebeb; -webkit-text-size-adjust:none" bgcolor="#eeebeb">
		  <table align="center" cellpadding="0" cellspacing="0" width="100%" height="100%">
		    <tr>
		      <td align="center" valign="top" style="background-color:#eeebeb" width="100%">
		
		      <center>
		
		        <table cellspacing="0" cellpadding="0" width="600" class="w320">
		          <tr>
		            <td align="center" valign="top">
		
					<br>
		            <table style="margin:0 auto;" cellspacing="0" cellpadding="0" width="100%">
		              <tr>
		                <td style="text-align: center;">
		                  <a href="#"><img class="w320" id="productImage" src="'.$image. '" alt="Worry Free Shipping" ></a>
		                </td>
		              </tr>
		            </table>
					<br>
		
		            <table cellspacing="0" cellpadding="0" width="100%" style="background-color:#3bcdb0;">
		              <tr>
		                <td style="background-color:#3bcdb0;">
		
		                  <table cellspacing="0" cellpadding="0" width="100%">
		                    <tr>
		                      <td style="font-size:40px; font-weight: 600; color: #ffffff; text-align:center;" class="mobile-spacing">
		                      <div class="mobile-br">&nbsp;</div>
		                        Support Ticket
		                      <br>
		                      </td>
		                    </tr>	                    
		                  </table>
		
		                  <table cellspacing="0" cellpadding="0" width="100%">
		                    <tr>
		                      <td>
		                        <img src="https://www.filepicker.io/api/file/4BgENLefRVCrgMGTAENj" style="max-width:100%; display:block;">
		                      </td>
		                    </tr>
		                  </table>
		
		                </td>
		              </tr>
		            </table>
		
		            <table cellspacing="0" cellpadding="0" class="force-full-width" bgcolor="#ffffff" >
		              <tr>
		                <td style="background-color:#ffffff;">
		                <br>
		
		                <center>
		
		                <table style="margin: 0 auto;" cellspacing="0" cellpadding="0" class="force-width-80">
		                  <tr>
		                    <td style="text-align:left; color: #000000; font-size: 15px;">
		                    <br>
		                    Hi,<br><br>
		                    We have received your new support ticket. The ticket can be referenced by the ID of '.$ticketId.' <br><br>
		                    We will be in touch as soon as we have a resolution.<br><br>
		                    Thanks, <br>
		                    The Worry Free Shipping Team <br>
		                  </tr>
		                </table>
		                </center>
		
		
		                <table style="margin:0 auto;" cellspacing="0" cellpadding="10" width="100%">
		                  <tr>
		                    <td style="text-align:center; margin:0 auto;">
		                    <br>
		                     	                   
		                      <br>
		                    </td>
		                  </tr>
		                </table>
		
		
		                <table cellspacing="0" cellpadding="0" bgcolor="#363636"  class="force-full-width">
		                  <tr>
		                    <td style="background-color:#363636; text-align:center;">
		                    <br>
		                    <br>
		                      
		                    </td>
		                  </tr>
		                  <tr>
		                    <td style="color:#f0f0f0; font-size: 14px; text-align:center; padding-bottom:4px;">
		                      © 2018 All Rights Reserved
		                    </td>
		                  </tr>
		                  <tr>
		                    <td style="color:#27aa90; font-size: 14px; text-align:center;">
		                      <a href="mailto:support@worryfreeshipping.com">Contact</a> 
		                    </td>
		                  </tr>
		                  <tr>
		                    <td style="font-size:12px;">
		                      &nbsp;
		                    </td>
		                  </tr>
		                </table>
		
		                </td>
		              </tr>
		            </table>
		            </td>
		          </tr>
		        </table>
		
		      </center>
		      </td>
		    </tr>
		  </table>
		  </body>
		</html>';
        $message .= "</body></html>";

        $mg = $this->mailGunKey();
        $result = $mg->messages()->send('mg.worryfreeshipping.com', [
            'from'    => 'Worry Free Shipping <support@worryfreeshipping.com>',
            'to'      => $email,
            'subject' => 'Worry Free Shipping Support Ticket',
            'html'    => $message,
            'o:tag'   => array('New Support Ticket')
        ]);

    }

    function sendMeIssueEmail($email, $ticketId, $ticketIssue){
        $message  = '<html><body>';
        $message .= 'User email '.$email.'<br>';
        $message .= 'Ticket Id '.$ticketId.'<br>';
        $message .= 'Ticket Issue '.$ticketIssue.'';
        $message .= "</body></html>";
        $mg = $this->mailGunKey();
        $result = $mg->messages()->send('mg.worryfreeshipping.com', [
            'from'    => 'Worry Free Shipping <support@worryfreeshipping.com>',
            'to'      => 'support@worryfreeshipping.com',
            'subject' => 'New Support Ticket',
            'html'    => $message
        ]);

        return $result;
    }

    function sendMeNewUserEmail($name, $city, $state, $country, $shopifyName) {
        $message  = '<html><body>';
        $message .= 'Name: '.$name.'<br>';
        $message .= 'City: '.$city.'<br>';
        $message .= 'State: '.$state.'<br>';
        $message .= 'Country: '.$country.'<br>';
        $message .= 'Shopify Store Name: '.$shopifyName.'<br>';
        $message .= "</body></html>";
        $mg = $this->mailGunKey();
        $result = $mg->messages()->send('mg.worryfreeshipping.com', [
            'from'    => 'Worry Free Shipping <support@worryfreeshipping.com>',
            'to'      => 'support@worryfreeshipping.com',
            'subject' => 'New User',
            'html'    => $message,
            'o:tag'   => array('New User')
        ]);

        return $result;
    }

    function sendMeNewPlanEmail($plan) {
        switch ($plan) {
            case "Trial":
                $userCost = "Free";
                $myEarnings = "None";
                $myEarnings2 = "None";
                break;
            case "Lite":
                $userCost = "$40";
                $myEarnings = "$22";
                $myEarnings2 = "$28";
                break;
            case "Standard":
                $userCost = "$140";
                $myEarnings = "$62";
                $myEarnings2 = "$92";
                break;
            case "Pro":
                $userCost = "$250";
                $myEarnings = "$75";
                $myEarnings2 = "$150";
                break;
            case "Enterprise":
                $userCost = "$400";
                $myEarnings = "$70";
                $myEarnings2 = "$220";
                break;
            default:
                $userCost = "Unknown";
                $myEarnings = "Unknown";
                $myEarnings2 = "Unknown";
        }
        $message  = '<html><body>';
        $message .= 'Plan: '.$plan.'<br>';
        $message .= 'User Cost: '.$userCost.'<br>';
        $message .= 'My Earnings at 5 cents a label: '.$myEarnings.'<br>';
        $message .= 'My Earnings at 2 cents a label: '.$myEarnings2.'<br>';
        $message .= "</body></html>";
        $mg = $this->mailGunKey();
        $result = $mg->messages()->send('mg.worryfreeshipping.com', [
            'from'    => 'Worry Free Shipping <support@worryfreeshipping.com>',
            'to'      => 'support@worryfreeshipping.com',
            'subject' => 'New Plan Started',
            'html'    => $message,
            'o:tag'   => array('New Plan')
        ]);

        return $result;
    }

    function sendmeNewLabelsPurchaseEmail($costPerLabel, $numberOfLabels, $price) {
        $myMoney = $price * .8;
        $message  = '<html><body>';
        $message .= 'Cost Per Label: '.$costPerLabel.'<br>';
        $message .= 'Number Of Labels: '.$numberOfLabels.'<br>';
        $message .= 'Price: '.$price.'<br>';
        $message .= 'My Earnings: '.$myMoney.'<br>';
        $message .= "</body></html>";
        $mg = $this->mailGunKey();
        $result = $mg->messages()->send('mg.worryfreeshipping.com', [
            'from'    => 'Worry Free Shipping <support@worryfreeshipping.com>',
            'to'      => 'support@worryfreeshipping.com',
            'subject' => 'New User',
            'html'    => $message,
            'o:tag'   => array('New User')
        ]);

        return $result;
    }

    function customerDataRequestEmail($payload){
        $message  = '<html><body>';
        $message .= 'Payload:'.var_dump($payload).'';
        $message .= "</body></html>";
        $mg = $this->mailGunKey();
        $result = $mg->messages()->send('mg.worryfreeshipping.com', [
            'from'    => 'Worry Free Shipping <support@worryfreeshipping.com>',
            'to'      => 'support@worryfreeshipping.com',
            'subject' => 'Customer Data Request',
            'html'    => $message,
            'o:tag'   => array('Customer Data Request')
        ]);

        return $result;
    }

    function customerDataEraseEmail($payload){
        $message  = '<html><body>';
        $message .= 'Payload:'.var_dump($payload).'';
        $message .= "</body></html>";
        $mg = $this->mailGunKey();
        $result = $mg->messages()->send('mg.worryfreeshipping.com', [
            'from'    => 'Worry Free Shipping <support@worryfreeshipping.com>',
            'to'      => 'support@worryfreeshipping.com',
            'subject' => 'Customer Data Erase',
            'html'    => $message,
            'o:tag'   => array('Customer Data Erase')
        ]);

        return $result;
    }

    function shopDataEraseEmail($payload){
        $message  = '<html><body>';
        $message .= 'Payload:'.var_dump($payload).'';
        $message .= "</body></html>";
        $mg = $this->mailGunKey();
        $result = $mg->messages()->send('mg.worryfreeshipping.com', [
            'from'    => 'Worry Free Shipping <support@worryfreeshipping.com>',
            'to'      => 'support@worryfreeshipping.com',
            'subject' => 'Shop Data Erase',
            'html'    => $message,
            'o:tag'   => array('Shop Data Erase')
        ]);

        return $result;
    }

    function shopRemovedEmail($payload){
        $message  = '<html><body>';
        $message .= 'Name:'. $payload["name"].'<br>';
        $message .= 'Email:'. $payload["email"].'<br>';
        $message .= "</body></html>";
        $mg = $this->mailGunKey();
        $result = $mg->messages()->send('mg.worryfreeshipping.com', [
            'from'    => 'Worry Free Shipping <support@worryfreeshipping.com>',
            'to'      => 'support@worryfreeshipping.com',
            'subject' => 'Shop Removed App',
            'html'    => $message,
            'o:tag'   => array('Shop Removed App')
        ]);

        return $result;
    }

    function customerEmail($email, $subject, $customerMessage, $name) {
        $message  = '<html><body>';
        $message .= 'Customer Name: '.$name;
        $message .= $customerMessage;
        $message .= "</body></html>";
        $mg = $this->mailGunKey();
        $result = $mg->messages()->send('mail1.r-renterprises.net', [
            'from'    => $email,
            'to'      => 'barideout@r-renterprises.com',
            'subject' => $subject,
            'html'    => $message,
            'o:tag'   => array('Customer Inquiry')
        ]);
    }
}